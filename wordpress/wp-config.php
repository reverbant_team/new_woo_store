<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpwc_store');

/** MySQL database username */
define('DB_USER', 'wpwc_user');

/** MySQL database password */
define('DB_PASSWORD', 'WpWc#%81aR7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[TDo7M1aqv49Z+>t.>:Q-wXDce+}Iu(P~Xip?YKy.MESY Gbx!cwz7 8hXQVqNVC');
define('SECURE_AUTH_KEY',  'v<ltm+-2eOOXzN5.TEY,0i7~qDtr-}iGoPEH7xfP1f3QW|tdSsweghPx}zW#]}k[');
define('LOGGED_IN_KEY',    'COx@mcI<q^8e14Er2V.+>_{V<EZ4n)QrT @uJEc!DE#9_p$%Phwa&>HaI`[[yES-');
define('NONCE_KEY',        'YD5h#&Ue2_|DhF`<ma] :E8vJu(>hiy34/517bZ:vF&10Ix:&/eB@4?:nb#-dS.)');
define('AUTH_SALT',        '*u[(Haa`)R,7F31*p%Qb)9Za[3q}|u0 +|AVY-,%bAsTX!>d>37s2M+q;FbLaca+');
define('SECURE_AUTH_SALT', 'bjM_8J1JJ(k*8>:L$56VOd@1M+i5mlRq[MQCH8ugnnwF-[ny8$y5Wj*.Z0JIzUF#');
define('LOGGED_IN_SALT',   'ajfCe6NJFnX({2Yw|h-P;=~K5)nAoIrXM B|NjK?a)E=iBcge/E8yJ(pO`})YjR2');
define('NONCE_SALT',       'P0Hp!Vtz$?+J7o^Y_k&g$o&8PtJ -L=E{GApXx2^q+]Su18fa~2Gr2Ek n/2[!J!');

define('FS_METHOD','direct');
define('FTP_PUBKEY','');//'/home/apache/.ssh/id_rsa.pub');
define('FTP_PRIKEY','');//'/home/apache/.ssh/id_rsa');
define('FTP_USER','wpwcstore');
define('FTP_PASS','');
define('FTP_HOST','127.0.0.1:22');
define( 'FTP_BASE', '/home/wpwcstore/wordpress/' );
define( 'FTP_CONTENT_DIR', '/home/wpwcstore/wordpress/wp-content/' );
define( 'FTP_PLUGIN_DIR ', '/home/wpwcstore/wordpress/wp-content/plugins/' );
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpwcs_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define( 'WP_DEBUG_DISPLAY', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
