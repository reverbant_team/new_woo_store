<?php 
/*
Template Name: WooCommerce
*/ 
?>
<?php

$rebellion_edge_id = get_option('woocommerce_shop_page_id');
$rebellion_edge_shop = get_post($rebellion_edge_id);
$rebellion_edge_sidebar = rebellion_edge_sidebar_layout();

if(get_post_meta($rebellion_edge_id, 'edgt_page_background_color', true) != ''){
	$rebellion_edge_background_color = 'background-color: '.esc_attr(get_post_meta($rebellion_edge_id, 'edgt_page_background_color', true));
}else{
	$rebellion_edge_background_color = '';
}

$rebellion_edge_content_style = '';
if(get_post_meta($rebellion_edge_id, 'edgt_content-top-padding', true) != '') {
	if(get_post_meta($rebellion_edge_id, 'edgt_content-top-padding-mobile', true) == 'yes') {
		$rebellion_edge_content_style = 'padding-top:'.esc_attr(get_post_meta($rebellion_edge_id, 'edgt_content-top-padding', true)).'px !important';
	} else {
		$rebellion_edge_content_style = 'padding-top:'.esc_attr(get_post_meta($rebellion_edge_id, 'edgt_content-top-padding', true)).'px';
	}
}

if ( get_query_var('paged') ) {
	$rebellion_edge_paged = get_query_var('paged');
} elseif ( get_query_var('page') ) {
	$rebellion_edge_paged = get_query_var('page');
} else {
	$rebellion_edge_paged = 1;
}

get_header();

rebellion_edge_get_title();
get_template_part('slider');

$rebellion_edge_full_width = false;

if ( rebellion_edge_options()->getOptionValue('edgtf_woo_products_list_full_width') == 'yes' && !is_singular('product') ) {
	$rebellion_edge_full_width = true;
}

if ( $rebellion_edge_full_width ) { ?>
	<div class="edgtf-full-width" <?php rebellion_edge_inline_style($rebellion_edge_background_color); ?>>
<?php } else { ?>
	<div class="edgtf-container" <?php rebellion_edge_inline_style($rebellion_edge_background_color); ?>>
<?php }
		if ( $rebellion_edge_full_width ) { ?>
			<div class="edgtf-full-width-inner" <?php rebellion_edge_inline_style($rebellion_edge_content_style); ?>>
		<?php } else { ?>
			<div class="edgtf-container-inner clearfix" <?php rebellion_edge_inline_style($rebellion_edge_content_style); ?>>
		<?php }

			//Woocommerce content
			if ( ! is_singular('product') ) {

				switch( $rebellion_edge_sidebar ) {

					case 'sidebar-33-right': ?>
						<div class="edgtf-two-columns-66-33 grid2 edgtf-woocommerce-with-sidebar clearfix">
							<div class="edgtf-column1">
								<div class="edgtf-column-inner">
									<?php rebellion_edge_woocommerce_content(); ?>
								</div>
							</div>
							<div class="edgtf-column2">
								<?php get_sidebar();?>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-25-right': ?>
						<div class="edgtf-two-columns-75-25 grid2 edgtf-woocommerce-with-sidebar clearfix">
							<div class="edgtf-column1 edgtf-content-left-from-sidebar">
								<div class="edgtf-column-inner">
									<?php rebellion_edge_woocommerce_content(); ?>
								</div>
							</div>
							<div class="edgtf-column2">
								<?php get_sidebar();?>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-33-left': ?>
						<div class="edgtf-two-columns-33-66 grid2 edgtf-woocommerce-with-sidebar clearfix">
							<div class="edgtf-column1">
								<?php get_sidebar();?>
							</div>
							<div class="edgtf-column2">
								<div class="edgtf-column-inner">
									<?php rebellion_edge_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					case 'sidebar-25-left': ?>
						<div class="edgtf-two-columns-25-75 grid2 edgtf-woocommerce-with-sidebar clearfix">
							<div class="edgtf-column1">
								<?php get_sidebar();?>
							</div>
							<div class="edgtf-column2 edgtf-content-right-from-sidebar">
								<div class="edgtf-column-inner">
									<?php rebellion_edge_woocommerce_content(); ?>
								</div>
							</div>
						</div>
					<?php
						break;
					default:
						rebellion_edge_woocommerce_content();
				}

			} else {
				rebellion_edge_woocommerce_content();
			} ?>

			</div>
	</div>
	<?php do_action('rebellion_edge_after_container_close'); ?>
<?php get_footer(); ?>
