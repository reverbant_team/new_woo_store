<?php
    /*
        Template Name: Blog: Standard
    */
?>
<?php get_header(); ?>
<?php rebellion_edge_get_title(); ?>
<?php get_template_part('slider'); ?>
<div class="edgtf-container">
    <?php do_action('rebellion_edge_after_container_open'); ?>
    <div class="edgtf-container-inner" >
    	<?php 
    	if(have_posts()): while (have_posts()) : the_post();
    	
    	the_content(); 
    	endwhile;
    	endif;
    	?>
        <?php rebellion_edge_get_blog('standard'); ?>
    </div>
    <?php do_action('rebellion_edge_before_container_close'); ?>
</div>
<?php do_action('rebellion_edge_after_container_close'); ?>
<?php get_footer(); ?>