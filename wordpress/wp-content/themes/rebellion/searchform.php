<form method="get" id="searchform" action="<?php echo esc_url(home_url( '/' )); ?>">
	 <div class="edgtf-search-wrapper">
		<input type="text" value="" placeholder="<?php esc_html_e('Search...', 'rebellion'); ?>" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="&#xf002;" />
	</div>
</form>