<?php

if(!function_exists('rebellion_edge_register_sidebars')) {
    /**
     * Function that registers theme's sidebars
     */
    function rebellion_edge_register_sidebars() {

        register_sidebar(array(
            'name'			=> esc_html__('Sidebar','rebellion'),
            'id'			=> 'sidebar',
            'description'	=> esc_html__('Default Sidebar','rebellion'),
            'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
            'after_widget'	=> '</div>',
            'before_title'	=> '<h4 class="edgtf-widget-title">',
            'after_title'	=> '</h4>'
        ));

    }

    add_action('widgets_init', 'rebellion_edge_register_sidebars');
}

if(!function_exists('rebellion_edge_add_support_custom_sidebar')) {
    /**
     * Function that adds theme support for custom sidebars. It also creates RebellionEdgeSidebar object
     */
    function rebellion_edge_add_support_custom_sidebar() {
        add_theme_support('RebellionEdgeSidebar');
        if (get_theme_support('RebellionEdgeSidebar')) new RebellionEdgeSidebar();
    }

    add_action('after_setup_theme', 'rebellion_edge_add_support_custom_sidebar');
}
