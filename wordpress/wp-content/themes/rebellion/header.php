<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <?php
    /**
     * @see rebellion_edge_header_meta() - hooked with 10
     * @see edgt_user_scalable - hooked with 10
     */
    ?>
	<?php do_action('rebellion_edge_header_meta'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class();?>>
<?php rebellion_edge_get_side_area(); ?>

<?php
if(rebellion_edge_options()->getOptionValue('smooth_page_transitions') == "yes") {
	$rebellion_edge_ajax_class = 'edgtf-mimic-ajax';
?>
	<div class="edgtf-smooth-transition-loader <?php echo esc_attr($rebellion_edge_ajax_class); ?>">
		<div class="edgtf-st-loader">
			<div class="edgtf-st-loader1">
				<?php rebellion_edge_loading_spinners(); ?>
			</div>
		</div>
	</div>
<?php } ?>

<div class="edgtf-wrapper">
    <div class="edgtf-wrapper-inner">
      
        <?php rebellion_edge_get_header(); ?>

        <?php if (rebellion_edge_options()->getOptionValue('show_back_button') == "yes") { ?>
            <a id='edgtf-back-to-top'  href='#'>
                <span class="edgtf-icon-stack">
                     <?php
                        rebellion_edge_icon_collections()->getBackToTopIcon('font_elegant');
                    ?>
                </span>
            </a>
        <?php } ?>

        <?php if (rebellion_edge_options()->getOptionValue('edgtf_enable_song_on_load') == "yes") { ?>
            <a class="edgtf-website-song">
                <audio src="<?php echo esc_url(rebellion_edge_options()->getOptionValue('edgtf_song_on_load')); ?>"></audio>
                <div class="edgtf-equalizer">
                  <div id="edgtf-equalizer-bar-1" class="edgtf-equalizer-bar"></div>
                  <div id="edgtf-equalizer-bar-2" class="edgtf-equalizer-bar"></div>
                  <div id="edgtf-equalizer-bar-3" class="edgtf-equalizer-bar"></div>
                  <div id="edgtf-equalizer-bar-4" class="edgtf-equalizer-bar"></div>
                </div>
            </a>
        <?php } ?>

        <?php rebellion_edge_get_full_screen_menu(); ?>

        <div class="edgtf-content" <?php rebellion_edge_content_elem_style_attr(); ?>>
            <?php if(rebellion_edge_is_ajax_enabled()) { ?>
            <div class="edgtf-meta">
                <?php do_action('rebellion_edge_ajax_meta'); ?>
                <span id="edgtf-page-id"><?php echo esc_html(get_queried_object_id()); ?></span>
                <div class="edgtf-body-classes"><?php echo esc_html(implode( ',', get_body_class())); ?></div>
            </div>
            <?php } ?>
            <div class="edgtf-content-inner">