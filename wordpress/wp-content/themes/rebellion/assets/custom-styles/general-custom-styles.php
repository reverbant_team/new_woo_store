<?php
if(!function_exists('rebellion_edge_design_styles')) {
    /**
     * Generates general custom styles
     */
    function rebellion_edge_design_styles() {

        $preload_background_styles = array();

        if(rebellion_edge_options()->getOptionValue('preload_pattern_image') !== ""){
            $preload_background_styles['background-image'] = 'url('.rebellion_edge_options()->getOptionValue('preload_pattern_image').') !important';
        }else{
            $preload_background_styles['background-image'] = 'url('.esc_url(EDGE_ASSETS_ROOT."/img/preload_pattern.png").') !important';
        }

        echo rebellion_edge_dynamic_css('.edgtf-preload-background', $preload_background_styles);

		if (rebellion_edge_options()->getOptionValue('google_fonts')){
			$font_family = rebellion_edge_options()->getOptionValue('google_fonts');
			if(rebellion_edge_is_font_option_valid($font_family)) {
				echo rebellion_edge_dynamic_css('body', array('font-family' => rebellion_edge_get_font_option_val($font_family)));
			}
		}

        if(rebellion_edge_options()->getOptionValue('first_color') !== "") {

			$first_color_array  = rebellion_edge_first_color_array();

			$color_selector = $first_color_array['color'];
            $color_important_selector = $first_color_array['color_important'];
            $background_color_selector = $first_color_array['background_color'];
            $background_color_important_selector = $first_color_array['background_color_important'];
            $border_color_selector = $first_color_array['border'];
            $border_color_important_selector = $first_color_array['border_important'];

            echo rebellion_edge_dynamic_css($color_selector, array('color' => rebellion_edge_options()->getOptionValue('first_color')));
            echo rebellion_edge_dynamic_css($color_important_selector, array('color' => rebellion_edge_options()->getOptionValue('first_color').'!important'));
            echo rebellion_edge_dynamic_css('::selection', array('background' => rebellion_edge_options()->getOptionValue('first_color')));
            echo rebellion_edge_dynamic_css('::-moz-selection', array('background' => rebellion_edge_options()->getOptionValue('first_color')));
            echo rebellion_edge_dynamic_css($background_color_selector, array('background-color' => rebellion_edge_options()->getOptionValue('first_color')));
            echo rebellion_edge_dynamic_css($background_color_important_selector, array('background-color' => rebellion_edge_options()->getOptionValue('first_color').'!important'));
            echo rebellion_edge_dynamic_css($border_color_selector, array('border-color' => rebellion_edge_options()->getOptionValue('first_color')));
            echo rebellion_edge_dynamic_css($border_color_important_selector, array('border-color' => rebellion_edge_options()->getOptionValue('first_color').'!important'));
        }

		if (rebellion_edge_options()->getOptionValue('page_background_color')) {
			$background_color_selector = array(
				'.edgtf-wrapper-inner',
				'.edgtf-content',
				'.edgtf-content-inner > .edgtf-container'
			);
			echo rebellion_edge_dynamic_css($background_color_selector, array('background-color' => rebellion_edge_options()->getOptionValue('page_background_color')));
		}

		if (rebellion_edge_options()->getOptionValue('selection_color')) {
			echo rebellion_edge_dynamic_css('::selection', array('background' => rebellion_edge_options()->getOptionValue('selection_color')));
			echo rebellion_edge_dynamic_css('::-moz-selection', array('background' => rebellion_edge_options()->getOptionValue('selection_color')));
		}

		$boxed_background_style = array();
		if (rebellion_edge_options()->getOptionValue('page_background_color_in_box')) {
			$boxed_background_style['background-color'] = rebellion_edge_options()->getOptionValue('page_background_color_in_box');
		}

		if (rebellion_edge_options()->getOptionValue('boxed_background_image')) {
			$boxed_background_style['background-image'] = 'url('.esc_url(rebellion_edge_options()->getOptionValue('boxed_background_image')).')';
			if(rebellion_edge_options()->getOptionValue('boxed_background_image_repeating') == 'yes') {
				$boxed_background_style['background-position'] = '0px 0px';
				$boxed_background_style['background-repeat'] = 'repeat';
			} else {
				$boxed_background_style['background-position'] = 'center 0px';
				$boxed_background_style['background-repeat'] = 'repeat';
			}
		}


		if (rebellion_edge_options()->getOptionValue('boxed_background_image_attachment')) {
			$boxed_background_style['background-attachment'] = (rebellion_edge_options()->getOptionValue('boxed_background_image_attachment'));
		}

		echo rebellion_edge_dynamic_css('.edgtf-boxed .edgtf-wrapper', $boxed_background_style);
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_design_styles');
}

if (!function_exists('rebellion_edge_h1_styles')) {

    function rebellion_edge_h1_styles() {

        $h1_styles = array();

        if(rebellion_edge_options()->getOptionValue('h1_color') !== '') {
            $h1_styles['color'] = rebellion_edge_options()->getOptionValue('h1_color');
        }
        if(rebellion_edge_options()->getOptionValue('h1_google_fonts') !== '-1') {
            $h1_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('h1_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('h1_fontsize') !== '') {
            $h1_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h1_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h1_lineheight') !== '') {
            $h1_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h1_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h1_texttransform') !== '') {
            $h1_styles['text-transform'] = rebellion_edge_options()->getOptionValue('h1_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('h1_fontstyle') !== '') {
            $h1_styles['font-style'] = rebellion_edge_options()->getOptionValue('h1_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('h1_fontweight') !== '') {
            $h1_styles['font-weight'] = rebellion_edge_options()->getOptionValue('h1_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('h1_letterspacing') !== '') {
            $h1_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h1_letterspacing')).'px';
        }

        $h1_selector = array(
            'h1'
        );

        if (!empty($h1_styles)) {
            echo rebellion_edge_dynamic_css($h1_selector, $h1_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_h1_styles');
}

if (!function_exists('rebellion_edge_h2_styles')) {

    function rebellion_edge_h2_styles() {

        $h2_styles = array();

        if(rebellion_edge_options()->getOptionValue('h2_color') !== '') {
            $h2_styles['color'] = rebellion_edge_options()->getOptionValue('h2_color');
        }
        if(rebellion_edge_options()->getOptionValue('h2_google_fonts') !== '-1') {
            $h2_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('h2_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('h2_fontsize') !== '') {
            $h2_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h2_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h2_lineheight') !== '') {
            $h2_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h2_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h2_texttransform') !== '') {
            $h2_styles['text-transform'] = rebellion_edge_options()->getOptionValue('h2_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('h2_fontstyle') !== '') {
            $h2_styles['font-style'] = rebellion_edge_options()->getOptionValue('h2_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('h2_fontweight') !== '') {
            $h2_styles['font-weight'] = rebellion_edge_options()->getOptionValue('h2_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('h2_letterspacing') !== '') {
            $h2_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h2_letterspacing')).'px';
        }

        $h2_selector = array(
            'h2'
        );

        if (!empty($h2_styles)) {
            echo rebellion_edge_dynamic_css($h2_selector, $h2_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_h2_styles');
}

if (!function_exists('rebellion_edge_h3_styles')) {

    function rebellion_edge_h3_styles() {

        $h3_styles = array();

        if(rebellion_edge_options()->getOptionValue('h3_color') !== '') {
            $h3_styles['color'] = rebellion_edge_options()->getOptionValue('h3_color');
        }
        if(rebellion_edge_options()->getOptionValue('h3_google_fonts') !== '-1') {
            $h3_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('h3_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('h3_fontsize') !== '') {
            $h3_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h3_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h3_lineheight') !== '') {
            $h3_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h3_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h3_texttransform') !== '') {
            $h3_styles['text-transform'] = rebellion_edge_options()->getOptionValue('h3_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('h3_fontstyle') !== '') {
            $h3_styles['font-style'] = rebellion_edge_options()->getOptionValue('h3_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('h3_fontweight') !== '') {
            $h3_styles['font-weight'] = rebellion_edge_options()->getOptionValue('h3_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('h3_letterspacing') !== '') {
            $h3_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h3_letterspacing')).'px';
        }

        $h3_selector = array(
            'h3'
        );

        if (!empty($h3_styles)) {
            echo rebellion_edge_dynamic_css($h3_selector, $h3_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_h3_styles');
}

if (!function_exists('rebellion_edge_h4_styles')) {

    function rebellion_edge_h4_styles() {

        $h4_styles = array();

        if(rebellion_edge_options()->getOptionValue('h4_color') !== '') {
            $h4_styles['color'] = rebellion_edge_options()->getOptionValue('h4_color');
        }
        if(rebellion_edge_options()->getOptionValue('h4_google_fonts') !== '-1') {
            $h4_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('h4_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('h4_fontsize') !== '') {
            $h4_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h4_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h4_lineheight') !== '') {
            $h4_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h4_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h4_texttransform') !== '') {
            $h4_styles['text-transform'] = rebellion_edge_options()->getOptionValue('h4_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('h4_fontstyle') !== '') {
            $h4_styles['font-style'] = rebellion_edge_options()->getOptionValue('h4_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('h4_fontweight') !== '') {
            $h4_styles['font-weight'] = rebellion_edge_options()->getOptionValue('h4_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('h4_letterspacing') !== '') {
            $h4_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h4_letterspacing')).'px';
        }

        $h4_selector = array(
            'h4'
        );

        if (!empty($h4_styles)) {
            echo rebellion_edge_dynamic_css($h4_selector, $h4_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_h4_styles');
}

if (!function_exists('rebellion_edge_h5_styles')) {

    function rebellion_edge_h5_styles() {

        $h5_styles = array();

        if(rebellion_edge_options()->getOptionValue('h5_color') !== '') {
            $h5_styles['color'] = rebellion_edge_options()->getOptionValue('h5_color');
        }
        if(rebellion_edge_options()->getOptionValue('h5_google_fonts') !== '-1') {
            $h5_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('h5_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('h5_fontsize') !== '') {
            $h5_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h5_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h5_lineheight') !== '') {
            $h5_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h5_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h5_texttransform') !== '') {
            $h5_styles['text-transform'] = rebellion_edge_options()->getOptionValue('h5_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('h5_fontstyle') !== '') {
            $h5_styles['font-style'] = rebellion_edge_options()->getOptionValue('h5_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('h5_fontweight') !== '') {
            $h5_styles['font-weight'] = rebellion_edge_options()->getOptionValue('h5_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('h5_letterspacing') !== '') {
            $h5_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h5_letterspacing')).'px';
        }

        $h5_selector = array(
            'h5'
        );

        if (!empty($h5_styles)) {
            echo rebellion_edge_dynamic_css($h5_selector, $h5_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_h5_styles');
}

if (!function_exists('rebellion_edge_h6_styles')) {

    function rebellion_edge_h6_styles() {

        $h6_styles = array();

        if(rebellion_edge_options()->getOptionValue('h6_color') !== '') {
            $h6_styles['color'] = rebellion_edge_options()->getOptionValue('h6_color');
        }
        if(rebellion_edge_options()->getOptionValue('h6_google_fonts') !== '-1') {
            $h6_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('h6_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('h6_fontsize') !== '') {
            $h6_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h6_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h6_lineheight') !== '') {
            $h6_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h6_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('h6_texttransform') !== '') {
            $h6_styles['text-transform'] = rebellion_edge_options()->getOptionValue('h6_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('h6_fontstyle') !== '') {
            $h6_styles['font-style'] = rebellion_edge_options()->getOptionValue('h6_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('h6_fontweight') !== '') {
            $h6_styles['font-weight'] = rebellion_edge_options()->getOptionValue('h6_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('h6_letterspacing') !== '') {
            $h6_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('h6_letterspacing')).'px';
        }

        $h6_selector = array(
            'h6'
        );

        if (!empty($h6_styles)) {
            echo rebellion_edge_dynamic_css($h6_selector, $h6_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_h6_styles');
}

if (!function_exists('rebellion_edge_text_styles')) {

    function rebellion_edge_text_styles() {

        $text_styles = array();

        if(rebellion_edge_options()->getOptionValue('text_color') !== '') {
            $text_styles['color'] = rebellion_edge_options()->getOptionValue('text_color');
        }
        if(rebellion_edge_options()->getOptionValue('text_google_fonts') !== '-1') {
            $text_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('text_google_fonts'));
        }
        if(rebellion_edge_options()->getOptionValue('text_fontsize') !== '') {
            $text_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('text_fontsize')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('text_lineheight') !== '') {
            $text_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('text_lineheight')).'px';
        }
        if(rebellion_edge_options()->getOptionValue('text_texttransform') !== '') {
            $text_styles['text-transform'] = rebellion_edge_options()->getOptionValue('text_texttransform');
        }
        if(rebellion_edge_options()->getOptionValue('text_fontstyle') !== '') {
            $text_styles['font-style'] = rebellion_edge_options()->getOptionValue('text_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('text_fontweight') !== '') {
            $text_styles['font-weight'] = rebellion_edge_options()->getOptionValue('text_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('text_letterspacing') !== '') {
            $text_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('text_letterspacing')).'px';
        }

        $text_selector = array(
            'p'
        );

        if (!empty($text_styles)) {
            echo rebellion_edge_dynamic_css($text_selector, $text_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_text_styles');
}

if (!function_exists('rebellion_edge_link_styles')) {

    function rebellion_edge_link_styles() {

        $link_styles = array();

        if(rebellion_edge_options()->getOptionValue('link_color') !== '') {
            $link_styles['color'] = rebellion_edge_options()->getOptionValue('link_color');
        }
        if(rebellion_edge_options()->getOptionValue('link_fontstyle') !== '') {
            $link_styles['font-style'] = rebellion_edge_options()->getOptionValue('link_fontstyle');
        }
        if(rebellion_edge_options()->getOptionValue('link_fontweight') !== '') {
            $link_styles['font-weight'] = rebellion_edge_options()->getOptionValue('link_fontweight');
        }
        if(rebellion_edge_options()->getOptionValue('link_fontdecoration') !== '') {
            $link_styles['text-decoration'] = rebellion_edge_options()->getOptionValue('link_fontdecoration');
        }

        $link_selector = array(
            'a',
            'p a'
        );

        if (!empty($link_styles)) {
            echo rebellion_edge_dynamic_css($link_selector, $link_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_link_styles');
}

if (!function_exists('rebellion_edge_link_hover_styles')) {

    function rebellion_edge_link_hover_styles() {

        $link_hover_styles = array();

        if(rebellion_edge_options()->getOptionValue('link_hovercolor') !== '') {
            $link_hover_styles['color'] = rebellion_edge_options()->getOptionValue('link_hovercolor');
        }
        if(rebellion_edge_options()->getOptionValue('link_hover_fontdecoration') !== '') {
            $link_hover_styles['text-decoration'] = rebellion_edge_options()->getOptionValue('link_hover_fontdecoration');
        }

        $link_hover_selector = array(
            'a:hover',
            'p a:hover'
        );

        if (!empty($link_hover_styles)) {
            echo rebellion_edge_dynamic_css($link_hover_selector, $link_hover_styles);
        }

        $link_heading_hover_styles = array();

        if(rebellion_edge_options()->getOptionValue('link_hovercolor') !== '') {
            $link_heading_hover_styles['color'] = rebellion_edge_options()->getOptionValue('link_hovercolor');
        }

        $link_heading_hover_selector = array(
            'h1 a:hover',
            'h2 a:hover',
            'h3 a:hover',
            'h4 a:hover',
            'h5 a:hover',
            'h6 a:hover'
        );

        if (!empty($link_heading_hover_styles)) {
            echo rebellion_edge_dynamic_css($link_heading_hover_selector, $link_heading_hover_styles);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_link_hover_styles');
}

if (!function_exists('rebellion_edge_smooth_page_transition_styles')) {

    function rebellion_edge_smooth_page_transition_styles() {
        
        $loader_style = array();

        if(rebellion_edge_options()->getOptionValue('smooth_pt_bgnd_color') !== '') {
            $loader_style['background-color'] = rebellion_edge_options()->getOptionValue('smooth_pt_bgnd_color');
        }

        $loader_selector = array('.edgtf-smooth-transition-loader');

        if (!empty($loader_style)) {
            echo rebellion_edge_dynamic_css($loader_selector, $loader_style);
        }

        $spinner_style = array();

        if(rebellion_edge_options()->getOptionValue('smooth_pt_spinner_color') !== '') {
            $spinner_style['background-color'] = rebellion_edge_options()->getOptionValue('smooth_pt_spinner_color');
        }

        $spinner_selectors = array(
            '.edgtf-st-loader .pulse',
            '.edgtf-st-loader .double_pulse .double-bounce1',
            '.edgtf-st-loader .double_pulse .double-bounce2',
            '.edgtf-st-loader .cube',
            '.edgtf-st-loader .rotating_cubes .cube1',
            '.edgtf-st-loader .rotating_cubes .cube2',
            '.edgtf-st-loader .edgtf-stripes > div',
            '.edgtf-st-loader .wave > div',
            '.edgtf-st-loader .two_rotating_circles .dot1',
            '.edgtf-st-loader .two_rotating_circles .dot2',
            '.edgtf-st-loader .five_rotating_circles .container1 > div',
            '.edgtf-st-loader .five_rotating_circles .container2 > div',
            '.edgtf-st-loader .five_rotating_circles .container3 > div',
            '.edgtf-st-loader .atom .ball-1:before',
            '.edgtf-st-loader .atom .ball-2:before',
            '.edgtf-st-loader .atom .ball-3:before',
            '.edgtf-st-loader .atom .ball-4:before',
            '.edgtf-st-loader .clock .ball:before',
            '.edgtf-st-loader .mitosis .ball',
            '.edgtf-st-loader .lines .line1',
            '.edgtf-st-loader .lines .line2',
            '.edgtf-st-loader .lines .line3',
            '.edgtf-st-loader .lines .line4',
            '.edgtf-st-loader .fussion .ball',
            '.edgtf-st-loader .fussion .ball-1',
            '.edgtf-st-loader .fussion .ball-2',
            '.edgtf-st-loader .fussion .ball-3',
            '.edgtf-st-loader .fussion .ball-4',
            '.edgtf-st-loader .wave_circles .ball',
            '.edgtf-st-loader .pulse_circles .ball'
        );

        if (!empty($spinner_style)) {
            echo rebellion_edge_dynamic_css($spinner_selectors, $spinner_style);
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_smooth_page_transition_styles');
}