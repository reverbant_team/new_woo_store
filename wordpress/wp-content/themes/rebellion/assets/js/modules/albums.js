(function($) {
    "use strict";

    var albums = {};
    edgtf.modules.albums = albums;

	albums.edgtfInitAlbumReviews = edgtfInitAlbumReviews;

	albums.edgtfOnDocumentReady = edgtfOnDocumentReady;
	albums.edgtfOnWindowLoad = edgtfOnWindowLoad;
	albums.edgtfOnWindowResize = edgtfOnWindowResize;
	albums.edgtfOnWindowScroll = edgtfOnWindowScroll;

    $(document).ready(edgtfOnDocumentReady);
    $(window).load(edgtfOnWindowLoad);
    $(window).resize(edgtfOnWindowResize);
    $(window).scroll(edgtfOnWindowScroll);
    
    /* 
        All functions to be called on $(document).ready() should be in this function
    */
    function edgtfOnDocumentReady() {
	    edgtfInitAlbumReviews();
    }

    /* 
        All functions to be called on $(window).load() should be in this function
    */
    function edgtfOnWindowLoad() {

    }

    /* 
        All functions to be called on $(window).resize() should be in this function
    */
    function edgtfOnWindowResize() {

    }

    /* 
        All functions to be called on $(window).scroll() should be in this function
    */
    function edgtfOnWindowScroll() {

    }


	function edgtfInitAlbumReviews(){

		var reviews = $('.edgtf-single-album-reviews');
		if(reviews.length){
			reviews.each(function(){

				var review = $(this);

				var auto = true;
				var controlNav = true;
				var directionNav = false;
				var slidesToShow = 1;

				review.slick({
					infinite: true,
					autoplay: auto,
					slidesToShow : slidesToShow,
					arrows: directionNav,
					dots: controlNav,
					dotsClass: 'edgtf-slick-dots',
					adaptiveHeight: true,
					prevArrow: '<span class="edgtf-slick-prev edgtf-prev-icon"><span class="arrow_carrot-left"></span></span>',
					nextArrow: '<span class="edgtf-slick-next edgtf-next-icon"><span class="arrow_carrot-right"></span></span>',
					customPaging: function(slider, i) {
						return '<span class="edgtf-slick-dot-inner"></span>';
					}
				});

			});
			$('.edgtf-single-album-reviews-holder').css('opacity', 1);
		}

	}

})(jQuery);
