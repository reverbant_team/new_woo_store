<?php $rebellion_edge_sidebar = rebellion_edge_sidebar_layout(); ?>
<?php get_header(); ?>
<?php 

$rebellion_edge_blog_page_range = rebellion_edge_get_blog_page_range();
$rebellion_edge_max_number_of_pages = rebellion_edge_get_max_number_of_pages();

if ( get_query_var('paged') ) { $rebellion_edge_paged = get_query_var('paged'); }
elseif ( get_query_var('page') ) { $rebellion_edge_paged = get_query_var('page'); }
else { $rebellion_edge_paged = 1; }
?>
<?php rebellion_edge_get_title(); ?>
	<div class="edgtf-container">
		<?php do_action('rebellion_edge_after_container_open'); ?>
		<div class="edgtf-container-inner clearfix">
			<div class="edgtf-container">
				<?php do_action('rebellion_edge_after_container_open'); ?>
				<div class="edgtf-container-inner" >
					<div class="edgtf-blog-holder edgtf-blog-type-standard">
				<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="edgtf-post-content">
							<div class="edgtf-post-text">
								<div class="edgtf-post-text-inner">
									<h4>
										<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
									</h4>
									<?php
										rebellion_edge_read_more_button();
									?>
								</div>
							</div>
						</div>
					</article>
					<?php endwhile; ?>
					<?php else: ?>
					<div class="entry">
						<p><?php esc_html_e('No posts were found.', 'rebellion'); ?></p>
					</div>
					<?php endif; ?>
				</div>
				<?php do_action('rebellion_edge_before_container_close'); ?>
			</div>
			</div>
		</div>
		<?php do_action('rebellion_edge_before_container_close'); ?>
	</div>
	<div class="edgtf-container edgtf-container-bottom-navigation">
		<div class="edgtf-container-inner">
			<?php
			if(rebellion_edge_options()->getOptionValue('pagination') == 'yes') {
				rebellion_edge_pagination($rebellion_edge_max_number_of_pages, $rebellion_edge_blog_page_range, $rebellion_edge_paged);
			}
			?>
		</div>
	</div>
	<?php do_action('rebellion_edge_after_container_close'); ?>
<?php get_footer(); ?>