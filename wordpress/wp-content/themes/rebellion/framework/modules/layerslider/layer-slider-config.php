<?php
	if(!function_exists('rebellion_edge_layerslider_overrides')) {
		/**
		 * Disables Layer Slider auto update box
		 */
		function rebellion_edge_layerslider_overrides() {
			$GLOBALS['lsAutoUpdateBox'] = false;
		}

		add_action('layerslider_ready', 'rebellion_edge_layerslider_overrides');
	}
?>