<?php

if ( ! function_exists('rebellion_edge_like') ) {
	/**
	 * Returns RebellionEdgeLike instance
	 *
	 * @return RebellionEdgeLike
	 */
	function rebellion_edge_like() {
		return RebellionEdgeLike::get_instance();
	}

}

function rebellion_edge_get_like() {

	echo wp_kses(rebellion_edge_like()->add_like(), array(
		'span' => array(
			'class' => true,
			'aria-hidden' => true,
			'style' => true,
			'id' => true
		),
		'i' => array(
			'class' => true,
			'style' => true,
			'id' => true
		),
		'a' => array(
			'href' => true,
			'class' => true,
			'id' => true,
			'title' => true,
			'style' => true
		)
	));
}

if ( ! function_exists('rebellion_edge_like_latest_posts') ) {
	/**
	 * Add like to latest post
	 *
	 * @return string
	 */
	function rebellion_edge_like_latest_posts() {
		return rebellion_edge_like()->add_like();
	}

}