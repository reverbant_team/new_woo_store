<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="edgtf-post-content">
		<div class="edgtf-post-image">
			<?php rebellion_edge_get_module_template_part('templates/parts/video', 'blog'); ?>
		</div>
		<div class="edgtf-post-text">
			<div class="edgtf-post-text-inner">
				<?php rebellion_edge_get_module_template_part('templates/lists/parts/title', 'blog', '', array('title_tag' => $title_tag)); ?>
				<div class="edgtf-post-info-part clearfix">
					<?php rebellion_edge_post_info(array('date' => 'yes')) ?>
				</div>
				<?php  if ($type == 'standard-whole-post') {
					the_content();
				}
				else{
					rebellion_edge_excerpt($excerpt_length);
				}
				?>
				<?php rebellion_edge_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>
				<?php if((has_tag() || rebellion_edge_get_social_share_html() != '')) : ?>
					<div class="edgtf-post-info-bottom">
						<div class="edgtf-post-info-bottom-left">
							<div class="edgtf-post-info-tags">
								<?php rebellion_edge_post_info(array('category' => 'yes')) ?>
							</div>
						</div>
						<?php if($share == 'yes') { ?>
							<div class="edgtf-post-info-bottom-right">
								<?php rebellion_edge_post_info(array('share' => 'yes')) ?>
							</div>
						<?php } ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>