<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="edgtf-post-content">
		<?php if(get_post_meta(get_the_ID(), "edgtf_audio_post_type_meta", true) == 'self' && get_post_meta(get_the_ID(), "edgtf_post_audio_link_meta", true) !== ""){
				rebellion_edge_get_module_template_part('templates/lists/parts/image', 'blog');
			 	rebellion_edge_get_module_template_part('templates/parts/audio', 'blog');
			 	?>
		<div class="edgtf-post-text">
			<div class="edgtf-post-text-inner">
				<?php rebellion_edge_get_module_template_part('templates/lists/parts/title', 'blog', '', array('title_tag' => $title_tag)); ?>
				<div class="edgtf-post-info-part clearfix">
					<?php rebellion_edge_post_info(array('date' => 'yes')) ?>
				</div>
				<?php  if ($type == 'standard-whole-post') {
					the_content();
				}
				else{
					rebellion_edge_excerpt($excerpt_length);
				}
				?>
				<?php rebellion_edge_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>
			</div>
		</div>
		<?php
			} elseif(get_post_meta(get_the_ID(), "edgtf_audio_post_type_meta", true) == 'soundcloud' && get_post_meta(get_the_ID(), "edgtf_post_audio_soundcloud_link_meta", true) !== ""){
			 	rebellion_edge_get_module_template_part('templates/parts/soundcloud', 'blog');
			} ?>
	</div>
</article>