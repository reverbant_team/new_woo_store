<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="edgtf-post-content">
		<div class="edgtf-post-text">
			<div class="edgtf-post-mark edgtf-link-mark">
				<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/link-mark.png" alt="link_mark" />
			</div>
			<div class="edgtf-post-text-inner clearfix">
				<div class="edgtf-post-title">
					<a href="<?php echo esc_url(get_post_meta(get_the_ID(), "edgtf_post_link_link_meta", true)); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
				</div>	
			</div>
		</div>
		<div class="edgtf-post-info">
			<?php rebellion_edge_post_info(array('category' => 'yes', 'date' => 'yes')) ?>
		</div>
		<?php the_content(); ?>
		<?php rebellion_edge_get_module_template_part('templates/lists/parts/pages-navigation', 'blog');  ?>
		<?php if(has_tag() || rebellion_edge_get_social_share_html() != '') : ?>
			<div class="edgtf-post-info-bottom">
				<div class="edgtf-post-info-bottom-left">
					<div class="edgtf-post-info-top">
					<?php has_tag() ? the_tags('', ', ', '') : ''; ?>
						</div>
				</div>
				<div class="edgtf-post-info-bottom-right">
					<?php rebellion_edge_post_info(array('share' => 'yes')) ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>