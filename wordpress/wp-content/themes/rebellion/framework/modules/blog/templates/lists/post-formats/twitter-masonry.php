<article id="post-<?php the_ID(); ?>" <?php post_class('edgtf-post-format-twitter'); ?>>
	<div class="edgtf-post-content">
		<div class="edgtf-post-mark edgtf-link-mark">
				<span class="social_twitter"></span>
			</div>
		<div class="edgtf-post-text">
			<div class="edgtf-post-text-inner">
				<div class="edgtf-ql-content">
					<h6 class="edgtf-post-twitter-user">&commat;<?php print $twitter_author; ?></h6>
					<h5><a href='<?php echo esc_url(get_post_meta(get_the_ID(), "edgtf_post_link_link_meta", true)); ?>'><?php print $twitter_text; ?></a></h5>
					<div class="edgtf-post-info">
						<div class="edgtf-post-info-top">
							<?php print $twitter_time; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>