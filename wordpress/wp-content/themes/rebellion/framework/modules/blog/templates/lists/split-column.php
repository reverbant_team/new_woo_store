<div class="edgtf-blog-holder edgtf-blog-type-split-column <?php echo esc_attr($blog_classes)?>"   data-blog-type="<?php echo esc_attr($blog_type)?>" <?php echo esc_attr(rebellion_edge_set_blog_holder_data_params()); ?> >
	<?php
		if($blog_query->have_posts()) : while ( $blog_query->have_posts() ) : $blog_query->the_post();
			rebellion_edge_get_post_format_html($blog_type);
		endwhile;
		else:
			rebellion_edge_get_module_template_part('templates/parts/no-posts', 'blog');
		endif;
	?>
	<?php
		if(rebellion_edge_options()->getOptionValue('pagination') == 'yes') {
			rebellion_edge_pagination($blog_query->max_num_pages, $blog_page_range, $paged);
		}
	?>
</div>
