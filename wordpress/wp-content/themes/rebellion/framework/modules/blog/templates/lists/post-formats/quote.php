<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="edgtf-post-content">
		<div class="edgtf-post-text">
			<?php if(get_post_meta(get_the_ID(), "edgtf_post_quote_text_meta", true)){ ?>
				<div class="edgtf-post-mark edgtf-quote-mark">
					<?php if(rebellion_edge_get_meta_field_intersect('predefined_color_skin', rebellion_edge_get_page_id()) == 'light') { ?>
						<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/quote-mark-light.png" alt="quote_mark" />
					<?php } else { ?>
						<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/quote-mark.png" alt="quote_mark" />
					<?php } ?>
				</div>
			<?php } ?>
			<div class="edgtf-post-text-inner">
				<<?php echo esc_attr($quote_title_tag); ?> class="edgtf-post-title">
						<a  href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo wp_kses_post(get_post_meta(get_the_ID(), "edgtf_post_quote_text_meta", true)); ?></a>
				</<?php echo esc_attr($quote_title_tag); ?>>
				<span class="edgtf-quote-author"> <?php the_title(); ?></span>
			</div>
		</div>
	</div>
</article>