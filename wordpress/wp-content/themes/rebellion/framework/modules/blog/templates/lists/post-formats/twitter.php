<article id="post-<?php the_ID(); ?>" <?php post_class('edgtf-post-format-twitter'); ?>>
		<div class="edgtf-post-content">
			<div class="edgtf-post-text">
				<div class="edgtf-post-mark edgtf-link-mark">
					<span class="social_twitter"></span>
				</div>
				<div class="edgtf-post-text-inner">
					<div class="edgtf-ql-content">
						<a href="<?php echo get_post_meta( get_the_ID(), 'edgtf_post_link_link_meta', true ); ?>">
							<div class="edgtf-tweet"><?php print $twitter_text; ?></div>
						</a>
						<span class="edgtf-tweet-author"><?php print $twitter_author; ?></span>
						<?php if((has_tag() || rebellion_edge_get_social_share_html() != '')) : ?>
						<div class="edgtf-post-info-bottom">
							<div class="edgtf-post-info-bottom-left">
								<?php if($type == 'standard'): ?>
									<div class="edgtf-post-info">
										<?php rebellion_edge_post_info(array('author' => 'no', 'category' => 'yes', 'date' => 'yes')) ?>
									</div>
								<?php endif; ?>
							</div>
							<?php if($share == 'yes') { ?>
								<div class="edgtf-post-info-bottom-right">
									<?php rebellion_edge_post_info(array('share' => 'yes')) ?>
								</div>
							<?php } ?>
						</div>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
</article>