<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="edgtf-post-content">
		<div class="edgtf-post-text">
			<div class="edgtf-post-mark edgtf-link-mark">
				<?php if(rebellion_edge_get_meta_field_intersect('predefined_color_skin', rebellion_edge_get_page_id()) == 'light') { ?>
					<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/link-mark-light.png" alt="link_mark" />
				<?php } else { ?>
					<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/link-mark-masonry.png" alt="link_mark" />
				<?php } ?>
			</div>
			<div class="edgtf-post-text-inner">
				<?php rebellion_edge_get_module_template_part('templates/lists/parts/title', 'blog', '', array('title_tag' => $title_tag)); ?>
			</div>
		</div>
	</div>
</article>