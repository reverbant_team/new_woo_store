<?php

if ( ! function_exists('rebellion_edge_blog_options_map') ) {

	function rebellion_edge_blog_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug' => '_blog_page',
				'title' => esc_html__('Blog', 'rebellion'),
				'icon' => 'fa fa-files-o'
			)
		);

		/**
		 * Blog Lists
		 */

		$custom_sidebars = rebellion_edge_get_custom_sidebars();

		$panel_blog_lists = rebellion_edge_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_lists',
				'title' => esc_html__('Blog Lists', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(array(
			'name'        => 'blog_list_type',
			'type'        => 'select',
			'label'       => esc_html__('Blog Layout for Archive Pages', 'rebellion'),
			'description' => esc_html__('Choose a default blog layout', 'rebellion'),
			'default_value' => 'standard',
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'standard'				=> esc_html__('Blog: Standard', 'rebellion'),
				'split-column'			=> esc_html__('Blog: Split Column', 'rebellion'),
				'masonry' 				=> esc_html__('Blog: Masonry', 'rebellion'),
				'masonry-full-width' 	=> esc_html__('Blog: Masonry Full Width', 'rebellion'),
				'standard-whole-post' 	=> esc_html__('Blog: Standard Whole Post', 'rebellion')
			)
		));

		rebellion_edge_add_admin_field(array(
			'name'        => 'archive_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Archive and Category Sidebar', 'rebellion'),
			'description' => esc_html__('Choose a sidebar layout for archived Blog Post Lists and Category Blog Lists', 'rebellion'),
			'parent'      => $panel_blog_lists,
			'options'     => array(
				'default'			=> esc_html__('No Sidebar', 'rebellion'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'rebellion'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'rebellion'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'rebellion'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'rebellion'),
			)
		));


		if(count($custom_sidebars) > 0) {
			rebellion_edge_add_admin_field(array(
				'name' => 'blog_custom_sidebar',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'rebellion'),
				'description' => esc_html__('Choose a sidebar to display on Blog Post Lists and Category Blog Lists. Default sidebar is "Sidebar Page"', 'rebellion'),
				'parent' => $panel_blog_lists,
				'options' => rebellion_edge_get_custom_sidebars()
			));
		}

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'pagination',
				'default_value' => 'yes',
				'label' => esc_html__('Pagination', 'rebellion'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display pagination links on bottom of Blog Post List', 'rebellion'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_edgtf_pagination_container'
				)
			)
		);

		$pagination_container = rebellion_edge_add_admin_container(
			array(
				'name' => 'edgtf_pagination_container',
				'hidden_property' => 'pagination',
				'hidden_value' => 'no',
				'parent' => $panel_blog_lists,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $pagination_container,
				'type' => 'text',
				'name' => 'blog_page_range',
				'default_value' => '',
				'label' => esc_html__('Pagination Range limit', 'rebellion'),
				'description' => esc_html__('Enter a number that will limit pagination to a certain range of links', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(array(
			'name'        => 'masonry_pagination',
			'type'        => 'select',
			'label'       => esc_html__('Pagination on Masonry', 'rebellion'),
			'description' => esc_html__('Choose a pagination style for Masonry Blog List', 'rebellion'),
			'parent'      => $pagination_container,
			'options'     => array(
				'standard'			=> esc_html__('Standard', 'rebellion'),
				'load-more'			=> esc_html__('Load More', 'rebellion'),
				'infinite-scroll' 	=> esc_html__('Infinite Scroll', 'rebellion')
			),
			
		));
		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'enable_load_more_pag',
				'default_value' => 'no',
				'label' => esc_html__('Load More Pagination on Other Lists', 'rebellion'),
				'parent' => $pagination_container,
				'description' => esc_html__('Enable Load More Pagination on other lists', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'masonry_filter',
				'default_value' => 'no',
				'label' => esc_html__('Masonry Filter', 'rebellion'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enabling this option will display category filter on Masonry and Masonry Full Width Templates', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);		
		rebellion_edge_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Number of Words in Excerpt', 'rebellion'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		rebellion_edge_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'standard_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Standard Type Number of Words in Excerpt', 'rebellion'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		rebellion_edge_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'masonry_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Masonry Type Number of Words in Excerpt', 'rebellion'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);
		rebellion_edge_add_admin_field(
			array(
				'type' => 'text',
				'name' => 'split_column_number_of_chars',
				'default_value' => '',
				'label' => esc_html__('Split Column Type Number of Words in Excerpt', 'rebellion'),
				'parent' => $panel_blog_lists,
				'description' => esc_html__('Enter a number of words in excerpt (article summary)', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		/**
		 * Blog Single
		 */
		$panel_blog_single = rebellion_edge_add_admin_panel(
			array(
				'page' => '_blog_page',
				'name' => 'panel_blog_single',
				'title' => esc_html__('Blog Single', 'rebellion')
			)
		);


		rebellion_edge_add_admin_field(array(
			'name'        => 'blog_single_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout', 'rebellion'),
			'description' => esc_html__('Choose a sidebar layout for Blog Single pages', 'rebellion'),
			'parent'      => $panel_blog_single,
			'options'     => array(
				'default'			=> esc_html__('No Sidebar', 'rebellion'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'rebellion'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'rebellion'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'rebellion'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'rebellion'),
			),
			'default_value'	=> 'default'
		));


		if(count($custom_sidebars) > 0) {
			rebellion_edge_add_admin_field(array(
				'name' => 'blog_single_custom_sidebar',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'rebellion'),
				'description' => esc_html__('Choose a sidebar to display on Blog Single pages. Default sidebar is "Sidebar"', 'rebellion'),
				'parent' => $panel_blog_single,
				'options' => rebellion_edge_get_custom_sidebars()
			));
		}

		rebellion_edge_add_admin_field(array(
			'name'          => 'blog_single_title_in_title_area',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Post Title in Title Area', 'rebellion'),
			'description'   => esc_html__('Enabling this option will show post title in title area on single post pages (will not take any effect on With Title Image type)', 'rebellion'),
			'parent'        => $panel_blog_single,
			'default_value' => 'no'
		));

		rebellion_edge_add_admin_field(array(
			'name'          => 'blog_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'rebellion'),
			'description'   => esc_html__('Enabling this option will show comments on your page.', 'rebellion'),
			'parent'        => $panel_blog_single,
			'default_value' => 'yes'
		));

		rebellion_edge_add_admin_field(array(
			'name'			=> 'blog_single_related_posts',
			'type'			=> 'yesno',
			'label'			=> esc_html__('Show Related Posts', 'rebellion'),
			'description'   => esc_html__('Enabling this option will show related posts on your single post.', 'rebellion'),
			'parent'        => $panel_blog_single,
			'default_value' => 'no'
		));

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_single_navigation',
				'default_value' => 'no',
				'label' => esc_html__('Enable Prev/Next Single Post Navigation Links', 'rebellion'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enable navigation links through the blog posts (left and right arrows will appear)', 'rebellion'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_edgtf_blog_single_navigation_container'
				)
			)
		);

		$blog_single_navigation_container = rebellion_edge_add_admin_container(
			array(
				'name' => 'edgtf_blog_single_navigation_container',
				'hidden_property' => 'blog_single_navigation',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_navigation_through_same_category',
				'default_value' => 'no',
				'label'       => esc_html__('Enable Navigation Only in Current Category', 'rebellion'),
				'description' => esc_html__('Limit your navigation only through current category', 'rebellion'),
				'parent'      => $blog_single_navigation_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'blog_author_info',
				'default_value' => 'no',
				'label' => esc_html__('Show Author Info Box', 'rebellion'),
				'parent' => $panel_blog_single,
				'description' => esc_html__('Enabling this option will display author name and descriptions on Blog Single pages', 'rebellion'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_edgtf_blog_single_author_info_container'
				)
			)
		);

		$blog_single_author_info_container = rebellion_edge_add_admin_container(
			array(
				'name' => 'edgtf_blog_single_author_info_container',
				'hidden_property' => 'blog_author_info',
				'hidden_value' => 'no',
				'parent' => $panel_blog_single,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type'        => 'yesno',
				'name' => 'blog_author_info_email',
				'default_value' => 'no',
				'label'       => esc_html__('Show Author Email', 'rebellion'),
				'description' => esc_html__('Enabling this option will show author email', 'rebellion'),
				'parent'      => $blog_single_author_info_container,
				'args' => array(
					'col_width' => 3
				)
			)
		);

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_blog_options_map', 12);

}











