<?php

if(!function_exists('rebellion_edge_overlapping_content_enabled')) {
    /**
     * Checks if overlapping content is enabled
     *
     * @return bool
     */
    function rebellion_edge_overlapping_content_enabled() {
        $id = rebellion_edge_get_page_id();

        return get_post_meta($id, 'edgtf_overlapping_content_enable_meta', true) === 'yes';
    }
}

if(!function_exists('rebellion_edge_overlapping_content_class')) {
    /**
     * Adds overlapping content class to body tag
     * if overlapping content is enabled
     * @param $classes
     *
     * @return array
     */
    function rebellion_edge_overlapping_content_class($classes) {
        if(rebellion_edge_overlapping_content_enabled()) {
            $classes[] = 'edgtf-overlapping-content-enabled';
        }

        return $classes;
    }

    add_filter('body_class', 'rebellion_edge_overlapping_content_class');
}

if(!function_exists('rebellion_edge_overlapping_content_amount')) {
    /**
     * Returns amount of overlapping content
     *
     * @return int
     */
    function rebellion_edge_overlapping_content_amount() {
        return 75;
    }
}

if(!function_exists('rebellion_edge_oc_content_top_padding')) {
    function rebellion_edge_oc_content_top_padding($style) {
	    $id = rebellion_edge_get_page_id();

	    $class_prefix = rebellion_edge_get_unique_page_class();

	    $content_selector = array(
		    $class_prefix.' .edgtf-content .edgtf-content-inner > .edgtf-container .edgtf-overlapping-content'
	    );

	    $content_class = array();

	    $padding = get_post_meta($id, 'edgtf_page_padding_meta', true);

		$content_class['padding'] = rebellion_edge_filter_px($padding).'px';


		if(!empty ($content_class)) {
			$style[] = rebellion_edge_dynamic_css($content_selector, $content_class);
		}

	    return $style;
    }

	add_filter('rebellion_edge_add_page_custom_style', 'rebellion_edge_oc_content_top_padding');
}