<?php

if(!function_exists('rebellion_edge_overlapping_content_opening_tag')) {
    /**
     * Prints opening HTML tags for overlapping content
     * Hooks to rebellion_edge_after_container_open
     */
    function rebellion_edge_overlapping_content_opening_tag() {
        if(rebellion_edge_overlapping_content_enabled()) : ?>
            <div class="edgtf-overlapping-content-holder">
            <div class="edgtf-overlapping-content">
            <div class="edgtf-overlapping-content-inner">
    <?php endif;
    }

    add_action('rebellion_edge_after_container_open', 'rebellion_edge_overlapping_content_opening_tag');
}

if(!function_exists('rebellion_edge_overlapping_content_closing_tag')) {
    /**
     * Prints closing HTML tags for overlapping content
     * Hooks to rebellion_edge_before_container_close
     */
    function rebellion_edge_overlapping_content_closing_tag() {
        if(rebellion_edge_overlapping_content_enabled()) : ?>
            </div> <!-- close .edgtf-overlapping-content-inner -->
            </div> <!-- close .edgtf-overlapping-content -->
            </div> <!-- close .edgtf-overlapping-content-holder -->
    <?php endif;
    }

    add_action('rebellion_edge_before_container_close', 'rebellion_edge_overlapping_content_closing_tag');
}