<?php  if ( rebellion_edge_options()->getOptionValue( 'event_pagination' ) == 'yes' ) : ?>

	<div class="edgtf-event-nav edgtf-grid-section">
		<div class="edgtf-event-nav-inner edgtf-section-inner">
			<?php if ( get_previous_post() !== '' ) : ?>
				<div class="edgtf-event-prev">
					<?php previous_post_link( '%link', esc_html__( 'Prev', 'rebellion' ) ); ?>
				</div>
			<?php endif; ?>

			<?php if ( $back_to_link !== '' ) : ?>
				<div class="edgtf-event-back-btn">
					<a href="<?php echo esc_url($back_to_link); ?>">
						<?php esc_html_e( 'All', 'rebellion' ); ?>
					</a>
				</div>
			<?php endif; ?>

			<?php if ( get_next_post() !== '' ) : ?>
				<div class="edgtf-event-next">
					<?php next_post_link( '%link', esc_html__( 'Next', 'rebellion' )); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

<?php endif; ?>