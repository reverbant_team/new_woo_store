<?php if(have_posts()): while(have_posts()) : the_post(); ?>
		<div class="edgtf-container">
		    <div class="edgtf-container-inner clearfix">
		    	<?php if(post_password_required()) {
	                echo get_the_password_form();
	            } else {
	                //event single html
	                ?>
	            <div <?php rebellion_edge_class_attribute($holder_class); ?>>
	                <div class="edgtf-two-columns-33-66 clearfix">
					    <div class="edgtf-column1">
					        <div class="edgtf-column-inner">
					           <div class="edgtf-event-image-buy-tickets-holder">
						           	<?php
						                //get event featured image
						                rebellion_edge_get_module_template_part('templates/single/parts/image','events');

						                //get event but tickets section
						                rebellion_edge_get_module_template_part('templates/single/parts/buy_tickets','events','', array('link' => $link, 'target' => $target, 'tickets_status' => $tickets_status));
						            ?>
					           </div>
					        </div>
					    </div>
					    <div class="edgtf-column2">
					        <div class="edgtf-column-inner">
					            <div class="edgtf-event-info-holder">
					                <?php
						                //get event details section
						                rebellion_edge_get_module_template_part('templates/single/parts/details', 'events', '', array('date' => $date,'time' => $time,'website' => $website,'organized_by' => $organized_by, 'location' => $location));

						                //get event about tour section
						                rebellion_edge_get_module_template_part('templates/single/parts/about_tour', 'events');

						                //get event map section
						                rebellion_edge_get_module_template_part('templates/single/parts/google_map', 'events', '', array('pin'=>$pin, 'location' => $location ));

						                //get event follow and share section
						                rebellion_edge_get_module_template_part('templates/single/parts/follow_and_share', 'events');
					                ?>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
	            <?php }
					//load event comments
					rebellion_edge_get_module_template_part('templates/single/parts/comments', 'events');
	             ?>

	        </div>
	    </div>
		<?php

			//load event navigation
			rebellion_edge_get_module_template_part('templates/single/parts/navigation', 'events', '', array('back_to_link' => $back_to_link));

			
		?>
<?php
	endwhile;
	endif;
?>