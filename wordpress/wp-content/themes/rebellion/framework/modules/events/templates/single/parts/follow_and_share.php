<?php if(rebellion_edge_options()->getOptionValue('enable_social_share') == 'yes'
        && rebellion_edge_options()->getOptionValue('enable_social_share_on_event') == 'yes') : ?>
<div class="edgtf-event-item edgtf-event-info edgtf-event-follow-share-holder">
    <h4 class='edgtf-event-section-title'><?php esc_html_e('Share', 'rebellion'); ?></h4>
    <div class="edgtf-event-item edgtf-event-info-content edgtf-event-folow-share">
     	<?php echo rebellion_edge_get_social_share_html() ?>
    </div>
</div>
<?php endif; ?>


