<?php 
  if($tickets_status == 'available'){
                        
    $button_params = array( 
      'text'         => esc_html__( 'Buy Tickets','rebellion' ),
      'custom_class' => 'event-buy-tickets-button',
      'link'         => $link,
      'target'       => $target,
      'type'         => 'outline'
    );

    echo rebellion_edge_execute_shortcode('edgtf_button', $button_params);
    }
?>