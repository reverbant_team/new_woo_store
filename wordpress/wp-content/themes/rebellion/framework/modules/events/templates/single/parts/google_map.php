<?php
    $args = array(
		'address1' => $location,
    	'pin' => $pin,
    	'custom_map_style' => '0',
    	'map_height' => '355',
    	'color_overlay' => '#ffffff',
    	'saturation' => '0',
    	'lightness' => '0',
    	'scroll_wheel' => '0',
        'zoom'  => '16'
    );

    if ($location != ''): 
?>
<div class="edgtf-event-item edgtf-event-info edgtf-event-map-holder">
    <h4 class='edgtf-event-section-title'><?php esc_html_e('Map', 'rebellion'); ?></h4>
    <div class="edgtf-event-item edgtf-event-info-content edgtf-event-map">
    	<?php echo rebellion_edge_get_google_map_html($args); ?>
    </div>
</div>

<?php endif; ?>
