<?php 
$event_types   = wp_get_post_terms(get_the_ID(), 'event-type');
$event_types_names = array();
if (($date != '') || ($time != '') || ($location != '') || ($website != '') || ($organized_by != '') || (is_array($event_types) && count($event_types))): ?>
<div class="edgtf-event-item edgtf-event-info edgtf-event-details-holder">
    <h4 class='edgtf-event-section-title'><?php esc_html_e('Details', 'rebellion'); ?></h4>
    <div class="edgtf-event-item edgtf-event-info-content edgtf-event-details">
        <?php if (($date != '') || ($time != '')): ?>
   		<div class="edgtf-event-item edgtf-event-details-time">
            <span><?php esc_html_e('Time:', 'rebellion') ?></span>
            <span><?php print date_i18n( 'F d, Y' , strtotime( $date ) ).' '.$time ?></span>
        </div>
        <?php endif; ?>
        <?php if ($location != ''): ?>
        <div class="edgtf-event-item edgtf-event-details-location">
            <span><?php esc_html_e('Location:', 'rebellion') ?></span>
            <span><?php echo esc_attr($location); ?></span>
        </div>
        <?php endif; ?>
        <?php if ($website != ''): ?>
        <div class="edgtf-event-item edgtf-event-details-website">
            <span><?php esc_html_e('Website:', 'rebellion') ?></span>
            <span><a href="<?php echo esc_url($website); ?>" target = '_blank'><?php echo esc_url($website); ?></a></span>
        </div>
        <?php endif; ?>
        <?php
            if(is_array($event_types) && count($event_types)) : ?>
        <div class="edgtf-event-item edgtf-event-details-event-type">
            <span><?php esc_html_e('Event Type:', 'rebellion') ?></span>
            <span><?php
                        foreach($event_types as $event_type) {
                            $event_types_names[] = $event_type->name;
                        }
                        echo esc_html(implode(', ', $event_types_names)); 
                ?>
            </span>
        </div>
        <?php endif; ?>
        <?php if ($organized_by != ''): ?>
        <div class="edgtf-event-item edgtf-event-details-organized-by">
            <span><?php esc_html_e('Organized By:', 'rebellion') ?></span>
            <span><?php echo esc_attr($organized_by); ?></span>
        </div>
        <?php endif; ?>
	</div>
</div>

<?php endif; ?>