<?php if (get_the_content() != ''): ?>
<div class="edgtf-event-item edgtf-event-info edgtf-event-content-holder">
    <h4 class='edgtf-event-section-title'><?php esc_html_e('About Tour', 'rebellion'); ?></h4>
    <div class="edgtf-event-item edgtf-event-info-content edgtf-event-single-content">
   		<?php the_content(); ?>
	</div>
</div>
<?php endif; ?>