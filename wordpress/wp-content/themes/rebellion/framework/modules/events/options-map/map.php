<?php

if ( ! function_exists('rebellion_edge_events_options_map') ) {

	function rebellion_edge_events_options_map() {

		rebellion_edge_add_admin_page(array(
			'slug'  => '_events',
			'title' => esc_html__('Events', 'rebellion'),
			'icon'  => 'fa fa-calendar'
		));

		$panel = rebellion_edge_add_admin_panel(array(
			'title' => esc_html__('Event', 'rebellion'),
			'name'  => 'panel_event',
			'page'  => '_events'
		));

		rebellion_edge_add_admin_field(array(
			'name'          => 'event_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'rebellion'),
			'description'   => esc_html__('Enabling this option will show comments on your page.', 'rebellion'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		rebellion_edge_add_admin_field(array(
			'name'          => 'event_pagination',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Pagination', 'rebellion'),
			'description'   => esc_html__('Enabling this option will turn on events pagination functionality.', 'rebellion'),
			'parent'        => $panel,
			'default_value' => 'no',
		));

		rebellion_edge_add_admin_field(array(
			'name'        => 'event_single_slug',
			'type'        => 'text',
			'label'       => esc_html__('Event Single Slug','rebellion'),
			'description' => esc_html__('Enter if you wish to use a different Single Project slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)','rebellion'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_events_options_map', 15);

}