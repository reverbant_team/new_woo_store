<?php

class RebellionEdgeSideAreaOpener extends RebellionEdgeWidget {
    public function __construct() {
        parent::__construct(
            'edgtf_side_area_opener', // Base ID
            esc_html__('Edge Side Area Opener','rebellion') // Name
        );

        $this->setParams();
    }

    protected function setParams() {

		$this->params = array(
			array(
				'name'			=> 'side_area_opener_icon_color',
				'type'			=> 'textfield',
				'title'			=> esc_html__('Icon Color','rebellion'),
				'description'	=> esc_html__('Define color for Side Area opener icon','rebellion')
			),
			array(
                'type' 			=> 'textfield',
                'title' 		=> esc_html__('Margin','rebellion'),
                'name' 			=> 'margin',
                'description' 	=> esc_html__('Margin (top right bottom left)','rebellion')
            )
		);

    }


    public function widget($args, $instance) {
		
		$sidearea_icon_styles = array();

		if ( !empty($instance['side_area_opener_icon_color']) ) {
			$sidearea_icon_styles[] = 'color: ' . $instance['side_area_opener_icon_color'];
		}

		if (!empty($instance['margin'])) {
            $sidearea_icon_styles[] = 'margin: '.$instance['margin'];
        }
		
		$icon_size = '';
		if ( rebellion_edge_options()->getOptionValue('side_area_predefined_icon_size') ) {
			$icon_size = rebellion_edge_options()->getOptionValue('side_area_predefined_icon_size');
		}
		?>
        <a class="edgtf-side-menu-button-opener <?php echo esc_attr( $icon_size ); ?>" <?php rebellion_edge_inline_style($sidearea_icon_styles) ?> href="javascript:void(0)">
            <?php echo rebellion_edge_get_side_menu_icon_html(); ?>
        </a>

    <?php }

}