<?php

if (!function_exists('rebellion_edge_register_widgets')) {

	function rebellion_edge_register_widgets() {

		$widgets = array(
			'RebellionEdgeLatestPosts',
			'RebellionEdgeSearchOpener',
			'RebellionEdgeSideAreaOpener',
			'RebellionEdgeStickySidebar',
			'RebellionEdgeSocialIconWidget',
			'RebellionEdgeSeparatorWidget'
		);

		foreach ($widgets as $widget) {
			register_widget($widget);
		}
	}
}

add_action('widgets_init', 'rebellion_edge_register_widgets');