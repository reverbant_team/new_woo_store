<?php

/**
 * Widget that adds separator boxes type
 *
 * Class Separator_Widget
 */
class RebellionEdgeSeparatorWidget extends RebellionEdgeWidget {
    /**
     * Set basic widget options and call parent class construct
     */
    public function __construct() {
        parent::__construct(
            'edgt_separator_widget', // Base ID
            esc_html__('Edge Separator Widget','rebellion') // Name
        );

        $this->setParams();
    }

    /**
     * Sets widget options
     */
    protected function setParams() {
        $this->params = array(
            array(
                'type' => 'dropdown',
                'title' => esc_html__('Type','rebellion'),
                'name' => 'type',
                'options' => array(
                    'normal' => esc_html__('Normal','rebellion'),
                    'full-width' => esc_html__('Full Width','rebellion')
                )
            ),
            array(
                'type' => 'dropdown',
                'title' => esc_html__('Position','rebellion'),
                'name' => 'position',
                'options' => array(
                    'center' => esc_html__('Center','rebellion'),
                    'left' => esc_html__('Left','rebellion'),
                    'right' => esc_html__('Right','rebellion')
                )
            ),
            array(
                'type' => 'dropdown',
                'title' => esc_html__('Style','rebellion'),
                'name' => 'border_style',
                'options' => array(
                    'solid' => esc_html__('Solid','rebellion'),
                    'dashed' => esc_html__('Dashed','rebellion'),
                    'dotted' => esc_html__('Dotted','rebellion')
                )
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Color','rebellion'),
                'name' => 'color'
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Width','rebellion'),
                'name' => 'width',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Thickness (px)','rebellion'),
                'name' => 'thickness',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Top Margin','rebellion'),
                'name' => 'top_margin',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'title' => esc_html__('Bottom Margin','rebellion'),
                'name' => 'bottom_margin',
                'description' => ''
            )
        );
    }

    /**
     * Generates widget's HTML
     *
     * @param array $args args from widget area
     * @param array $instance widget's options
     */
    public function widget($args, $instance) {

        extract($args);

        //prepare variables
        $params = '';

        //is instance empty?
        if(is_array($instance) && count($instance)) {
            //generate shortcode params
            foreach($instance as $key => $value) {
                $params .= " $key='$value' ";
            }
        }

        echo '<div class="widget edgtf-separator-widget">';

        //finally call the shortcode
        echo do_shortcode("[edgtf_separator $params]"); // XSS OK

        echo '</div>'; //close div.edgtf-separator-widget
    }
}