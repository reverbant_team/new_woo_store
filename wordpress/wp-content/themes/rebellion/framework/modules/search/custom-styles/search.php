<?php

if (!function_exists('rebellion_edge_search_opener_icon_size')) {

	function rebellion_edge_search_opener_icon_size()
	{

		if (rebellion_edge_options()->getOptionValue('header_search_icon_size')) {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener, .edgtf-header-standard .edgtf-search-opener', array(
				'font-size' => rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('header_search_icon_size')) . 'px'
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_opener_icon_size');

}

if (!function_exists('rebellion_edge_search_opener_icon_colors')) {

	function rebellion_edge_search_opener_icon_colors()
	{

		if (rebellion_edge_options()->getOptionValue('header_search_icon_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener', array(
				'color' => rebellion_edge_options()->getOptionValue('header_search_icon_color')
			));
		}

		if (rebellion_edge_options()->getOptionValue('header_search_icon_hover_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener:hover', array(
				'color' => rebellion_edge_options()->getOptionValue('header_search_icon_hover_color')
			));
		}

		if (rebellion_edge_options()->getOptionValue('header_light_search_icon_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-light-header .edgtf-page-header > div:not(.edgtf-sticky-header) .edgtf-search-opener,
			.edgtf-light-header.edgtf-header-style-on-scroll .edgtf-page-header .edgtf-search-opener,
			.edgtf-light-header .edgtf-top-bar .edgtf-search-opener', array(
				'color' => rebellion_edge_options()->getOptionValue('header_light_search_icon_color') . ' !important'
			));
		}

		if (rebellion_edge_options()->getOptionValue('header_light_search_icon_hover_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-light-header .edgtf-page-header > div:not(.edgtf-sticky-header) .edgtf-search-opener:hover,
			.edgtf-light-header.edgtf-header-style-on-scroll .edgtf-page-header .edgtf-search-opener:hover,
			.edgtf-light-header .edgtf-top-bar .edgtf-search-opener:hover', array(
				'color' => rebellion_edge_options()->getOptionValue('header_light_search_icon_hover_color') . ' !important'
			));
		}

		if (rebellion_edge_options()->getOptionValue('header_dark_search_icon_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-dark-header .edgtf-page-header > div:not(.edgtf-sticky-header) .edgtf-search-opener,
			.edgtf-dark-header.edgtf-header-style-on-scroll .edgtf-page-header .edgtf-search-opener,
			.edgtf-dark-header .edgtf-top-bar .edgtf-search-opener', array(
				'color' => rebellion_edge_options()->getOptionValue('header_dark_search_icon_color') . ' !important'
			));
		}
		if (rebellion_edge_options()->getOptionValue('header_dark_search_icon_hover_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-dark-header .edgtf-page-header > div:not(.edgtf-sticky-header) .edgtf-search-opener:hover,
			.edgtf-dark-header.edgtf-header-style-on-scroll .edgtf-page-header .edgtf-search-opener:hover,
			.edgtf-dark-header .edgtf-top-bar .edgtf-search-opener:hover', array(
				'color' => rebellion_edge_options()->getOptionValue('header_dark_search_icon_hover_color') . ' !important'
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_opener_icon_colors');

}

if (!function_exists('rebellion_edge_search_opener_icon_background_colors')) {

	function rebellion_edge_search_opener_icon_background_colors()
	{

		if (rebellion_edge_options()->getOptionValue('search_icon_background_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener', array(
				'background-color' => rebellion_edge_options()->getOptionValue('search_icon_background_color')
			));
		}

		if (rebellion_edge_options()->getOptionValue('search_icon_background_hover_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener:hover', array(
				'background-color' => rebellion_edge_options()->getOptionValue('search_icon_background_hover_color')
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_opener_icon_background_colors');
}

if (!function_exists('rebellion_edge_search_opener_text_styles')) {

	function rebellion_edge_search_opener_text_styles()
	{
		$text_styles = array();

		if (rebellion_edge_options()->getOptionValue('search_icon_text_color') !== '') {
			$text_styles['color'] = rebellion_edge_options()->getOptionValue('search_icon_text_color');
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_fontsize') !== '') {
			$text_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_icon_text_fontsize')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_lineheight') !== '') {
			$text_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_icon_text_lineheight')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_texttransform') !== '') {
			$text_styles['text-transform'] = rebellion_edge_options()->getOptionValue('search_icon_text_texttransform');
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('search_icon_text_google_fonts')) . ', sans-serif';
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_fontstyle') !== '') {
			$text_styles['font-style'] = rebellion_edge_options()->getOptionValue('search_icon_text_fontstyle');
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_fontweight') !== '') {
			$text_styles['font-weight'] = rebellion_edge_options()->getOptionValue('search_icon_text_fontweight');
		}		
		if (rebellion_edge_options()->getOptionValue('search_icon_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_icon_text_letterspacing')).'px';
		}

		if (!empty($text_styles)) {
			echo rebellion_edge_dynamic_css('.edgtf-search-icon-text', $text_styles);
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_text_color_hover') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener:hover .edgtf-search-icon-text', array(
				'color' => rebellion_edge_options()->getOptionValue('search_icon_text_color_hover')
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_opener_text_styles');
}

if (!function_exists('rebellion_edge_search_opener_spacing')) {

	function rebellion_edge_search_opener_spacing()
	{
		$spacing_styles = array();

		if (rebellion_edge_options()->getOptionValue('search_padding_left') !== '') {
			$spacing_styles['padding-left'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_padding_left')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_padding_right') !== '') {
			$spacing_styles['padding-right'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_padding_right')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_margin_left') !== '') {
			$spacing_styles['margin-left'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_margin_left')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_margin_right') !== '') {
			$spacing_styles['margin-right'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_margin_right')) . 'px';
		}

		if (!empty($spacing_styles)) {
			echo rebellion_edge_dynamic_css('.edgtf-search-opener', $spacing_styles);
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_opener_spacing');
}

if (!function_exists('rebellion_edge_search_bar_background')) {

	function rebellion_edge_search_bar_background()
	{

		if (rebellion_edge_options()->getOptionValue('search_background_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-cover, .edgtf-search-fade .edgtf-fullscreen-search-holder .edgtf-fullscreen-search-table, .edgtf-fullscreen-search-overlay, .edgtf-search-slide-window-top, .edgtf-search-slide-window-top input[type="text"]', array(
				'background-color' => rebellion_edge_options()->getOptionValue('search_background_color')
			));
		}
	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_bar_background');
}

if (!function_exists('rebellion_edge_search_text_styles')) {

	function rebellion_edge_search_text_styles()
	{
		$text_styles = array();

		if (rebellion_edge_options()->getOptionValue('search_text_color') !== '') {
			$text_styles['color'] = rebellion_edge_options()->getOptionValue('search_text_color');
			echo rebellion_edge_dynamic_css('.edgt_search_field::-webkit-input-placeholder',array('color' => $text_styles['color']));
			echo rebellion_edge_dynamic_css('.edgt_search_field:-moz-placeholder',array('color' => $text_styles['color']));
			echo rebellion_edge_dynamic_css('.edgt_search_field::-moz-placeholder',array('color' => $text_styles['color']));
			echo rebellion_edge_dynamic_css('.edgt_search_field:-ms-input-placeholder',array('color' => $text_styles['color']));
		}
		if (rebellion_edge_options()->getOptionValue('search_text_fontsize') !== '') {
			$text_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_text_fontsize')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_text_texttransform') !== '') {
			$text_styles['text-transform'] = rebellion_edge_options()->getOptionValue('search_text_texttransform');
		}
		if (rebellion_edge_options()->getOptionValue('search_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('search_text_google_fonts')) . ', sans-serif';
		}
		if (rebellion_edge_options()->getOptionValue('search_text_fontstyle') !== '') {
			$text_styles['font-style'] = rebellion_edge_options()->getOptionValue('search_text_fontstyle');
		}
		if (rebellion_edge_options()->getOptionValue('search_text_fontweight') !== '') {
			$text_styles['font-weight'] = rebellion_edge_options()->getOptionValue('search_text_fontweight');
		}
		if (rebellion_edge_options()->getOptionValue('search_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo rebellion_edge_dynamic_css('.edgtf-search-cover input[type="text"], .edgtf-fullscreen-search-opened .edgtf-form-holder .edgtf-search-field, .edgtf-search-slide-window-top input[type="text"]', $text_styles);
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_text_styles');
}

if (!function_exists('rebellion_edge_search_label_styles')) {

	function rebellion_edge_search_label_styles()
	{
		$text_styles = array();

		if (rebellion_edge_options()->getOptionValue('search_label_text_color') !== '') {
			$text_styles['color'] = rebellion_edge_options()->getOptionValue('search_label_text_color');
		}
		if (rebellion_edge_options()->getOptionValue('search_label_text_fontsize') !== '') {
			$text_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_label_text_fontsize')) . 'px';
		}
		if (rebellion_edge_options()->getOptionValue('search_label_text_texttransform') !== '') {
			$text_styles['text-transform'] = rebellion_edge_options()->getOptionValue('search_label_text_texttransform');
		}
		if (rebellion_edge_options()->getOptionValue('search_label_text_google_fonts') !== '-1') {
			$text_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('search_label_text_google_fonts')) . ', sans-serif';
		}
		if (rebellion_edge_options()->getOptionValue('search_label_text_fontstyle') !== '') {
			$text_styles['font-style'] = rebellion_edge_options()->getOptionValue('search_label_text_fontstyle');
		}
		if (rebellion_edge_options()->getOptionValue('search_label_text_fontweight') !== '') {
			$text_styles['font-weight'] = rebellion_edge_options()->getOptionValue('search_label_text_fontweight');
		}
		if (rebellion_edge_options()->getOptionValue('search_label_text_letterspacing') !== '') {
			$text_styles['letter-spacing'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_label_text_letterspacing')) . 'px';
		}

		if (!empty($text_styles)) {
			echo rebellion_edge_dynamic_css('.edgtf-fullscreen-search-holder .edgtf-search-label', $text_styles);
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_label_styles');
}

if (!function_exists('rebellion_edge_search_icon_styles')) {

	function rebellion_edge_search_icon_styles()
	{

		if (rebellion_edge_options()->getOptionValue('search_icon_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-slide-window-top > i, .edgtf-fullscreen-search-holder .edgtf-search-submit', array(
				'color' => rebellion_edge_options()->getOptionValue('search_icon_color')
			));
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_hover_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-slide-window-top > i:hover, .edgtf-fullscreen-search-holder .edgtf-search-submit:hover', array(
				'color' => rebellion_edge_options()->getOptionValue('search_icon_hover_color')
			));
		}
		if (rebellion_edge_options()->getOptionValue('search_icon_size') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-slide-window-top > i, .edgtf-fullscreen-search-holder .edgtf-search-submit', array(
				'font-size' => rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_icon_size')) . 'px'
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_icon_styles');
}

if (!function_exists('rebellion_edge_search_close_icon_styles')) {

	function rebellion_edge_search_close_icon_styles()
	{

		if (rebellion_edge_options()->getOptionValue('search_close_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-slide-window-top .edgtf-search-close i, .edgtf-search-cover .edgtf-search-close i,.edgtf-search-cover .edgtf-search-close span, .edgtf-fullscreen-search-close', array(
				'color' => rebellion_edge_options()->getOptionValue('search_close_color')
			));
		}
		if (rebellion_edge_options()->getOptionValue('search_close_hover_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-slide-window-top .edgtf-search-close i:hover, .edgtf-search-cover .edgtf-search-close i:hover,.edgtf-search-cover .edgtf-search-close span:hover, .edgtf-fullscreen-search-close:hover', array(
				'color' => rebellion_edge_options()->getOptionValue('search_close_hover_color')
			));
		}
		if (rebellion_edge_options()->getOptionValue('search_close_size') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-search-slide-window-top .edgtf-search-close i, .edgtf-search-cover .edgtf-search-close i,.edgtf-search-cover .edgtf-search-close span, .edgtf-fullscreen-search-close', array(
				'font-size' => rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('search_close_size')) . 'px'
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_close_icon_styles');
}

if (!function_exists('rebellion_edge_search_border_styles')) {

	function rebellion_edge_search_border_styles()
	{

		if (rebellion_edge_options()->getOptionValue('search_border_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-fullscreen-search-holder .edgtf-field-holder', array(
				'border-color' => rebellion_edge_options()->getOptionValue('search_border_color')
			));
		}
		if (rebellion_edge_options()->getOptionValue('search_border_focus_color') !== '') {
			echo rebellion_edge_dynamic_css('.edgtf-fullscreen-search-holder .edgtf-field-holder .edgtf-line', array(
				'background-color' => rebellion_edge_options()->getOptionValue('search_border_focus_color')
			));
		}

	}

	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_search_border_styles');
}

?>
