<?php

if(!function_exists('rebellion_edge_button_map')) {
    function rebellion_edge_button_map() {
        $panel = rebellion_edge_add_admin_panel(array(
            'title' => esc_html__('Button', 'rebellion'),
            'name'  => 'panel_button',
            'page'  => '_elements_page'
        ));

        //Typography options
        rebellion_edge_add_admin_section_title(array(
            'name' => 'typography_section_title',
            'title' => esc_html__('Typography', 'rebellion'),
            'parent' => $panel
        ));

        $typography_group = rebellion_edge_add_admin_group(array(
            'name' => 'typography_group',
            'title' => esc_html__('Typography', 'rebellion'),
            'description' => esc_html__('Setup typography for all button types', 'rebellion'),
            'parent' => $panel
        ));

        $typography_row = rebellion_edge_add_admin_row(array(
            'name' => 'typography_row',
            'next' => true,
            'parent' => $typography_group
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'fontsimple',
            'name'          => 'button_font_family',
            'default_value' => '',
            'label'         => esc_html__('Font Family', 'rebellion'),
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_text_transform',
            'default_value' => '',
            'label'         => esc_html__('Text Transform', 'rebellion'),
            'options'       => rebellion_edge_get_text_transform_array()
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'selectsimple',
            'name'          => 'button_font_style',
            'default_value' => '',
            'label'         => esc_html__('Font Style', 'rebellion'),
            'options'       => rebellion_edge_get_font_style_array()
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $typography_row,
            'type'          => 'textsimple',
            'name'          => 'button_letter_spacing',
            'default_value' => '',
            'label'         => esc_html__('Letter Spacing', 'rebellion'),
            'args'          => array(
                'suffix' => 'px'
            )
        ));

        $typography_row2 = rebellion_edge_add_admin_row(array(
            'name' => 'typography_row2',
            'next' => true,
            'parent' => $typography_group
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $typography_row2,
            'type'          => 'selectsimple',
            'name'          => 'button_font_weight',
            'default_value' => '',
            'label'         => esc_html__('Font Weight', 'rebellion'),
            'options'       => rebellion_edge_get_font_weight_array()
        ));

        //Outline type options
        rebellion_edge_add_admin_section_title(array(
            'name' => 'type_section_title',
            'title' => esc_html__('Types', 'rebellion'),
            'parent' => $panel
        ));

        $outline_group = rebellion_edge_add_admin_group(array(
            'name' => 'outline_group',
            'title' => esc_html__('Outline Type', 'rebellion'),
            'description' => esc_html__('Setup outline button type', 'rebellion'),
            'parent' => $panel
        ));

        $outline_row = rebellion_edge_add_admin_row(array(
            'name' => 'outline_row',
            'next' => true,
            'parent' => $outline_group
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $outline_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'rebellion'),
            'description'   => ''
        ));

        $outline_row2 = rebellion_edge_add_admin_row(array(
            'name' => 'outline_row2',
            'next' => true,
            'parent' => $outline_group
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $outline_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_outline_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color', 'rebellion'),
            'description'   => ''
        ));

        //Solid type options
        $solid_group = rebellion_edge_add_admin_group(array(
            'name' => 'solid_group',
            'title' => esc_html__('Solid Type', 'rebellion'),
            'description' => esc_html__('Setup solid button type', 'rebellion'),
            'parent' => $panel
        ));

        $solid_row = rebellion_edge_add_admin_row(array(
            'name' => 'solid_row',
            'next' => true,
            'parent' => $solid_group
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_text_color',
            'default_value' => '',
            'label'         => esc_html__('Text Hover Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Background Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $solid_row,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_bg_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Background Color', 'rebellion'),
            'description'   => ''
        ));

        $solid_row2 = rebellion_edge_add_admin_row(array(
            'name' => 'solid_row2',
            'next' => true,
            'parent' => $solid_group
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_border_color',
            'default_value' => '',
            'label'         => esc_html__('Border Color', 'rebellion'),
            'description'   => ''
        ));

        rebellion_edge_add_admin_field(array(
            'parent'        => $solid_row2,
            'type'          => 'colorsimple',
            'name'          => 'btn_solid_hover_border_color',
            'default_value' => '',
            'label'         => esc_html__('Hover Border Color', 'rebellion'),
            'description'   => ''
        ));
    }

    add_action('rebellion_edge_options_elements_map', 'rebellion_edge_button_map');
}