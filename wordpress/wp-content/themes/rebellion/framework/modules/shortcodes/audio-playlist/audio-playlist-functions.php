<?php

if(!function_exists('rebellion_edge_get_audio_playlist_html')) {
    /**
     * Calls audio playlist shortcode with given parameters and returns it's output
     * @param $params
     *
     * @return mixed|string
     */
    function rebellion_edge_get_audio_playlist_html($params) {
        $audio_playlist_html = rebellion_edge_execute_shortcode('edgtf_audio_playlist', $params);
        $audio_playlist_html = str_replace("\n", '', $audio_playlist_html);
        return $audio_playlist_html;
    }
}