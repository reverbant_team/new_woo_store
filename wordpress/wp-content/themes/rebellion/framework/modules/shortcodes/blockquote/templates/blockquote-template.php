<?php
/**
 * Blockquote shortcode template
 */
?>

<blockquote class="edgtf-blockquote-shortcode" <?php rebellion_edge_inline_style($blockquote_style); ?> >
	<div class="edgtf-icon-quotations-holder">
		<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/quote-mark.png" alt="quote_mark" />
	</div>
	<p class="edgtf-blockquote-text">
		<span><?php echo esc_attr($text); ?></span>
	</p>
</blockquote>