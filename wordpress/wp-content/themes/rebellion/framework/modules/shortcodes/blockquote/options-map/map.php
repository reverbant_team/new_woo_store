<?php

if ( ! function_exists('rebellion_edge_blockquote_options_map') ) {
	/**
	 * Add Blockquote options to elements page
	 */
	function rebellion_edge_blockquote_options_map() {

		$panel_blockquote = rebellion_edge_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_blockquote',
				'title' => esc_html__('Blockquote','rebellion')
			)
		);

	}

	add_action( 'rebellion_edge_options_elements_map', 'rebellion_edge_blockquote_options_map');

}