<div class="edgtf-boxed-icon-holder" <?php rebellion_edge_inline_style($holder_style); ?>>
	<div class="edgtf-boxed-icon">
		<?php if($link): ?>
			<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>">
		<?php endif; ?>
			<div class="edgtf-boxed-icon-inner">
				<?php if(!empty($custom_icon)) : ?>
					<div class="edgtf-boxed-icon-custom-icon">
						<?php echo wp_get_attachment_image($custom_icon, 'full'); ?>
					</div>
				<?php else: ?>
					<?php echo rebellion_edge_execute_shortcode('edgtf_icon', $icon_parameters); ?>
				<?php endif; ?>
				<?php if ($icon_title != '') { ?>
					<span class="edgtf-boxed-icon-title" <?php rebellion_edge_inline_style($icon_title_style); ?>><?php echo esc_attr($icon_title) ?></span>
				<?php } ?>
			</div>
		<?php if($link): ?>
			</a>
		<?php endif; ?>
	</div>
</div>