<?php
namespace RebellionEdge\Modules\Shortcodes\BoxedIcon;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

class BoxedIcon implements ShortcodeInterface{
	private $base;

	function __construct() {
		$this->base = 'edgtf_boxed_icon';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')){
			vc_map(
				array(
					'name' => esc_html__('Boxed Icon', 'rebellion'),
					'base' => $this->base,
					'as_child' => array('only' => 'edgtf_boxed_icons'),
					'as_parent' => array('except' => 'vc_row, vc_accordion'),
					'content_element' => true,
					'category' => esc_html__('by EDGE', 'rebellion'),
					'icon' => 'icon-wpb-boxed-icon extended-custom-icon',
					'params' => array_merge(
						array(
							array(
								'type' => 'colorpicker',
								'heading' => esc_html__('Background Color', 'rebellion'),
								'param_name' => 'background_color',
								'value' => '',
							)
						),
						rebellion_edge_icon_collections()->getVCParamsArray(array(),'',true),
						array(
							array(
								'type'       => 'attach_image',
								'heading'    => esc_html__('Custom Icon', 'rebellion'),
								'param_name' => 'custom_icon'
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__('Icon Title', 'rebellion'),
								'param_name' => 'icon_title',
								'group'      => esc_html__('Icon Settings', 'rebellion'),
								'admin_label' => true
							),
							array(
								'type'       => 'colorpicker',
								'heading'    => esc_html__('Icon Title Color', 'rebellion'),
								'param_name' => 'icon_title_color',
								'dependency' => array('element' => 'icon_title', 'not_empty' => true),
								'group'      => esc_html__('Icon Settings', 'rebellion'),
							),
							array(
								'type'        => 'dropdown',
								'heading'     => esc_html__('Icon Size', 'rebellion'),
								'param_name'  => 'icon_size',
								'value'       => array(
									esc_html__('Tiny' , 'rebellion')      => 'edgtf-icon-tiny',
									esc_html__('Small', 'rebellion')      => 'edgtf-icon-small',
									esc_html__('Medium', 'rebellion')     => 'edgtf-icon-medium',
									esc_html__('Large', 'rebellion')      => 'edgtf-icon-large',
									esc_html__('Very Large', 'rebellion') => 'edgtf-icon-huge'
								),
								'admin_label' => true,
								'save_always' => true,
								'group'       => esc_html__('Icon Settings', 'rebellion'),
							),
							array(
								'type'       => 'textfield',
								'heading'    => esc_html__('Custom Icon Size (px)', 'rebellion'),
								'param_name' => 'custom_icon_size',
								'group'      => esc_html__('Icon Settings', 'rebellion')
							),
							array(
								'type'       => 'colorpicker',
								'heading'    => esc_html__('Icon Color', 'rebellion'),
								'param_name' => 'icon_color',
								'group'      => esc_html__('Icon Settings', 'rebellion')
							),
							array(
								'type'       => 'colorpicker',
								'heading'    => esc_html__('Icon Hover Color', 'rebellion'),
								'param_name' => 'icon_hover_color',
								'group'      => esc_html__('Icon Settings', 'rebellion')
							),
							array(
								'type'        => 'textfield',
								'heading'     => esc_html__('Link', 'rebellion'),
								'param_name'  => 'link',
								'value'       => '',
								'admin_label' => true
							),
							array(
								'type'       => 'dropdown',
								'heading'    => esc_html__('Target', 'rebellion'),
								'param_name' => 'target',
								'value'      => array(
									esc_html__('Self', 'rebellion')  => '_self',
									esc_html__('Blank', 'rebellion') => '_blank'
								),
								'dependency' => array('element' => 'link', 'not_empty' => true),
							)
						)
					)
				)
			);			
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'background_color'	=> '',
			'custom_icon'		=> '',
			'icon_title'		=> '',
			'icon_title_color'		=> '',
			'icon_size'			=> '',
			'custom_icon_size'	=> '',
			'icon_color'		=> '',
			'icon_hover_color'	=> '',
			'link'				=> '',
			'target'			=> '_self',
		);

		$args = array_merge($args, rebellion_edge_icon_collections()->getShortcodeParams());
		$params = shortcode_atts($args, $atts);
		extract($params);
		$params['icon_parameters'] = $this->getIconParameters($params);
		$params['holder_style'] = $this->getHolderStyle($params);
		$params['icon_title_style'] = $this->getTitleStyle($params);
		$html = rebellion_edge_get_shortcode_module_template_part('templates/boxed-icon-template', 'boxed-icons', '', $params);

		return $html;
	}

	private function getIconParameters($params) {
		$params_array = array();

		if(empty($params['custom_icon'])) {
			$iconPackName = rebellion_edge_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);

			$params_array['icon_pack']   = $params['icon_pack'];
			$params_array[$iconPackName] = $params[$iconPackName];

			if(!empty($params['icon_size'])) {
				$params_array['size'] = $params['icon_size'];
			}

			if(!empty($params['custom_icon_size'])) {
				$params_array['custom_size'] = $params['custom_icon_size'];
			}



			$params_array['icon_color'] = $params['icon_color'];

			if(!empty($params['icon_hover_color'])) {
				$params_array['hover_icon_color'] = $params['icon_hover_color'];
			}
		}

		return $params_array;
	}

	private function getHolderStyle($params) {
		$style = array();

		if($params['background_color'] != '') {
			$style[] = 'background-color:'.$params['background_color'];
		}

		return $style;
	}

	private function getTitleStyle($params) {
		$style = array();

		if($params['icon_title_color'] != '') {
			$style[] = 'color:'.$params['icon_title_color'];
		}

		return $style;
	}

}
