<?php

if(!function_exists('rebellion_edge_get_google_map_html')) {
    /**
     * Calls google map shortcode with given parameters and returns it's output
     * @param $params
     *
     * @return mixed|string
     */
    function rebellion_edge_get_google_map_html($params) {
        $google_map_html = rebellion_edge_execute_shortcode('edgtf_google_map', $params);
        $google_map_html = str_replace("\n", '', $google_map_html);
        
        return $google_map_html;
    }
}