<?php
namespace RebellionEdge\Modules\Shortcodes\AlbumDisc;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;


/**
 * Class AlbumDisc that represents audio playlist shortcode
 * @package RebellionEdge\Modules\Shortcodes\AlbumDisc
 */
class AlbumDisc implements ShortcodeInterface {
    /**
     * @var string
     */
    private $base;

    /**
     * Sets base attribute and registers shortcode with Visual Composer
     */
    public function __construct() {
        $this->base = 'edgtf_album_disc';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    /**
     * Returns base attribute
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Maps shortcode to Visual Composer
     */
    public function vcMap() {
        vc_map(array(
            'name'                      => esc_html__('Album Disc', 'rebellion'),
            'base'                      => $this->base,
            'category'                  => esc_html__('by EDGE', 'rebellion'),
            'icon'                      => 'icon-wpb-album-disc extended-custom-icon',
            'allowed_container_element' => 'vc_row',
            'params'                    =>  array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('CD Case Image', 'rebellion'),
                        'param_name' => 'cd_case_image'
                    ),
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__('CD Image', 'rebellion'),
                        'param_name' => 'cd_image',
                        'description' => esc_html('Set an image to be placed upon the CD template', 'rebellion')
                    ),
                    array(
                        'type' => 'colorpicker',
                        'heading' => esc_html__('CD Image Outer Border Color', 'rebellion'),
                        'param_name' => 'cd_image_outer_border_color',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => esc_html__('Link', 'rebellion'),
                        'param_name'  => 'link',
                        'admin_label' => true
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading'     => esc_html__('Link Target', 'rebellion'),
                        'param_name' => 'link_target',
                        'value' => array(
                            esc_html__('Same Window', 'rebellion')  => '_self',
                            esc_html__('New Window', 'rebellion')   => '_blank'
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading'     => esc_html__('Animate', 'rebellion'),
                        'param_name' => 'animate',
                        'value' => array(
                            esc_html__('On Appear', 'rebellion')  => 'appear',
                            esc_html__('On Hover', 'rebellion')   => 'hover'
                        ),
                        'admin_label' => true,
                        'group' => esc_html__('Behavior', 'rebellion')
                    ),
                ), 
        ));
    }

    /**
     * Renders HTML for album disc shortcode
     *
     * @param array $atts
     * @param null $content
     *
     * @return string
     */
    public function render($atts, $content = null) {
        $args = array(  
            'cd_case_image' => '',
            'cd_image' => '',
            'cd_image_outer_border_color' => '',
            'link' => '',
            'link_target' => '_self',
            'animate' => 'appear',
        );

        $params = shortcode_atts($args, $atts);
        extract($params);

        $params['holder_classes'] = $this->getHolderClasses($params);
        $params['cd_image_styles'] = $this->getCDImageStyles($params);

        $html = rebellion_edge_get_shortcode_module_template_part('templates/album-disc-template', 'album-disc', '', $params);

        return $html;
    }


    /**
     * Return Holder classes
     *
     * @param $params
     * @return array
     */
    private function getHolderClasses($params) {
        $holder_classes = array();

        if ($params['animate'] !== '') {
            $holder_classes[] = 'edgtf-animate-on-'. $params['animate'];
        }

        return implode(' ', $holder_classes);
    }

     /**
     * Returns array of cd image styles
     *
     * @param $params
     *
     * @return array
     */
    private function getCDImageStyles($params) {
        $styles = array();

        if(!empty($params['cd_image_outer_border_color'])) {
            $styles[] = 'border-color: '.$params['cd_image_outer_border_color'];
        }

        return $styles;
    }
}