<div class="edgtf-blog-list-item-text-holder" <?php rebellion_edge_inline_style($bg_color); ?>>
	<div class="edgtf-blog-list-item-mark edgtf-quote-mark">
		<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/quote-mark-masonry.png" alt="quote_mark" />
	</div>
	<div class="edgtf-blog-list-item-title" <?php rebellion_edge_inline_style($title_color); ?>>
		<a  href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo wp_kses_post(get_post_meta(get_the_ID(), "edgtf_post_quote_text_meta", true)); ?></a>
	</div>
	<span class="edgtf-quote-author" <?php rebellion_edge_inline_style($post_info_color); ?>><?php the_title(); ?></span>
	<div class="edgtf-blog-list-item-info-section" <?php rebellion_edge_inline_style($post_info_color); ?>>
		<?php rebellion_edge_post_info(array(
			'date' => 'no',
			'category' => 'no',
			'author' => 'no',
			'comments' => 'no',
			'like' => 'no'
		)) ?>
	</div>
</div>

