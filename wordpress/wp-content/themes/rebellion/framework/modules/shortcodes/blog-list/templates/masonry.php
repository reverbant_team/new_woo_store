<li class="edgtf-blog-list-item edgtf-blog-list-masonry-item edgtf-blog-list-<?php echo rebellion_edge_get_post_format('');?> ">
	<div class="edgtf-blog-list-item-inner">
		<?php 
			$post_format = rebellion_edge_get_post_format('');
			echo rebellion_edge_get_shortcode_module_template_part('templates/post-formats/' . $post_format, 'blog-list','',$params);
		?>
 </div>	
</li>