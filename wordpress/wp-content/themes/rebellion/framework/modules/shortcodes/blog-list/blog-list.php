<?php

namespace RebellionEdge\Modules\Shortcodes\BlogList;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class BlogList
 */
class BlogList implements ShortcodeInterface {
	/**
	* @var string
	*/
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_blog_list';
		
		add_action('vc_before_init', array($this,'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('Blog List', 'rebellion'),
			'base' => $this->base,
			'icon' => 'icon-wpb-blog-list extended-custom-icon',
			'category' => esc_html__('by EDGE', 'rebellion'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Type', 'rebellion'),
						'param_name' => 'type',
						'value' => array(
							esc_html__('Boxes','rebellion') => 'boxes',
							esc_html__('Minimal','rebellion') => 'minimal',
							esc_html__('Masonry','rebellion') => 'masonry',
							esc_html__('Image in box','rebellion') => 'image_in_box'
						),
						'description' => '',
						'admin_label' => true
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Number of Posts','rebellion'),
						'param_name' => 'number_of_posts',
						'description' => ''
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Number of Columns','rebellion'),
						'param_name' => 'number_of_columns',
						'value' => array(
							esc_html__('One','rebellion') => '1',
							esc_html__('Two','rebellion') => '2',
							esc_html__('Three','rebellion') => '3',
							esc_html__('Four','rebellion') => '4'
						),
						'description' => '',
						'dependency' => Array('element' => 'type', 'value' => array('boxes')),
   
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Order By','rebellion'),
						'param_name' => 'order_by',
						'value' => array(
							esc_html__('Title','rebellion') => 'title',
							esc_html__('Date','rebellion') => 'date'
						)
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Order','rebellion'),
						'param_name' => 'order',
						'value' => array(
							esc_html__('ASC','rebellion') => 'ASC',
							esc_html__('DESC','rebellion') => 'DESC'
						)
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Category Slug','rebellion'),
						'param_name' => 'category',
						'admin_label' => true,
						'description' => esc_html__('Leave empty for all or use comma for list','rebellion')
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Image Size','rebellion'),
						'param_name' => 'image_size',
						'value' => array(
							esc_html__('Original','rebellion') => 'original',
							esc_html__('Landscape','rebellion') => 'landscape',
							esc_html__('Square','rebellion') => 'square'
						),
						'description' => '',
						'dependency' => array('element' => 'type', 'value' => array('boxes'))
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('Text length','rebellion'),
						'param_name' => 'text_length',
						'description' => esc_html__('Number of characters','rebellion')
					),
						array(
						'type' => 'colorpicker',
						'heading' => esc_html__('Title Color','rebellion'),
						'param_name' => 'title_color',
						'description' => '',
						'dependency'  => array('element' => 'type','value'=> array('boxes','masonry')),
					),
					array(
						'type' => 'colorpicker',
						'heading' => esc_html__('Post Info Color','rebellion'),
						'param_name' => 'post_info_color',
						'description' => '',
						'dependency'  => array('element' => 'type','value'=> array('boxes','masonry')),
					),
					array(
						'type' => 'colorpicker',
						'heading' => esc_html__('Text Color','rebellion'),
						'param_name' => 'text_color',
						'description' => '',
						'dependency'  => array('element' => 'type','value'=> array('boxes','masonry')),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Box Shadow','rebellion'),
						'param_name' => 'box_shadow',
						'description' => '',
						'value' => array(
							esc_html__('No','rebellion')   => 'no',
							esc_html__('Yes','rebellion') => 'yes',
						),
						'dependency'  => array('element' => 'type','value'=> array('boxes','masonry')),
					),
					array(
						'type' => 'colorpicker',
						'heading' => esc_html__('Background Color','rebellion'),
						'param_name' => 'bg_color',
						'description' => '',
						'dependency'  => array('element' => 'type','value'=> array('boxes','masonry')),
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Title Tag','rebellion'),
						'param_name' => 'title_tag',
						'value' => array(
							''   => '',
							'h2' => 'h2',
							'h3' => 'h3',
							'h4' => 'h4',
							'h5' => 'h5',
							'h6' => 'h6',
						),
						'description' => '',
						'dependency'  => array('element' => 'type','value'=> array('boxes','minimal','image_in_box')),
					),
					array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Show Load More','rebellion') ,
                        'param_name' => 'show_load_more',
                        'value' => array(
                            esc_html__('No','rebellion')  => 'no',
                            esc_html__('Yes','rebellion') => 'yes'
                        )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__('Load More Skin','rebellion') ,
                        'param_name' => 'load_more_skin',
                        'value' => array(
                            esc_html__('Dark','rebellion')  => 'dark',
                            esc_html__('Light','rebellion') => 'light'
                        ),
                        'dependency'  => array('element' => 'show_load_more','value'=> array('yes')),
                    ),
				)
		) );

	}
	public function render($atts, $content = null) {
		
		$default_atts = array(
			'type' 					=> 'boxes',
            'number_of_posts' 		=> '',
            'number_of_columns'		=> '',
            'image_size'			=> 'original',
            'order_by'				=> 'title',
            'order'					=> 'ASC',
            'category'				=> '',
            'title_tag'				=> 'h4',
			'text_length'			=> '90',
			'title_color'			=> '',
			'post_info_color'		=> '',
			'text_color'			=> '',
			'box_shadow'			=> 'no',
			'bg_color'				=> '',
			'show_load_more'		=> '',
			'load_more_skin'		=> 'dark'
        );
		
		$params = shortcode_atts($default_atts, $atts);
		extract($params);
		$params['holder_classes'] = $this->getBlogHolderClasses($params);
	
		$queryArray = $this->generateBlogQueryArray($params);
		$query_result = new \WP_Query($queryArray);
		$params['query_result'] = $query_result;	
		
        $thumbImageSize = $this->generateImageSize($params);
		$params['thumb_image_size'] = $thumbImageSize;

		$params['data_atts'] = $this->getDataAtts($params);
		$params['data_atts'] .= 'data-max-num-pages = '.$query_result->max_num_pages;


		$params['title_color'] = $this->getBlogListTitleColor($params);
		$params['post_info_color'] = $this->getBlogListPostInfoColor($params);
		$params['text_color'] = $this->getBlogListTextColor($params);
		$params['bg_color'] = $this->getHolderBackgroundColor($params);

		$html ='';
        $html .= rebellion_edge_get_shortcode_module_template_part('templates/blog-list-holder', 'blog-list', '', $params);
		return $html;
		
	}

	/**
	   * Generates holder classes
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getBlogHolderClasses($params){
		$holderClasses = '';
		
		$columnNumber = $this->getColumnNumberClass($params);
		
		if(!empty($params['type'])){
			switch($params['type']){
				case 'image_in_box':
					$holderClasses = 'edgtf-image-in-box';
				break;
				case 'boxes' : 
					$holderClasses = 'edgtf-boxes';
				break;	
				case 'masonry' : 
					$holderClasses = 'edgtf-masonry';
				break;
				case 'minimal' :
					$holderClasses = 'edgtf-minimal';
				break;
				default: 
					$holderClasses = 'edgtf-boxes';
			}
		}
		
		$holderClasses .= ' '.$columnNumber;

        if(!empty($params['bg_color'])) {
            $holderClasses .= ' edgtf-holder-with-padding';
        }

        if($params['box_shadow'] == 'yes') {
            $holderClasses .= ' edgtf-holder-with-shadow';
            $holderClasses .= ' edgtf-holder-with-padding';
        }

        if($params['load_more_skin'] == 'light') {
            $holderClasses .= ' edgtf-holder-load-more-light';
        }

		return $holderClasses;
		
	}

	/** 
	   * Generates column classes for boxes type
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function getColumnNumberClass($params){
		
		$columnsNumber = '';
		$type = $params['type'];
		$columns = $params['number_of_columns'];
		
        if ($type == 'boxes') {
            switch ($columns) {
                case 1:
                    $columnsNumber = 'edgtf-one-column';
                    break;
                case 2:
                    $columnsNumber = 'edgtf-two-columns';
                    break;
                case 3:
                    $columnsNumber = 'edgtf-three-columns';
                    break;
                case 4:
                    $columnsNumber = 'edgtf-four-columns';
                    break;
                default:
					$columnsNumber = 'edgtf-one-column';
                    break;
            }
        }
		return $columnsNumber;
	}

	/**
	   * Generates query array
	   *
	   * @param $params
	   *
	   * @return array
	*/
	public function generateBlogQueryArray($params){

		$queryArray = array();
		
		$queryArray = array(
			'orderby' => $params['order_by'],
			'order' => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'category_name' => $params['category']
		);

		$paged = '';
        if(empty($params['next_page'])) {
            if(get_query_var('paged')) {
                $paged = get_query_var('paged');
            } elseif(get_query_var('page')) {
                $paged = get_query_var('page');
            }
        }

        if(!empty($params['next_page'])){
            $queryArray['paged'] = $params['next_page'];

        } else{
            $queryArray['paged'] = 1;
        }

		return $queryArray;
	}

	/**
	   * Generates image size option
	   *
	   * @param $params
	   *
	   * @return string
	*/
	private function generateImageSize($params){
		$thumbImageSize = '';
		$imageSize = $params['image_size'];
		
		if ($imageSize !== '' && $imageSize == 'landscape') {
            $thumbImageSize .= 'rebellion_edge_landscape';
        } else if($imageSize === 'square'){
			$thumbImageSize .= 'rebellion_edge_square';
		} else if ($imageSize !== '' && $imageSize == 'original') {
            $thumbImageSize .= 'full';
        }
		return $thumbImageSize;
	}

	private function getBlogListTitleColor($params) {

		$title_color = array();

		if ($params['title_color'] !== '') {
			$title_color[] = 'color:' . $params['title_color'];
		}

		return implode(';', $title_color);
	}

	private function getBlogListPostInfoColor($params) {

		$post_info_color = array();

		if ($params['post_info_color'] !== '') {
			$post_info_color[] = 'color:' . $params['post_info_color'];
		}

		return implode(';', $post_info_color);
	}

	private function getBlogListTextColor($params) {

		$text_color = array();

		if ($params['text_color'] !== '') {
			$text_color[] = 'color:' . $params['text_color'];
		}

		return implode(';', $text_color);
	}

	private function getHolderBackgroundColor($params) {

		$bg_color = array();

		if ($params['bg_color'] !== '') {
			$bg_color[] = 'background-color:' . $params['bg_color'];
		}

		return implode(';', $bg_color);
	}

	/**
     * Generates datta attributes array
     *
     * @param $params
     *
     * @return array
     */
    public function getDataAtts($params){

        $data_attr = array();
        $data_return_string = '';

        if(get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif(get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }

        if(!empty($paged)) {
            $data_attr['data-next-page'] = $paged+1;
        }
        if(!empty($params['order_by'])){
            $data_attr['data-order-by'] = $params['order_by'];
        }
        if(!empty($params['order'])){
            $data_attr['data-order'] = $params['order'];
        }
        if(!empty($params['number_of_posts'])){
            $data_attr['data-number'] = $params['number_of_posts'];
        }
        if(!empty($params['title_tag'])){
            $data_attr['data-title-tag'] = $params['title_tag'];
        }
        if(!empty($params['title_color'])){
            $data_attr['data-title-color'] = $params['title_color'];
        }
        if(!empty($params['text_color'])){
            $data_attr['data-text-color'] = $params['text_color'];
        }
        if(!empty($params['post_info_color'])){
            $data_attr['data-post-info-color'] = $params['post_info_color'];
        }
        if(!empty($params['box_shadow'])){
            $data_attr['data-box-shadow'] = $params['box_shadow'];
        }
        if(!empty($params['bg_color'])){
            $data_attr['data-bg-color'] = $params['bg_color'];
        }
        if(!empty($params['show_load_more'])){
            $data_attr['data-show-load-more'] = $params['show_load_more'];
        }
        if(!empty($params['load_more_skin'])){
            $data_attr['data-load-more-skin'] = $params['load_more_skin'];
        }
        if(!empty($params['text_length'])){
            $data_attr['data-text-length'] = $params['text_length'];
        }
        if(!empty($params['category'])){
            $data_attr['data-category'] = $params['category'];
        }
        if(!empty($params['image-size'])){
            $data_attr['data-image-size'] = $params['image-size'];
        }
        if(!empty($params['type'])){
            $data_attr['data-type'] = $params['type'];
        }
        
        foreach($data_attr as $key => $value) {
            if($key !== '') {
                $data_return_string .= $key . '= "' . esc_attr( $value ) . '" ';
            }
        }
        return $data_return_string;
    }


}
	
