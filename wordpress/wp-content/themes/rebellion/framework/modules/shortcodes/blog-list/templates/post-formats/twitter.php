<?php $params_social = rebellion_edge_generate_instagram_twitter_params('twitter'); ?>

<div class="edgtf-blog-list-item-text-holder" <?php rebellion_edge_inline_style($bg_color); ?>>
<div class="edgtf-blog-list-item-mark edgtf-link-mark">
<span class="social_twitter"></span>
</div>
	<div class="edgtf-ql-content">
		<h6 class="edgtf-blog-list-item-twitter-user" <?php rebellion_edge_inline_style($post_info_color); ?>>&commat;<?php print $params_social['twitter_author']; ?></h6>
		<h5 <?php rebellion_edge_inline_style($text_color); ?>><?php print $params_social['twitter_text']; ?></h5>
		<div class="edgtf-blog-list-item-info-section" <?php rebellion_edge_inline_style($post_info_color); ?>>
			<?php print $params_social['twitter_time']; ?>
		</div>
	</div>
</div>