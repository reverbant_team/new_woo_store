<div class="edgtf-blog-list-item-text-holder" <?php rebellion_edge_inline_style($bg_color); ?>>
	<div class="edgtf-blog-list-item-mark edgtf-link-mark">
		<img src="<?php echo EDGE_ASSETS_ROOT ;?>/img/link-mark-masonry.png" alt="link_mark" />
	</div>
	<div class="edgtf-blog-list-item-title " <?php rebellion_edge_inline_style($title_color); ?>>
		<a href="<?php echo esc_url(get_permalink()) ?>" >
			<?php echo wp_kses_post(get_the_title()) ?>
		</a>
	</div>
	<div class="edgtf-blog-list-item-info-section" <?php rebellion_edge_inline_style($post_info_color); ?>>
		<?php rebellion_edge_post_info(array(
			'date' => 'no',
			'category' => 'no',
			'author' => 'no',
			'comments' => 'no',
			'like' => 'no'
		)) ?>
	</div>
</div>