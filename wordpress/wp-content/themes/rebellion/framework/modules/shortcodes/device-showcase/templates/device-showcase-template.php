<div class="edgtf-device-showcase <?php echo esc_attr($holder_classes); ?>">
	<div class="edgtf-ds-images-holder">
		<div class="edgtf-ds-desktop-device edgtf-device">
			<?php if ($desktop_image_link != '') { ?>
				<a href="<?php echo esc_url($desktop_image_link) ?>" target="<?php echo esc_attr($desktop_image_link_target) ?>"></a>
			<?php } ?>
			<img src="<?php echo EDGE_ASSETS_ROOT ?>/css/img/browser.png" alt="<?php echo esc_html__('desktop browser window','rebellion')?>" />
			<div class="edgtf-ds-desktop-image-holder">
				<div class="edgtf-ds-deskop-image edgtf-device-image" style="background-image: url(<?php echo wp_get_attachment_url( $desktop_image ); ?>)">
					
				</div>
			</div>
		</div>
		<div class="edgtf-ds-tablet-device edgtf-device">
			<?php if ($tablet_image_link != '') { ?>
				<a href="<?php echo esc_url($tablet_image_link) ?>" target="<?php echo esc_attr($tablet_image_link_target) ?>"></a>
			<?php } ?>
			<img src="<?php echo EDGE_ASSETS_ROOT ?>/css/img/tablet.png" alt="<?php echo esc_html__('tablet device frame','rebellion')?>" />
			<div class="edgtf-ds-tablet-image-holder">
				<div class="edgtf-ds-tablet-image edgtf-device-image" style="background-image: url(<?php echo wp_get_attachment_url( $tablet_image ); ?>)">
					
				</div>
			</div>
		</div>
		<div class="edgtf-ds-phone-device edgtf-device">
			<?php if ($phone_image_link != '') { ?>
				<a href="<?php echo esc_url($phone_image_link) ?>" target="<?php echo esc_attr($phone_image_link_target) ?>"></a>
			<?php } ?>
			<img src="<?php echo EDGE_ASSETS_ROOT ?>/css/img/phone.png" alt="<?php echo esc_html__('phone device frame','rebellion')?>" />
			<div class="edgtf-ds-phone-image-holder">
				<div class="edgtf-ds-phone-image edgtf-device-image" style="background-image: url(<?php echo wp_get_attachment_url( $phone_image ); ?>)">
					
				</div>
			</div>
		</div>
	</div>
	<div class="edgtf-ds-text-holder">
		<h5 class="edgtf-ds-title"><?php echo esc_attr($title); ?></h5>
		<span class="edgtf-ds-description"><?php echo esc_attr($description); ?></span>
	</div>
</div>