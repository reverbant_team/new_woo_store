<?php
namespace RebellionEdge\Modules\Shortcodes\Banner;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class ItemShowcase
 */
class Banner implements ShortcodeInterface  {
    private $base; 
    
    function __construct() {
        $this->base = 'edgtf_banner';

        add_action('vc_before_init', array($this, 'vcMap'));
    }
    
    /**
        * Returns base for shortcode
        * @return string
     */
    public function getBase() {
        return $this->base;
    }   
    
    public function vcMap() {
                        
        vc_map( array(
            'name' => esc_html__('Banner', 'rebellion'),
            'base' => $this->base,
            'category' => esc_html__('by EDGE', 'rebellion'),
            'icon' => 'icon-wpb-banner extended-custom-icon',
            'params' => array(
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__('Image', 'rebellion'),
                    'param_name' => 'item_image'
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__('Link', 'rebellion'),
                    'param_name' => 'link'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__('Link Target', 'rebellion'),
                    'param_name' => 'link_target',
                    'value' => array(
                        ''                             => '',
                        esc_html__('Self', 'rebellion')  => '_self',
                        esc_html__('Blank', 'rebellion') => '_blank'
                    )
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__('Title', 'rebellion'),
                    'admin_label' => true,                    
                    'param_name' => 'banner_title',
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html__('Image Hover', 'rebellion'),
                    'param_name' => 'image_hover',
                    'value' => array(
                        esc_html__('No Hover', 'rebellion')   => '',
                        esc_html__('Zoom', 'rebellion')       => 'zoom'
                    ),
                    'dependency' => array('element' => 'item_image', 'not_empty' => true),
                )
            )
        ) );

    }

    /**
     * Renders shortcodes HTML
     *
     * @param $atts array of shortcode params
     * @param $content string shortcode content
     * @return string
     */

    public function render($atts, $content = null) {
        
        $args = array(
            'item_image' => '',
            'banner_title' => '',
            'link' => '',
            'link_target' => '',
            'image_hover' => ''

        );

        $params = shortcode_atts($args, $atts);
        $params['banner_classes'] = $this->getBannerClass($params);

        extract($params);
        $params['content']= $content;

        if($params['link_target'] == ''){
            $params['link_target'] = '_self';
        }

        $html = rebellion_edge_get_shortcode_module_template_part('templates/banner-template', 'banner', '', $params);

        return $html;

    }

    /**
     * Return Separator classes
     *
     * @param $params
     * @return array
     */
    private function getBannerClass($params) {

        $banner_classes = array();
        
        if($params['image_hover'] != '') {
            $banner_classes[] = 'edgtf-bih-'.$params['image_hover'];
        }

        return implode(' ', $banner_classes);

    }

  }
