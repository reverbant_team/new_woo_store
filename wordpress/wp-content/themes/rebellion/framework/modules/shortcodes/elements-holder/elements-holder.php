<?php
namespace RebellionEdge\Modules\Shortcodes\ElementsHolder;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

class ElementsHolder implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'edgtf_elements_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Elements Holder', 'rebellion'),
			'base' => $this->base,
			'icon' => 'icon-wpb-elements-holder extended-custom-icon',
			'category' => esc_html__('by EDGE', 'rebellion'),
			'as_parent' => array('only' => 'edgtf_elements_holder_item'),
			'js_view' => 'VcColumnView',
			'params' => array(
				array(
					'type' => 'colorpicker',
					'class' => '',
					'heading' => esc_html__('Background Color', 'rebellion'),
					'param_name' => 'background_color',
					'value' => '',
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'heading' => esc_html__('Columns', 'rebellion'),
					'admin_label' => true,
					'param_name' => 'number_of_columns',
					'value' => array(
                        esc_html__('1 Column', 'rebellion')      	=> 'one-column',
                        esc_html__('2 Columns', 'rebellion')    		=> 'two-columns',
                        esc_html__('2 Columns(33/66)', 'rebellion') 	=> 'two-columns-grid-33-66',
                        esc_html__('2 Columns(66/33)', 'rebellion')  => 'two-columns-grid-66-33',
                        esc_html__('3 Columns', 'rebellion')     	=> 'three-columns',
                        esc_html__('4 Columns', 'rebellion')    	 	=> 'four-columns',
                        esc_html__('5 Columns', 'rebellion')     	=> 'five-columns',
                        esc_html__('6 Columns', 'rebellion')     	=> 'six-columns'
					),
					'description' => ''
				),
				array(
					'type' => 'checkbox',
					'class' => '',
					'heading' => esc_html__('Items Float Left', 'rebellion'),
					'param_name' => 'items_float_left',
					'value' => array(esc_html__('Make Items Float Left?', 'rebellion') => 'yes'),
					'description' => ''
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness', 'rebellion'),
					'heading' => esc_html__('Switch to One Column', 'rebellion'),
					'param_name' => 'switch_to_one_column',
					'value' => array(
                        esc_html__('Default', 'rebellion')    		=> '',
                        esc_html__('Below 1280px', 'rebellion') 	=> '1280',
                        esc_html__('Below 1024px', 'rebellion')    	=> '1024',
                        esc_html__('Below 768px', 'rebellion')     	=> '768',
                        esc_html__('Below 600px', 'rebellion')    	=> '600',
                        esc_html__('Below 480px', 'rebellion')    	=> '480',
                        esc_html__('Never', 'rebellion')    			=> 'never'
					),
					'description' => esc_html__('Choose on which stage item will be in one column', 'rebellion')
				),
				array(
					'type' => 'dropdown',
					'class' => '',
					'group' => esc_html__('Width & Responsiveness', 'rebellion'),
					'heading' => esc_html__('Choose Alignment In Responsive Mode', 'rebellion'),
					'param_name' => 'alignment_one_column',
					'value' => array(
                        esc_html__('Default', 'rebellion')    	=> '',
                        esc_html__('Left', 'rebellion') 		=> 'left',
                        esc_html__('Center', 'rebellion')    	=> 'center',
                        esc_html__('Right', 'rebellion')     	=> 'right'
					),
					'description' => esc_html__('Alignment When Items are in One Column', 'rebellion')
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'number_of_columns' 		=> '',
			'switch_to_one_column'		=> '',
			'alignment_one_column' 		=> '',
			'items_float_left' 			=> '',
			'background_color' 			=> ''
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html						= '';

		$elements_holder_classes = array();
		$elements_holder_classes[] = 'edgtf-elements-holder';
		$elements_holder_style = '';

		if($number_of_columns != ''){
			$elements_holder_classes[] .= 'edgtf-'.$number_of_columns ;
		}

		if($switch_to_one_column != ''){
			$elements_holder_classes[] = 'edgtf-responsive-mode-' . $switch_to_one_column ;
		} else {
			$elements_holder_classes[] = 'edgtf-responsive-mode-768' ;
		}

		if($alignment_one_column != ''){
			$elements_holder_classes[] = 'edgtf-one-column-alignment-' . $alignment_one_column ;
		}

		if($items_float_left !== ''){
			$elements_holder_classes[] = 'edgtf-elements-items-float';
		}

		if($background_color != ''){
			$elements_holder_style .= 'background-color:'. $background_color . ';';
		}

		$elements_holder_class = implode(' ', $elements_holder_classes);

		$html .= '<div ' . rebellion_edge_get_class_attribute($elements_holder_class) . ' ' . rebellion_edge_get_inline_attr($elements_holder_style, 'style'). '>';
			$html .= do_shortcode($content);
		$html .= '</div>';

		return $html;

	}

}
