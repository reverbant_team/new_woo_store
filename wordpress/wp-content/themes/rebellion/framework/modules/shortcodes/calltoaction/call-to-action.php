<?php
namespace RebellionEdge\Modules\Shortcodes\CallToAction;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;
/**
 * Class CallToAction
 */
class CallToAction implements ShortcodeInterface {

	/**
	 * @var string
	 */
	private $base;

	public function __construct() {
		$this->base = 'edgtf_call_to_action';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	/**
	 * Maps shortcode to Visual Composer. Hooked on vc_before_init
	 *
	 * @see edgt_core_get_carousel_slider_array_vc()
	 */
	public function vcMap() {

		$call_to_action_button_icons_array = array();
		$call_to_action_button_IconCollections = rebellion_edge_icon_collections()->iconCollections;
		foreach($call_to_action_button_IconCollections as $collection_key => $collection) {

			$call_to_action_button_icons_array[] = array(
				'type' => 'dropdown',
				'heading' => esc_html__('Button Icon', 'rebellion'),
				'param_name' => 'button_'.$collection->param,
				'value' => $collection->getIconsArray(),
				'save_always' => true,
				'dependency' => Array('element' => 'button_icon_pack', 'value' => array($collection_key))
			);

		}

		vc_map( array(
				'name' => esc_html__('Call To Action', 'rebellion'),
				'base' => $this->getBase(),
				'category' => esc_html__('by EDGE', 'rebellion'),
				'icon' => 'icon-wpb-call-to-action extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params' => array_merge(
					array(
						array(
							'type'          => 'dropdown',
							'heading'       => esc_html__('Full Width', 'rebellion'),
							'param_name'    => 'full_width',
							'admin_label'	=> true,
							'value'         => array(
								esc_html__('Yes', 'rebellion')       => 'yes',
								esc_html__('No', 'rebellion')        => 'no'
							),
							'save_always' 	=> true,
							'description'   => '',
						),
						array(
							'type'          => 'dropdown',
							'heading'       => esc_html__('Content in grid', 'rebellion'),
							'param_name'    => 'content_in_grid',
							'value'         => array(
								esc_html__('Yes', 'rebellion')       => 'yes',
								esc_html__('No', 'rebellion')        => 'no'
							),
							'save_always'	=> true,
							'description'   => '',
							'dependency'    => array('element' => 'full_width', 'value' => 'yes')
						),
						array(
							'type'          => 'dropdown',
							'heading'       => esc_html__('Grid size', 'rebellion'),
							'param_name'    => 'grid_size',
							'value'         => array(
								'75/25'     => '75',
								'50/50'     => '50',
								'66/33'     => '66'
							),
							'save_always' 	=> true,
							'description'   => '',
							'dependency'    => array('element' => 'content_in_grid', 'value' => 'yes')
						),
						array(
							'type' 			=> 'dropdown',
							'heading'		=> esc_html__('Type', 'rebellion'),
							'param_name' 	=> 'type',
							'admin_label' 	=> true,
							'value' 		=> array(
								esc_html__('Normal', 'rebellion') 	  => 'normal',
								esc_html__('With Icon', 'rebellion')  => 'with-icon',
							),
							'save_always' 	=> true,
							'description' 	=> ''
						)
					),
					rebellion_edge_icon_collections()->getVCParamsArray(array('element' => 'type', 'value' => array('with-icon'))),
					array(
						array(
							'type' 			=> 'colorpicker',
							'heading' 		=> esc_html__('Background Color', 'rebellion'),
							'param_name' 	=> 'bg_color',
							'admin_label' 	=> true,
							'group'			=> esc_html__('Design Options', 'rebellion')
						),
						array(
							'type' 			=> 'textfield',
							'heading' 		=> esc_html__('Icon Size (px)', 'rebellion'),
							'param_name' 	=> 'icon_size',
							'description' 	=> '',
							'dependency' 	=> Array('element' => 'type', 'value' => array('with-icon')),
							'group'			=> esc_html__('Design Options', 'rebellion'),
						),
						array(
							'type' 			=> 'textfield',
							'heading' 		=> esc_html__('Box Padding (top right bottom left) px', 'rebellion'),
							'param_name' 	=> 'box_padding',
							'admin_label' 	=> true,
							'description' 	=> esc_html__('Default padding is 20px on all sides', 'rebellion'),
							'group'			=> esc_html__('Design Options', 'rebellion')
						),
						array(
							'type' 			=> 'textfield',
							'heading' 		=> esc_html__('Default Text Font Size (px)', 'rebellion'),
							'param_name' 	=> 'text_size',
							'description' 	=> esc_html__('Font size for p tag', 'rebellion'),
							'group'			=> esc_html__('Design Options', 'rebellion'),
						),
						array(
							'type' 			=> 'dropdown',
							'heading' 		=> esc_html__('Show Button', 'rebellion'),
							'param_name' 	=> 'show_button',
							'value' 		=> array(
								esc_html__('Yes', 'rebellion') 		=> 'yes',
								esc_html__('No', 'rebellion') 		=> 'no'
							),
							'admin_label' 	=> true,
							'save_always' 	=> true,
							'description' 	=> ''
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Position', 'rebellion'),
							'param_name' => 'button_position',
							'value' => array(
								esc_html__('Default/right', 'rebellion') => '',
								esc_html__('Center', 'rebellion') => 'center',
								esc_html__('Left', 'rebellion') => 'left'
							),
							'description' => '',
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Skin', 'rebellion'),
							'param_name' => 'button_skin',
							'value' => array(
								esc_html__('Dark', 'rebellion') => 'dark',
								esc_html__('Light', 'rebellion') => 'light',
							),
							'description' => '',
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'			=> esc_html__('Design Options', 'rebellion'),
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Size', 'rebellion'),
							'param_name' => 'button_size',
							'value' => array(
								esc_html__('Default', 'rebellion') => '',
								esc_html__('Small', 'rebellion') => 'small',
								esc_html__('Medium', 'rebellion') => 'medium',
								esc_html__('Large', 'rebellion') => 'large',
								esc_html__('Extra Large', 'rebellion') => 'big_large'
							),
							'description' => '',
							'dependency' => array('element' => 'show_button', 'value' => array('yes')),
							'group'			=> esc_html__('Design Options', 'rebellion'),
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__('Button Text', 'rebellion'),
							'param_name' => 'button_text',
							'admin_label' 	=> true,
							'description' => esc_html__('Default text is "button"', 'rebellion'),
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__('Button Link', 'rebellion'),
							'param_name' => 'button_link',
							'description' => '',
							'admin_label' 	=> true,
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Target', 'rebellion'),
							'param_name' => 'button_target',
							'value' => array(
								'' => '',
								esc_html__('Self', 'rebellion') => '_self',
								esc_html__('Blank', 'rebellion') => '_blank'
							),
							'description' => '',
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						),
						array(
							'type' => 'dropdown',
							'heading' => esc_html__('Button Icon Pack', 'rebellion'),
							'param_name' => 'button_icon_pack',
							'value' => array_merge(array(esc_html__('No Icon', 'rebellion') => ''),rebellion_edge_icon_collections()->getIconCollectionsVC()),
							'save_always' => true,
							'dependency' => array('element' => 'show_button', 'value' => array('yes'))
						)
					),
					$call_to_action_button_icons_array,
					array(
						array(
							'type' => 'textarea_html',
							'admin_label' => true,
							'heading' => esc_html__('Content', 'rebellion'),
							'param_name' => 'content',
							'value' => '<p>'.esc_html__('I am test text for Call to action.', 'rebellion').'</p>',
							'description' => ''
						)
					)
				)
		) );

	}

	/**
	 * Renders shortcodes HTML
	 *
	 * @param $atts array of shortcode params
	 * @param $content string shortcode content
	 * @return string
	 */
	public function render($atts, $content = null) {

		$args = array(
			'type' => 'normal',
			'full_width' => 'yes',
			'content_in_grid' => 'yes',
			'grid_size' => '75',
			'bg_color'	=> '',
			'icon_size' => '',
			'box_padding' => '20px',
			'text_size' => '',
			'show_button' => 'yes',
			'button_position' => 'right',
			'button_size' => '',
			'button_skin' => '',
			'button_link' => '',
			'button_text' => 'button',
			'button_target' => '',
			'button_icon_pack' => ''
		);

		$call_to_action_icons_form_fields = array();

		foreach (rebellion_edge_icon_collections()->iconCollections as $collection_key => $collection) {

			$call_to_action_icons_form_fields['button_' . $collection->param ] = '';

		}

		$args = array_merge($args, rebellion_edge_icon_collections()->getShortcodeParams(),$call_to_action_icons_form_fields);

		$params = shortcode_atts($args, $atts);

		$params['content']= preg_replace('#^<\/p>|<p>$#', '', $content);
		$params['text_wrapper_classes'] = $this->getTextWrapperClasses($params);
		$params['content_styles'] = $this->getContentStyles($params);
		$params['call_to_action_styles'] = $this->getCallToActionStyles($params);
		$params['icon'] = $this->getCallToActionIcon($params);
		$params['button_parameters'] = $this->getButtonParameters($params);
		$params['bg_style'] = $this->getBackgroundStyles($params);

		//Get HTML from template
		$html = rebellion_edge_get_shortcode_module_template_part('templates/call-to-action-template', 'calltoaction', '', $params);

		return $html;

	}

	/**
	 * Return Classes for Call To Action text wrapper
	 *
	 * @param $params
	 * @return string
	 */
	private function getTextWrapperClasses($params) {
		return ( $params['show_button'] == 'yes') ? 'edgtf-call-to-action-column1 edgtf-call-to-action-cell' : '';
	}

	/**
	 * Return CSS styles for Call To Action Icon
	 *
	 * @param $params
	 * @return string
	 */
	private function getBackgroundStyles($params) {
		$bg_style = array();

		if ($params['bg_color'] !== '') {
			$bg_style[] = 'background-color: ' . $params['bg_color'];
		}

		return implode(';', $bg_style);
	}

	/**
	 * Return CSS styles for Call To Action Icon
	 *
	 * @param $params
	 * @return string
	 */
	private function getIconStyles($params) {
		$icon_style = array();

		if ($params['icon_size'] !== '') {
			$icon_style[] = 'font-size: ' . $params['icon_size'] . 'px';
		}

		return implode(';', $icon_style);
	}

	/**
	 * Return CSS styles for Call To Action Content
	 *
	 * @param $params
	 * @return string
	 */
	private function getContentStyles($params) {
		$content_styles = array();

		if ($params['text_size'] !== '') {
			$content_styles[] = 'font-size: ' . $params['text_size'] . 'px';
		}

		return implode(';', $content_styles);
	}

	/**
	 * Return CSS styles for Call To Action shortcode
	 *
	 * @param $params
	 * @return string
	 */
	private function getCallToActionStyles($params) {
		$call_to_action_styles = array();

		if ($params['box_padding'] != '') {
			$call_to_action_styles[] = 'padding: ' . $params['box_padding'] . ';';
		}

		return implode(';', $call_to_action_styles);
	}

	/**
	 * Return Icon for Call To Action Shortcode
	 *
	 * @param $params
	 * @return mixed
	 */
	private function getCallToActionIcon($params) {

		$icon = rebellion_edge_icon_collections()->getIconCollectionParamNameByKey($params['icon_pack']);
		$iconStyles = array();
		$iconStyles['icon_attributes']['style'] = $this->getIconStyles($params);
		$call_to_action_icon = '';
		if(!empty($params[$icon])){			
			$call_to_action_icon = rebellion_edge_icon_collections()->renderIcon( $params[$icon], $params['icon_pack'], $iconStyles );
		}
		return $call_to_action_icon;

	}
	
	private function getButtonParameters($params) {
		$button_params_array = array();
		
		if(!empty($params['button_link'])) {
			$button_params_array['link'] = $params['button_link'];
		}
		
		if(!empty($params['button_size'])) {
			$button_params_array['size'] = $params['button_size'];
		}

		if(!empty($params['button_skin'])) {
			$button_params_array['button_skin'] = $params['button_skin'];
		}
		
		if(!empty($params['button_icon_pack'])) {
			$button_params_array['icon_pack'] = $params['button_icon_pack'];
			$iconPackName = rebellion_edge_icon_collections()->getIconCollectionParamNameByKey($params['button_icon_pack']);
			$button_params_array[$iconPackName] = $params['button_'.$iconPackName];		
		}
				
		if(!empty($params['button_target'])) {
			$button_params_array['target'] = $params['button_target'];
		}
		
		if(!empty($params['button_text'])) {
			$button_params_array['text'] = $params['button_text'];
		}
		
		return $button_params_array;
	}
}