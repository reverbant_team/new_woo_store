<?php

if ( ! function_exists('rebellion_edge_call_to_action_options_map') ) {
	/**
	 * Add Call to Action options to elements page
	 */
	function rebellion_edge_call_to_action_options_map() {

		$panel_call_to_action = rebellion_edge_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_call_to_action',
				'title' => esc_html__('Call To Action', 'rebellion')
			)
		);

	}

	add_action( 'rebellion_edge_options_elements_map', 'rebellion_edge_call_to_action_options_map');

}