<?php

if ( ! function_exists('rebellion_edge_pricing_table_options_map') ) {
	/**
	 * Add Pricing Table options to elements page
	 */
	function rebellion_edge_pricing_table_options_map() {

		$panel_pricing_table = rebellion_edge_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_pricing_table',
				'title' => esc_html__('Pricing Table', 'rebellion')
			)
		);

	}

	add_action( 'rebellion_edge_options_elements_map', 'rebellion_edge_pricing_table_options_map');

}