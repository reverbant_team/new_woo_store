<?php
namespace RebellionEdge\Modules\Shortcodes\PricingTable;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

class PricingTable implements ShortcodeInterface{
	private $base;
	function __construct() {
		$this->base = 'edgtf_pricing_table';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map( array(
			'name' => esc_html__('Pricing Table', 'rebellion'),
			'base' => $this->base,
			'icon' => 'icon-wpb-pricing-table extended-custom-icon',
			'category' => esc_html__('by EDGE', 'rebellion'),
			'allowed_container_element' => 'vc_row',
			'as_child' => array('only' => 'edgtf_pricing_tables'),
			'params' => array(
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Title', 'rebellion'),
					'param_name' => 'title',
					'value' => esc_html__('Basic Plan', 'rebellion'),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Price', 'rebellion'),
					'param_name' => 'price',
					'description' => esc_html__('Default value is 100', 'rebellion')
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Currency', 'rebellion'),
					'param_name' => 'currency',
					'description' => esc_html__('Default mark is $', 'rebellion')
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Price Period', 'rebellion'),
					'param_name' => 'price_period',
					'description' => esc_html__('Default label is "per month"', 'rebellion')
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => esc_html__('Show Button', 'rebellion'),
					'param_name' => 'show_button',
					'value' => array(
                        esc_html__('Default', 'rebellion') => '',
                        esc_html__('Yes', 'rebellion') => 'yes',
                        esc_html__('No', 'rebellion') => 'no'
					),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Button Text', 'rebellion'),
					'param_name' => 'button_text',
					'dependency' => array('element' => 'show_button',  'value' => 'yes') 
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Button Link', 'rebellion'),
					'param_name' => 'link',
					'dependency' => array('element' => 'show_button',  'value' => 'yes')
				),
				array(
					'type' => 'dropdown',
					'admin_label' => true,
					'heading' => esc_html__('Active', 'rebellion'),
					'param_name' => 'active',
					'value' => array(
                        esc_html__('No', 'rebellion') => 'no',
                        esc_html__('Yes', 'rebellion') => 'yes'
					),
					'save_always' => true,
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'admin_label' => true,
					'heading' => esc_html__('Active text', 'rebellion'),
					'param_name' => 'active_text',
					'description' => esc_html__('Best choice', 'rebellion'),
					'dependency' => array('element' => 'active', 'value' => 'yes')
				),
				array(
					'type' => 'textarea_html',
					'holder' => 'div',
					'class' => '',
					'heading' => esc_html__('Content', 'rebellion'),
					'param_name' => 'content',
					'value' => '<li>content content content</li><li>content content content</li><li>content content content</li>',
					'description' => ''
				)
			)
		));
	}

	public function render($atts, $content = null) {
	
		$args = array(
			'title'         			   => esc_html__('Basic Plan', 'rebellion'),
			'price'         			   => '100',
			'currency'      			   => '$',
			'price_period'  			   => esc_html__('Per Month', 'rebellion'),
			'active'        			   => 'no',
			'active_text'   			   => esc_html__('Best choice', 'rebellion'),
			'show_button'				   => 'yes',
			'link'          			   => '',
			'button_text'   			   => 'button'
		);
		$params = shortcode_atts($args, $atts);
		extract($params);

		$html						= '';
		$pricing_table_clasess		= 'edgtf-price-table';
		
		if($active == 'yes') {
			$pricing_table_clasess .= ' edgtf-active';
		}
		
		$params['pricing_table_classes'] = $pricing_table_clasess;
        $params['content'] = preg_replace('#^<\/p>|<p>$#', '', $content);
		
		$html .= rebellion_edge_get_shortcode_module_template_part('templates/pricing-table-template','pricing-table', '', $params);
		return $html;

	}

}
