<?php

if ( ! function_exists('rebellion_edge_team_options_map') ) {
	/**
	 * Add Team options to elements page
	 */
	function rebellion_edge_team_options_map() {

		$panel_team = rebellion_edge_add_admin_panel(
			array(
				'page' => '_elements_page',
				'name' => 'panel_team',
				'title' => esc_html__('Team', 'rebellion')
			)
		);

	}

	add_action( 'rebellion_edge_options_elements_map', 'rebellion_edge_team_options_map');

}