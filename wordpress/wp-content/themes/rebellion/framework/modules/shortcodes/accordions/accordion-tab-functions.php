<?php

if(!function_exists('rebellion_edge_get_accordion_tab_html')) {
    /**
     * Calls accordion tab shortcode with given parameters and returns it's output
     * @param $params
     *
     * @return mixed|string
     */
    function rebellion_edge_get_accordion_tab_html($params) {
        $accordion_tab_html = rebellion_edge_execute_shortcode('edgtf_accordion_tab', $params);
        $accordion_tab_html = str_replace("\n", '', $accordion_tab_html);
        return $accordion_tab_html;
    }
}