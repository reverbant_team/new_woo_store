<?php 

if(!function_exists('rebellion_edge_accordions_typography_styles')){
	function rebellion_edge_accordions_typography_styles(){
		$selector = '.edgtf-accordion-holder .edgtf-title-holder';
		$styles = array();
		
		$font_family = rebellion_edge_options()->getOptionValue('accordions_font_family');
		if(rebellion_edge_is_font_option_valid($font_family)){
			$styles['font-family'] = rebellion_edge_get_font_option_val($font_family);
		}
		
		$text_transform = rebellion_edge_options()->getOptionValue('accordions_text_transform');
       if(!empty($text_transform)) {
           $styles['text-transform'] = $text_transform;
       }

       $font_style = rebellion_edge_options()->getOptionValue('accordions_font_style');
       if(!empty($font_style)) {
           $styles['font-style'] = $font_style;
       }

       $letter_spacing = rebellion_edge_options()->getOptionValue('accordions_letter_spacing');
       if($letter_spacing !== '') {
           $styles['letter-spacing'] = rebellion_edge_filter_px($letter_spacing).'px';
       }

       $font_weight = rebellion_edge_options()->getOptionValue('accordions_font_weight');
       if(!empty($font_weight)) {
           $styles['font-weight'] = $font_weight;
       }

       echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_accordions_typography_styles');
}

if(!function_exists('rebellion_edge_accordions_inital_title_color_styles')){
	function rebellion_edge_accordions_inital_title_color_styles(){
		$selector = '.edgtf-accordion-holder.edgtf-initial .edgtf-title-holder';
		$styles = array();
		
		if(rebellion_edge_options()->getOptionValue('accordions_title_color')) {
           $styles['color'] = rebellion_edge_options()->getOptionValue('accordions_title_color');
       }
		echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_accordions_inital_title_color_styles');
}

if(!function_exists('rebellion_edge_accordions_active_title_color_styles')){
	
	function rebellion_edge_accordions_active_title_color_styles(){
		$selector = array(
			'.edgtf-accordion-holder.edgtf-initial .edgtf-title-holder.ui-state-active',
			'.edgtf-accordion-holder.edgtf-initial .edgtf-title-holder.ui-state-hover'
		);
		$styles = array();
		
		if(rebellion_edge_options()->getOptionValue('accordions_title_color_active')) {
           $styles['color'] = rebellion_edge_options()->getOptionValue('accordions_title_color_active');
       }
		
		echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_accordions_active_title_color_styles');
}
if(!function_exists('rebellion_edge_accordions_inital_icon_color_styles')){
	
	function rebellion_edge_accordions_inital_icon_color_styles(){
		$selector = '.edgtf-accordion-holder.edgtf-initial .edgtf-title-holder .edgtf-accordion-mark';
		$styles = array();
		
		if(rebellion_edge_options()->getOptionValue('accordions_icon_color')) {
           $styles['color'] = rebellion_edge_options()->getOptionValue('accordions_icon_color');
       }
		if(rebellion_edge_options()->getOptionValue('accordions_icon_back_color')) {
           $styles['background-color'] = rebellion_edge_options()->getOptionValue('accordions_icon_back_color');
       }
		echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_accordions_inital_icon_color_styles');
}
if(!function_exists('rebellion_edge_accordions_active_icon_color_styles')){
	
	function rebellion_edge_accordions_active_icon_color_styles(){
		$selector = array(
			'.edgtf-accordion-holder.edgtf-initial .edgtf-title-holder.ui-state-active  .edgtf-accordion-mark',
			'.edgtf-accordion-holder.edgtf-initial .edgtf-title-holder.ui-state-hover  .edgtf-accordion-mark'
		);
		$styles = array();
		
		if(rebellion_edge_options()->getOptionValue('accordions_icon_color_active')) {
           $styles['color'] = rebellion_edge_options()->getOptionValue('accordions_icon_color_active');
       }
		if(rebellion_edge_options()->getOptionValue('accordions_icon_back_color_active')) {
           $styles['background-color'] = rebellion_edge_options()->getOptionValue('accordions_icon_back_color_active');
       }
		echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_accordions_active_icon_color_styles');
}

if(!function_exists('rebellion_edge_boxed_accordions_inital_color_styles')){
	function rebellion_edge_boxed_accordions_inital_color_styles(){
		$selector = '.edgtf-accordion-holder.edgtf-boxed .edgtf-title-holder';
		$styles = array();
		
		if(rebellion_edge_options()->getOptionValue('boxed_accordions_color')) {
           $styles['color'] = rebellion_edge_options()->getOptionValue('boxed_accordions_color');
           echo rebellion_edge_dynamic_css('.edgtf-accordion-holder.edgtf-boxed .edgtf-title-holder .edgtf-accordion-mark', array('color' => rebellion_edge_options()->getOptionValue('boxed_accordions_color')));
       }
		if(rebellion_edge_options()->getOptionValue('boxed_accordions_back_color')) {
           $styles['background-color'] = rebellion_edge_options()->getOptionValue('boxed_accordions_back_color');
       }
		if(rebellion_edge_options()->getOptionValue('boxed_accordions_border_color')) {
           $styles['border-color'] = rebellion_edge_options()->getOptionValue('boxed_accordions_border_color');
       }
		
		echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_boxed_accordions_inital_color_styles');
}
if(!function_exists('rebellion_edge_boxed_accordions_active_color_styles')){

	function rebellion_edge_boxed_accordions_active_color_styles(){
		$selector = array(
			'.edgtf-accordion-holder.edgtf-boxed.ui-accordion .edgtf-title-holder.ui-state-active',
			'.edgtf-accordion-holder.edgtf-boxed.ui-accordion .edgtf-title-holder.ui-state-hover'
		);
		$selector_icons = array(
			'.edgtf-accordion-holder.edgtf-boxed .edgtf-title-holder.ui-state-active .edgtf-accordion-mark',
			'.edgtf-accordion-holder.edgtf-boxed .edgtf-title-holder.ui-state-hover .edgtf-accordion-mark'
		);
		$styles = array();
		
		if(rebellion_edge_options()->getOptionValue('boxed_accordions_color_active')) {
           $styles['color'] = rebellion_edge_options()->getOptionValue('boxed_accordions_color_active');
           echo rebellion_edge_dynamic_css($selector_icons, array('color' => rebellion_edge_options()->getOptionValue('boxed_accordions_color_active')));
       }
		if(rebellion_edge_options()->getOptionValue('boxed_accordions_back_color_active')) {
           $styles['background-color'] = rebellion_edge_options()->getOptionValue('boxed_accordions_back_color_active');
       }
		if(rebellion_edge_options()->getOptionValue('boxed_accordions_border_color_active')) {
           $styles['border-color'] = rebellion_edge_options()->getOptionValue('boxed_accordions_border_color_active');
       }
		
		echo rebellion_edge_dynamic_css($selector, $styles);
	}
	add_action('rebellion_edge_style_dynamic', 'rebellion_edge_boxed_accordions_active_color_styles');
}