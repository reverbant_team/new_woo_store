<<?php echo esc_attr($title_tag)?> class="clearfix edgtf-title-holder">
	<span class="edgtf-accordion-mark edgtf-left-mark">
		<span class="edgtf-accordion-mark-icon">
			<span class="arrow_carrot-up"></span>
			<span class="arrow_carrot-down"></span>
		</span>
	</span>
	<span class="edgtf-tab-title">
		<span class="edgtf-tab-title-inner">
			<?php echo esc_attr($title)?>
		</span>
	</span>
</<?php echo esc_attr($title_tag)?>>
<div class="edgtf-accordion-content">
	<div class="edgtf-accordion-content-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>