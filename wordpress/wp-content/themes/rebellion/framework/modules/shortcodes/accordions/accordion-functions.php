<?php

if(!function_exists('rebellion_edge_get_accordion_html')) {
    /**
     * Calls accordion shortcode with given parameters and returns it's output
     * @param $params
     *
     * @return mixed|string
     */
    function rebellion_edge_get_accordion_html($params) {
        $accordion_html = rebellion_edge_execute_shortcode('edgtf_accordion', $params);
        $accordion_html = str_replace("\n", '', $accordion_html);
        return $accordion_html;
    }
}