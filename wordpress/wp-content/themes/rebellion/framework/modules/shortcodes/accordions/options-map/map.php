<?php 
if(!function_exists('rebellion_edge_accordions_map')) {
    /**
     * Add Accordion options to elements panel
     */
   function rebellion_edge_accordions_map() {
		
       $panel = rebellion_edge_add_admin_panel(array(
           'title' => esc_html__('Accordions','rebellion'),
           'name'  => 'panel_accordions',
           'page'  => '_elements_page'
       ));

       //Typography options
       rebellion_edge_add_admin_section_title(array(
           'name' => 'typography_section_title',
           'title' => esc_html__('Typography','rebellion'),			
           'parent' => $panel
       ));

       $typography_group = rebellion_edge_add_admin_group(array(
           'name' => 'typography_group',
           'title' => esc_html__('Typography','rebellion'),
			     'description' => esc_html__('Setup typography for accordions navigation','rebellion'),
           'parent' => $panel
       ));

       $typography_row = rebellion_edge_add_admin_row(array(
           'name' => 'typography_row',
           'next' => true,
           'parent' => $typography_group
       ));

       rebellion_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'fontsimple',
           'name'          => 'accordions_font_family',
           'default_value' => '',
           'label'         => esc_html__('Font Family','rebellion'),
       ));

       rebellion_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'selectsimple',
           'name'          => 'accordions_text_transform',
           'default_value' => '',
           'label'         => esc_html__('Text Transform','rebellion'),
           'options'       => rebellion_edge_get_text_transform_array()
       ));

       rebellion_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'selectsimple',
           'name'          => 'accordions_font_style',
           'default_value' => '',
           'label'         => esc_html__('Font Style','rebellion'),
           'options'       => rebellion_edge_get_font_style_array()
       ));

       rebellion_edge_add_admin_field(array(
           'parent'        => $typography_row,
           'type'          => 'textsimple',
           'name'          => 'accordions_letter_spacing',
           'default_value' => '',
           'label'         => esc_html__('Letter Spacing','rebellion'),
           'args'          => array(
               'suffix' => 'px'
           )
       ));

       $typography_row2 = rebellion_edge_add_admin_row(array(
           'name' => 'typography_row2',
           'next' => true,
           'parent' => $typography_group
       ));		
		
       rebellion_edge_add_admin_field(array(
           'parent'        => $typography_row2,
           'type'          => 'selectsimple',
           'name'          => 'accordions_font_weight',
           'default_value' => '',
           'label'         => esc_html__('Font Weight','rebellion'),
           'options'       => rebellion_edge_get_font_weight_array()
       ));
		
		//Initial Accordion Color Styles
		
		rebellion_edge_add_admin_section_title(array(
           'name' => 'accordion_color_section_title',
           'title' => esc_html__('Basic Accordions Color Styles','rebellion'),			
           'parent' => $panel
       ));
		
		$accordions_color_group = rebellion_edge_add_admin_group(array(
           'name' => 'accordions_color_group',
           'title' => esc_html__('Accordion Color Styles','rebellion'),
			'description' => esc_html__('Set color styles for accordion title','rebellion'),
           'parent' => $panel
       ));
		$accordions_color_row = rebellion_edge_add_admin_row(array(
           'name' => 'accordions_color_row',
           'next' => true,
           'parent' => $accordions_color_group
       ));
		rebellion_edge_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_title_color',
           'default_value' => '',
           'label'         => esc_html__('Title Color','rebellion')
       ));
		rebellion_edge_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_icon_color',
           'default_value' => '',
           'label'         => esc_html__('Icon Color','rebellion')
       ));
		rebellion_edge_add_admin_field(array(
           'parent'        => $accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_icon_back_color',
           'default_value' => '',
           'label'         => esc_html__('Icon Background Color','rebellion')
       ));
		
		$active_accordions_color_group = rebellion_edge_add_admin_group(array(
           'name' => 'active_accordions_color_group',
           'title' => esc_html__('Active and Hover Accordion Color Styles','rebellion'),
			     'description' => esc_html__('Set color styles for active and hover accordions','rebellion'),
           'parent' => $panel
       ));
		$active_accordions_color_row = rebellion_edge_add_admin_row(array(
           'name' => 'active_accordions_color_row',
           'next' => true,
           'parent' => $active_accordions_color_group
       ));
		rebellion_edge_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_title_color_active',
           'default_value' => '',
           'label'         => esc_html__('Title Color','rebellion')
       ));
		rebellion_edge_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_icon_color_active',
           'default_value' => '',
           'label'         => esc_html__('Icon Color','rebellion')
       ));
		rebellion_edge_add_admin_field(array(
           'parent'        => $active_accordions_color_row,
           'type'          => 'colorsimple',
           'name'          => 'accordions_icon_back_color_active',
           'default_value' => '',
           'label'         => esc_html__('Icon Background Color','rebellion')
       ));
       
   }
   add_action('rebellion_edge_options_elements_map', 'rebellion_edge_accordions_map');
}