<div class="edgtf-frame-carousel">
	<div class="edgtf-frame-carousel-carousel" <?php echo rebellion_edge_get_inline_attrs($carousel_data); ?>>
		<?php foreach ($images as $image) { ?>
		<div class="edgtf-frame-carousel-images-holder">
			<div class="edgtf-frame-carousel-image-holder">
				<?php echo rebellion_edge_generate_thumbnail($image['carousel_image']['id'], null, 485, 275); ?>
			</div>
			<div class="edgtf-frame-carousel-active-image-holder">
				<?php echo rebellion_edge_generate_thumbnail($image['active_image']['id'], null, 640, 400); ?>
			</div>
		</div>
		<?php } ?>

	</div>
	<div class="edgtf-frame-carousel-monitor-holder">
		<?php if ($skin == 'light') { ?> 
			<img alt="" src="<?php echo EDGE_ASSETS_ROOT; ?>/img/monitor-light.png" class="edgtf-frame-carousel-monitor">
		<?php } else {?>
			<img alt="" src="<?php echo EDGE_ASSETS_ROOT; ?>/img/monitor.png" class="edgtf-frame-carousel-monitor">
		<?php } ?>
		<div class="edgtf-frame-carousel-monitor-image-holder">
			<img alt="" src="<?php echo EDGE_ASSETS_ROOT; ?>/img/frame-monitor-image.png" class="edgtf-frame-carousel-active-image">
		</div>
	</div>
</div>