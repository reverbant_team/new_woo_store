<?php
namespace RebellionEdge\Modules\Shortcodes\UnorderedList;

use RebellionEdge\Modules\Shortcodes\Lib\ShortcodeInterface;

/**
 * Class unordered List
 */
class UnorderedList implements ShortcodeInterface{

	private $base;

	function __construct() {
		$this->base='edgtf_unordered_list';
		
		add_action('vc_before_init', array($this, 'vcMap'));
	}

	/**\
	 * Returns base for shortcode
	 * @return string
	 */
	public function getBase() {
		return $this->base;
	}

	public function vcMap() {

		vc_map( array(
			'name' => esc_html__('List - Unordered', 'rebellion'),
			'base' => $this->base,
			'icon' => 'icon-wpb-unordered-list extended-custom-icon',
			'category' => esc_html__('by EDGE', 'rebellion'),
			'allowed_container_element' => 'vc_row',
			'params' => array(
				array(
					'type'			=> 'dropdown',
					'admin_label'	=> true,
					'heading'		=> esc_html__('Style', 'rebellion'),
					'param_name'	=> 'style',
					'value'			=> array(
                        esc_html__('Circle', 'rebellion') => 'circle',
                        esc_html__('Line', 'rebellion')	 => 'line',
                        esc_html__('Arrow', 'rebellion')  => 'arrow'
					)
				),
				array(
					'type'			=> 'dropdown',
					'admin_label'	=> true,
					'heading'		=> esc_html__('Animate List', 'rebellion'),
					'param_name'	=> 'animate',
					'value'			=> array(
                        esc_html__('No', 'rebellion') => 'no',
                        esc_html__('Yes', 'rebellion') => 'yes'
					)
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__('Font Weight', 'rebellion'),
					'param_name' => 'font_weight',
					'value' => array(
                        esc_html__('Default', 'rebellion')  => '',
                        esc_html__('Light', 'rebellion')    => 'light',
                        esc_html__('Normal', 'rebellion')   => 'normal',
                        esc_html__('Bold', 'rebellion')     => 'bold'
					),
					'description' => ''
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__('Padding left (px)', 'rebellion'),
					'param_name' => 'padding_left',
					'value' => ''
				),
				array(
					'type' => 'textarea_html',
					'heading' => esc_html__('Content', 'rebellion'),
					'param_name' => 'content',
					'value' => '<ul><li>Lorem Ipsum</li><li>Lorem Ipsum</li><li>Lorem Ipsum</li></ul>',
					'description' => ''
				)
			)
		) );
	}

	public function render($atts, $content = null) {
		$args = array(
            'style' => '',
            'animate' => '',
            'font_weight' => '',
            'padding_left' => ''
        );
		$params = shortcode_atts($args, $atts);
		
		//Extract params for use in method
		extract($params);
		
		$list_item_classes = "";

        if ($style != '') {
			$list_item_classes .= ' edgtf-'.$style;         
        }

		if ($animate == 'yes') {
			$list_item_classes .= ' edgtf-animate-list';
		}
		
		$list_style = '';
		if($padding_left != '') {
			$list_style .= 'padding-left: ' . $padding_left .'px;';
		}
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
        $html = '<div class="edgtf-unordered-list '.$list_item_classes.'" '.  rebellion_edge_get_inline_style($list_style).'>'.do_shortcode($content).'</div>';
        return $html;
	}
}