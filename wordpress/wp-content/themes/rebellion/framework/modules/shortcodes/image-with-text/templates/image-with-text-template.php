<?php
/**
 * Image With Text shortcode template
 */
?>

<div <?php rebellion_edge_class_attribute($holder_classes); ?> <?php echo rebellion_edge_get_inline_attrs($holder_data); ?>>
	<?php if ($link !== '') { ?>
		<a href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($link_target) ?>" class="edgtf-iwt-link"></a>
	<?php } ?>
	<div class="edgtf-iwt-content">
		<div class="edgtf-iwt-image">
			<?php echo wp_get_attachment_image($image,'full');?>
		</div>
		<div class="edgtf-iwt-text-holder">
			<div class="edgtf-iwt-text-holder-table">
				<div class="edgtf-iwt-text-holder-cell">
					<h4 class="edgtf-iwt-title" <?php rebellion_edge_inline_style($title_styles); ?>><?php echo wp_kses_post($title) ?></h4>
				</div>
			</div>
		</div>
	</div>
</div>