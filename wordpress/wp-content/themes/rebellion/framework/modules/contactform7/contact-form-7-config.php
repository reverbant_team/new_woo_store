<?php
if ( ! function_exists('rebellion_edge_contact_form_map') ) {
	/**
	 * Map Contact Form 7 shortcode
	 * Hooks on vc_after_init action
	 */
	function rebellion_edge_contact_form_map()
	{

		vc_add_param('contact-form-7', array(
			'type' => 'dropdown',
			'heading' =>  esc_html__('Style', 'rebellion'),
			'param_name' => 'html_class',
			'value' => array(
				esc_html__('Default', 'rebellion') => 'default',
				esc_html__('Custom Style 1', 'rebellion') => 'cf7_custom_style_1',
				esc_html__('Custom Style 2', 'rebellion') => 'cf7_custom_style_2',
				esc_html__('Custom Style 3', 'rebellion') => 'cf7_custom_style_3',
				esc_html__('Custom Style 4', 'rebellion') => 'cf7_custom_style_4',
				esc_html__('Custom Style 5', 'rebellion') => 'cf7_custom_style_5',
				esc_html__('Custom Style 6', 'rebellion') => 'cf7_custom_style_6'
			),
			'description' => esc_html__('You can style each form element individually in Edge Options > Contact Form 7', 'rebellion')
		));

	}
	add_action('vc_after_init', 'rebellion_edge_contact_form_map');
}
?>