<?php

if ( ! function_exists('rebellion_edge_contact_form_7_options_map') ) {

	function rebellion_edge_contact_form_7_options_map() {

		rebellion_edge_add_admin_page(array(
			'slug'	=> '_contact_form7_page',
			'title'	=> esc_html__('Contact Form 7', 'rebellion'),
			'icon'	=> 'fa fa-envelope-o'
		));

		$panel_contact_form_style_1 = rebellion_edge_add_admin_panel(array(
			'page'	=> '_contact_form7_page',
			'name'	=> 'panel_contact_form_style_1',
			'title'	=> esc_html__('Custom Style 1', 'rebellion')
		));

		//Text Typography

		$typography_text_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_text_group',
			'title'			=> esc_html__('Form Text Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements text', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$typography_text_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row1',
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_focus_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_text_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_text_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		$typography_text_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row2',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_1_text_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_text_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_text_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_text_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		$typography_text_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row3',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_text_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Labels Typography

		$typography_label_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_label_group',
			'title'			=> esc_html__('Form Label Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements label', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$typography_label_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row1',
			'parent'	=> $typography_label_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_label_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_label_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_label_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_1_label_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		$typography_label_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row2',
			'next'		=> true,
			'parent'	=> $typography_label_group
		));


		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_label_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_label_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_label_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_label_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Background and Border

		$background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'background_border_group',
			'title'			=> esc_html__('Form Elements Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup form elements background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row1',
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_focus_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_focus_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Transparency', 'rebellion')
		));

		$background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row2',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_focus_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_focus_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Transparency', 'rebellion')
		));

		$background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row3',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'yesnosimple',
			'name'			=> 'cf7_style_1_border_bottom_only',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Border Bottom Only', 'rebellion'),
		));

		// Form Elements Padding

		$padding_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'padding_group',
			'title'			=> esc_html__('Elements Padding', 'rebellion'),
			'description'	=> esc_html__('Setup form elements padding', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$padding_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'padding_row',
			'parent'	=> $padding_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Right', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Left', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Margin

		$margin_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'margin_group',
			'title'			=> esc_html__('Elements Margin', 'rebellion'),
			'description'	=> esc_html__('Setup form elements margin', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$margin_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'margin_row',
			'parent'	=> $margin_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_margin_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_margin_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Textarea

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_1,
			'type'			=> 'text',
			'name'			=> 'cf7_style_1_textarea_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Textarea Height', 'rebellion'),
			'description'	=> esc_html__('Enter height for textarea form element', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Typography

		$button_typography_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_typography_group',
			'title'			=> esc_html__('Button Typography', 'rebellion'),
			'description'	=> esc_html__('Setup button text typography', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$button_typography_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row1',
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_button_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_button_hover_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_1_button_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion')
		));

		$button_typography_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row2',
			'next'		=> true,
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_button_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=> rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_button_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_1_button_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Background and Border

		$button_background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_background_border_group',
			'title'			=> esc_html__('Button Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup button background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_1
		));

		$button_background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row1',
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_button_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_button_hover_bckg_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_hover_bckg_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Transparency', 'rebellion')
		));

		$button_background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row2',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_button_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_1_button_hover_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_hover_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Transparency', 'rebellion')
		));

		$button_background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row3',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_1_button_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Height

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_1,
			'type'			=> 'text',
			'name'			=> 'cf7_style_1_button_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Height', 'rebellion'),
			'description'	=> esc_html__('Insert form button height', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Padding

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_1,
			'type'			=> 'text',
			'name'			=> 'cf7_style_1_button_padding',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Left/Right Padding', 'rebellion'),
			'description'	=> esc_html__('Enter value for button left and right padding', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		$panel_contact_form_style_2 = rebellion_edge_add_admin_panel(array(
			'page'	=> '_contact_form7_page',
			'name'	=> 'panel_contact_form_style_2',
			'title'	=> esc_html__('Custom Style 2', 'rebellion')
		));

		//Text Typography

		$typography_text_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_text_group',
			'title'			=> esc_html__('Form Text Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements text', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$typography_text_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row1',
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_focus_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_text_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_text_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		$typography_text_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row2',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_2_text_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_text_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_text_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_text_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		$typography_text_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row3',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_text_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Labels Typography

		$typography_label_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_label_group',
			'title'			=> esc_html__('Form Label Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements label', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$typography_label_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row1',
			'parent'	=> $typography_label_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_label_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_label_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_label_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_2_label_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		$typography_label_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row2',
			'next'		=> true,
			'parent'	=> $typography_label_group
		));


		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_label_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_label_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_label_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_label_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Background and Border

		$background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'background_border_group',
			'title'			=> esc_html__('Form Elements Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup form elements background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row1',
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_focus_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_focus_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Transparency', 'rebellion')
		));

		$background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row2',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_focus_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_focus_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Transparency', 'rebellion')
		));

		$background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row3',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'yesnosimple',
			'name'			=> 'cf7_style_2_border_bottom_only',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Border Bottom Only', 'rebellion'),
		));

		// Form Elements Padding

		$padding_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'padding_group',
			'title'			=> esc_html__('Elements Padding', 'rebellion'),
			'description'	=> esc_html__('Setup form elements padding', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$padding_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'padding_row',
			'parent'	=> $padding_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Right', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Left', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Margin

		$margin_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'margin_group',
			'title'			=> esc_html__('Elements Margin', 'rebellion'),
			'description'	=> esc_html__('Setup form elements margin', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$margin_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'margin_row',
			'parent'	=> $margin_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_margin_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_margin_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Textarea

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_2,
			'type'			=> 'text',
			'name'			=> 'cf7_style_2_textarea_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Textarea Height', 'rebellion'),
			'description'	=> esc_html__('Enter height for textarea form element', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Typography

		$button_typography_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_typography_group',
			'title'			=> esc_html__('Button Typography', 'rebellion'),
			'description'	=> esc_html__('Setup button text typography', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$button_typography_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row1',
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_button_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_button_hover_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_2_button_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion')
		));

		$button_typography_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row2',
			'next'		=> true,
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_button_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=> rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_button_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_2_button_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Background and Border

		$button_background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_background_border_group',
			'title'			=> esc_html__('Button Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup button background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_2
		));

		$button_background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row1',
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_button_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_button_hover_bckg_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_hover_bckg_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Transparency', 'rebellion')
		));

		$button_background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row2',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_button_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_2_button_hover_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_hover_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Transparency', 'rebellion')
		));

		$button_background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row3',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_2_button_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Height

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_2,
			'type'			=> 'text',
			'name'			=> 'cf7_style_2_button_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Height', 'rebellion'),
			'description'	=> esc_html__('Insert form button height', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Padding

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_2,
			'type'			=> 'text',
			'name'			=> 'cf7_style_2_button_padding',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Left/Right Padding', 'rebellion'),
			'description'	=> esc_html__('Enter value for button left and right padding', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		$panel_contact_form_style_3 = rebellion_edge_add_admin_panel(array(
			'page'	=> '_contact_form7_page',
			'name'	=> 'panel_contact_form_style_3',
			'title'	=> esc_html__('Custom Style 3', 'rebellion')
		));

		//Text Typography

		$typography_text_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_text_group',
			'title'			=> esc_html__('Form Text Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements text', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$typography_text_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row1',
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_focus_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_text_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_text_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		$typography_text_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row2',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_3_text_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_text_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_text_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_text_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		$typography_text_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row3',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_text_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Labels Typography

		$typography_label_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_label_group',
			'title'			=> esc_html__('Form Label Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements label', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$typography_label_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row1',
			'parent'	=> $typography_label_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_label_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_label_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_label_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_3_label_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		$typography_label_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row2',
			'next'		=> true,
			'parent'	=> $typography_label_group
		));


		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_label_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_label_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_label_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_label_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Background and Border

		$background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'background_border_group',
			'title'			=> esc_html__('Form Elements Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup form elements background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row1',
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_focus_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_focus_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Transparency', 'rebellion')
		));

		$background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row2',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_focus_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_focus_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Transparency', 'rebellion')
		));

		$background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row3',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'yesnosimple',
			'name'			=> 'cf7_style_3_border_bottom_only',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Border Bottom Only', 'rebellion'),
		));

		// Form Elements Padding

		$padding_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'padding_group',
			'title'			=> esc_html__('Elements Padding', 'rebellion'),
			'description'	=> esc_html__('Setup form elements padding', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$padding_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'padding_row',
			'parent'	=> $padding_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Right', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Left', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Margin

		$margin_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'margin_group',
			'title'			=> esc_html__('Elements Margin', 'rebellion'),
			'description'	=> esc_html__('Setup form elements margin', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$margin_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'margin_row',
			'parent'	=> $margin_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_margin_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_margin_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Textarea

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_3,
			'type'			=> 'text',
			'name'			=> 'cf7_style_3_textarea_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Textarea Height', 'rebellion'),
			'description'	=> esc_html__('Enter height for textarea form element', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Typography

		$button_typography_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_typography_group',
			'title'			=> esc_html__('Button Typography', 'rebellion'),
			'description'	=> esc_html__('Setup button text typography', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$button_typography_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row1',
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_button_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_button_hover_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_3_button_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion')
		));

		$button_typography_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row2',
			'next'		=> true,
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_button_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=> rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_button_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_3_button_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Background and Border

		$button_background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_background_border_group',
			'title'			=> esc_html__('Button Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup button background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_3
		));

		$button_background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row1',
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_button_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_button_hover_bckg_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_hover_bckg_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Transparency', 'rebellion')
		));

		$button_background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row2',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_button_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_3_button_hover_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_hover_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Transparency', 'rebellion')
		));

		$button_background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row3',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_3_button_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Height

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_3,
			'type'			=> 'text',
			'name'			=> 'cf7_style_3_button_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Height', 'rebellion'),
			'description'	=> esc_html__('Insert form button height', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Padding

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_3,
			'type'			=> 'text',
			'name'			=> 'cf7_style_3_button_padding',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Left/Right Padding', 'rebellion'),
			'description'	=> esc_html__('Enter value for button left and right padding', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		$panel_contact_form_style_4 = rebellion_edge_add_admin_panel(array(
			'page'	=> '_contact_form7_page',
			'name'	=> 'panel_contact_form_style_4',
			'title'	=> esc_html__('Custom Style 4', 'rebellion')
		));

		//Text Typography

		$typography_text_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_text_group',
			'title'			=> esc_html__('Form Text Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements text', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$typography_text_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row1',
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_focus_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_text_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_text_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		$typography_text_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row2',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_4_text_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_text_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_text_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_text_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		$typography_text_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row3',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_text_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Labels Typography

		$typography_label_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_label_group',
			'title'			=> esc_html__('Form Label Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements label', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$typography_label_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row1',
			'parent'	=> $typography_label_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_label_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_label_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_label_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_4_label_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		$typography_label_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row2',
			'next'		=> true,
			'parent'	=> $typography_label_group
		));


		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_label_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_label_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_label_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_label_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Background and Border

		$background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'background_border_group',
			'title'			=> esc_html__('Form Elements Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup form elements background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row1',
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_focus_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_focus_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Transparency', 'rebellion')
		));

		$background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row2',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_focus_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_focus_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Transparency', 'rebellion')
		));

		$background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row3',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'yesnosimple',
			'name'			=> 'cf7_style_4_border_bottom_only',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Border Bottom Only', 'rebellion'),
		));

		// Form Elements Padding

		$padding_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'padding_group',
			'title'			=> esc_html__('Elements Padding', 'rebellion'),
			'description'	=> esc_html__('Setup form elements padding', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$padding_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'padding_row',
			'parent'	=> $padding_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Right', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Left', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Margin

		$margin_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'margin_group',
			'title'			=> esc_html__('Elements Margin', 'rebellion'),
			'description'	=> esc_html__('Setup form elements margin', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$margin_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'margin_row',
			'parent'	=> $margin_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_margin_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_margin_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Textarea

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_4,
			'type'			=> 'text',
			'name'			=> 'cf7_style_4_textarea_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Textarea Height', 'rebellion'),
			'description'	=> esc_html__('Enter height for textarea form element', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Typography

		$button_typography_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_typography_group',
			'title'			=> esc_html__('Button Typography', 'rebellion'),
			'description'	=> esc_html__('Setup button text typography', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$button_typography_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row1',
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_button_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_button_hover_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_4_button_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion')
		));

		$button_typography_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row2',
			'next'		=> true,
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_button_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=> rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_button_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_4_button_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Background and Border

		$button_background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_background_border_group',
			'title'			=> esc_html__('Button Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup button background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_4
		));

		$button_background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row1',
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_button_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_button_hover_bckg_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_hover_bckg_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Transparency', 'rebellion')
		));

		$button_background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row2',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_button_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_4_button_hover_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_hover_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Transparency', 'rebellion')
		));

		$button_background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row3',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_4_button_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Height

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_4,
			'type'			=> 'text',
			'name'			=> 'cf7_style_4_button_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Height', 'rebellion'),
			'description'	=> esc_html__('Insert form button height', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Padding

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_4,
			'type'			=> 'text',
			'name'			=> 'cf7_style_4_button_padding',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Left/Right Padding', 'rebellion'),
			'description'	=> esc_html__('Enter value for button left and right padding', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));


		/*** Custom Style 5  ***/


		$panel_contact_form_style_5 = rebellion_edge_add_admin_panel(array(
			'page'	=> '_contact_form7_page',
			'name'	=> 'panel_contact_form_style_5',
			'title'	=> esc_html__('Custom Style 5', 'rebellion')
		));

		//Text Typography

		$typography_text_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_text_group',
			'title'			=> esc_html__('Form Text Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements text', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$typography_text_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row1',
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_focus_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_text_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_text_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		$typography_text_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row2',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_5_text_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_text_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_text_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_text_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		$typography_text_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row3',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_text_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Labels Typography

		$typography_label_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_label_group',
			'title'			=> esc_html__('Form Label Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements label', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$typography_label_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row1',
			'parent'	=> $typography_label_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_label_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_label_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_label_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_5_label_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		$typography_label_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row2',
			'next'		=> true,
			'parent'	=> $typography_label_group
		));


		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_label_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_label_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_label_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_label_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Background and Border

		$background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'background_border_group',
			'title'			=> esc_html__('Form Elements Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup form elements background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row1',
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_focus_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_focus_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Transparency', 'rebellion')
		));

		$background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row2',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_focus_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_focus_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Transparency', 'rebellion')
		));

		$background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row3',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'yesnosimple',
			'name'			=> 'cf7_style_5_border_bottom_only',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Border Bottom Only', 'rebellion'),
		));

		// Form Elements Padding

		$padding_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'padding_group',
			'title'			=> esc_html__('Elements Padding', 'rebellion'),
			'description'	=> esc_html__('Setup form elements padding', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$padding_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'padding_row',
			'parent'	=> $padding_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Right', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Left', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Margin

		$margin_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'margin_group',
			'title'			=> esc_html__('Elements Margin', 'rebellion'),
			'description'	=> esc_html__('Setup form elements margin', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$margin_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'margin_row',
			'parent'	=> $margin_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_margin_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_margin_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Textarea

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_5,
			'type'			=> 'text',
			'name'			=> 'cf7_style_5_textarea_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Textarea Height', 'rebellion'),
			'description'	=> esc_html__('Enter height for textarea form element', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Typography

		$button_typography_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_typography_group',
			'title'			=> esc_html__('Button Typography', 'rebellion'),
			'description'	=> esc_html__('Setup button text typography', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$button_typography_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row1',
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_button_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_button_hover_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_5_button_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion')
		));

		$button_typography_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row2',
			'next'		=> true,
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_button_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=> rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_button_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_5_button_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Background and Border

		$button_background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_background_border_group',
			'title'			=> esc_html__('Button Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup button background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_5
		));

		$button_background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row1',
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_button_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_button_hover_bckg_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_hover_bckg_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Transparency', 'rebellion')
		));

		$button_background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row2',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_button_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_5_button_hover_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_hover_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Transparency', 'rebellion')
		));

		$button_background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row3',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_5_button_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Height

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_5,
			'type'			=> 'text',
			'name'			=> 'cf7_style_5_button_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Height', 'rebellion'),
			'description'	=> esc_html__('Insert form button height', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Padding

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_5,
			'type'			=> 'text',
			'name'			=> 'cf7_style_5_button_padding',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Left/Right Padding', 'rebellion'),
			'description'	=> esc_html__('Enter value for button left and right padding', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		/*** Custom Style 6 ***/

		$panel_contact_form_style_6 = rebellion_edge_add_admin_panel(array(
			'page'	=> '_contact_form7_page',
			'name'	=> 'panel_contact_form_style_6',
			'title'	=> esc_html__('Custom Style 6', 'rebellion')
		));

		//Text Typography

		$typography_text_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_text_group',
			'title'			=> esc_html__('Form Text Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements text', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$typography_text_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row1',
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_focus_text_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_text_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_text_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		$typography_text_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row2',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_6_text_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_text_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_text_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_text_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		$typography_text_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_text_row3',
			'next'		=> true,
			'parent'	=> $typography_text_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_text_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_text_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Labels Typography

		$typography_label_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'typography_label_group',
			'title'			=> esc_html__('Form Label Typography', 'rebellion'),
			'description'	=> esc_html__('Setup typography for form elements label', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$typography_label_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row1',
			'parent'	=> $typography_label_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_label_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_label_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_label_line_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Line Height', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_6_label_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion'),
		));

		$typography_label_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'typography_label_row2',
			'next'		=> true,
			'parent'	=> $typography_label_group
		));


		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_label_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=>  rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_label_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_label_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $typography_label_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_label_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Background and Border

		$background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'background_border_group',
			'title'			=> esc_html__('Form Elements Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup form elements background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row1',
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_focus_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_focus_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Background Transparency', 'rebellion')
		));

		$background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row2',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_focus_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_focus_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Focus Border Transparency', 'rebellion')
		));

		$background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'background_border_row3',
			'next'		=> true,
			'parent'	=> $background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $background_border_row3,
			'type'			=> 'yesnosimple',
			'name'			=> 'cf7_style_6_border_bottom_only',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Border Bottom Only', 'rebellion'),
		));

		// Form Elements Padding

		$padding_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'padding_group',
			'title'			=> esc_html__('Elements Padding', 'rebellion'),
			'description'	=> esc_html__('Setup form elements padding', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$padding_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'padding_row',
			'parent'	=> $padding_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Right', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $padding_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Padding Left', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Form Elements Margin

		$margin_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'margin_group',
			'title'			=> esc_html__('Elements Margin', 'rebellion'),
			'description'	=> esc_html__('Setup form elements margin', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$margin_row = rebellion_edge_add_admin_row(array(
			'name'		=> 'margin_row',
			'parent'	=> $margin_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_margin_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Top', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $margin_row,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_margin_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Margin Bottom', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Textarea

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_6,
			'type'			=> 'text',
			'name'			=> 'cf7_style_6_textarea_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Textarea Height', 'rebellion'),
			'description'	=> esc_html__('Enter height for textarea form element', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Typography

		$button_typography_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_typography_group',
			'title'			=> esc_html__('Button Typography', 'rebellion'),
			'description'	=> esc_html__('Setup button text typography', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$button_typography_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row1',
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_button_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_button_hover_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_font_size',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Size', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row1,
			'type'			=> 'fontsimple',
			'name'			=> 'cf7_style_6_button_font_family',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Family', 'rebellion')
		));

		$button_typography_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_typography_row2',
			'next'		=> true,
			'parent'	=> $button_typography_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_button_font_style',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Style', 'rebellion'),
			'options'		=> rebellion_edge_get_font_style_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_button_font_weight',
			'default_value'	=> '',
			'label'			=> esc_html__('Font Weight', 'rebellion'),
			'options'		=> rebellion_edge_get_font_weight_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'selectsimple',
			'name'			=> 'cf7_style_6_button_text_transform',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Transform', 'rebellion'),
			'options'		=> rebellion_edge_get_text_transform_array()
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_typography_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_letter_spacing',
			'default_value'	=> '',
			'label'			=> esc_html__('Letter Spacing', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Background and Border

		$button_background_border_group = rebellion_edge_add_admin_group(array(
			'name'			=> 'button_background_border_group',
			'title'			=> esc_html__('Button Background and Border', 'rebellion'),
			'description'	=> esc_html__('Setup button background and border style', 'rebellion'),
			'parent'		=> $panel_contact_form_style_6
		));

		$button_background_border_row1 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row1',
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_button_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_background_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_button_hover_bckg_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row1,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_hover_bckg_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Background Hover Transparency', 'rebellion')
		));

		$button_background_border_row2 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row2',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_button_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Transparency', 'rebellion'),
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'colorsimple',
			'name'			=> 'cf7_style_6_button_hover_border_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Color', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row2,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_hover_border_transparency',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Hover Transparency', 'rebellion')
		));

		$button_background_border_row3 = rebellion_edge_add_admin_row(array(
			'name'		=> 'button_background_border_row3',
			'next'		=> true,
			'parent'	=> $button_background_border_group
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_border_width',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Width', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'parent'		=> $button_background_border_row3,
			'type'			=> 'textsimple',
			'name'			=> 'cf7_style_6_button_border_radius',
			'default_value'	=> '',
			'label'			=> esc_html__('Border Radius', 'rebellion'),
			'args'			=> array(
				'suffix' => 'px'
			)
		));

		// Button Height

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_6,
			'type'			=> 'text',
			'name'			=> 'cf7_style_6_button_height',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Height', 'rebellion'),
			'description'	=> esc_html__('Insert form button height', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));

		// Button Padding

		rebellion_edge_add_admin_field(array(
			'parent'		=> $panel_contact_form_style_6,
			'type'			=> 'text',
			'name'			=> 'cf7_style_6_button_padding',
			'default_value'	=> '',
			'label'			=> esc_html__('Button Left/Right Padding', 'rebellion'),
			'description'	=> esc_html__('Enter value for button left and right padding', 'rebellion'),
			'args'			=> array(
				'col_width' => '3',
				'suffix' => 'px'
			)
		));


	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_contact_form_7_options_map', 21);

}