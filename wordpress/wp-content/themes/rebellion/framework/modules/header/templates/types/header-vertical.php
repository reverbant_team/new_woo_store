<?php do_action('rebellion_edge_before_page_header'); ?>
<aside class="edgtf-vertical-menu-area">
    <div class="edgtf-vertical-menu-area-inner">
        <div class="edgtf-vertical-area-background"></div>
        <?php if(!$hide_logo) {
            rebellion_edge_get_logo();
        } ?>
        <div class="edgtf-vertical-menu-holder">
            <div class="edgtf-vertical-menu-table">
                <div class="edgtf-vertical-menu-table-cell">
                    <?php rebellion_edge_get_vertical_main_menu(); ?>
                    <div class="edgtf-vertical-area-widget-holder">
            			<?php if($widget_area) : ?>
            				<?php dynamic_sidebar($widget_area); ?>
            			<?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>

<?php do_action('rebellion_edge_after_page_header'); ?>