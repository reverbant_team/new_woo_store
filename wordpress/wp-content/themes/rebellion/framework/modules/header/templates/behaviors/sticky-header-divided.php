<?php do_action('rebellion_edge_before_sticky_header'); ?>

	<div class="edgtf-sticky-header">
		<?php do_action('rebellion_edge_after_sticky_menu_html_open'); ?>
		<div class="edgtf-sticky-holder">
			<?php if($sticky_header_in_grid) : ?>
			<div class="edgtf-grid">
				<?php endif; ?>
                <div class="edgtf-vertical-align-containers">
                    <div class="edgtf-position-left">
                        <div class="edgtf-position-left-inner">
                            <?php rebellion_edge_get_divided_left_main_menu('sticky'); ?>
                        </div>
                    </div>
                    <div class="edgtf-position-center">
                        <div class="edgtf-position-center-inner">
                            <?php if(!$hide_logo) {
                                rebellion_edge_get_logo('divided-sticky');
                            } ?>
                        </div>
                    </div>
                    <div class="edgtf-position-right">
                        <div class="edgtf-position-right-inner">
                            <?php rebellion_edge_get_divided_right_main_menu('sticky'); ?>
                        </div>
                    </div>
                </div>
				<?php if($sticky_header_in_grid) : ?>
			</div>
		<?php endif; ?>
		</div>
	</div>

<?php do_action('rebellion_edge_after_sticky_header'); ?>