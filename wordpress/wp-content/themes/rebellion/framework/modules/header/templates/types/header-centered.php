<?php do_action('rebellion_edge_before_page_header'); ?>

<header class="edgtf-page-header">
    <div class="edgtf-logo-area">
        <?php if($logo_area_in_grid) : ?>
        <div class="edgtf-grid">
        <?php endif; ?>
			<?php do_action( 'rebellion_edge_after_header_logo_area_html_open' )?>
            <div class="edgtf-vertical-align-containers">
                <div class="edgtf-position-center">
                    <div class="edgtf-position-center-inner">
                        <?php if(!$hide_logo) {
                            rebellion_edge_get_logo('centered');
                        } ?>
                    </div>
                </div>
            </div>
        <?php if($logo_area_in_grid) : ?>
        </div>
        <?php endif; ?>
    </div>
    <?php if($show_fixed_wrapper) : ?>
        <div class="edgtf-fixed-wrapper">
    <?php endif; ?>
    <div class="edgtf-menu-area">
        <?php if($menu_area_in_grid) : ?>
            <div class="edgtf-grid">
        <?php endif; ?>
			<?php do_action( 'rebellion_edge_after_header_menu_area_html_open' )?>
            <div class="edgtf-vertical-align-containers">
                <div class="edgtf-position-center">
                    <div class="edgtf-position-center-inner">
                        <?php rebellion_edge_get_main_menu(); ?>
						<?php if($widget_area) : ?>
							<?php dynamic_sidebar($widget_area); ?>
						<?php endif; ?>
                    </div>
                </div>

            </div>
        <?php if($menu_area_in_grid) : ?>
        </div>
        <?php endif; ?>
    </div>
    <?php if($show_fixed_wrapper) : ?>
        </div>
    <?php endif; ?>
    <?php if($show_sticky) {
        rebellion_edge_get_sticky_header('centered');
    } ?>
</header>

<?php do_action('rebellion_edge_after_page_header'); ?>

