<?php do_action('rebellion_edge_before_page_header'); ?>

<header class="edgtf-page-header">
    <?php if($show_fixed_wrapper) : ?>
        <div class="edgtf-fixed-wrapper">
    <?php endif; ?>
    <?php do_action('rebellion_edge_after_header_area_html_open'); ?>
    <div class="edgtf-menu-area">
		<?php do_action( 'rebellion_edge_after_header_menu_area_html_open' )?>
        <?php if($menu_area_in_grid) : ?>
            <div class="edgtf-grid">
        <?php endif; ?>
        <div class="edgtf-vertical-align-containers">
            <div class="edgtf-position-left">
                <div class="edgtf-position-left-inner">
                    <?php rebellion_edge_get_divided_left_main_menu(); ?>
                </div>
            </div>
            <div class="edgtf-position-center">
                <div class="edgtf-position-center-inner">
                    <?php if(!$hide_logo) {
						rebellion_edge_get_logo('divided');
                    } ?>
                </div>
            </div>
            <div class="edgtf-position-right">
                <div class="edgtf-position-right-inner">
                    <?php rebellion_edge_get_divided_right_main_menu(); ?>
                </div>
            </div>
        </div>
        <?php if($menu_area_in_grid) : ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if($show_fixed_wrapper) { ?>
        <?php do_action('rebellion_edge_end_of_page_header_html'); ?>
        </div>
    <?php } else {
        do_action('rebellion_edge_end_of_page_header_html');
    } ?>
    <?php if($show_sticky) {
		rebellion_edge_get_sticky_header('divided');
    } ?>
</header>

<?php do_action('rebellion_edge_after_page_header'); ?>