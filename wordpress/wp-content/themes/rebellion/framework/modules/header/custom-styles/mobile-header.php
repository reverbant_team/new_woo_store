<?php

if(!function_exists('rebellion_edge_mobile_header_general_styles')) {
    /**
     * Generates general custom styles for mobile header
     */
    function rebellion_edge_mobile_header_general_styles() {
        $mobile_header_styles = array();
        if(rebellion_edge_options()->getOptionValue('mobile_header_height') !== '') {
            $mobile_header_styles['height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_header_height')).'px';
        }

        if(rebellion_edge_options()->getOptionValue('mobile_header_background_color')) {
            $mobile_header_styles['background-color'] = rebellion_edge_options()->getOptionValue('mobile_header_background_color');
        }

        echo rebellion_edge_dynamic_css('.edgtf-mobile-header .edgtf-mobile-header-inner', $mobile_header_styles);
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_mobile_header_general_styles');
}

if(!function_exists('rebellion_edge_mobile_navigation_styles')) {
    /**
     * Generates styles for mobile navigation
     */
    function rebellion_edge_mobile_navigation_styles() {
        $mobile_nav_styles = array();
        if(rebellion_edge_options()->getOptionValue('mobile_menu_background_color')) {
            $mobile_nav_styles['background-color'] = rebellion_edge_options()->getOptionValue('mobile_menu_background_color');
        }

        echo rebellion_edge_dynamic_css('.edgtf-mobile-header .edgtf-mobile-nav', $mobile_nav_styles);

        $mobile_nav_item_styles = array();
        if(rebellion_edge_options()->getOptionValue('mobile_menu_separator_color') !== '') {
            $mobile_nav_item_styles['border-bottom-color'] = rebellion_edge_options()->getOptionValue('mobile_menu_separator_color');
        }

        if(rebellion_edge_options()->getOptionValue('mobile_text_color') !== '') {
            $mobile_nav_item_styles['color'] = rebellion_edge_options()->getOptionValue('mobile_text_color');
        }

        if(rebellion_edge_is_font_option_valid(rebellion_edge_options()->getOptionValue('mobile_font_family'))) {
            $mobile_nav_item_styles['font-family'] = rebellion_edge_get_formatted_font_family(rebellion_edge_options()->getOptionValue('mobile_font_family'));
        }

        if(rebellion_edge_options()->getOptionValue('mobile_font_size') !== '') {
            $mobile_nav_item_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_font_size')).'px';
        }

        if(rebellion_edge_options()->getOptionValue('mobile_line_height') !== '') {
            $mobile_nav_item_styles['line-height'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_line_height')).'px';
        }

        if(rebellion_edge_options()->getOptionValue('mobile_text_transform') !== '') {
            $mobile_nav_item_styles['text-transform'] = rebellion_edge_options()->getOptionValue('mobile_text_transform');
        }

        if(rebellion_edge_options()->getOptionValue('mobile_font_style') !== '') {
            $mobile_nav_item_styles['font-style'] = rebellion_edge_options()->getOptionValue('mobile_font_style');
        }

        if(rebellion_edge_options()->getOptionValue('mobile_font_weight') !== '') {
            $mobile_nav_item_styles['font-weight'] = rebellion_edge_options()->getOptionValue('mobile_font_weight');
        }

        $mobile_nav_item_selector = array(
            '.edgtf-mobile-header .edgtf-mobile-nav a',
            '.edgtf-mobile-header .edgtf-mobile-nav h4'
        );

        echo rebellion_edge_dynamic_css($mobile_nav_item_selector, $mobile_nav_item_styles);

        $mobile_nav_item_hover_styles = array();
        if(rebellion_edge_options()->getOptionValue('mobile_text_hover_color') !== '') {
            $mobile_nav_item_hover_styles['color'] = rebellion_edge_options()->getOptionValue('mobile_text_hover_color');
        }

        $mobile_nav_item_selector_hover = array(
            '.edgtf-mobile-header .edgtf-mobile-nav a:hover',
            '.edgtf-mobile-header .edgtf-mobile-nav h4:hover'
        );

        echo rebellion_edge_dynamic_css($mobile_nav_item_selector_hover, $mobile_nav_item_hover_styles);
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_mobile_navigation_styles');
}

if(!function_exists('rebellion_edge_mobile_logo_styles')) {
    /**
     * Generates styles for mobile logo
     */
    function rebellion_edge_mobile_logo_styles() {
        if(rebellion_edge_options()->getOptionValue('mobile_logo_height') !== '') { ?>
            @media only screen and (max-width: 1000px) {
            <?php echo rebellion_edge_dynamic_css(
                '.edgtf-mobile-header .edgtf-mobile-logo-wrapper a',
                array('height' => rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_logo_height')).'px !important')
            ); ?>
            }
        <?php }

        if(rebellion_edge_options()->getOptionValue('mobile_logo_height_phones') !== '') { ?>
            @media only screen and (max-width: 480px) {
            <?php echo rebellion_edge_dynamic_css(
                '.edgtf-mobile-header .edgtf-mobile-logo-wrapper a',
                array('height' => rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_logo_height_phones')).'px !important')
            ); ?>
            }
        <?php }

        if(rebellion_edge_options()->getOptionValue('mobile_header_height') !== '') {
            $max_height = intval(rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_header_height')) * 0.9).'px';
            echo rebellion_edge_dynamic_css('.edgtf-mobile-header .edgtf-mobile-logo-wrapper a', array('max-height' => $max_height));
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_mobile_logo_styles');
}

if(!function_exists('rebellion_edge_mobile_icon_styles')) {
    /**
     * Generates styles for mobile icon opener
     */
    function rebellion_edge_mobile_icon_styles() {
        $mobile_icon_styles = array();
        if(rebellion_edge_options()->getOptionValue('mobile_icon_color') !== '') {
            $mobile_icon_styles['color'] = rebellion_edge_options()->getOptionValue('mobile_icon_color');
        }

        if(rebellion_edge_options()->getOptionValue('mobile_icon_size') !== '') {
            $mobile_icon_styles['font-size'] = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_icon_size')).'px';
        }

        echo rebellion_edge_dynamic_css('.edgtf-mobile-header .edgtf-mobile-menu-opener a', $mobile_icon_styles);

        if(rebellion_edge_options()->getOptionValue('mobile_icon_hover_color') !== '') {
            echo rebellion_edge_dynamic_css(
                '.edgtf-mobile-header .edgtf-mobile-menu-opener a:hover',
                array('color' => rebellion_edge_options()->getOptionValue('mobile_icon_hover_color')));
        }
    }

    add_action('rebellion_edge_style_dynamic', 'rebellion_edge_mobile_icon_styles');
}