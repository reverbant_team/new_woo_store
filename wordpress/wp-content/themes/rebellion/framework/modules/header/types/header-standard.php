<?php
namespace RebellionEdge\Modules\Header\Types;

use RebellionEdge\Modules\Header\Lib\HeaderType;

/**
 * Class that represents Header Standard layout and option
 *
 * Class HeaderStandard
 */
class HeaderStandard extends HeaderType {
    protected $heightOfTransparency;
    protected $heightOfCompleteTransparency;
    protected $headerHeight;
    protected $mobileHeaderHeight;

    /**
     * Sets slug property which is the same as value of option in DB
     */
    public function __construct() {
        $this->slug = 'header-standard';

        if(!is_admin()) {

            $menuAreaHeight       = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('menu_area_height_header_standard'));
            $this->menuAreaHeight = $menuAreaHeight !== '' ? (int)$menuAreaHeight : 66;

            $mobileHeaderHeight       = rebellion_edge_filter_px(rebellion_edge_options()->getOptionValue('mobile_header_height'));
            $this->mobileHeaderHeight = $mobileHeaderHeight !== '' ? (int)$mobileHeaderHeight : 100;

            add_action('wp', array($this, 'setHeaderHeightProps'));

            add_filter('rebellion_edge_js_global_variables', array($this, 'getGlobalJSVariables'));
            add_filter('rebellion_edge_per_page_js_vars', array($this, 'getPerPageJSVariables'));
            add_filter('rebellion_edge_add_page_custom_style', array($this, 'headerPerPageStyles'));

        }
    }

    /**
     * Loads template file for this header type
     *
     * @param array $parameters associative array of variables that needs to passed to template
     */
    public function loadTemplate($parameters = array()) {

        $parameters['menu_area_in_grid'] = rebellion_edge_get_meta_field_intersect('menu_area_in_grid_header_standard') == 'yes' ? true : false;

        $parameters = apply_filters('rebellion_edge_header_standard_parameters', $parameters);

        rebellion_edge_get_module_template_part('templates/types/'.$this->slug, $this->moduleName, '', $parameters);
    }

    /**
     * Sets header height properties after WP object is set up
     */
    public function setHeaderHeightProps(){
        $this->heightOfTransparency         = $this->calculateHeightOfTransparency();
        $this->heightOfCompleteTransparency = $this->calculateHeightOfCompleteTransparency();
        $this->headerHeight                 = $this->calculateHeaderHeight();
        $this->mobileHeaderHeight           = $this->calculateMobileHeaderHeight();
    }

    /**
     * Returns total height of transparent parts of header
     *
     * @return int
     */
    public function calculateHeightOfTransparency() {
        $id = rebellion_edge_get_page_id();
        $transparencyHeight = 0;

        if(get_post_meta($id, 'edgtf_menu_area_background_color_header_standard_meta', true) !== ''){
            $menuAreaTransparent = get_post_meta($id, 'edgtf_menu_area_background_color_header_standard_meta', true) !== '' &&
                                   get_post_meta($id, 'edgtf_menu_area_background_transparency_header_standard_meta', true) !== '1';
        } else {
            $menuAreaTransparent = rebellion_edge_options()->getOptionValue('menu_area_background_color_header_standard') !== '' &&
                                   rebellion_edge_options()->getOptionValue('menu_area_background_transparency_header_standard') !== '1';
        }


        $sliderExists = get_post_meta($id, 'edgtf_page_slider_meta', true) !== '';

        if($sliderExists){
            $menuAreaTransparent = true;
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;

            if(($sliderExists && rebellion_edge_is_top_bar_enabled())
               || rebellion_edge_is_top_bar_enabled() && rebellion_edge_is_top_bar_transparent()) {
                $transparencyHeight += rebellion_edge_get_top_bar_height();
            }
        }

        return $transparencyHeight;
    }

    /**
     * Returns height of completely transparent header parts
     *
     * @return int
     */
    public function calculateHeightOfCompleteTransparency() {
        $id = rebellion_edge_get_page_id();
        $transparencyHeight = 0;

        if(get_post_meta($id, 'edgtf_menu_area_background_color_header_standard_meta', true) !== ''){
            $menuAreaTransparent = get_post_meta($id, 'edgtf_menu_area_background_color_header_standard_meta', true) !== '' &&
                                   get_post_meta($id, 'edgtf_menu_area_background_transparency_header_standard_meta', true) === '0';
        } else {
            $menuAreaTransparent = rebellion_edge_options()->getOptionValue('menu_area_background_color_header_standard') !== '' &&
                                   rebellion_edge_options()->getOptionValue('menu_area_background_transparency_header_standard') === '0';
        }

        if($menuAreaTransparent) {
            $transparencyHeight = $this->menuAreaHeight;
        }

        return $transparencyHeight;
    }


    /**
     * Returns total height of header
     *
     * @return int|string
     */
    public function calculateHeaderHeight() {
        $headerHeight = $this->menuAreaHeight;
        if(rebellion_edge_is_top_bar_enabled()) {
            $headerHeight += rebellion_edge_get_top_bar_height();
        }

        return $headerHeight;
    }

    /**
     * Returns total height of mobile header
     *
     * @return int|string
     */
    public function calculateMobileHeaderHeight() {
        $mobileHeaderHeight = $this->mobileHeaderHeight;

        return $mobileHeaderHeight;
    }

    /**
     * Returns global js variables of header
     *
     * @param $globalVariables
     * @return int|string
     */
    public function getGlobalJSVariables($globalVariables) {
        $globalVariables['edgtfLogoAreaHeight'] = 0;
        $globalVariables['edgtfMenuAreaHeight'] = $this->headerHeight;
        $globalVariables['edgtfMobileHeaderHeight'] = $this->mobileHeaderHeight;

        return $globalVariables;
    }

    /**
     * Returns per page js variables of header
     *
     * @param $perPageVars
     * @return int|string
     */
    public function getPerPageJSVariables($perPageVars) {
        //calculate transparency height only if header has no sticky behaviour
        if(!in_array(rebellion_edge_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
            $perPageVars['edgtfHeaderTransparencyHeight'] = $this->headerHeight - (rebellion_edge_get_top_bar_height() + $this->heightOfCompleteTransparency);
        }else{
            $perPageVars['edgtfHeaderTransparencyHeight'] = 0;
        }

        return $perPageVars;
    }

    public function headerPerPageStyles($style) {
        $id                     = rebellion_edge_get_page_id();
        $class_prefix           = rebellion_edge_get_unique_page_class();
        $main_menu_style        = array();
        $main_menu_grid_style   = array();
        $disable_border         = rebellion_edge_get_meta_field_intersect('border_bottom_header_standard',$id) == 'no';

        $main_menu_selector = array($class_prefix.'.edgtf-header-standard .edgtf-page-header .edgtf-menu-area');
     
        /* header style - start */
        if(!$disable_border) {
            $header_border_color = get_post_meta($id, 'edgtf_border_bottom_color_header_standard_meta', true);
            if($header_border_color !== '') {
                $header_border_transparency = get_post_meta($id, 'edgtf_border_bottom_transparency_header_standard_meta', true);
                if($header_border_transparency !== '') {
                    $main_menu_style['border-bottom-color'] = rebellion_edge_rgba_color($header_border_color, $header_border_transparency);
                } else {
                    $main_menu_style['border-bottom-color'] = $header_border_color;
                }
            }
        }
        else {
            $main_menu_style['border-bottom'] = 'none';
        }

        $menu_area_background_color = get_post_meta($id, 'edgtf_menu_area_background_color_header_standard_meta', true);
        if($menu_area_background_color !== '') {
            $menu_area_background_transparency = get_post_meta($id, 'edgtf_menu_area_background_transparency_header_standard_meta', true);
            if($menu_area_background_transparency !== '') {
                $main_menu_style['background-color'] = rebellion_edge_rgba_color($menu_area_background_color, $menu_area_background_transparency);
            } else {
                $main_menu_style['background-color'] = $menu_area_background_color;
            }
        }

        /* header style - end */

        

        $style[] = rebellion_edge_dynamic_css($main_menu_selector, $main_menu_style);

        return $style;
    }
}