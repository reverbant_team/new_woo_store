<?php

if ( ! function_exists('rebellion_edge_header_options_map') ) {

	function rebellion_edge_header_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug' => '_header_page',
				'title' => esc_html__('Header', 'rebellion'),
				'icon' => 'fa fa-header'
			)
		);

		$panel_header = rebellion_edge_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header',
				'title' => esc_html__('Header', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'radiogroup',
				'name' => 'header_type',
				'default_value' => 'header-standard',
				'label' => esc_html__('Choose Header Type', 'rebellion'),
				'description' => esc_html__('Select the type of header you would like to use', 'rebellion'),
				'options' => array(
					'header-standard' => array(
						'image' => EDGE_ROOT . '/framework/admin/assets/img/header-standard.png',
						'label' => esc_html__('Header Standard', 'rebellion')
					),
					'header-vertical' => array(
						'image' => EDGE_ROOT . '/framework/admin/assets/img/header-vertical.png',
						'label' => esc_html__('Header Vertical', 'rebellion')
					),
					'header-full-screen' => array(
						'image' => EDGE_ROOT . '/framework/admin/assets/img/header-full-screen.png',
						'label' => esc_html__('Header Full Screen', 'rebellion')
					),
					'header-divided' => array(
						'image' => EDGE_ROOT . '/framework/admin/assets/img/header-divided.png',
						'label' => esc_html__('Header Divided', 'rebellion')
					),
					'header-centered' => array(
						'image' => EDGE_ROOT . '/framework/admin/assets/img/header-centered.png',
						'label' => esc_html__('Header Centered', 'rebellion')
					)
				),
				'args' => array(
					'use_images' => true,
					'hide_labels' => true,
					'dependence' => true,
					'show' => array(
						'header-standard'		=> '#edgtf_panel_header_standard,#edgtf_header_behaviour,#edgtf_panel_fixed_header,#edgtf_panel_sticky_header,#edgtf_panel_main_menu',
						'header-vertical'		=> '#edgtf_panel_header_vertical,#edgtf_panel_vertical_main_menu',
						'header-full-screen'	=> '#edgtf_panel_header_full_screen,#edgtf_fullscreen_menu',
						'header-divided'		=> '#edgtf_panel_header_divided,#edgtf_header_behaviour,#edgtf_panel_sticky_header,#edgtf_panel_main_menu',
						'header-centered'		=> '#edgtf_panel_header_centered,#edgtf_header_behaviour,#edgtf_panel_sticky_header,#edgtf_panel_main_menu',
					),
					'hide' => array(
						'header-standard'		=> '#edgtf_panel_header_vertical,#edgtf_panel_vertical_main_menu,#edgtf_panel_header_full_screen,#edgtf_fullscreen_menu,#edgtf_panel_header_centered,#edgtf_panel_header_divided',
						'header-vertical'		=> '#edgtf_panel_header_standard,#edgtf_header_behaviour,#edgtf_panel_fixed_header,#edgtf_panel_sticky_header,#edgtf_panel_main_menu,#edgtf_panel_header_full_screen,#edgtf_fullscreen_menu,#edgtf_panel_header_centered,#edgtf_panel_header_divided',
						'header-full-screen'	=> '#edgtf_panel_header_standard,#edgtf_header_behaviour,#edgtf_panel_fixed_header,#edgtf_panel_sticky_header,#edgtf_panel_main_menu,#edgtf_panel_header_vertical,#edgtf_panel_vertical_main_menu,#edgtf_panel_header_centered,#edgtf_panel_header_divided',
						'header-divided'		=> '#edgtf_panel_header_standard,#edgtf_panel_header_full_screen,#edgtf_panel_header_centered,#edgtf_panel_header_vertical,#edgtf_panel_vertical_main_menu,#edgtf_panel_header_full_screen,#edgtf_panel_fixed_header,#edgtf_fullscreen_menu',
						'header-centered'		=> '#edgtf_panel_header_standard,#edgtf_panel_header_full_screen,#edgtf_panel_header_divided,#edgtf_panel_header_vertical,#edgtf_panel_vertical_main_menu,#edgtf_panel_header_full_screen,#edgtf_panel_fixed_header,#edgtf_fullscreen_menu',

					)
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_behaviour',
				'default_value' => 'sticky-header-on-scroll-up',
				'label' => esc_html__('Choose Header behaviour', 'rebellion'),
				'description' => esc_html__('Select the behaviour of header when you scroll down to page', 'rebellion'),
				'options' => array(
					'sticky-header-on-scroll-up' => esc_html__('Sticky on scrol up', 'rebellion'),
					'sticky-header-on-scroll-down-up' => esc_html__('Sticky on scrol up/down', 'rebellion'),
					'fixed-on-scroll' => esc_html__('Fixed on scroll', 'rebellion')
				),
                'hidden_property' => 'header_type',
                'hidden_value' => '',
                'hidden_values' => array('header-vertical','header-full-screen'),
				'args' => array(
					'dependence' => true,
					'show' => array(
						'sticky-header-on-scroll-up' => '#edgtf_panel_sticky_header',
						'sticky-header-on-scroll-down-up' => '#edgtf_panel_sticky_header',
						'fixed-on-scroll' => '#edgtf_panel_fixed_header'
					),
					'hide' => array(
						'sticky-header-on-scroll-up' => '#edgtf_panel_fixed_header',
						'sticky-header-on-scroll-down-up' => '#edgtf_panel_fixed_header',
						'fixed-on-scroll' => '#edgtf_panel_sticky_header',
					)
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'top_bar',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Top Bar', 'rebellion'),
				'description' => esc_html__('Enabling this option will show top bar area', 'rebellion'),
				'parent' => $panel_header,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#edgtf_top_bar_container"
				)
			)
		);

		$top_bar_container = rebellion_edge_add_admin_container(array(
			'name' => 'top_bar_container',
			'parent' => $panel_header,
			'hidden_property' => 'top_bar',
			'hidden_value' => 'no'
		));

		rebellion_edge_add_admin_field(
			array(
				'parent' => $top_bar_container,
				'type' => 'select',
				'name' => 'top_bar_layout',
				'default_value' => 'three-columns',
				'label' => esc_html__('Choose top bar layout', 'rebellion'),
				'description' => esc_html__('Select the layout for top bar', 'rebellion'),
				'options' => array(
					'two-columns' => esc_html__('Two columns', 'rebellion'),
					'three-columns' => esc_html__('Three columns', 'rebellion')
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"two-columns" => "#edgtf_top_bar_layout_container",
						"three-columns" => ""
					),
					"show" => array(
						"two-columns" => "",
						"three-columns" => "#edgtf_top_bar_layout_container"
					)
				)
			)
		);

		$top_bar_layout_container = rebellion_edge_add_admin_container(array(
			'name' => 'top_bar_layout_container',
			'parent' => $top_bar_container,
			'hidden_property' => 'top_bar_layout',
			'hidden_value' => '',
			'hidden_values' => array("two-columns"),
		));

		rebellion_edge_add_admin_field(
			array(
				'parent' => $top_bar_layout_container,
				'type' => 'select',
				'name' => 'top_bar_column_widths',
				'default_value' => '30-30-30',
				'label' => esc_html__('Choose column widths', 'rebellion'),
				'description' => '',
				'options' => array(
					'30-30-30' => '33% - 33% - 33%',
					'25-50-25' => '25% - 50% - 25%'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'top_bar_in_grid',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__('Top Bar in grid', 'rebellion'),
				'description' => esc_html__('Set top bar content to be in grid', 'rebellion'),
				'parent' => $top_bar_container,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#edgtf_top_bar_in_grid_container"
				)
			)
		);

		$top_bar_in_grid_container = rebellion_edge_add_admin_container(array(
			'name' => 'top_bar_in_grid_container',
			'parent' => $top_bar_container,
			'hidden_property' => 'top_bar_in_grid',
			'hidden_value' => 'no'
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'top_bar_grid_background_color',
			'type' => 'color',
			'label' => esc_html__('Grid Background Color', 'rebellion'),
			'description' => esc_html__('Set grid background color for top bar', 'rebellion'),
			'parent' => $top_bar_in_grid_container
		));


		rebellion_edge_add_admin_field(array(
			'name' => 'top_bar_grid_background_transparency',
			'type' => 'text',
			'label' => esc_html__('Grid Background Transparency', 'rebellion'),
			'description' => esc_html__('Set grid background transparency for top bar', 'rebellion'),
			'parent' => $top_bar_in_grid_container,
			'args' => array('col_width' => 3)
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'top_bar_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color', 'rebellion'),
			'description' => esc_html__('Set background color for top bar', 'rebellion'),
			'parent' => $top_bar_container
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'top_bar_background_transparency',
			'type' => 'text',
			'label' => esc_html__('Background Transparency', 'rebellion'),
			'description' => esc_html__('Set background transparency for top bar', 'rebellion'),
			'parent' => $top_bar_container,
			'args' => array('col_width' => 3)
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'top_bar_height',
			'type' => 'text',
			'label' => esc_html__('Top bar height', 'rebellion'),
			'description' => esc_html__('Enter top bar height (Default is 40px)', 'rebellion'),
			'parent' => $top_bar_container,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'hide_top_bar_on_responsive',
			'type' => 'yesno',
			'default_value' => 'yes',
			'label' => esc_html__('Hide Top Bar on Responsive', 'rebellion'),
			'description' => esc_html__('Enabling this option you will hide top header area on responsive', 'rebellion'),
			'parent' => $top_bar_container,
		));
		
		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'select',
				'name' => 'header_style',
				'default_value' => '',
				'label' => esc_html__('Header Skin', 'rebellion'),
				'description' => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'rebellion'),
				'options' => array(
					'' => '',
					'light-header' => esc_html__('Light', 'rebellion'),
					'dark-header' => esc_html__('Dark', 'rebellion')
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header,
				'type' => 'yesno',
				'name' => 'enable_header_style_on_scroll',
				'default_value' => 'no',
				'label' => esc_html__('Enable Header Style on Scroll', 'rebellion'),
				'description' => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'rebellion'),
			)
		);


		$panel_header_standard = rebellion_edge_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_standard',
				'title' => esc_html__('Header Standard', 'rebellion'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
                    'header-vertical',
					'header-full-screen',
					'header-centered',
					'header-divided',
				)
			)
		);

		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $panel_header_standard,
				'name' => 'menu_area_title',
				'title' => esc_html__('Menu Area', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'yesno',
				'name' => 'menu_area_in_grid_header_standard',
				'default_value' => 'yes',
				'label' => esc_html__('Header in grid', 'rebellion'),
				'description' => esc_html__('Set header content to be in grid', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'color',
				'name' => 'menu_area_background_color_header_standard',
				'default_value' => '',
				'label' => esc_html__('Background color', 'rebellion'),
				'description' => esc_html__('Set background color for header', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'text',
				'name' => 'menu_area_background_transparency_header_standard',
				'default_value' => '',
				'label' => esc_html__('Background transparency', 'rebellion'),
				'description' => esc_html__('Set background transparency for header', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_standard,
				'type'          => 'yesno',
				'name'          => 'border_bottom_header_standard',
				'default_value' => 'yes',
				'label'         => esc_html__('Enable Border Bottom', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_border_bottom_header_standard_container'
				)
			)
		);

		$border_bottom_header_standard_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_standard,
				'name'            => 'border_bottom_header_standard_container',
				'hidden_property' => 'border_bottom_header_standard',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $border_bottom_header_standard_container,
				'type'          => 'color',
				'name'          => 'border_bottom_color_header_standard',
				'default_value' => '',
				'label'         => esc_html__('Border Bottom Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $border_bottom_header_standard_container,
				'type' => 'text',
				'name' => 'border_bottom_transparency_header_standard',
				'default_value' => '',
				'label' => esc_html__('Border Bottom Transparency','rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_standard,
				'type' => 'text',
				'name' => 'menu_area_height_header_standard',
				'default_value' => '',
				'label' => esc_html__('Height', 'rebellion'),
				'description' => esc_html__('Enter header height (default is 66px)', 'rebellion'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);


		/***************** Divided Header Layout *******************/

		$panel_header_divided = rebellion_edge_add_admin_panel(
			array(
				'page'            => '_header_page',
				'name'            => 'panel_header_divided',
				'title'           => esc_html__('Header Divided', 'rebellion'),
				'hidden_property' => 'header_type',
				'hidden_value'    => '',
				'hidden_values'   => array(
					'header-standard',
					'header-full-screen',
					'header-centered',
					'header-vertical'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_divided,
				'type'          => 'yesno',
				'name'          => 'menu_area_in_grid_header_divided',
				'default_value' => 'no',
				'label'         => esc_html__('Header In Grid', 'rebellion'),
				'description'   => esc_html__('Set header content to be in grid', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_menu_area_in_grid_header_divided_container'
				)
			)
		);

		$menu_area_in_grid_header_divided_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_divided,
				'name'            => 'menu_area_in_grid_header_divided_container',
				'hidden_property' => 'menu_area_in_grid_header_divided',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_header_divided_container,
				'type'          => 'color',
				'name'          => 'menu_area_grid_background_color_header_divided',
				'default_value' => '',
				'label'         => esc_html__('Grid Background Color', 'rebellion'),
				'description'   => esc_html__('Set grid background color for header area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_header_divided_container,
				'type'          => 'text',
				'name'          => 'menu_area_grid_background_transparency_header_divided',
				'default_value' => '',
				'label'         => esc_html__('Grid Background Transparency', 'rebellion'),
				'description'   => esc_html__('Set grid background transparency for header', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_header_divided_container,
				'type'          => 'yesno',
				'name'          => 'menu_area_in_grid_border_header_divided',
				'default_value' => 'no',
				'label'         => esc_html__('Grid Area Border', 'rebellion'),
				'description'   => esc_html__('Set border on grid area', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_menu_area_in_grid_border_header_divided_container'
				)
			)
		);

		$menu_area_in_grid_border_header_divided_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $menu_area_in_grid_header_divided_container,
				'name'            => 'menu_area_in_grid_border_header_divided_container',
				'hidden_property' => 'menu_area_in_grid_border_header_divided',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_border_header_divided_container,
				'type'          => 'color',
				'name'          => 'menu_area_in_grid_border_color_header_divided',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
				'description'   => esc_html__('Set border color for grid area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_divided,
				'type'          => 'color',
				'name'          => 'menu_area_background_color_header_divided',
				'default_value' => '',
				'label'         => esc_html__('Background Color', 'rebellion'),
				'description'   => esc_html__('Set background color for header', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_divided,
				'type'          => 'text',
				'name'          => 'menu_area_background_transparency_header_divided',
				'default_value' => '',
				'label'         => esc_html__('Background Transparency', 'rebellion'),
				'description'   => esc_html__('Set background transparency for header', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);


		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_divided,
				'type'          => 'yesno',
				'name'          => 'menu_area_border_header_divided',
				'default_value' => 'yes',
				'label'         => esc_html__('Header Area Border', 'rebellion'),
				'description'   => esc_html__('Set border on header area', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_menu_area_border_header_divided_container'
				)
			)
		);

		$menu_area_border_header_divided_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_divided,
				'name'            => 'menu_area_border_header_divided_container',
				'hidden_property' => 'menu_area_border_header_divided',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_border_header_divided_container,
				'type'          => 'color',
				'name'          => 'menu_area_border_color_header_divided',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
				'description'   => esc_html__('Set border color for header', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_divided,
				'type'          => 'text',
				'name'          => 'menu_area_height_header_divided',
				'default_value' => '',
				'label'         => 'Height',
				'description'   => esc_html__('Enter Header Height (default is 92px)', 'rebellion'),
				'args'          => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			)
		);

		/***************** Divided Header Layout - end *******************/

		/***************** Centered Header Layout - start ****************/

		$panel_header_centered = rebellion_edge_add_admin_panel(
			array(
				'page'            => '_header_page',
				'name'            => 'panel_header_centered',
				'title'           => esc_html__('Header Centered', 'rebellion'),
				'hidden_property' => 'header_type',
				'hidden_value'    => '',
				'hidden_values'   => array(
					'header-vertical',
					'header-standard',
					'header-full-screen',
					'header-divided',
				)
			)
		);

		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $panel_header_centered,
				'name'   => 'logo_menu_area_title',
				'title'  => esc_html__('Logo Area', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'yesno',
				'name'          => 'logo_area_in_grid_header_centered',
				'default_value' => 'no',
				'label'         => esc_html__('Logo Area In Grid', 'rebellion'),
				'description'   => esc_html__('Set menu area content to be in grid', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_logo_area_in_grid_header_centered_container'
				)
			)
		);

		$logo_area_in_grid_header_centered_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_centered,
				'name'            => 'logo_area_in_grid_header_centered_container',
				'hidden_property' => 'logo_area_in_grid_header_centered',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $logo_area_in_grid_header_centered_container,
				'type'          => 'color',
				'name'          => 'logo_area_grid_background_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Grid Background Color', 'rebellion'),
				'description'   => esc_html__('Set grid background color for logo area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $logo_area_in_grid_header_centered_container,
				'type'          => 'text',
				'name'          => 'logo_area_grid_background_transparency_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Grid Background Transparency', 'rebellion'),
				'description'   => esc_html__('Set grid background transparency', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $logo_area_in_grid_header_centered_container,
				'type'          => 'yesno',
				'name'          => 'logo_area_in_grid_border_header_centered',
				'default_value' => 'no',
				'label'         => esc_html__('Grid Area Border', 'rebellion'),
				'description'   => esc_html__('Set border on grid area', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_logo_area_in_grid_border_header_centered_container'
				)
			)
		);

		$logo_area_in_grid_border_header_centered_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $logo_area_in_grid_header_centered_container,
				'name'            => 'logo_area_in_grid_border_header_centered_container',
				'hidden_property' => 'logo_area_in_grid_border_header_centered',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $logo_area_in_grid_border_header_centered_container,
				'type'          => 'color',
				'name'          => 'logo_area_in_grid_border_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
				'description'   => esc_html__('Set border color for grid area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'color',
				'name'          => 'logo_area_background_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Background color', 'rebellion'),
				'description'   => esc_html__('Set background color for logo area', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'text',
				'name'          => 'logo_area_background_transparency_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Background transparency', 'rebellion'),
				'description'   => esc_html__('Set background transparency for logo area', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'yesno',
				'name'          => 'logo_area_border_header_centered',
				'default_value' => 'no',
				'label'         => esc_html__('Logo Area Border', 'rebellion'),
				'description'   => esc_html__('Set border on logo area', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_logo_area_border_header_centered_container'
				)
			)
		);

		$logo_area_border_header_centered_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_centered,
				'name'            => 'logo_area_border_header_centered_container',
				'hidden_property' => 'logo_area_border_header_centered',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $logo_area_border_header_centered_container,
				'type'          => 'color',
				'name'          => 'logo_area_border_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
				'description'   => esc_html__('Set border color for logo area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'text',
				'name'          => 'logo_wrapper_padding_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Logo Padding', 'rebellion'),
				'description'   => esc_html__('Insert padding in format: 0px 0px 1px 0px', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'text',
				'name'          => 'logo_area_height_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Height', 'rebellion'),
				'description'   => esc_html__('Enter logo area height (default is 155px)', 'rebellion'),
				'args'          => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			)
		);


		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $panel_header_centered,
				'name'   => 'main_menu_area_title',
				'title'  => esc_html__('Menu Area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'yesno',
				'name'          => 'menu_area_in_grid_header_centered',
				'default_value' => 'no',
				'label'         => esc_html__('Menu Area In Grid', 'rebellion'),
				'description'   => esc_html__('Set menu area content to be in grid', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_menu_area_in_grid_header_centered_container'
				)
			)
		);

		$menu_area_in_grid_header_centered_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_centered,
				'name'            => 'menu_area_in_grid_header_centered_container',
				'hidden_property' => 'menu_area_in_grid_header_centered',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_header_centered_container,
				'type'          => 'color',
				'name'          => 'menu_area_grid_background_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Grid Background Color', 'rebellion'),
				'description'   => esc_html__('Set grid background color for menu area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_header_centered_container,
				'type'          => 'text',
				'name'          => 'menu_area_grid_background_transparency_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Grid Background Transparency', 'rebellion'),
				'description'   => esc_html__('Set grid background transparency', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_header_centered_container,
				'type'          => 'yesno',
				'name'          => 'menu_area_in_grid_border_header_centered',
				'default_value' => 'no',
				'label'         => esc_html__('Grid Area Border', 'rebellion'),
				'description'   => esc_html__('Set border on grid area', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_menu_area_in_grid_border_header_centered_container'
				)
			)
		);

		$menu_area_in_grid_border_header_centered_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $menu_area_in_grid_header_centered_container,
				'name'            => 'menu_area_in_grid_border_header_centered_container',
				'hidden_property' => 'menu_area_in_grid_border_header_centered',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_in_grid_border_header_centered_container,
				'type'          => 'color',
				'name'          => 'menu_area_in_grid_border_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
				'description'   => esc_html__('Set border color for grid area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'color',
				'name'          => 'menu_area_background_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Background color', 'rebellion'),
				'description'   => esc_html__('Set background color for menu area', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'text',
				'name'          => 'menu_area_background_transparency_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Background transparency', 'rebellion'),
				'description'   => esc_html__('Set background transparency for menu area', 'rebellion'),
				'args'          => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'yesno',
				'name'          => 'menu_area_border_header_centered',
				'default_value' => 'no',
				'label'         => esc_html__('Menu Area Border', 'rebellion'),
				'description'   => esc_html__('Set border on menu area', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_menu_area_border_header_centered_container'
				)
			)
		);

		$menu_area_border_header_centered_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_centered,
				'name'            => 'menu_area_border_header_centered_container',
				'hidden_property' => 'menu_area_border_header_centered',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $menu_area_border_header_centered_container,
				'type'          => 'color',
				'name'          => 'menu_area_border_color_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
				'description'   => esc_html__('Set border color for header area', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_centered,
				'type'          => 'text',
				'name'          => 'menu_area_height_header_centered',
				'default_value' => '',
				'label'         => esc_html__('Height', 'rebellion'),
				'description'   => esc_html__('Enter menu area height (default is 92px)', 'rebellion'),
				'args'          => array(
					'col_width' => 3,
					'suffix'    => 'px'
				)
			)
		);

		/***************** Centered Header Layout - end ****************/

        $panel_header_vertical = rebellion_edge_add_admin_panel(
            array(
                'page' => '_header_page',
                'name' => 'panel_header_vertical',
                'title' => esc_html__('Header Vertical', 'rebellion'),
                'hidden_property' => 'header_type',
                'hidden_value' => '',
                'hidden_values' => array(
                    'header-standard',
					'header-full-screen',
					'header-centered',
					'header-divided',
                )
            )
        );

            rebellion_edge_add_admin_field(array(
                'name' => 'vertical_header_background_color',
                'type' => 'color',
                'label' => esc_html__('Background Color', 'rebellion'),
                'description' => esc_html__('Set background color for vertical menu', 'rebellion'),
                'parent' => $panel_header_vertical
            ));

            rebellion_edge_add_admin_field(array(
                'name' => 'vertical_header_transparency',
                'type' => 'text',
                'label' => esc_html__('Transparency', 'rebellion'),
                'description' => esc_html__('Enter transparency for vertical menu (value from 0 to 1)', 'rebellion'),
                'parent' => $panel_header_vertical,
                'args' => array(
                    'col_width' => 1
                )
            ));

            rebellion_edge_add_admin_field(
                array(
                    'name' => 'vertical_header_background_image',
                    'type' => 'image',
                    'default_value' => '',
                    'label' => esc_html__('Background Image', 'rebellion'),
                    'description' => esc_html__('Set background image for vertical menu', 'rebellion'),
                    'parent' => $panel_header_vertical
                )
            );


		$panel_header_full_screen = rebellion_edge_add_admin_panel(
			array(
				'page' => '_header_page',
				'name' => 'panel_header_full_screen',
				'title' => esc_html__('Header Full Screen', 'rebellion'),
				'hidden_property' => 'header_type',
				'hidden_value' => '',
				'hidden_values' => array(
					'header-standard',
					'header-vertical',
					'header-centered',
					'header-divided',
				)
			)
		);


		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'yesno',
				'name' => 'menu_area_in_grid_header_full_screen',
				'default_value' => 'yes',
				'label' => esc_html__('Header in grid', 'rebellion'),
				'description' => esc_html__('Set header content to be in grid', 'rebellion')
			)
		);



		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'color',
				'name' => 'menu_area_background_color_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Background color', 'rebellion'),
				'description' => esc_html__('Set background color for header', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'text',
				'name' => 'menu_area_background_transparency_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Background transparency', 'rebellion'),
				'description' => esc_html__('Set background transparency for header', 'rebellion'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $panel_header_full_screen,
				'type'          => 'yesno',
				'name'          => 'border_bottom_header_full_screen',
				'default_value' => 'no',
				'label'         => esc_html__('Enable Border Bottom', 'rebellion'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_border_bottom_header_full_screen_container'
				)
			)
		);

		$border_bottom_header_full_screen_container = rebellion_edge_add_admin_container(
			array(
				'parent'          => $panel_header_full_screen,
				'name'            => 'border_bottom_header_full_screen_container',
				'hidden_property' => 'border_bottom_header_full_screen',
				'hidden_value'    => 'no'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent'        => $border_bottom_header_full_screen_container,
				'type'          => 'color',
				'name'          => 'border_bottom_color_header_full_screen',
				'default_value' => '',
				'label'         => esc_html__('Border Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $border_bottom_header_full_screen_container,
				'type' => 'text',
				'name' => 'border_bottom_transparency_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Border Bottom Transparency', 'rebellion'),
				'args' => array(
					'col_width' => 3,
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_header_full_screen,
				'type' => 'text',
				'name' => 'menu_area_height_header_full_screen',
				'default_value' => '',
				'label' => esc_html__('Height', 'rebellion'),
				'description' => esc_html__('Enter header height (default is 66px)', 'rebellion'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);


		$panel_sticky_header = rebellion_edge_add_admin_panel(
			array(
				'title' => esc_html__('Sticky Header', 'rebellion'),
				'name' => 'panel_sticky_header',
				'page' => '_header_page',
				'hidden_property' => 'header_behaviour',
				'hidden_values' => array(
					'fixed-on-scroll'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'scroll_amount_for_sticky',
				'type' => 'text',
				'label' => esc_html__('Scroll Amount for Sticky', 'rebellion'),
				'description' => esc_html__('Enter scroll amount for Sticky Menu to appear (deafult is header height)', 'rebellion'),
				'parent' => $panel_sticky_header,
				'args' => array(
					'col_width' => 2,
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'sticky_header_in_grid',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Sticky Header in grid', 'rebellion'),
				'description' => esc_html__('Set sticky header content to be in grid', 'rebellion'),
				'parent' => $panel_sticky_header,
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "",
					"dependence_show_on_yes" => "#edgtf_sticky_header_in_grid_container"
				)
			)
		);

		$sticky_header_in_grid_container = rebellion_edge_add_admin_container(array(
			'name' => 'sticky_header_in_grid_container',
			'parent' => $panel_sticky_header,
			'hidden_property' => 'sticky_header_in_grid',
			'hidden_value' => 'no'
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_header_grid_background_color',
			'type' => 'color',
			'label' => esc_html__('Grid Background Color', 'rebellion'),
			'description' => esc_html__('Set grid background color for sticky header', 'rebellion'),
			'parent' => $sticky_header_in_grid_container
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_header_grid_transparency',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Grid Transparency', 'rebellion'),
			'description' => esc_html__('Enter transparency for sticky header grid (value from 0 to 1)', 'rebellion'),
			'parent' => $sticky_header_in_grid_container,
			'args' => array(
				'col_width' => 1
			)
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_header_background_color',
			'type' => 'color',
			'label' => esc_html__('Background Color', 'rebellion'),
			'description' => esc_html__('Set background color for sticky header', 'rebellion'),
			'parent' => $panel_sticky_header
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_header_transparency',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Transparency', 'rebellion'),
			'description' => esc_html__('Enter transparency for sticky header (value from 0 to 1)', 'rebellion'),
			'parent' => $panel_sticky_header,
			'args' => array(
				'col_width' => 1
			)
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_header_height',
			'type' => 'text',
			'label' => esc_html__('Sticky Header Height', 'rebellion'),
			'description' => esc_html__('Enter height for sticky header (default is 66px)', 'rebellion'),
			'parent' => $panel_sticky_header,
			'args' => array(
				'col_width' => 2,
				'suffix' => 'px'
			)
		));

		$group_sticky_header_menu = rebellion_edge_add_admin_group(array(
			'title' => esc_html__('Sticky Header Menu', 'rebellion'),
			'name' => 'group_sticky_header_menu',
			'parent' => $panel_sticky_header,
			'description' => esc_html__('Define styles for sticky menu items', 'rebellion'),
		));

		$row1_sticky_header_menu = rebellion_edge_add_admin_row(array(
			'name' => 'row1',
			'parent' => $group_sticky_header_menu
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_color',
			'type' => 'colorsimple',
			'label' => esc_html__('Text Color', 'rebellion'),
			'description' => '',
			'parent' => $row1_sticky_header_menu
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'sticky_hovercolor',
			'type' => 'colorsimple',
			'label' => esc_html__('Hover/Active color', 'rebellion'),
			'description' => '',
			'parent' => $row1_sticky_header_menu
		));

		$row2_sticky_header_menu = rebellion_edge_add_admin_row(array(
			'name' => 'row2',
			'parent' => $group_sticky_header_menu
		));

		rebellion_edge_add_admin_field(
			array(
				'name' => 'sticky_google_fonts',
				'type' => 'fontsimple',
				'label' => esc_html__('Font Family', 'rebellion'),
				'default_value' => '-1',
				'parent' => $row2_sticky_header_menu,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_fontsize',
				'label' => esc_html__('Font Size', 'rebellion'),
				'default_value' => '',
				'parent' => $row2_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_lineheight',
				'label' => esc_html__('Line height', 'rebellion'),
				'default_value' => '',
				'parent' => $row2_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_texttransform',
				'label' => esc_html__('Text transform', 'rebellion'),
				'default_value' => '',
				'options' => rebellion_edge_get_text_transform_array(),
				'parent' => $row2_sticky_header_menu
			)
		);

		$row3_sticky_header_menu = rebellion_edge_add_admin_row(array(
			'name' => 'row3',
			'parent' => $group_sticky_header_menu
		));

		rebellion_edge_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'rebellion'),
				'options' => rebellion_edge_get_font_style_array(),
				'parent' => $row3_sticky_header_menu
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'selectblanksimple',
				'name' => 'sticky_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'rebellion'),
				'options' => rebellion_edge_get_font_weight_array(),
				'parent' => $row3_sticky_header_menu
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'textsimple',
				'name' => 'sticky_letterspacing',
				'label' => esc_html__('Letter Spacing', 'rebellion'),
				'default_value' => '',
				'parent' => $row3_sticky_header_menu,
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$panel_fixed_header = rebellion_edge_add_admin_panel(
			array(
				'title' => esc_html__('Fixed Header', 'rebellion'),
				'name' => 'panel_fixed_header',
				'page' => '_header_page',
				'hidden_property' => 'header_behaviour',
				'hidden_values' => array('sticky-header-on-scroll-up', 'sticky-header-on-scroll-down-up')
			)
		);

		rebellion_edge_add_admin_field(array(
			'name' => 'fixed_header_grid_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Grid Background Color', 'rebellion'),
			'description' => esc_html__('Set grid background color for fixed header', 'rebellion'),
			'parent' => $panel_fixed_header
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'fixed_header_grid_transparency',
			'type' => 'text',
			'default_value' => '',
			'label' => esc_html__('Header Transparency Grid', 'rebellion'),
			'description' => esc_html__('Enter transparency for fixed header grid (value from 0 to 1)', 'rebellion'),
			'parent' => $panel_fixed_header,
			'args' => array(
				'col_width' => 1
			)
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'fixed_header_background_color',
			'type' => 'color',
			'default_value' => '',
			'label' => esc_html__('Background Color', 'rebellion'),
			'description' => esc_html__('Set background color for fixed header', 'rebellion'),
			'parent' => $panel_fixed_header
		));

		rebellion_edge_add_admin_field(array(
			'name' => 'fixed_header_transparency',
			'type' => 'text',
			'label' => esc_html__('Header Transparency', 'rebellion'),
			'description' => esc_html__('Enter transparency for fixed header (value from 0 to 1)', 'rebellion'),
			'parent' => $panel_fixed_header,
			'args' => array(
				'col_width' => 1
			)
		));


		$panel_main_menu = rebellion_edge_add_admin_panel(
			array(
				'title' => esc_html__('Main Menu', 'rebellion'),
				'name' => 'panel_main_menu',
				'page' => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values' => array(
					'header-vertical',
					'header-full-screen'
				)
			)
		);

		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $panel_main_menu,
				'name' => 'main_menu_area_title',
				'title' => esc_html__('Main Menu General Settings', 'rebellion')
			)
		);


		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_item_icon_position',
				'default_value' => 'left',
				'label' => esc_html__('Icon Position in 1st Level Menu', 'rebellion'),
				'description' => esc_html__('Choose position of icon selected in Appearance->Menu->Menu Structure', 'rebellion'),
				'options' => array(
					'left' => esc_html__('Left', 'rebellion'),
					'top' => esc_html__('Top', 'rebellion')
				),
				'args' => array(
					'dependence' => true,
					'hide' => array(
						'left' => '#edgtf_menu_item_icon_position_container'
					),
					'show' => array(
						'top' => '#edgtf_menu_item_icon_position_container'
					)
				)
			)
		);

		$menu_item_icon_position_container = rebellion_edge_add_admin_container(
			array(
				'parent' => $panel_main_menu,
				'name' => 'menu_item_icon_position_container',
				'hidden_property' => 'menu_item_icon_position',
				'hidden_value' => 'left'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $menu_item_icon_position_container,
				'type' => 'text',
				'name' => 'menu_item_icon_size',
				'default_value' => '',
				'label' => esc_html__('Icon Size', 'rebellion'),
				'description' => esc_html__('Choose position of icon selected in Appearance->Menu->Menu Structure', 'rebellion'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_item_style',
				'default_value' => 'small_item',
				'label' => esc_html__('Item Height in 1st Level Menu', 'rebellion'),
				'description' => esc_html__('Choose menu item height', 'rebellion'),
				'options' => array(
					'small_item' => esc_html__('Small', 'rebellion'),
					'large_item' => esc_html__('Big', 'rebellion')
				)
			)
		);

		$drop_down_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'drop_down_group',
				'title' => esc_html__('Main Dropdown Menu', 'rebellion'),
				'description' => esc_html__('Choose a color and transparency for the main menu background (0 = fully transparent, 1 = opaque)', 'rebellion')
			)
		);

		$drop_down_row1 = rebellion_edge_add_admin_row(
			array(
				'parent' => $drop_down_group,
				'name' => 'drop_down_row1',
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_background_color',
				'default_value' => '',
				'label' => esc_html__('Background Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'textsimple',
				'name' => 'dropdown_background_transparency',
				'default_value' => '',
				'label' => esc_html__('Transparency', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_separator_color',
				'default_value' => '',
				'label' => esc_html__('Item Bottom Separator Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $drop_down_row1,
				'type' => 'yesnosimple',
				'name' => 'enable_dropdown_separator_full_width',
				'default_value' => 'yes',
				'label' => esc_html__('Item Separator Full Width', 'rebellion'),
			)
		);

		$drop_down_padding_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'drop_down_padding_group',
				'title' => esc_html__('Main Dropdown Menu Padding', 'rebellion'),
				'description' => esc_html__('Choose a top/bottom padding for dropdown menu', 'rebellion')
			)
		);

		$drop_down_padding_row = rebellion_edge_add_admin_row(
			array(
				'parent' => $drop_down_padding_group,
				'name' => 'drop_down_padding_row',
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $drop_down_padding_row,
				'type' => 'textsimple',
				'name' => 'dropdown_top_padding',
				'default_value' => '',
				'label' => esc_html__('Top Padding', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $drop_down_padding_row,
				'type' => 'textsimple',
				'name' => 'dropdown_bottom_padding',
				'default_value' => '',
				'label' => esc_html__('Bottom Padding', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'select',
				'name' => 'menu_dropdown_appearance',
				'default_value' => 'default',
				'label' => esc_html__('Main Dropdown Menu Appearance', 'rebellion'),
				'description' => esc_html__('Choose appearance for dropdown menu', 'rebellion'),
				'options' => array(
					'dropdown-default' => esc_html__('Default', 'rebellion'),
					'dropdown-slide-from-bottom' => esc_html__('Slide From Bottom', 'rebellion'),
					'dropdown-slide-from-top' => esc_html__('Slide From Top', 'rebellion'),
					'dropdown-animate-height' => esc_html__('Animate Height', 'rebellion'),
					'dropdown-slide-from-left' => esc_html__('Slide From Left', 'rebellion')
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'text',
				'name' => 'dropdown_top_position',
				'default_value' => '',
				'label' => esc_html__('Dropdown position', 'rebellion'),
				'description' => esc_html__('Enter value in percentage of entire header height', 'rebellion'),
				'args' => array(
					'col_width' => 3,
					'suffix' => '%'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_main_menu,
				'type' => 'yesno',
				'name' => 'enable_wide_menu_background',
				'default_value' => 'no',
				'label' => esc_html__('Enable Full Width Background for Wide Dropdown Type', 'rebellion'),
				'description' => esc_html__('Enabling this option will show full width background  for wide dropdown type', 'rebellion'),
			)
		);

		$first_level_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'first_level_group',
				'title' => esc_html__('1st Level Menu', 'rebellion'),
				'description' => esc_html__('Define styles for 1st level in Top Navigation Menu', 'rebellion')
			)
		);

		$first_level_row1 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row1'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => 'menu_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => 'menu_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Hover Text Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row1,
				'type' => 'colorsimple',
				'name' => 'menu_activecolor',
				'default_value' => '',
				'label' => esc_html__('Active Text Color', 'rebellion'),
			)
		);

		$first_level_row2 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row2',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_text_background_color',
				'default_value' => '',
				'label' => esc_html__('Text Background Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_hover_background_color',
				'default_value' => '',
				'label' => esc_html__('Hover Text Background Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row2,
				'type' => 'colorsimple',
				'name' => 'menu_active_background_color',
				'default_value' => '',
				'label' => esc_html__('Active Text Background Color', 'rebellion'),
			)
		);

		$first_level_row3 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row3',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'colorsimple',
				'name' => 'menu_light_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Light Menu Hover Text Color', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'colorsimple',
				'name' => 'menu_light_activecolor',
				'default_value' => '',
				'label' => esc_html__('Light Menu Active Text Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row3,
				'type' => 'colorsimple',
				'name' => 'menu_light_border_color',
				'default_value' => '',
				'label' => esc_html__('Light Menu Border Hover/Active Color', 'rebellion'),
			)
		);

		$first_level_row4 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row4',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row4,
				'type' => 'colorsimple',
				'name' => 'menu_dark_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Dark Menu Hover Text Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row4,
				'type' => 'colorsimple',
				'name' => 'menu_dark_activecolor',
				'default_value' => '',
				'label' => esc_html__('Dark Menu Active Text Color', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row4,
				'type' => 'colorsimple',
				'name' => 'menu_dark_border_color',
				'default_value' => '',
				'label' => esc_html__('Dark Menu Border Hover/Active Color', 'rebellion'),
			)
		);

		$first_level_row5 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row5',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row5,
				'type' => 'fontsimple',
				'name' => 'menu_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'rebellion'),
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row5,
				'type' => 'textsimple',
				'name' => 'menu_fontsize',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row5,
				'type' => 'textsimple',
				'name' => 'menu_hover_background_color_transparency',
				'default_value' => '',
				'label' => esc_html__('Hover Background Color Transparency', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row5,
				'type' => 'textsimple',
				'name' => 'menu_active_background_color_transparency',
				'default_value' => '',
				'label' => esc_html__('Active Background Color Transparency', 'rebellion'),
			)
		);

		$first_level_row6 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row6',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row6,
				'type' => 'selectblanksimple',
				'name' => 'menu_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font Style', 'rebellion'),
				'options' => rebellion_edge_get_font_style_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row6,
				'type' => 'selectblanksimple',
				'name' => 'menu_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font Weight', 'rebellion'),
				'options' => rebellion_edge_get_font_weight_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row6,
				'type' => 'textsimple',
				'name' => 'menu_letterspacing',
				'default_value' => '',
				'label' => esc_html__('Letter Spacing', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row6,
				'type' => 'selectblanksimple',
				'name' => 'menu_texttransform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'rebellion'),
				'options' => rebellion_edge_get_text_transform_array()
			)
		);

		$first_level_row7 = rebellion_edge_add_admin_row(
			array(
				'parent' => $first_level_group,
				'name' => 'first_level_row7',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row7,
				'type' => 'textsimple',
				'name' => 'menu_lineheight',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row7,
				'type' => 'textsimple',
				'name' => 'menu_padding_left_right',
				'default_value' => '',
				'label' => esc_html__('Padding Left/Right', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $first_level_row7,
				'type' => 'textsimple',
				'name' => 'menu_margin_left_right',
				'default_value' => '',
				'label' => esc_html__('Margin Left/Right', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'second_level_group',
				'title' => esc_html__('2nd Level Menu', 'rebellion'),
				'description' => esc_html__('Define styles for 2nd level in Top Navigation Menu', 'rebellion')
			)
		);

		$second_level_row1 = rebellion_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row1'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'rebellion')
			)
		);

		$second_level_row2 = rebellion_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row2',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_fontsize',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_lineheight',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_padding_top_bottom',
				'default_value' => '',
				'label' => esc_html__('Padding Top/Bottom', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_row3 = rebellion_edge_add_admin_row(
			array(
				'parent' => $second_level_group,
				'name' => 'second_level_row3',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font style', 'rebellion'),
				'options' => rebellion_edge_get_font_style_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'rebellion'),
				'options' => rebellion_edge_get_font_weight_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_letterspacing',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_texttransform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'rebellion'),
				'options' => rebellion_edge_get_text_transform_array()
			)
		);

		$second_level_wide_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'second_level_wide_group',
				'title' => esc_html__('2nd Level Wide Menu', 'rebellion'),
				'description' => esc_html__('Define styles for 2nd level in Wide Menu', 'rebellion')
			)
		);

		$second_level_wide_row1 = rebellion_edge_add_admin_row(
			array(
				'parent' => $second_level_wide_group,
				'name' => 'second_level_wide_row1'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_color',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_hovercolor',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'rebellion')
			)
		);

		$second_level_wide_row2 = rebellion_edge_add_admin_row(
			array(
				'parent' => $second_level_wide_group,
				'name' => 'second_level_wide_row2',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_wide_google_fonts',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_fontsize',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_lineheight',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_padding_top_bottom',
				'default_value' => '',
				'label' => esc_html__('Padding Top/Bottom', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$second_level_wide_row3 = rebellion_edge_add_admin_row(
			array(
				'parent' => $second_level_wide_group,
				'name' => 'second_level_wide_row3',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontstyle',
				'default_value' => '',
				'label' => esc_html__('Font style', 'rebellion'),
				'options' => rebellion_edge_get_font_style_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontweight',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'rebellion'),
				'options' => rebellion_edge_get_font_weight_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_letterspacing',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $second_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_texttransform',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'rebellion'),
				'options' => rebellion_edge_get_text_transform_array()
			)
		);

		$third_level_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'third_level_group',
				'title' => esc_html__('3nd Level Menu', 'rebellion'),
				'description' => esc_html__('Define styles for 3nd level in Top Navigation Menu', 'rebellion')
			)
		);

		$third_level_row1 = rebellion_edge_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => 'third_level_row1'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_color_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_hovercolor_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'rebellion')
			)
		);

		$third_level_row2 = rebellion_edge_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => 'third_level_row2',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_google_fonts_thirdlvl',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_fontsize_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_lineheight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$third_level_row3 = rebellion_edge_add_admin_row(
			array(
				'parent' => $third_level_group,
				'name' => 'third_level_row3',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontstyle_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font style', 'rebellion'),
				'options' => rebellion_edge_get_font_style_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_fontweight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'rebellion'),
				'options' => rebellion_edge_get_font_weight_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_letterspacing_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_texttransform_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'rebellion'),
				'options' => rebellion_edge_get_text_transform_array()
			)
		);


		/***********************************************************/
		$third_level_wide_group = rebellion_edge_add_admin_group(
			array(
				'parent' => $panel_main_menu,
				'name' => 'third_level_wide_group',
				'title' => esc_html__('3rd Level Wide Menu', 'rebellion'),
				'description' => esc_html__('Define styles for 3rd level in Wide Menu', 'rebellion')
			)
		);

		$third_level_wide_row1 = rebellion_edge_add_admin_row(
			array(
				'parent' => $third_level_wide_group,
				'name' => 'third_level_wide_row1'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_color_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Color', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row1,
				'type' => 'colorsimple',
				'name' => 'dropdown_wide_hovercolor_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Hover/Active Color', 'rebellion')
			)
		);

		$third_level_wide_row2 = rebellion_edge_add_admin_row(
			array(
				'parent' => $third_level_wide_group,
				'name' => 'third_level_wide_row2',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row2,
				'type' => 'fontsimple',
				'name' => 'dropdown_wide_google_fonts_thirdlvl',
				'default_value' => '-1',
				'label' => esc_html__('Font Family', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_fontsize_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font Size', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row2,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_lineheight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Line Height', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		$third_level_wide_row3 = rebellion_edge_add_admin_row(
			array(
				'parent' => $third_level_wide_group,
				'name' => 'third_level_wide_row3',
				'next' => true
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontstyle_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font style', 'rebellion'),
				'options' => rebellion_edge_get_font_style_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_fontweight_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Font weight', 'rebellion'),
				'options' => rebellion_edge_get_font_weight_array()
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'textsimple',
				'name' => 'dropdown_wide_letterspacing_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Letter spacing', 'rebellion'),
				'args' => array(
					'suffix' => 'px'
				)
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $third_level_wide_row3,
				'type' => 'selectblanksimple',
				'name' => 'dropdown_wide_texttransform_thirdlvl',
				'default_value' => '',
				'label' => esc_html__('Text Transform', 'rebellion'),
				'options' => rebellion_edge_get_text_transform_array()
			)
		);

        $panel_vertical_main_menu = rebellion_edge_add_admin_panel(
            array(
                'title' => esc_html__('Vertical Main Menu', 'rebellion'),
                'name' => 'panel_vertical_main_menu',
                'page' => '_header_page',
                'hidden_property' => 'header_type',
                'hidden_values' => array(
					'header-standard',
					'header-full-screen',
					'header-centered',
					'header-divided',
				)
            )
        );

        $drop_down_group = rebellion_edge_add_admin_group(
            array(
                'parent' => $panel_vertical_main_menu,
                'name' => 'vertical_drop_down_group',
                'title' => esc_html__('Main Dropdown Menu', 'rebellion'),
                'description' => esc_html__('Set a style for dropdown menu', 'rebellion')
            )
        );

        $vertical_drop_down_row1 = rebellion_edge_add_admin_row(
            array(
                'parent' => $drop_down_group,
                'name' => 'edgtf_drop_down_row1',
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'parent' => $vertical_drop_down_row1,
                'type' => 'colorsimple',
                'name' => 'vertical_dropdown_background_color',
                'default_value' => '',
                'label' => esc_html__('Background Color', 'rebellion'),
            )
        );

        $group_vertical_first_level = rebellion_edge_add_admin_group(array(
            'name'			=> 'group_vertical_first_level',
            'title'			=> esc_html__('1st level', 'rebellion'),
            'description'	=> esc_html__('Define styles for 1st level menu', 'rebellion'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_first_level_1 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_1',
                'parent'	=> $group_vertical_first_level
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_1st_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color', 'rebellion'),
                'parent'		=> $row_vertical_first_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_1st_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color', 'rebellion'),
                'parent'		=> $row_vertical_first_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_lineheight',
                'default_value'	=> '',
                'label'			=> esc_html__('Line Height', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_1
            ));

            $row_vertical_first_level_2 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_2',
                'parent'	=> $group_vertical_first_level,
                'next'		=> true
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform', 'rebellion'),
                'options'		=> rebellion_edge_get_text_transform_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_1st_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family', 'rebellion'),
                'parent'		=> $row_vertical_first_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style', 'rebellion'),
                'options'		=> rebellion_edge_get_font_style_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_1st_fontweight',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Weight', 'rebellion'),
                'options'		=> rebellion_edge_get_font_weight_array(),
                'parent'		=> $row_vertical_first_level_2
            ));

            $row_vertical_first_level_3 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_first_level_3',
                'parent'	=> $group_vertical_first_level,
                'next'		=> true
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_1st_letter_spacing',
                'default_value'	=> '',
                'label'			=> esc_html__('Letter Spacing', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_first_level_3
            ));

        $group_vertical_second_level = rebellion_edge_add_admin_group(array(
            'name'			=> 'group_vertical_second_level',
            'title'			=> esc_html__('2nd level', 'rebellion'),
            'description'	=> esc_html__('Define styles for 2nd level menu', 'rebellion'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_second_level_1 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_1',
                'parent'	=> $group_vertical_second_level
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_2nd_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color', 'rebellion'),
                'parent'		=> $row_vertical_second_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_2nd_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color', 'rebellion'),
                'parent'		=> $row_vertical_second_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_lineheight',
                'default_value'	=> '',
                'label'			=> esc_html__('Line Height', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_1
            ));

            $row_vertical_second_level_2 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_2',
                'parent'	=> $group_vertical_second_level,
                'next'		=> true
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform', 'rebellion'),
                'options'		=> rebellion_edge_get_text_transform_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_2nd_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family', 'rebellion'),
                'parent'		=> $row_vertical_second_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style', 'rebellion'),
                'options'		=> rebellion_edge_get_font_style_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_2nd_fontweight',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Weight', 'rebellion'),
                'options'		=> rebellion_edge_get_font_weight_array(),
                'parent'		=> $row_vertical_second_level_2
            ));

            $row_vertical_second_level_3 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_second_level_3',
                'parent'	=> $group_vertical_second_level,
                'next'		=> true
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_2nd_letter_spacing',
                'default_value'	=> '',
                'label'			=> esc_html__('Letter Spacing', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_second_level_3
            ));

        $group_vertical_third_level = rebellion_edge_add_admin_group(array(
            'name'			=> 'group_vertical_third_level',
            'title'			=> esc_html__('3rd level', 'rebellion'),
            'description'	=> esc_html__('Define styles for 3rd level menu', 'rebellion'),
            'parent'		=> $panel_vertical_main_menu
        ));

            $row_vertical_third_level_1 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_1',
                'parent'	=> $group_vertical_third_level
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_3rd_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Color', 'rebellion'),
                'parent'		=> $row_vertical_third_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'colorsimple',
                'name'			=> 'vertical_menu_3rd_hover_color',
                'default_value'	=> '',
                'label'			=> esc_html__('Hover/Active Color', 'rebellion'),
                'parent'		=> $row_vertical_third_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_fontsize',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Size', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_1
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_lineheight',
                'default_value'	=> '',
                'label'			=> esc_html__('Line Height', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_1
            ));

            $row_vertical_third_level_2 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_2',
                'parent'	=> $group_vertical_third_level,
                'next'		=> true
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_texttransform',
                'default_value'	=> '',
                'label'			=> esc_html__('Text Transform', 'rebellion'),
                'options'		=> rebellion_edge_get_text_transform_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'fontsimple',
                'name'			=> 'vertical_menu_3rd_google_fonts',
                'default_value'	=> '-1',
                'label'			=> esc_html__('Font Family', 'rebellion'),
                'parent'		=> $row_vertical_third_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_fontstyle',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Style', 'rebellion'),
                'options'		=> rebellion_edge_get_font_style_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'selectblanksimple',
                'name'			=> 'vertical_menu_3rd_fontweight',
                'default_value'	=> '',
                'label'			=> esc_html__('Font Weight', 'rebellion'),
                'options'		=> rebellion_edge_get_font_weight_array(),
                'parent'		=> $row_vertical_third_level_2
            ));

            $row_vertical_third_level_3 = rebellion_edge_add_admin_row(array(
                'name'		=> 'row_vertical_third_level_3',
                'parent'	=> $group_vertical_third_level,
                'next'		=> true
            ));

            rebellion_edge_add_admin_field(array(
                'type'			=> 'textsimple',
                'name'			=> 'vertical_menu_3rd_letter_spacing',
                'default_value'	=> '',
                'label'			=> esc_html__('Letter Spacing', 'rebellion'),
                'args'			=> array(
                    'suffix'	=> 'px'
                ),
                'parent'		=> $row_vertical_third_level_3
            ));
	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_header_options_map', 4);

}