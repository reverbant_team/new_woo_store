<?php

if ( ! function_exists('rebellion_edge_logo_options_map') ) {

	function rebellion_edge_logo_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug' => '_logo_page',
				'title' => esc_html__('Logo', 'rebellion'),
				'icon' => 'fa fa-coffee'
			)
		);

		$panel_logo = rebellion_edge_add_admin_panel(
			array(
				'page' => '_logo_page',
				'name' => 'panel_logo',
				'title' => esc_html__('Logo', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'parent' => $panel_logo,
				'type' => 'yesno',
				'name' => 'hide_logo',
				'default_value' => 'no',
				'label' => esc_html__('Hide Logo', 'rebellion'),
				'description' => esc_html__('Enabling this option will hide logo image', 'rebellion'),
				'args' => array(
					"dependence" => true,
					"dependence_hide_on_yes" => "#edgtf_hide_logo_container",
					"dependence_show_on_yes" => ""
				)
			)
		);

		$hide_logo_container = rebellion_edge_add_admin_container(
			array(
				'parent' => $panel_logo,
				'name' => 'hide_logo_container',
				'hidden_property' => 'hide_logo',
				'hidden_value' => 'yes'
			)
		);

		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $hide_logo_container,
				'name'   => 'default_logo_title',
				'title'  => esc_html__('Standard, Full Screen and Vertical Header Logo', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'logo_image',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Default', 'rebellion'),
				'description' => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent' => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'logo_image_dark',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_dark.png",
				'label' => esc_html__('Logo Image - Dark', 'rebellion'),
				'description' => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent' => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'logo_image_light',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_light.png",
				'label' => esc_html__('Logo Image - Light', 'rebellion'),
				'description' => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent' => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'logo_image_sticky',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Sticky', 'rebellion'),
				'description' => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent' => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $hide_logo_container,
				'name'   => 'divided_logo_title',
				'title'  => esc_html__('Divided Header Logo', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_divided',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_divided.png",
				'label'         => esc_html__('Logo Image - Divided', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_divided_dark',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_divided_dark.png",
				'label'         => esc_html__('Logo Image - Divided Dark', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_divided_light',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_divided_light.png",
				'label'         => esc_html__('Logo Image - Divided Light', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_divided_sticky',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_divided.png",
				'label'         => esc_html__('Logo Image - Divided Sticky', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_section_title(
			array(
				'parent' => $hide_logo_container,
				'name'   => 'centered_logo_title',
				'title'  => esc_html__('Centered Header Logo', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_centered',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_centered.png",
				'label'         => esc_html__('Logo Image - Centered', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_centered_dark',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_centered_dark.png",
				'label'         => esc_html__('Logo Image - Centered Dark', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_centered_light',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_centered_light.png",
				'label'         => esc_html__('Logo Image - Centered Light', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'logo_image_centered_sticky',
				'type'          => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo_centered.png",
				'label'         => esc_html__('Logo Image - Centered Sticky', 'rebellion'),
				'description'   => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent'        => $hide_logo_container
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name' => 'logo_image_mobile',
				'type' => 'image',
				'default_value' => EDGE_ASSETS_ROOT."/img/logo.png",
				'label' => esc_html__('Logo Image - Mobile', 'rebellion'),
				'description' => esc_html__('Choose a default logo image to display ', 'rebellion'),
				'parent' => $hide_logo_container
			)
		);

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_logo_options_map', 2);

}