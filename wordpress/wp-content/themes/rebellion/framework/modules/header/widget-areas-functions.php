<?php

if(!function_exists('rebellion_edge_register_top_header_areas')) {
    /**
     * Registers widget areas for top header bar when it is enabled
     */
    function rebellion_edge_register_top_header_areas() {
        $top_bar_layout  = rebellion_edge_options()->getOptionValue('top_bar_layout');
            register_sidebar(array(
                'name'          => esc_html__('Top Bar Left', 'rebellion'),
                'id'            => 'edgtf-top-bar-left',
                'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-top-bar-widget">',
                'after_widget'  => '</div>'
            ));

            //register this widget area only if top bar layout is three columns
            if($top_bar_layout === 'three-columns') {
                register_sidebar(array(
                    'name'          => esc_html__('Top Bar Center', 'rebellion'),
                    'id'            => 'edgtf-top-bar-center',
                    'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-top-bar-widget">',
                    'after_widget'  => '</div>'
                ));
            }

            register_sidebar(array(
                'name'          => esc_html__('Top Bar Right', 'rebellion'),
                'id'            => 'edgtf-top-bar-right',
                'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-top-bar-widget">',
                'after_widget'  => '</div>'
            ));
    }

    add_action('widgets_init', 'rebellion_edge_register_top_header_areas');
}

if(!function_exists('rebellion_edge_header_widget_areas')) {
    /**
     * Registers widget areas for standard header type
     */
    function rebellion_edge_header_widget_areas() {
            register_sidebar(array(
                'name'          => esc_html__('Header Widget Area', 'rebellion'),
                'id'            => 'edgtf-header-widget-area',
                'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-header-widget">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear in header', 'rebellion')
            ));
    }

    add_action('widgets_init', 'rebellion_edge_header_widget_areas');
}


if(!function_exists('rebellion_edge_register_mobile_header_areas')) {
    /**
     * Registers widget areas for mobile header
     */
    function rebellion_edge_register_mobile_header_areas() {
        if(rebellion_edge_is_responsive_on()) {
            register_sidebar(array(
                'name'          => esc_html__('Right From Mobile Logo', 'rebellion'),
                'id'            => 'edgtf-right-from-mobile-logo',
                'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-right-from-mobile-logo">',
                'after_widget'  => '</div>',
                'description'   => esc_html__('Widgets added here will appear on the right hand side from the mobile logo', 'rebellion')
            ));
        }
    }

    add_action('widgets_init', 'rebellion_edge_register_mobile_header_areas');
}

if(!function_exists('rebellion_edge_register_sticky_header_areas')) {
	/**
	 * Registers widget area for sticky header
	 */
	function rebellion_edge_register_sticky_header_areas() {
		if(in_array(rebellion_edge_options()->getOptionValue('header_behaviour'), array('sticky-header-on-scroll-up','sticky-header-on-scroll-down-up'))) {
			register_sidebar(array(
				'name'          => esc_html__('Sticky Header Widget Area', 'rebellion'),
				'id'            => 'edgtf-sticky-header-widget-area',
				'before_widget' => '<div id="%1$s" class="widget %2$s edgtf-sticky-right">',
				'after_widget'  => '</div>',
				'description'   => esc_html__('Widgets added here will appear on the right hand side from the sticky menu', 'rebellion')
			));
		}
	}

	add_action('widgets_init', 'rebellion_edge_register_sticky_header_areas');
}