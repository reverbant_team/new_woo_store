<?php

use RebellionEdge\Modules\Header\Lib\HeaderFactory;

if(!function_exists('rebellion_edge_get_header')) {
	/**
	 * Loads header HTML based on header type option. Sets all necessary parameters for header
	 * and defines rebellion_edge_header_type_parameters filter
	 */
	function rebellion_edge_get_header() {

		//will be read from options
		$header_type     = rebellion_edge_get_meta_field_intersect('header_type');
		$header_behavior = rebellion_edge_options()->getOptionValue('header_behaviour');

		if(HeaderFactory::getInstance()->validHeaderObject()) {
			$parameters = array(
				'hide_logo'          => rebellion_edge_options()->getOptionValue('hide_logo') == 'yes' ? true : false,
				'show_sticky'        => in_array($header_behavior, array(
					'sticky-header-on-scroll-up',
					'sticky-header-on-scroll-down-up'
				)) ? true : false,
				'show_fixed_wrapper' => in_array($header_behavior, array('fixed-on-scroll')) ? true : false,
				'widget_area' => rebellion_edge_get_header_widget_area()
			);

			$parameters = apply_filters('rebellion_edge_header_type_parameters', $parameters, $header_type);

			HeaderFactory::getInstance()->getHeaderObject()->loadTemplate($parameters);
		}
	}
}

if(!function_exists('rebellion_edge_get_header_top')) {
	/**
	 * Loads header top HTML and sets parameters for it
	 */
	function rebellion_edge_get_header_top() {

		//generate column width class
		switch(rebellion_edge_options()->getOptionValue('top_bar_layout')) {
			case ('two-columns'):
				$column_widht_class = '50-50';
				break;
			case ('three-columns'):
				$column_widht_class = rebellion_edge_options()->getOptionValue('top_bar_column_widths');
				break;
		}

		$params = array(
			'column_widths'      => $column_widht_class,
			'show_widget_center' => rebellion_edge_options()->getOptionValue('top_bar_layout') == 'three-columns' ? true : false,
			'show_header_top'    => rebellion_edge_is_top_bar_enabled(),
			'top_bar_in_grid'    => rebellion_edge_options()->getOptionValue('top_bar_in_grid') == 'yes' ? true : false
		);

		$params = apply_filters('rebellion_edge_header_top_params', $params);

		rebellion_edge_get_module_template_part('templates/parts/header-top', 'header', '', $params);
	}
}

if(!function_exists('rebellion_edge_get_logo')) {
	/**
	 * Loads logo HTML
	 *
	 * @param $slug
	 */
	function rebellion_edge_get_logo($slug = '') {

		$slug = $slug !== '' ? $slug : rebellion_edge_get_meta_field_intersect('header_type');

		$logo_image_dark = rebellion_edge_options()->getOptionValue('logo_image_dark');
		$logo_image_light = rebellion_edge_options()->getOptionValue('logo_image_light');


		switch ($slug){
			case 'sticky':
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image_sticky');
				break;
			case 'divided':
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image_divided');
				$logo_image_dark  = rebellion_edge_options()->getOptionValue('logo_image_divided_dark');
				$logo_image_light = rebellion_edge_options()->getOptionValue('logo_image_divided_light');
				break;
			case 'divided-sticky':
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image_divided_sticky');
				$logo_image_dark  = rebellion_edge_options()->getOptionValue('logo_image_divided_dark');
				$logo_image_light = rebellion_edge_options()->getOptionValue('logo_image_divided_light');
				break;
			case 'centered':
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image_centered');
				$logo_image_dark  = rebellion_edge_options()->getOptionValue('logo_image_centered_dark');
				$logo_image_light = rebellion_edge_options()->getOptionValue('logo_image_centered_light');
				break;
			case 'centered-sticky':
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image_centered_sticky');
				$logo_image_dark  = rebellion_edge_options()->getOptionValue('logo_image_centered_dark');
				$logo_image_light = rebellion_edge_options()->getOptionValue('logo_image_centered_light');
				break;
			case 'vertical':
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image_vertical');
				break;
			default:
				$logo_image = rebellion_edge_options()->getOptionValue('logo_image');
				break;
		}

		//get logo image dimensions and set style attribute for image link.
		$logo_dimensions = rebellion_edge_get_image_dimensions($logo_image);

		$logo_height = '';
		$logo_styles = '';
		if(is_array($logo_dimensions) && array_key_exists('height', $logo_dimensions)) {
			$logo_height = $logo_dimensions['height'];
			$logo_styles = 'height: '.intval($logo_height / 2).'px;'; //divided with 2 because of retina screens
		}

		$params = array(
			'logo_image'  => $logo_image,
			'logo_image_dark' => $logo_image_dark,
			'logo_image_light' => $logo_image_light,
			'logo_styles' => $logo_styles
		);

		rebellion_edge_get_module_template_part('templates/parts/logo', 'header', $slug, $params);
	}
}

if(!function_exists('rebellion_edge_get_main_menu')) {
	/**
	 * Loads main menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function rebellion_edge_get_main_menu($additional_class = 'edgtf-default-nav') {
		rebellion_edge_get_module_template_part('templates/parts/navigation', 'header', '', array('additional_class' => $additional_class));
	}
}

if(!function_exists('rebellion_edge_get_full_screen_opener')) {
	/**
	 * Loads main menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function rebellion_edge_get_full_screen_opener() {
		rebellion_edge_get_module_template_part('templates/parts/full-screen-opener', 'header', '');
	}
}

if(!function_exists('rebellion_edge_get_sticky_menu')) {
	/**
	 * Loads sticky menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function rebellion_edge_get_sticky_menu($additional_class = 'edgtf-default-nav') {
		rebellion_edge_get_module_template_part('templates/parts/sticky-navigation', 'header', '', array('additional_class' => $additional_class));
	}
}

if(!function_exists('rebellion_edge_get_divided_left_main_menu')) {
	/**
	 * Loads main menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function rebellion_edge_get_divided_left_main_menu($slug = '', $additional_class = 'edgtf-default-nav') {
		rebellion_edge_get_module_template_part('templates/parts/divided-left-navigation', 'header', $slug, array('additional_class' => $additional_class));
	}
}

if(!function_exists('rebellion_edge_get_divided_right_main_menu')) {
	/**
	 * Loads main menu HTML
	 *
	 * @param string $additional_class addition class to pass to template
	 */
	function rebellion_edge_get_divided_right_main_menu($slug = '', $additional_class = 'edgtf-default-nav') {
		rebellion_edge_get_module_template_part('templates/parts/divided-right-navigation', 'header', $slug, array('additional_class' => $additional_class));
	}
}

if(!function_exists('rebellion_edge_get_vertical_main_menu')) {
	/**
	 * Loads vertical menu HTML
	 */
	function rebellion_edge_get_vertical_main_menu() {
		rebellion_edge_get_module_template_part('templates/parts/vertical-navigation', 'header', '');
	}
}



if(!function_exists('rebellion_edge_get_sticky_header')) {
	/**
	 * Loads sticky header behavior HTML
	 */
	function rebellion_edge_get_sticky_header($slug = '') {

		$parameters = array(
			'hide_logo'             => rebellion_edge_options()->getOptionValue('hide_logo') == 'yes' ? true : false,
			'sticky_header_in_grid' => rebellion_edge_get_meta_field_intersect('sticky_header_in_grid') == 'yes' ? true : false,
			'widget_area'			=> rebellion_edge_get_sticky_header_widget_area()
		);

		rebellion_edge_get_module_template_part('templates/behaviors/sticky-header', 'header', $slug, $parameters);
	}
}

if(!function_exists('rebellion_edge_get_mobile_header')) {
	/**
	 * Loads mobile header HTML only if responsiveness is enabled
	 */
	function rebellion_edge_get_mobile_header() {
		if(rebellion_edge_is_responsive_on()) {
			$header_type = rebellion_edge_get_meta_field_intersect('header_type');

			//this could be read from theme options
			$mobile_header_type = 'mobile-header';

			$parameters = array(
				'show_logo'              => rebellion_edge_options()->getOptionValue('hide_logo') == 'yes' ? false : true,
				'menu_opener_icon'       => rebellion_edge_icon_collections()->getMobileMenuIcon(rebellion_edge_options()->getOptionValue('mobile_icon_pack'), true),
				'show_navigation_opener' => has_nav_menu('main-navigation')
			);

			rebellion_edge_get_module_template_part('templates/types/'.$mobile_header_type, 'header', $header_type, $parameters);
		}
	}
}

if(!function_exists('rebellion_edge_get_mobile_logo')) {
	/**
	 * Loads mobile logo HTML. It checks if mobile logo image is set and uses that, else takes normal logo image
	 *
	 * @param string $slug
	 */
	function rebellion_edge_get_mobile_logo($slug = '') {

		$slug = $slug !== '' ? $slug : rebellion_edge_get_meta_field_intersect('header_type');

		//check if mobile logo has been set and use that, else use normal logo
		if(rebellion_edge_options()->getOptionValue('logo_image_mobile') !== '') {
			$logo_image = rebellion_edge_options()->getOptionValue('logo_image_mobile');
		} else {
			$logo_image = rebellion_edge_options()->getOptionValue('logo_image');
		}

		//get logo image dimensions and set style attribute for image link.
		$logo_dimensions = rebellion_edge_get_image_dimensions($logo_image);

		$logo_height = '';
		$logo_styles = '';
		if(is_array($logo_dimensions) && array_key_exists('height', $logo_dimensions)) {
			$logo_height = $logo_dimensions['height'];
			$logo_styles = 'height: '.intval($logo_height / 2).'px'; //divided with 2 because of retina screens
		}

		//set parameters for logo
		$parameters = array(
			'logo_image'      => $logo_image,
			'logo_dimensions' => $logo_dimensions,
			'logo_height'     => $logo_height,
			'logo_styles'     => $logo_styles
		);

		rebellion_edge_get_module_template_part('templates/parts/mobile-logo', 'header', $slug, $parameters);
	}
}

if(!function_exists('rebellion_edge_get_mobile_nav')) {
	/**
	 * Loads mobile navigation HTML
	 */
	function rebellion_edge_get_mobile_nav() {

		$slug = rebellion_edge_get_meta_field_intersect('header_type');

		rebellion_edge_get_module_template_part('templates/parts/mobile-navigation', 'header', $slug);
	}
}


if( !function_exists('rebellion_edge_sticky_header_style') ) {

	/**
	 * Function that return sticky header style
	 */

	function rebellion_edge_sticky_header_style($style) {
		$id = rebellion_edge_get_page_id();
		$class_prefix = rebellion_edge_get_unique_page_class();

		$header_selector = array(
			$class_prefix.' .edgtf-page-header .edgtf-sticky-header .edgtf-sticky-holder'
		);

		$header_style = array();
		$header_background_color = get_post_meta($id, "edgtf_sticky_header_background_color_meta", true);

		if($header_background_color){
			$header_style['background-color'] = $header_background_color;
		}

		$current_style = rebellion_edge_dynamic_css($header_selector, $header_style);
		$style[]       = $current_style;

		return $style;

	}
	add_filter('rebellion_edge_add_page_custom_style', 'rebellion_edge_sticky_header_style');
}

if( !function_exists('rebellion_edge_get_header_widget_area') ) {

	/**
	 * Function that return widget area
	 */

	function rebellion_edge_get_header_widget_area() {

		$id = rebellion_edge_get_page_id();
		$show_widget_area = get_post_meta($id, "edgtf_show_header_widget_area_meta", true);

		$widget_area = '';

		if($show_widget_area != 'no') {
			$custom_widget_area = get_post_meta($id, "edgtf_custom_header_sidebar_meta", true);
			if ($custom_widget_area != '' && is_active_sidebar($custom_widget_area)) {
				$widget_area = $custom_widget_area;
			} elseif(is_active_sidebar('edgtf-header-widget-area')) {
				$widget_area = 'edgtf-header-widget-area';
			}
		}

		return $widget_area;
	}

}

if( !function_exists('rebellion_edge_get_sticky_header_widget_area') ) {

	/**
	 * Function that return widget area
	 */

	function rebellion_edge_get_sticky_header_widget_area() {

		$id = rebellion_edge_get_page_id();
		$show_widget_area = get_post_meta($id, "edgtf_show_header_widget_area_meta", true);

		$widget_area = '';
		if($show_widget_area != 'no') {
			$custom_widget_area = get_post_meta($id, "edgtf_custom_sticky_header_sidebar_meta", true);
			if ($custom_widget_area != '' && is_active_sidebar($custom_widget_area)) {
				$widget_area = $custom_widget_area;
			} elseif(is_active_sidebar('edgtf-sticky-header-widget-area')) {
				$widget_area = 'edgtf-sticky-header-widget-area';
			}
		}

		return $widget_area;
	}

}