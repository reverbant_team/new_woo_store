<?php

//top header bar
add_action('rebellion_edge_before_page_header', 'rebellion_edge_get_header_top');

//mobile header
add_action('rebellion_edge_after_page_header', 'rebellion_edge_get_mobile_header');