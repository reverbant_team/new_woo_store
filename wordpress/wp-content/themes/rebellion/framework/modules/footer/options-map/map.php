<?php

if ( ! function_exists('rebellion_edge_footer_options_map') ) {
	/**
	 * Add footer options
	 */
	function rebellion_edge_footer_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug' => '_footer_page',
				'title' => esc_html__('Footer', 'rebellion'),
				'icon' => 'fa fa-sort-amount-asc'
			)
		);

		$footer_panel = rebellion_edge_add_admin_panel(
			array(
				'title' => esc_html__('Footer', 'rebellion'),
				'name' => 'footer',
				'page' => '_footer_page'
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'uncovering_footer',
				'default_value' => 'no',
				'label' => esc_html__('Uncovering Footer', 'rebellion'),
				'description' => esc_html__('Enabling this option will make Footer gradually appear on scroll', 'rebellion'),
				'parent' => $footer_panel,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'footer_in_grid',
				'default_value' => 'yes',
				'label' => esc_html__('Footer in Grid', 'rebellion'),
				'description' => esc_html__('Enabling this option will place Footer content in grid', 'rebellion'),
				'parent' => $footer_panel,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_top',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Top', 'rebellion'),
				'description' => esc_html__('Enabling this option will show Footer Top area', 'rebellion'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_show_footer_top_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_top_container = rebellion_edge_add_admin_container(
			array(
				'name' => 'show_footer_top_container',
				'hidden_property' => 'show_footer_top',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns',
				'default_value' => '4',
				'label' => esc_html__('Footer Top Columns', 'rebellion'),
				'description' => esc_html__('Choose number of columns for Footer Top area', 'rebellion'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'5' => '3(25%+25%+50%)',
					'6' => '3(50%+25%+25%)',
					'4' => '4'
				),
				'parent' => $show_footer_top_container,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_top_columns_alignment',
				'default_value' => '',
				'label' => esc_html__('Footer Top Columns Alignment', 'rebellion'),
				'description' => esc_html__('Text Alignment in Footer Columns', 'rebellion'),
				'options' => array(
					'left' => esc_html__('Left', 'rebellion'),
					'center' => esc_html__('Center', 'rebellion'),
					'right' => esc_html__('Right', 'rebellion')
				),
				'parent' => $show_footer_top_container,
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'type' => 'yesno',
				'name' => 'show_footer_bottom',
				'default_value' => 'yes',
				'label' => esc_html__('Show Footer Bottom', 'rebellion'),
				'description' => esc_html__('Enabling this option will show Footer Bottom area', 'rebellion'),
				'args' => array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#edgtf_show_footer_bottom_container'
				),
				'parent' => $footer_panel,
			)
		);

		$show_footer_bottom_container = rebellion_edge_add_admin_container(
			array(
				'name' => 'show_footer_bottom_container',
				'hidden_property' => 'show_footer_bottom',
				'hidden_value' => 'no',
				'parent' => $footer_panel
			)
		);


		rebellion_edge_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'footer_bottom_columns',
				'default_value' => '3',
				'label' => esc_html__('Footer Bottom Columns', 'rebellion'),
				'description' => esc_html__('Choose number of columns for Footer Bottom area', 'rebellion'),
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent' => $show_footer_bottom_container,
			)
		);

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_footer_options_map', 10);

}