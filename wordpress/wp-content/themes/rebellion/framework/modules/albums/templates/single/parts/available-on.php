<?php
	$stores = get_post_meta(get_the_ID(), 'edgtf_store_name', true);
	$stores_links = get_post_meta(get_the_ID(), 'edgtf_store_link', true);
	$i = 0;
?>
<?php if(is_array($stores) && count($stores) > 0 && implode($stores) != ''): ?>
	<div class="edgtf-single-album-stores-holder">
		<h4 class="edgtf-single-album-stores-title"><?php esc_html_e('Available On', 'rebellion'); ?></h4>
		<div class="edgtf-single-album-stores clearfix">
			<?php
			foreach($stores as $store) : ?>
				<span class="edgtf-single-album-store">
					<?php if(isset($stores_links[$i]) && $stores_links[$i]) : ?>
						<a class="edgtf-single-album-store-link" href="<?php echo esc_url($stores_links[$i]); ?>" target = "_blank">
							<?php echo edgtf_fn_single_stores_names_and_images($store, $store_type); ?>
						</a>
					<?php else: ?>
						<?php echo edgtf_fn_single_stores_names_and_images($store, $store_type); ?>
					<?php endif; ?>
				</span>
				<?php
				$i++;
			endforeach;
			?>
		</div>
	</div>
<?php endif; ?>