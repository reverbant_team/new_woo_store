<?php
	$lyrics = get_post_meta(get_the_ID(), 'edgtf_track_lyrics', true);
	$titles = get_post_meta(get_the_ID(), 'edgtf_track_title', true);
	$i = 0;
?>
<?php if(is_array($lyrics) && count($lyrics) > 0 && implode($lyrics) !== ''): ?>
	<div class="edgtf-lyrics-holder-inner">
		<h4 class="edgtf-lyrics-holder-title"><?php esc_html_e('Lyrics', 'rebellion'); ?></h4>
		<div class="edgtf-accordion-holder clearfix edgtf-accordion edgtf-initial">
			<?php
				foreach($lyrics as $lyrics) :
					if (($lyrics !== '') && ($titles[$i] !== '')):
			?>
				<p class="clearfix edgtf-title-holder">
					<span class="edgtf-accordion-mark edgtf-left-mark">
						<span class="edgtf-accordion-mark-icon">
							<span class="arrow_carrot-up"></span>
							<span class="arrow_carrot-down"></span>
						</span>
					</span>
					<span class="edgtf-tab-title">
						<span class="edgtf-tab-title-inner">
							<?php echo esc_attr($titles[$i])?>
						</span>
					</span>
				</p>
				<div class="edgtf-accordion-content">
					<div class="edgtf-accordion-content-inner">
							<?php echo nl2br($lyrics); ?>
					</div>
				</div>
			<?php
				endif;
				$i++;
				endforeach;
			?>
		</div>
	</div>
<?php endif; ?>
