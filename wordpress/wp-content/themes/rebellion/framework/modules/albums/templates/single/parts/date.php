<?php 
	$release_date = get_post_meta(get_the_ID(), 'edgtf_album_release_date', true);
	if ($release_date !== '') {
		?>
		<div class="edgtf-album-details edgtf-album-date">
			<span><?php esc_html_e('Release Date:', 'rebellion'); ?></span>
			<span><?php echo esc_attr($release_date); ?></span>
		</div>

		<?php
	}
	    ?>