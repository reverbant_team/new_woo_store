<?php if(have_posts()): while(have_posts()) : the_post(); ?>
		<div class="edgtf-container">
		    <div class="edgtf-container-inner clearfix">
		    	<?php if(post_password_required()) {
	                echo get_the_password_form();
	            } else {
	                //album single html
	                ?>
	            <div class="edgtf-album-compact edgtf-album-<?php echo esc_attr($album_skin); ?> " >
	                <div class="edgtf-two-columns-50-50 clearfix">
						<div class="edgtf-two-columns-50-50-inner clearfix">
							<div class="edgtf-column">
								<div class="edgtf-column-inner">
									<?php
									//get album artist
									rebellion_edge_get_module_template_part('templates/single/parts/album-title','albums');
									//get album featured image
									rebellion_edge_get_module_template_part('templates/single/parts/image','albums');
									?>
									<div class="edgtf-album-details-holder">
										<?php
										//get album artist
										rebellion_edge_get_module_template_part('templates/single/parts/artist','albums');
										
										//get album label
										rebellion_edge_get_module_template_part('templates/single/parts/label','albums');

										//get album date
										rebellion_edge_get_module_template_part('templates/single/parts/date','albums');

										//get album genre
										rebellion_edge_get_module_template_part('templates/single/parts/genre','albums');

										//get album people
										rebellion_edge_get_module_template_part('templates/single/parts/people','albums');
										?>
									</div>
									<div class="edgtf-about-album-holder">
										<?php
											//get about album
											rebellion_edge_get_module_template_part('templates/single/parts/about','albums');
										?>
									</div>
								</div>
							</div>
							<div class="edgtf-column">
								<div class="edgtf-column-inner">
									<div class="edgtf-album-tracks-holder ">
										<?php
											//get album tracks
											rebellion_edge_get_module_template_part('templates/single/parts/playlist','albums','',$params);
										?>
									</div>
									<?php
									//get album available on
									rebellion_edge_get_module_template_part('templates/single/parts/available-on','albums','', array('store_type' => $store_type));

									//get album review
									rebellion_edge_get_module_template_part('templates/single/parts/follow_and_share','albums');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
		        <?php } 
					//load comments
					rebellion_edge_get_module_template_part('templates/single/parts/comments', 'albums');
				?>
	        </div>
	    </div>
	    <div class="edgtf-album-navigation-holder edgtf-compact-<?php echo esc_attr($album_skin); ?>">
			<?php
				//get navigation
				rebellion_edge_get_module_template_part('templates/single/parts/navigation','albums','',$params);
			?>
		</div>
<?php
	endwhile;
	endif;
?>