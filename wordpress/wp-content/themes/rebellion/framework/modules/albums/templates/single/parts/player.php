<?php 
    $id = get_the_ID();
    $args = array(
			'type'		=> 'simple',
			'album'		=> $id,
			'bg_color'	=> '',
			'skin'		=> '',
	);

    echo rebellion_edge_execute_shortcode('edgtf_album_player', $args);
?>