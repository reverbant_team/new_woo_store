<?php if ( has_post_thumbnail() ) { ?>
	<div class="edgtf-album-image">
			<?php the_post_thumbnail('full'); ?>
	</div>
<?php } ?>