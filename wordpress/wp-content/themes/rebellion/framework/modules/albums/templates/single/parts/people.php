<?php 
	$people = get_post_meta(get_the_ID(), 'edgtf_album_people', true);
	if ($people !== '') {
		?>
		<div class="edgtf-album-details edgtf-album-people">
			<span><?php esc_html_e('People:', 'rebellion'); ?></span>
			<span><?php echo esc_attr($people); ?></span>
		</div>

		<?php
	}
	    ?>