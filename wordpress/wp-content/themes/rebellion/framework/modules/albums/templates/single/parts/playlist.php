<h4 class="edgtf-tracks-holder-title"><?php esc_html_e('Tracklist', 'rebellion'); ?></h4>
<?php 
    $id = get_the_ID();
    if ($album_skin == 'light') {
    	$album_skin = 'default';
    }
    $args = array(
			'album'		=> $id,
			'album_skin' => $album_skin,
	);

    echo rebellion_edge_execute_shortcode('edgtf_album', $args);
?>