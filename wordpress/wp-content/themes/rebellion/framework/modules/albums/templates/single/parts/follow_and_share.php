<?php if(rebellion_edge_options()->getOptionValue('enable_social_share') == 'yes'
        && rebellion_edge_options()->getOptionValue('enable_social_share_on_album') == 'yes') : ?>
<div class="edgtf-album-follow-share-holder">
    <h4 class='edgtf-album-follow-share-holder-title'><?php esc_html_e('Share', 'rebellion'); ?></h4>
    <div class="edgtf-album-follow-share">
     	<?php echo rebellion_edge_get_social_share_html() ?>
    </div>
</div>
<?php endif; ?>