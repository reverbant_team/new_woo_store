<?php 
$videolink = get_post_meta( get_the_ID(), "edgtf_album_video_link_meta", true );
if ($videolink !== ''): ?>
	<h2 class="edgtf-latest-video-holder-title"><?php esc_html_e('Latest Video', 'rebellion'); ?></h2>
	<div class="edgtf-latest-video">
	<?php	
		$embed     = wp_oembed_get( $videolink );
		print $embed;
	?>
	</div>
<?php endif; ?>