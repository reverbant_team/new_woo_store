<h4 class="edgtf-about-album-holder-title"><?php esc_html_e('About Album', 'rebellion'); ?></h4>
<div class="edgtf-about-album-content">
	<?php the_content(); ?>
</div>