<?php

if(!function_exists('rebellion_edge_single_album')) {
    function rebellion_edge_single_album() {
    	$back_to_link = get_post_meta( get_the_ID(), 'edgtf_album_back_to_link', true );
    	$album_type = rebellion_edge_get_meta_field_intersect('album_type');
		$store_type = ($album_type == 'comprehensive') ? 'image' : 'text';
        $params = array(
        	'back_to_link' => $back_to_link,
        	'store_type' => $store_type,
        );

		$album_skin = get_post_meta( get_the_ID(), 'edgtf_album_skin', true );
		$params['album_skin'] = ($album_skin == '')? 'default' : $album_skin;

        rebellion_edge_get_module_template_part('templates/single/'.$album_type, 'albums', '', $params);
    }
}

if(!function_exists('edgtf_fn_single_stores_names_and_images')) {
	function edgtf_fn_single_stores_names_and_images($store, $type = '') {

		switch ($store):
			case 'google-play':
				$name = esc_html__('Google Play', 'rebellion');
				break;
			case 'bandcamp':
				$name = esc_html__('Bandcamp', 'rebellion');
				break;
			case 'spotify':
				$name = esc_html__('Spotify', 'rebellion');
				break;
			case 'amazonmp3':
				$name = esc_html__('AmazonMP3', 'rebellion');
				break;
			case 'deezer':
				$name = esc_html__('Deezer', 'rebellion');
				break;
			default:
				$name = esc_html__('iTunes', 'rebellion');
				break;
			endswitch;

		$image = '<img src="'.EDGE_ASSETS_ROOT.'/img/stores/'.$store.'.png" alt="'.$name.'" />';

		if($type == 'image') {
			return $image;
		}

		return $name;

	}
}