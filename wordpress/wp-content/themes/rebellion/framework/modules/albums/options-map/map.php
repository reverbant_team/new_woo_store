<?php

if ( ! function_exists('rebellion_edge_albums_options_map') ) {

	function rebellion_edge_albums_options_map() {

		rebellion_edge_add_admin_page(array(
			'slug'  => '_albums',
			'title' => esc_html__('Albums','rebellion'),
			'icon'  => 'fa fa-music'
		));

		$panel = rebellion_edge_add_admin_panel(array(
			'title' => esc_html__('Albums','rebellion'),
			'name'  => 'panel_albums',
			'page'  => '_albums'
		));

		rebellion_edge_add_admin_field(
			array(
				'name'			=> 'album_type',
				'type'			=> 'select',
				'label'			=> esc_html__('Album Type', 'rebellion'),
				'default_value'	=> 'comprehensive',
				'options' => array(
					'comprehensive' => esc_html__('Album Comprehensive','rebellion'),
					'minimal'		=> esc_html__('Album Minimal','rebellion'),
					'compact'		=> esc_html__('Album Compact','rebellion')
				),
				'parent'      => $panel
			)
		);

		rebellion_edge_add_admin_field(array(
			'name'          => 'album_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments','rebellion'),
			'description'   => esc_html__('Enabling this option will show comments on your album.','rebellion'),
			'parent'        => $panel,
			'default_value' => 'no'
		));

		rebellion_edge_add_admin_field(array(
			'name'          => 'album_pagination',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Pagination','rebellion'),
			'description'   => esc_html__('Enabling this option will turn on album pagination functionality.','rebellion'),
			'parent'        => $panel,
			'default_value' => 'no',
		));

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_albums_options_map', 14);

}