<?php

if(!function_exists('rebellion_edge_bandsintown_assets')) {
	/**
	 * Loads all assets for bandsintown plugin
	 */
	function rebellion_edge_bandsintown_assets() {
		wp_enqueue_style('rebellion_edge_bandsintown', EDGE_ASSETS_ROOT.'/css/bandsintown.min.css');

		if(rebellion_edge_is_responsive_on()) {
			wp_enqueue_style('rebellion_edge_bandsintown_responsive', EDGE_ASSETS_ROOT.'/css/bandsintown-responsive.min.css');
		}
	}

	add_action('wp_enqueue_scripts', 'rebellion_edge_bandsintown_assets', 20);
}
