<?php

if(rebellion_edge_visual_composer_installed()) {

	if(!function_exists('rebellion_edge_bandsintown_events')) {
		function rebellion_edge_bandsintown_events() {
			vc_map(array(
				'name'                    => esc_html__('Bandsintown Events', 'rebellion'),
				'base'                    => 'bandsintown_events',
				'category'                =>  esc_html__('by EDGE', 'rebellion'),
				'icon'                    => 'icon-wpb-bandsintown-events extended-custom-icon',
				'show_settings_on_create' => true,
				'params'                  => array(
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Artist', 'rebellion'),
						'param_name'  => 'artist',
						'admin_label' => true
					),
					array(
						'type'        => 'textfield',
						'heading'     => esc_html__('Display Limit', 'rebellion'),
						'param_name'  => 'display_limit',
						'admin_label' => true
					)
				)
			));
		}

		add_action('vc_before_init', 'rebellion_edge_bandsintown_events');
	}

}

