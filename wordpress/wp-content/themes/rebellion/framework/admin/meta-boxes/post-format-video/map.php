<?php

/*** Video Post Format ***/

if(!function_exists('rebellion_edge_map_post_format_video_meta_fields')) {

    function rebellion_edge_map_post_format_video_meta_fields() {


		$video_post_format_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Video Post Format', 'rebellion'),
				'name' 	=> 'post_format_video_meta'
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_video_type_meta',
				'type'        => 'select',
				'label'       => esc_html__('Video Type', 'rebellion'),
				'description' => esc_html__('Choose video type', 'rebellion'),
				'parent'      => $video_post_format_meta_box,
				'default_value' => 'youtube',
				'options'     => array(
					'youtube' => esc_html__('Youtube', 'rebellion'),
					'vimeo' => esc_html__('Vimeo', 'rebellion'),
					'self' => esc_html__('Self Hosted', 'rebellion')
				),
				'args' => array(
				'dependence' => true,
				'hide' => array(
					'youtube' => '#edgtf_edgtf_video_self_hosted_container',
					'vimeo' => '#edgtf_edgtf_video_self_hosted_container',
					'self' => '#edgtf_edgtf_video_embedded_container'
				),
				'show' => array(
					'youtube' => '#edgtf_edgtf_video_embedded_container',
					'vimeo' => '#edgtf_edgtf_video_embedded_container',
					'self' => '#edgtf_edgtf_video_self_hosted_container')
			)
			)
		);

		$edgtf_video_embedded_container = rebellion_edge_add_admin_container(
			array(
				'parent' => $video_post_format_meta_box,
				'name' => 'edgtf_video_embedded_container',
				'hidden_property' => 'edgtf_video_type_meta',
				'hidden_value' => 'self'
			)
		);

		$edgtf_video_self_hosted_container = rebellion_edge_add_admin_container(
			array(
				'parent' => $video_post_format_meta_box,
				'name' => 'edgtf_video_self_hosted_container',
				'hidden_property' => 'edgtf_video_type_meta',
				'hidden_values' => array('youtube', 'vimeo')
			)
		);



		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_id_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video ID', 'rebellion'),
				'description' => esc_html__('Enter Video ID', 'rebellion'),
				'parent'      => $edgtf_video_embedded_container,

			)
		);


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_image_meta',
				'type'        => 'image',
				'label'       => esc_html__('Video Image', 'rebellion'),
				'description' => esc_html__('Upload video image', 'rebellion'),
				'parent'      => $edgtf_video_self_hosted_container,

			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_webm_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video WEBM', 'rebellion'),
				'description' => esc_html__('Enter video URL for WEBM format', 'rebellion'),
				'parent'      => $edgtf_video_self_hosted_container,

			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_mp4_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video MP4', 'rebellion'),
				'description' => esc_html__('Enter video URL for MP4 format', 'rebellion'),
				'parent'      => $edgtf_video_self_hosted_container,

			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_video_ogv_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Video OGV', 'rebellion'),
				'description' => esc_html__('Enter video URL for OGV format', 'rebellion'),
				'parent'      => $edgtf_video_self_hosted_container,

			)
		);
	}

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_post_format_video_meta_fields');
}