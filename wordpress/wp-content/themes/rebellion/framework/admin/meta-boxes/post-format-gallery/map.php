<?php

/*** Gallery Post Format ***/
if(!function_exists('rebellion_edge_map_post_format_gallery_meta_fields')) {

    function rebellion_edge_map_post_format_gallery_meta_fields() {

		$gallery_post_format_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Gallery Post Format', 'rebellion'),
				'name' 	=> 'post_format_gallery_meta'
			)
		);

		rebellion_edge_add_multiple_images_field(
			array(
				'name'        => 'edgtf_post_gallery_images_meta',
				'label'       => esc_html__('Gallery Images', 'rebellion'),
				'description' => esc_html__('Choose your gallery images', 'rebellion'),
				'parent'      => $gallery_post_format_meta_box,
			)
		);
	}

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_post_format_gallery_meta_fields');
}
