<?php

//Testimonials

if(!function_exists('rebellion_edge_map_testimonials_meta_fields')) {

    function rebellion_edge_map_testimonials_meta_fields() {

        $testimonial_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('testimonials'),
                'title' => esc_html__('Testimonial', 'rebellion'),
                'name' => 'testimonial_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        	=> 'edgtf_testimonial_title',
                    'type'        	=> 'text',
                    'label'       	=> esc_html__('Title', 'rebellion'),
                    'description' 	=> esc_html__('Enter testimonial title', 'rebellion'),
                    'parent'      	=> $testimonial_meta_box,
                )
            );


            rebellion_edge_add_meta_box_field(
                array(
                    'name'        	=> 'edgtf_testimonial_author',
                    'type'        	=> 'text',
                    'label'       	=> esc_html__('Author', 'rebellion'),
                    'description' 	=> esc_html__('Enter author name', 'rebellion'),
                    'parent'      	=> $testimonial_meta_box,
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        	=> 'edgtf_testimonial_author_position',
                    'type'        	=> 'text',
                    'label'       	=> esc_html__('Job Position', 'rebellion'),
                    'description' 	=> esc_html__('Enter job position', 'rebellion'),
                    'parent'      	=> $testimonial_meta_box,
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        	=> 'edgtf_testimonial_text',
                    'type'        	=> 'text',
                    'label'       	=> esc_html__('Text', 'rebellion'),
                    'description' 	=> esc_html__('Enter testimonial text', 'rebellion'),
                    'parent'      	=> $testimonial_meta_box,
                )
            );
        }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_testimonials_meta_fields');
}