<?php

if(!function_exists('rebellion_edge_map_sidebar_meta_fields')) {

    function rebellion_edge_map_sidebar_meta_fields() {

        $custom_sidebars = rebellion_edge_get_custom_sidebars();

        $sidebar_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('page', 'post', 'event', 'album'),
                'title' => esc_html__('Sidebar', 'rebellion'),
                'name' => 'sidebar_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_sidebar_meta',
                    'type'        => 'select',
                    'label'       => esc_html__('Layout', 'rebellion'),
                    'description' => esc_html__('Choose the sidebar layout', 'rebellion'),
                    'parent'      => $sidebar_meta_box,
                    'options'     => array(
        						''			        => esc_html__('Default', 'rebellion'),
        						'no-sidebar'		=> esc_html__('No Sidebar', 'rebellion'),
        						'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'rebellion'),
        						'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'rebellion'),
        						'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'rebellion'),
        						'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'rebellion'),
        					)
                )
            );

        if(count($custom_sidebars) > 0) {
            rebellion_edge_add_meta_box_field(array(
                'name' => 'edgtf_custom_sidebar_meta',
                'type' => 'selectblank',
                'label' => esc_html__('Choose Widget Area in Sidebar', 'rebellion'),
                'description' => esc_html__('Choose Custom Widget area to display in Sidebar"', 'rebellion'),
                'parent' => $sidebar_meta_box,
                'options' => $custom_sidebars
            ));
        }
    }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_sidebar_meta_fields');
}
