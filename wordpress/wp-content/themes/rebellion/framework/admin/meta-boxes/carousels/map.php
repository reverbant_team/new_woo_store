<?php

//Carousels

if(!function_exists('rebellion_edge_map_carousels_meta_fields')) {

    function rebellion_edge_map_carousels_meta_fields() {

        $carousel_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('carousels'),
                'title' => esc_html__('Carousel', 'rebellion'),
                'name' => 'carousel_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_carousel_image',
                    'type'        => 'image',
                    'label'       => esc_html__('Carousel Image', 'rebellion'),
                    'description' => esc_html__('Choose carousel image (min width needs to be 215px)', 'rebellion'),
                    'parent'      => $carousel_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_carousel_hover_image',
                    'type'        => 'image',
                    'label'       => esc_html__('Carousel Hover Image', 'rebellion'),
                    'description' => esc_html__('Choose carousel hover image (min width needs to be 215px)', 'rebellion'),
                    'parent'      => $carousel_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_carousel_item_link',
                    'type'        => 'text',
                    'label'       => esc_html__('Link', 'rebellion'),
                    'description' => esc_html__('Enter the URL to which you want the image to link to (e.g. http://www.example.com)', 'rebellion'),
                    'parent'      => $carousel_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_carousel_item_target',
                    'type'        => 'selectblank',
                    'label'       => esc_html__('Target', 'rebellion'),
                    'description' => esc_html__('Specify where to open the linked document', 'rebellion'),
                    'parent'      => $carousel_meta_box,
                    'options' => array(
                    	'_self' => esc_html__('Self', 'rebellion'),
                    	'_blank' => esc_html__('Blank', 'rebellion')
                	)
                )
            );
        }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_carousels_meta_fields');
}
