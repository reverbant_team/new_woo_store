<?php

//Albums

if(!function_exists('rebellion_edge_map_album_meta_fields')) {

    function rebellion_edge_map_album_meta_fields() {

		$album_meta_box = rebellion_edge_add_meta_box(
		    array(
		        'scope' => array('album'),
		        'title' => esc_html__('Album', 'rebellion'),
		        'name' => 'album_meta_box'
		    )
		);

		rebellion_edge_add_meta_box_field(
				array(
					'name'        => 'edgtf_album_type_meta',
					'type'        => 'select',
					'label'       => esc_html__('Album Type', 'rebellion'),
					'description' => '',
					'options' => array(
		                '' => esc_html__('Default','rebellion'),
		                'comprehensive' => esc_html__('Album Comprehensive','rebellion'),
		                'minimal'		=> esc_html__('Album Minimal','rebellion'),
		                'compact'		=> esc_html__('Album Compact','rebellion')
		            ),
					'parent'      => $album_meta_box
				)
		);

		rebellion_edge_add_meta_box_field(
				array(
					'name'        => 'edgtf_album_skin',
					'type'        => 'select',
					'label'       => esc_html__('Album Skin', 'rebellion'),
					'description' => '',
					'options' => array(
		                'light' => esc_html__('Light','rebellion'),
		                'dark' => esc_html__('Dark','rebellion')
		            ),
					'parent'      => $album_meta_box
				)
		);

		rebellion_edge_add_meta_box_field(
				array(
					'name'        => 'edgtf_album_release_date',
					'type'        => 'date',
					'label'       => esc_html__('Release Date', 'rebellion'),
					'description' => '',
					'parent'      => $album_meta_box
				)
		);
		rebellion_edge_add_meta_box_field(
				array(
					'name'        => 'edgtf_album_people',
					'type'        => 'text',
					'label'       => esc_html__('People', 'rebellion'),
					'description' => '',
					'parent'      => $album_meta_box
				)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_album_video_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Latest Video Link', 'rebellion'),
				'description' => esc_html__('Enter Video Link', 'rebellion'),
				'parent'      => $album_meta_box,

			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_album_back_to_link',
				'type'        => 'text',
				'label'       => esc_html__('"Back To" Link','rebellion'),
				'description' => esc_html__('Choose "Back To" page to link from album single page', 'rebellion'),
				'parent'      => $album_meta_box,

			)
		);


		$tracks_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' => array('album'),
				'title' => esc_html__('Tracks', 'rebellion'),
				'name' => 'tracks_meta_box'
			)
		);
		rebellion_edge_add_repeater_field(
			array(
				'name'        => 'edgtf_tracks_repeater',
				'label'       => esc_html__('Tracks', 'rebellion'),
				'fields' => array(
					array(
						'name'        => 'edgtf_track_file',
						'type'        => 'file',
						'label'       => esc_html__('Audio File', 'rebellion'),
					),
					array(
						'name'        => 'edgtf_track_title',
						'type'        => 'text',
						'label'       => esc_html__('Title', 'rebellion'),
					),
					array(
						'name'        => 'edgtf_track_buy_link',
						'type'        => 'text',
						'label'       => esc_html__('Buy Link', 'rebellion'),
					),
					array(
						'name'        	=> 'edgtf_track_free_download',
						'type'        	=> 'yesno',
						'default_value'  => 'no',
						'label'      	=> esc_html__('Free Download', 'rebellion'),
					),
					array(
						'name'        => 'edgtf_track_video_link',
						'type'        => 'text',
						'label'       => esc_html__('Video Link', 'rebellion'),
					),
					array(
						'name'        => 'edgtf_track_lyrics',
						'type'        => 'textarea',
						'label'       => esc_html__('Lyrics', 'rebellion')
					)

				),
				'parent'      => $tracks_meta_box,
				'description' => ''
			)
		);

		$reviews_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' => array('album'),
				'title' => esc_html__('Reviews', 'rebellion'),
				'name' => 'reviews_meta_box'
			)
		);
		rebellion_edge_add_repeater_field(
			array(
				'name'        => 'edgtf_reviews_repeater',
				'label'       => esc_html__('Reviews', 'rebellion'),
				'fields' => array(
					array(
						'name'        => 'edgtf_review_author',
						'type'        => 'text',
						'label'       => esc_html__('Review Author', 'rebellion'),
					),
					array(
						'name'        => 'edgtf_review_text',
						'type'        => 'textarea',
						'label'       => esc_html__('Review Title', 'rebellion')
					)

				),
				'parent'      => $reviews_meta_box
			)
		);
		$stores_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' => array('album'),
				'title' => esc_html__('Stores', 'rebellion'),
				'name' => 'stores_meta_box'
			)
		);
		rebellion_edge_add_repeater_field(
			array(
				'name'        => 'edgtf_stores_repeater',
				'label'       => esc_html__('Stores', 'rebellion'),
				'fields' => array(
					array(
						'name'        => 'edgtf_store_name',
						'type'        => 'select',
						'options'	  => array(
							'itunes'		=> esc_html__('iTunes', 'rebellion'),
							'google-play'	=> esc_html__('Google Play', 'rebellion'),
							'bandcamp'		=> esc_html__('Bandcamp', 'rebellion'),
							'spotify'		=> esc_html__('Spotify', 'rebellion'),
							'amazonmp3'		=> esc_html__('AmazonMP3', 'rebellion'),
							'deezer'		=> esc_html__('Deezer', 'rebellion')
						),
						'label'       => esc_html__('Store', 'rebellion'),
					),
					array(
						'name'        => 'edgtf_store_link',
						'type'        => 'text',
						'label'       => esc_html__('Store Link', 'rebellion')
					)

				),
				'parent'      => $stores_meta_box
			)
		);
    }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_album_meta_fields');
}
