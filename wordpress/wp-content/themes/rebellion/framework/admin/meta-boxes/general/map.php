<?php

if(!function_exists('rebellion_edge_map_general_meta_fields')) {

    function rebellion_edge_map_general_meta_fields() {

		$general_meta_box = rebellion_edge_add_meta_box(
		    array(
		        'scope' => array('page', 'post', 'event', 'album'),
		        'title' => esc_html__('General', 'rebellion'),
		        'name' => 'general_meta'
		    )
		);

			rebellion_edge_add_meta_box_field(
				array(
					'name' => 'edgtf_first_color_meta',
					'type' => 'color',
					'default_value' => '',
					'label' => esc_html__('First Main Color', 'rebellion'),
					'description' => esc_html__('Choose background color for page content', 'rebellion'),
					'parent' => $general_meta_box
				)
			);
			
			rebellion_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_predefined_color_skin_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__('Predefined Color Skin', 'rebellion'),
					'description'   => esc_html__('Choose predefined color skin', 'rebellion'),
					'parent'        => $general_meta_box,
					'options'       => array(
						''			=> esc_html__('Default/Dark', 'rebellion'),
						'dark'		=> esc_html__('Dark', 'rebellion'),
						'light'   	=> esc_html__('Light', 'rebellion')
					)
				)
			);

		    rebellion_edge_add_meta_box_field(
		        array(
		            'name' => 'edgtf_page_background_color_meta',
		            'type' => 'color',
		            'default_value' => '',
		            'label' => esc_html__('Page Background Color', 'rebellion'),
		            'description' => esc_html__('Choose background color for page content', 'rebellion'),
		            'parent' => $general_meta_box
		        )
		    );

			$content_background_image_group = rebellion_edge_add_admin_group(array(
				'name'   => 'edgtf_content_background_image_group',
				'parent' => $general_meta_box,
				'title'  => esc_html__('Page Background Image', 'rebellion')
			));

			$content_background_image_row1 = rebellion_edge_add_admin_row(array(
				'name'   => 'edgtf_content_background_image_row1',
				'parent' => $content_background_image_group
			));

			rebellion_edge_add_meta_box_field(array(
				'name'   => 'edgtf_page_background_image_meta',
				'type'   => 'imagesimple',
				'label'  => esc_html__('Background Image', 'rebellion'),
				'parent' => $content_background_image_row1
			));

			rebellion_edge_add_meta_box_field(array(
				'name'          => 'edgtf_page_background_image_repeat_meta',
				'type'          => 'yesnosimple',
				'default_value' => 'no',
				'label'         => esc_html__('Repeat Background Image?', 'rebellion'),
				'parent'        => $content_background_image_row1
			));

			rebellion_edge_add_meta_box_field(
				array(
					'name' => 'edgtf_page_background_image_vertical_position_meta',
					'type' => 'selectsimple',
					'default_value' => 'center',
					'label' => esc_html__('Background Image Vertical Position', 'rebellion'),
					'options'       => array(
						'center'	=> esc_html__('Center', 'rebellion'),
						'top'		=> esc_html__('Top', 'rebellion'),
						'bottom'   	=> esc_html__('Bottom', 'rebellion')
					),
					'parent' => $content_background_image_row1
				)
			);

			rebellion_edge_add_meta_box_field(
				array(
					'name' => 'edgtf_page_background_image_size_meta',
					'type' => 'selectsimple',
					'default_value' => 'contain',
					'label' => esc_html__('Background Image Size', 'rebellion'),
					'options'       => array(
						'contain'	=> esc_html__('Contain', 'rebellion'),
						'cover'		=> esc_html__('Cover', 'rebellion')
					),
					'parent' => $content_background_image_row1
				)
			);

		rebellion_edge_add_meta_box_field(
				array(
					'name' => 'edgtf_page_padding_meta',
					'type' => 'text',
					'default_value' => '',
					'label' => esc_html__('Page Padding', 'rebellion'),
					'description' => esc_html__('Insert padding in format 10px 10px 10px 10px', 'rebellion'),
					'parent' => $general_meta_box
				)
			);

			rebellion_edge_add_meta_box_field(array(
				'name'          => 'edgtf_overlapping_content_enable_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__('Enable Overlapping Content', 'rebellion'),
				'description'   => esc_html__('Enabling this option will make content overlap title area', 'rebellion'),
				'parent'        => $general_meta_box
			));

		    rebellion_edge_add_meta_box_field(
		        array(
		            'name' => 'edgtf_page_slider_meta',
		            'type' => 'text',
		            'default_value' => '',
		            'label' => esc_html__('Slider Shortcode', 'rebellion'),
		            'description' => esc_html__('Paste your slider shortcode here', 'rebellion'),
		            'parent' => $general_meta_box
		        )
		    );

			$boxed_option      = rebellion_edge_options()->getOptionValue('boxed');
			$boxed_default_dependency = array(
				'' => '#edgtf_boxed_container'
			);

			$boxed_show_array = array(
				'yes' => '#edgtf_boxed_container'
			);

			$boxed_hide_array = array(
				'no' => '#edgtf_boxed_container'
			);

			if($boxed_option === 'yes') {
				$boxed_show_array = array_merge($boxed_show_array, $boxed_default_dependency);
				$temp_boxed_no = array(
					'hidden_value' => 'no'
				);
			} else {
				$boxed_hide_array = array_merge($boxed_hide_array, $boxed_default_dependency);
				$temp_boxed_no = array(
					'hidden_values'   => array('','no')
				);
			}

			rebellion_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_boxed_meta',
					'type'          => 'select',
					'label'         => esc_html__('Boxed Layout', 'rebellion'),
					'description'   => '',
					'parent'        => $general_meta_box,
					'default_value' => '',
					'options'     => array(
						''		=> esc_html__('Default', 'rebellion'),
						'yes'	=> esc_html__('Yes', 'rebellion'),
						'no'	=> esc_html__('No', 'rebellion')
					),
					'args' => array(
						"dependence" => true,
						'show'       => $boxed_show_array,
						'hide'       => $boxed_hide_array
					)
				)
			);

			$boxed_container = rebellion_edge_add_admin_container_no_style(
				array_merge(
					array(
						'parent'            => $general_meta_box,
						'name'              => 'boxed_container',
						'hidden_property'   => 'edgtf_boxed_meta'
					),
					$temp_boxed_no
				)
			);

			rebellion_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_page_background_color_in_box_meta',
					'type'          => 'color',
					'label'         => esc_html__('Page Background Color', 'rebellion'),
					'description'   => esc_html__('Choose the page background color outside box.', 'rebellion'),
					'parent'        => $boxed_container
				)
			);

			rebellion_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_boxed_background_image_meta',
					'type'          => 'image',
					'label'         => esc_html__('Background Image', 'rebellion'),
					'description'   => esc_html__('Choose an image to be displayed in background', 'rebellion'),
					'parent'        => $boxed_container
				)
			);

			rebellion_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_boxed_background_image_repeating_meta',
					'type'          => 'select',
					'default_value' => 'no',
					'label'         => esc_html__('Use Background Image as Pattern', 'rebellion'),
					'description'   => esc_html__('Set this option to "yes" to use the background image as repeating pattern', 'rebellion'),
					'parent'        => $boxed_container,
					'options'       => array(
						'no'	=>	esc_html__('No', 'rebellion'),
						'yes'	=>	esc_html__('Yes', 'rebellion')

					)
				)
			);

			rebellion_edge_add_meta_box_field(
				array(
					'name'          => 'edgtf_boxed_background_image_attachment_meta',
					'type'          => 'select',
					'default_value' => 'fixed',
					'label'         => esc_html__('Background Image Behaviour', 'rebellion'),
					'description'   => esc_html__('Choose background image behaviour', 'rebellion'),
					'parent'        => $boxed_container,
					'options'       => array(
						'fixed'     => esc_html__('Fixed', 'rebellion'),
						'scroll'    => esc_html__('Scroll', 'rebellion')
					)
				)
			);
		}

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_general_meta_fields');
}