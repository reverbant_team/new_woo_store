<?php

//Events

if(!function_exists('rebellion_edge_map_events_meta_fields')) {

    function rebellion_edge_map_events_meta_fields() {

        $event_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('event'),
                'title' => esc_html__('Event', 'rebellion'),
                'name' => 'event_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_tickets_status',
                    'type'        => 'select',
                    'label'       => esc_html__('Tickets Status','rebellion'),
                    'description' => '',
                    'options' => array(
                        'available' => esc_html__('Available','rebellion'),
                        'free' => esc_html__('Free','rebellion'),
                        'sold' => esc_html__('Sold Out','rebellion')
                    ),
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_date',
                    'type'        => 'date',
                    'label'       => esc_html__('Date','rebellion'),
                    'description' => '',
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_time',
                    'type'        => 'text',
                    'label'       => esc_html__('Time','rebellion'),
                    'description' => esc_html__('Please input the time in a HH:MM format. If you are using a 12 hour time format, please also input AM or PM markers.','rebellion'),
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_location',
                    'type'        => 'text',
                    'label'       => esc_html__('Location','rebellion'),
                    'description' => '',
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_pin',
                    'type'        => 'image',
                    'label'       => esc_html__('Pin','rebellion'),
                    'description' => esc_html__('Upload Google Map Pin Image','rebellion'),
                    'parent'      => $event_meta_box
                )
            );
            
            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_website',
                    'type'        => 'text',
                    'label'       => esc_html__('Website','rebellion'),
                    'description' => '',
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_organized_by',
                    'type'        => 'text',
                    'label'       => esc_html__('Organized By','rebellion'),
                    'description' => '',
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_link',
                    'type'        => 'text',
                    'label'       => esc_html__('Buy Tickets Link','rebellion'),
                    'description' => esc_html__('Enter the external link where users can buy the tickets','rebellion'),
                    'parent'      => $event_meta_box
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_item_target',
                    'type'        => 'selectblank',
                    'label'       => esc_html__('Target','rebellion'),
                    'description' => '',
                    'parent'      => $event_meta_box,
                    'options' => array(
                    	'_self' => esc_html__('Self','rebellion'),
                    	'_blank' => esc_html__('Blank','rebellion')
                	)
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_event_back_to_link',
                    'type'        => 'text',
                    'label'       => esc_html__('"Back To" Link','rebellion'),
                    'description' => esc_html__('Choose "Back To" page to link from event single page','rebellion'),
                    'parent'      => $event_meta_box,
                )
            );
        }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_events_meta_fields');
}