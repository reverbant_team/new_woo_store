<?php

if(!function_exists('rebellion_edge_map_header_meta_fields')) {

    function rebellion_edge_map_header_meta_fields() {

		$header_meta_box = rebellion_edge_add_meta_box(
		    array(
		        'scope' => array('page', 'post', 'event', 'album'),
		        'title' => esc_html__('Header', 'rebellion'),
		        'name' => 'header_meta'
		    )
		);

		$temp_holder_show		= '';
		$temp_holder_hide		= '';
		$temp_array_standard	= array();
		$temp_array_vertical	= array();
		$temp_array_full_screen	= array();
		$temp_array_centered	= array();
		$temp_array_divided		= array();
		$temp_array_behaviour	= array();
		switch (rebellion_edge_options()->getOptionValue('header_type')) {

			case 'header-standard':
				$temp_holder_show = '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_behaviour_meta_container';
				$temp_holder_hide = '#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container';

				$temp_array_standard = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-vertical','header-full-screen','header-centered','header-divided')
				);
				$temp_array_vertical = array(
					'hidden_values' => array('','header-standard','header-full-screen','header-centered','header-divided')
				);
				$temp_array_full_screen = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-centered','header-divided')
				);
				$temp_array_centered = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-divided')
				);
				$temp_array_divided = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;

			case 'header-vertical':
				$temp_holder_show = '#edgtf_edgtf_header_vertical_type_meta_container';
				$temp_holder_hide = '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container,#edgtf_edgtf_header_behaviour_meta_container';

				$temp_array_standard = array(
					'hidden_values' => array('', 'header-vertical', 'header-full-screen','header-full-screen','header-divided')
				);
				$temp_array_vertical = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-standard','header-full-screen','header-centered','header-divided')
				);
				$temp_array_full_screen = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-centered','header-divided')
				);
				$temp_array_centered = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-divided')
				);
				$temp_array_divided = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'default',
					'hidden_values' => array('','header-vertical')
				);
				break;
			case 'header-full-screen':
				$temp_holder_show = '#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_behaviour_meta_container';
				$temp_holder_hide = '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container';
				$temp_array_standard = array(
					'hidden_values' => array('', 'header-vertical', 'header-full-screen','header-centered','header-divided')
				);

				$temp_array_vertical = array(
					'hidden_values' => array('', 'header-standard','header-full-screen','header-centered','header-divided')
				);
				$temp_array_full_screen = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-standard','header-vertical','header-centered','header-divided')
				);
				$temp_array_centered = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-divided')
				);
				$temp_array_divided = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-centered')
				);

				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;

			case 'header-centered':
				$temp_holder_show = '#edgtf_edgtf_header_centered_type_meta_container,#edgtf_edgtf_header_behaviour_meta_container';
				$temp_holder_hide = '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container';
				$temp_array_standard = array(
					'hidden_values' => array('', 'header-vertical', 'header-full-screen','header-centered','header-divided')
				);

				$temp_array_vertical = array(
					'hidden_values' => array('', 'header-standard','header-full-screen','header-centered','header-divided')
				);

				$temp_array_full_screen = array(
					'hidden_values' => array('', 'header-standard','header-vertical','header-centered','header-divided')
				);
				$temp_array_centered = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-standard', 'header-vertical','header-full-screen','header-divided')
				);
				$temp_array_divided = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;

			case 'header-divided':
				$temp_holder_show = '#edgtf_edgtf_header_divided_type_meta_container,#edgtf_edgtf_header_behaviour_meta_container';
				$temp_holder_hide = '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container';
				$temp_array_standard = array(
					'hidden_values' => array('', 'header-vertical', 'header-full-screen','header-full-screen','header-centered')
				);

				$temp_array_vertical = array(
					'hidden_values' => array('', 'header-standard','header-full-screen','header-centered','header-divided')
				);

				$temp_array_full_screen = array(
					'hidden_values' => array('','header-vertical','header-standard','header-centered','header-divided')
				);
				$temp_array_centered = array(
					'hidden_values' => array('','header-standard', 'header-vertical','header-full-screen','header-divided')
				);
				$temp_array_divided = array(
					'hidden_value' => 'default',
					'hidden_values' => array('header-standard', 'header-vertical','header-full-screen','header-centered')
				);
				$temp_array_behaviour = array(
					'hidden_value' => 'header-vertical'
				);
				break;
		}



		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_header_type_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Choose Header Type', 'rebellion'),
				'description' => esc_html__('Select header type layout', 'rebellion'),
				'parent' => $header_meta_box,
				'options' => array(
					'' 						=> esc_html__('Default', 'rebellion'),
					'header-standard'		=> esc_html__('Standard Header Layout', 'rebellion'),
					'header-vertical'		=> esc_html__('Vertical Header Layout', 'rebellion'),
					'header-full-screen'	=> esc_html__('Full Screen Header Layout', 'rebellion'),
					'header-centered'		=> esc_html__('Centered Header Layout', 'rebellion'),
					'header-divided'		=> esc_html__('Divided Header Layout', 'rebellion')
				),
				'args' => array(
					"dependence" => true,
					"hide" => array(
						"" => $temp_holder_hide,
						'header-standard'		=> '#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container',
						'header-vertical'		=> '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container',
						'header-full-screen'	=> '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container',
						'header-divided'		=> '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_centered_type_meta_container',
						'header-centered'		=> '#edgtf_edgtf_header_standard_type_meta_container,#edgtf_edgtf_header_vertical_type_meta_container,#edgtf_edgtf_header_full_screen_type_meta_container,#edgtf_edgtf_header_divided_type_meta_container',

					),
					"show" => array(
						'' => $temp_holder_show,
						'header-standard'		=> '#edgtf_edgtf_header_standard_type_meta_container',
						'header-vertical'		=> '#edgtf_edgtf_header_vertical_type_meta_container',
						'header-full-screen'	=> '#edgtf_edgtf_header_full_screen_type_meta_container',
						'header-divided'		=> '#edgtf_edgtf_header_divided_type_meta_container',
						'header-centered'		=> '#edgtf_edgtf_header_centered_type_meta_container',
					)
				)
			)
		);
		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_header_style_meta',
				'type' => 'select',
				'default_value' => '',
				'label' => esc_html__('Header Skin', 'rebellion'),
				'description' => esc_html__('Choose a header style to make header elements (logo, main menu, side menu button) in that predefined style', 'rebellion'),
				'parent' => $header_meta_box,
				'options' => array(
					'' => '',
					'light-header' => esc_html__('Light', 'rebellion'),
					'dark-header' => esc_html__('Dark', 'rebellion')
				)
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'parent' => $header_meta_box,
				'type' => 'select',
				'name' => 'edgtf_enable_header_style_on_scroll_meta',
				'default_value' => '',
				'label' => esc_html__('Enable Header Style on Scroll', 'rebellion'),
				'description' => esc_html__('Enabling this option, header will change style depending on row settings for dark/light style', 'rebellion'),
				'options' => array(
					'' => '',
					'no' => esc_html__('No', 'rebellion'),
					'yes' => esc_html__('Yes', 'rebellion')
				)
			)
		);



		$header_standard_type_meta_container = rebellion_edge_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'edgtf_header_standard_type_meta_container',
					'hidden_property' => 'edgtf_header_type_meta',

				),
				$temp_array_standard
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_menu_area_background_color_header_standard_meta',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'rebellion'),
				'description' => esc_html__('Choose a background color for header area', 'rebellion'),
				'parent' => $header_standard_type_meta_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_menu_area_background_transparency_header_standard_meta',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent' => $header_standard_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_border_bottom_header_standard_meta',
			'type'          => 'select',
			'label'         => esc_html__('Enable Border Bottom', 'rebellion'),
			'parent'        => $header_standard_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_standard_border_bottom_color_container',
					'no'  => '#edgtf_standard_border_bottom_color_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_standard_border_bottom_color_container'
				)
			)
		));

		$border_bottom_color_standard_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'standard_border_bottom_color_container',
			'parent'          => $header_standard_type_meta_container,
			'hidden_property' => 'edgtf_border_bottom_header_standard_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_border_bottom_color_header_standard_meta',
				'type' => 'color',
				'label' => esc_html__('Border Bottom Color', 'rebellion'),
				'description' => esc_html__('Choose a border bottom color for header area', 'rebellion'),
				'parent' => $border_bottom_color_standard_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_border_bottom_transparency_header_standard_meta',
				'type' => 'text',
				'label' => esc_html__('Border Bottom Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the header border bottom color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent' => $border_bottom_color_standard_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);


		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_in_grid_header_standard_meta',
			'type'          => 'select',
			'label'         => esc_html__('Header In Grid', 'rebellion'),
			'description'   => esc_html__('Set header content to be in grid', 'rebellion'),
			'parent'        => $header_standard_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => esc_html__('Default', 'rebellion'),
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			)
			)
		);

		$header_vertical_type_meta_container = rebellion_edge_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'edgtf_header_vertical_type_meta_container',
					'hidden_property' => 'edgtf_header_type_meta',
					'hidden_values' => array('header-standard')
				),
				$temp_array_vertical
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_vertical_header_background_color_meta',
			'type'        => 'color',
			'label'       => esc_html__('Background Color', 'rebellion'),
			'description' => esc_html__('Set background color for vertical menu', 'rebellion'),
			'parent'      => $header_vertical_type_meta_container
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_vertical_header_transparency_meta',
			'type'        => 'text',
			'label'       => esc_html__('Background Transparency', 'rebellion'),
			'description' => esc_html__('Enter transparency for vertical menu (value from 0 to 1)', 'rebellion'),
			'parent'      => $header_vertical_type_meta_container,
			'args'        => array(
				'col_width' => 1
			)
		));

		rebellion_edge_add_meta_box_field(
			array(
				'name'          => 'edgtf_vertical_header_background_image_meta',
				'type'          => 'image',
				'default_value' => '',
				'label'         => esc_html__('Background Image', 'rebellion'),
				'description'   => esc_html__('Set background image for vertical menu', 'rebellion'),
				'parent'        => $header_vertical_type_meta_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_disable_vertical_header_background_image_meta',
				'type' => 'yesno',
				'default_value' => 'no',
				'label' => esc_html__('Disable Background Image', 'rebellion'),
				'description' => esc_html__('Enabling this option will hide background image in Vertical Menu', 'rebellion'),
				'parent' => $header_vertical_type_meta_container
			)
		);

		$header_full_screen_type_meta_container = rebellion_edge_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'edgtf_header_full_screen_type_meta_container',
					'hidden_property' => 'edgtf_header_type_meta',

				),
				$temp_array_full_screen
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_menu_area_background_color_header_full_screen_meta',
				'type' => 'color',
				'label' => esc_html__('Background Color', 'rebellion'),
				'description' => esc_html__('Choose a background color for Full Screen header area', 'rebellion'),
				'parent' => $header_full_screen_type_meta_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_menu_area_background_transparency_header_full_screen_meta',
				'type' => 'text',
				'label' => esc_html__('Background Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the Full Screen header background color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent' => $header_full_screen_type_meta_container,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_border_bottom_header_full_screen_meta',
			'type'          => 'select',
			'label'         => esc_html__('Enable Border Bottom', 'rebellion'),
			'parent'        => $header_full_screen_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_full_screen_border_bottom_container',
					'no'  => '#edgtf_full_screen_border_bottom_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_full_screen_border_bottom_container'
				)
			)
		));

		$border_bottom_color_full_screen = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'full_screen_border_bottom_container',
			'parent'          => $header_full_screen_type_meta_container,
			'hidden_property' => 'edgtf_border_bottom_header_full_screen_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_border_bottom_color_header_full_screen_meta',
				'type' => 'color',
				'label' => esc_html__('Border Bottom Color', 'rebellion'),
				'description' => esc_html__('Choose a border bottom color for Full Screen header area', 'rebellion'),
				'parent' => $border_bottom_color_full_screen
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_border_bottom_transparency_header_full_screen_meta',
				'type' => 'text',
				'label' => esc_html__('Border Bottom Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the Full Screen header border bottom color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent' => $border_bottom_color_full_screen,
				'args' => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
				'name'          => 'edgtf_menu_area_in_grid_header_full_screen_meta',
				'type'          => 'select',
				'label'         => esc_html__('Header In Grid', 'rebellion'),
				'description'   => esc_html__('Set header content to be in grid for the Full Screen header', 'rebellion'),
				'parent'        => $header_full_screen_type_meta_container,
				'default_value' => '',
				'options'       => array(
					''    => esc_html__('Default', 'rebellion'),
					'no'  => esc_html__('No', 'rebellion'),
					'yes' => esc_html__('Yes', 'rebellion')
				)
			)
		);

		$header_divided_type_meta_container = rebellion_edge_add_admin_container(
			array_merge(
				array(
					'parent'          => $header_meta_box,
					'name'            => 'edgtf_header_divided_type_meta_container',
					'hidden_property' => 'edgtf_header_type_meta',

				),
				$temp_array_divided
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_in_grid_header_divided_meta',
			'type'          => 'select',
			'label'         => esc_html__('Header In Grid', 'rebellion'),
			'description'   => esc_html__('Set header content to be in grid', 'rebellion'),
			'parent'        => $header_divided_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => esc_html__('Default', 'rebellion'),
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_menu_area_in_grid_header_divided_container',
					'no'  => '#edgtf_menu_area_in_grid_header_divided_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_menu_area_in_grid_header_divided_container'
				)
			)
		));

		$menu_area_in_grid_header_divided_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'menu_area_in_grid_header_divided_container',
			'parent'          => $header_divided_type_meta_container,
			'hidden_property' => 'edgtf_menu_area_in_grid_header_divided_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_grid_background_color_header_divided_meta',
				'type'        => 'color',
				'label'       => esc_html__('Grid Background Color', 'rebellion'),
				'description' => esc_html__('Set grid background color for header area', 'rebellion'),
				'parent'      => $menu_area_in_grid_header_divided_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_grid_background_transparency_header_divided_meta',
				'type'        => 'text',
				'label'       => esc_html__('Grid Background Transparency', 'rebellion'),
				'description' => esc_html__('Set grid background transparency for header (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent'      => $menu_area_in_grid_header_divided_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_in_grid_border_header_divided_meta',
			'type'          => 'select',
			'label'         => esc_html__('Grid Area Border', 'rebellion'),
			'description'   => esc_html__('Set border on grid area', 'rebellion'),
			'parent'        => $menu_area_in_grid_header_divided_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_menu_area_in_grid_border_header_divided_container',
					'no'  => '#edgtf_menu_area_in_grid_border_header_divided_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_menu_area_in_grid_border_header_divided_container'
				)
			)
		));

		$menu_area_in_grid_border_header_divided_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'menu_area_in_grid_border_header_divided_container',
			'parent'          => $header_divided_type_meta_container,
			'hidden_property' => 'edgtf_menu_area_in_grid_border_header_divided_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_menu_area_in_grid_border_color_header_divided_meta',
			'type'        => 'color',
			'label'       => esc_html__('Border Color', 'rebellion'),
			'description' => esc_html__('Set border color for grid area', 'rebellion'),
			'parent'      => $menu_area_in_grid_border_header_divided_container
		));


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_background_color_header_divided_meta',
				'type'        => 'color',
				'label'       => esc_html__('Background Color', 'rebellion'),
				'description' => esc_html__('Choose a background color for header area', 'rebellion'),
				'parent'      => $header_divided_type_meta_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_background_transparency_header_divided_meta',
				'type'        => 'text',
				'label'       => esc_html__('Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the header background color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent'      => $header_divided_type_meta_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_border_header_divided_meta',
			'type'          => 'select',
			'label'         => esc_html__('Header Area Border', 'rebellion'),
			'description'   => esc_html__('Set border on header area', 'rebellion'),
			'parent'        => $header_divided_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_border_bottom_color_container',
					'no'  => '#edgtf_border_bottom_color_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_border_bottom_color_container'
				)
			)
		));

		$border_bottom_color_divided_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'border_bottom_color_container',
			'parent'          => $header_divided_type_meta_container,
			'hidden_property' => 'edgtf_menu_area_border_header_divided_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_menu_area_border_color_header_divided_meta',
			'type'        => 'color',
			'label'       => esc_html__('Border Color', 'rebellion'),
			'description' => esc_html__('Choose color of header bottom border', 'rebellion'),
			'parent'      => $border_bottom_color_divided_container
		));


		/*** Header Centered ***/


		$header_centered_type_meta_container = rebellion_edge_add_admin_container(
			array_merge(
				array(
					'parent'          => $header_meta_box,
					'name'            => 'edgtf_header_centered_type_meta_container',
					'hidden_property' => 'edgtf_header_type_meta',

				),
				$temp_array_centered
			)
		);

		rebellion_edge_add_admin_section_title(array(
			'name'   => 'logo_area_centered_title',
			'parent' => $header_centered_type_meta_container,
			'title'  => esc_html__('Logo Area', 'rebellion')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_logo_area_in_grid_header_centered_meta',
			'type'          => 'select',
			'label'         => esc_html__('Logo Area In Grid', 'rebellion'),
			'description'   => esc_html__('Set logo area content to be in grid', 'rebellion'),
			'parent'        => $header_centered_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => esc_html__('Default', 'rebellion'),
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_logo_area_in_grid_header_centered_container',
					'no'  => '#edgtf_logo_area_in_grid_header_centered_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_logo_area_in_grid_header_centered_container'
				)
			)
		));

		$logo_area_in_grid_header_centered_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'logo_area_in_grid_header_centered_container',
			'parent'          => $header_centered_type_meta_container,
			'hidden_property' => 'edgtf_logo_area_in_grid_header_centered_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_area_grid_background_color_header_centered_meta',
				'type'        => 'color',
				'label'       => esc_html__('Grid Background Color', 'rebellion'),
				'description' => esc_html__('Set grid background color for logo area', 'rebellion'),
				'parent'      => $logo_area_in_grid_header_centered_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_area_grid_background_transparency_header_centered_meta',
				'type'        => 'text',
				'label'       => esc_html__('Grid Background Transparency', 'rebellion'),
				'description' => esc_html__('Set grid background transparency for logo area (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent'      => $logo_area_in_grid_header_centered_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_logo_area_in_grid_border_header_centered_meta',
			'type'          => 'select',
			'label'         => esc_html__('Grid Area Border', 'rebellion'),
			'description'   => esc_html__('Set border on grid area', 'rebellion'),
			'parent'        => $logo_area_in_grid_header_centered_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_logo_area_in_grid_border_header_centered_container',
					'no'  => '#edgtf_logo_area_in_grid_border_header_centered_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_logo_area_in_grid_border_header_centered_container'
				)
			)
		));

		$logo_area_in_grid_border_header_centered_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'logo_area_in_grid_border_header_centered_container',
			'parent'          => $logo_area_in_grid_header_centered_container,
			'hidden_property' => 'edgtf_logo_area_in_grid_border_header_centered_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_logo_area_in_grid_border_color_header_centered_meta',
			'type'        => 'color',
			'label'       => esc_html__('Border Color', 'rebellion'),
			'description' => esc_html__('Set border color for grid area', 'rebellion'),
			'parent'      => $logo_area_in_grid_border_header_centered_container
		));


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_area_background_color_header_centered_meta',
				'type'        => 'color',
				'label'       => esc_html__('Background Color', 'rebellion'),
				'description' => esc_html__('Choose a background color for logo area', 'rebellion'),
				'parent'      => $header_centered_type_meta_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_logo_area_background_transparency_header_centered_meta',
				'type'        => 'text',
				'label'       => esc_html__('Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the logo area background color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent'      => $header_centered_type_meta_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_logo_area_border_header_centered_meta',
			'type'          => 'select',
			'label'         => esc_html__('Logo Area Border', 'rebellion'),
			'description'   => esc_html__('Set border on logo area', 'rebellion'),
			'parent'        => $header_centered_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_logo_border_bottom_color_container',
					'no'  => '#edgtf_logo_border_bottom_color_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_logo_border_bottom_color_container'
				)
			)
		));

		$border_bottom_color_centered_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'logo_border_bottom_color_container',
			'parent'          => $header_centered_type_meta_container,
			'hidden_property' => 'edgtf_logo_area_border_header_centered_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_logo_area_border_color_header_centered_meta',
			'type'        => 'color',
			'label'       => esc_html__('Border Color', 'rebellion'),
			'description' => esc_html__('Choose color of logo area bottom border', 'rebellion'),
			'parent'      => $border_bottom_color_centered_container
		));

		rebellion_edge_add_admin_section_title(array(
			'name'   => 'menu_area_centered_title',
			'parent' => $header_centered_type_meta_container,
			'title'  => esc_html__('Menu Area', 'rebellion')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_in_grid_header_centered_meta',
			'type'          => 'select',
			'label'         => esc_html__('Menu Area In Grid', 'rebellion'),
			'description'   => esc_html__('Set menu area content to be in grid', 'rebellion'),
			'parent'        => $header_centered_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => esc_html__('Default', 'rebellion'),
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_menu_area_in_grid_header_centered_container',
					'no'  => '#edgtf_menu_area_in_grid_header_centered_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_menu_area_in_grid_header_centered_container'
				)
			)
		));

		$menu_area_in_grid_header_centered_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'menu_area_in_grid_header_centered_container',
			'parent'          => $header_centered_type_meta_container,
			'hidden_property' => 'edgtf_menu_area_in_grid_header_centered_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_grid_background_color_header_centered_meta',
				'type'        => 'color',
				'label'       => esc_html__('Grid Background Color', 'rebellion'),
				'description' => esc_html__('Set grid background color for menu area', 'rebellion'),
				'parent'      => $menu_area_in_grid_header_centered_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_grid_background_transparency_header_centered_meta',
				'type'        => 'text',
				'label'       => esc_html__('Grid Background Transparency', 'rebellion'),
				'description' => esc_html__('Set grid background transparency for menu area (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent'      => $menu_area_in_grid_header_centered_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_in_grid_border_header_centered_meta',
			'type'          => 'select',
			'label'         => esc_html__('Grid Area Border', 'rebellion'),
			'description'   => esc_html__('Set border on grid area', 'rebellion'),
			'parent'        => $menu_area_in_grid_header_centered_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_menu_area_in_grid_border_header_centered_container',
					'no'  => '#edgtf_menu_area_in_grid_border_header_centered_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_menu_area_in_grid_border_header_centered_container'
				)
			)
		));

		$menu_area_in_grid_border_header_centered_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'menu_area_in_grid_border_header_centered_container',
			'parent'          => $menu_area_in_grid_header_centered_container,
			'hidden_property' => 'edgtf_menu_area_in_grid_border_header_centered_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_menu_area_in_grid_border_color_header_centered_meta',
			'type'        => 'color',
			'label'       => esc_html__('Border Color', 'rebellion'),
			'description' => esc_html__('Set border color for grid area', 'rebellion'),
			'parent'      => $menu_area_in_grid_border_header_centered_container
		));


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_background_color_header_centered_meta',
				'type'        => 'color',
				'label'       => esc_html__('Background Color', 'rebellion'),
				'description' => esc_html__('Choose a background color for menu area', 'rebellion'),
				'parent'      => $header_centered_type_meta_container
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_menu_area_background_transparency_header_centered_meta',
				'type'        => 'text',
				'label'       => esc_html__('Transparency', 'rebellion'),
				'description' => esc_html__('Choose a transparency for the menu area background color (0 = fully transparent, 1 = opaque)', 'rebellion'),
				'parent'      => $header_centered_type_meta_container,
				'args'        => array(
					'col_width' => 2
				)
			)
		);

		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_menu_area_border_header_centered_meta',
			'type'          => 'select',
			'label'         => esc_html__('Menu Area Border', 'rebellion'),
			'description'   => esc_html__('Set border on menu area', 'rebellion'),
			'parent'        => $header_centered_type_meta_container,
			'default_value' => '',
			'options'       => array(
				''    => '',
				'no'  => esc_html__('No', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion')
			),
			'args'          => array(
				'dependence' => true,
				'hide'       => array(
					''    => '#edgtf_menu_border_bottom_color_container',
					'no'  => '#edgtf_menu_border_bottom_color_container',
					'yes' => ''
				),
				'show'       => array(
					''    => '',
					'no'  => '',
					'yes' => '#edgtf_menu_border_bottom_color_container'
				)
			)
		));

		$border_bottom_color_centered_container = rebellion_edge_add_admin_container(array(
			'type'            => 'container',
			'name'            => 'menu_border_bottom_color_container',
			'parent'          => $header_centered_type_meta_container,
			'hidden_property' => 'edgtf_menu_area_border_header_centered_meta',
			'hidden_value'    => 'no',
			'hidden_values'   => array('', 'no')
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_menu_area_border_color_header_centered_meta',
			'type'        => 'color',
			'label'       => esc_html__('Border Color', 'rebellion'),
			'description' => esc_html__('Choose color of menu area bottom border', 'rebellion'),
			'parent'      => $border_bottom_color_centered_container
		));


		$header_behaviour_meta_container = rebellion_edge_add_admin_container(
			array_merge(
				array(
					'parent' => $header_meta_box,
					'name' => 'edgtf_header_behaviour_meta_container',
					'hidden_property' => 'edgtf_header_type_meta',
				),
				$temp_array_behaviour
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'            => 'edgtf_scroll_amount_for_sticky_meta',
				'type'            => 'text',
				'label'           => esc_html__('Scroll amount for sticky header appearance', 'rebellion'),
				'description'     => esc_html__('Define scroll amount for sticky header appearance', 'rebellion'),
				'parent'          => $header_behaviour_meta_container,
				'args'            => array(
					'col_width' => 2,
					'suffix'    => 'px'
				),
				'hidden_property' => 'edgtf_header_behaviour',
				'hidden_values'   => array("sticky-header-on-scroll-up", "fixed-on-scroll")
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'            => 'edgtf_sticky_header_in_grid_meta',
				'type'            => 'select',
				'label'           => esc_html__('Sticky Header in grid', 'rebellion'),
				'description'     => esc_html__('Set sticky header content to be in grid', 'rebellion'),
				'parent'          => $header_behaviour_meta_container,
				'options'       => array(
					''    => esc_html__('Default', 'rebellion'),
					'no'  => esc_html__('No', 'rebellion'),
					'yes' => esc_html__('Yes', 'rebellion')
				),
				'hidden_property' => 'edgtf_header_behaviour',
				'hidden_value'   => array("fixed-on-scroll")
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'            => 'edgtf_sticky_header_background_color_meta',
				'type'            => 'color',
				'label'           => esc_html__('Sticky Header Background Color', 'rebellion'),
				'description'     => esc_html__('Set background color for sticky header', 'rebellion'),
				'parent'          => $header_behaviour_meta_container,
				'hidden_property' => 'edgtf_header_behaviour',
				'hidden_value'   => array("fixed-on-scroll")
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name' => 'edgtf_show_header_widget_area_meta',
				'type' => 'yesno',
				'default_value' => 'yes',
				'label' => esc_html__('Show Header Widget Area', 'rebellion'),
				'description' => esc_html__('Enabling this option will show widget area in header', 'rebellion'),
				'parent' => $header_meta_box
			)
		);

		$rebellion_custom_sidebars = rebellion_edge_get_custom_sidebars();
		if(count($rebellion_custom_sidebars) > 0) {
			rebellion_edge_add_meta_box_field(array(
				'name' => 'edgtf_custom_header_sidebar_meta',
				'type' => 'selectblank',
				'label' => esc_html__('Choose Custom Widget Area in Header', 'rebellion'),
				'description' => esc_html__('Choose custom widget area to display in header area"', 'rebellion'),
				'parent' => $header_meta_box,
				'options' => $rebellion_custom_sidebars
			));
		}
		if(count($rebellion_custom_sidebars) > 0) {
			rebellion_edge_add_meta_box_field(array(
				'name' => 'edgtf_custom_sticky_header_sidebar_meta',
				'type' => 'selectblank',
				'label' => esc_html__('Choose Custom Widget Area in Sticky Header', 'rebellion'),
				'description' => esc_html__('Choose custom widget area to display in sticky header area"', 'rebellion'),
				'parent' => $header_meta_box,
				'options' => $rebellion_custom_sidebars
			));
		}
		rebellion_edge_add_admin_section_title(array(
			'name'   => 'top_bar_section_title',
			'parent' => $header_meta_box,
			'title'  => esc_html__('Top Bar', 'rebellion')
		));

		$top_bar_global_option      = rebellion_edge_options()->getOptionValue('top_bar');
		$top_bar_default_dependency = array(
			'' => '#edgtf_top_bar_container_no_style'
		);

		$top_bar_show_array = array(
			'yes' => '#edgtf_top_bar_container_no_style'
		);

		$top_bar_hide_array = array(
			'no' => '#edgtf_top_bar_container_no_style'
		);

		if($top_bar_global_option === 'yes') {
			$top_bar_show_array = array_merge($top_bar_show_array, $top_bar_default_dependency);
			$temp_top_no = array(
				'hidden_value' => 'no'
			);
		} else {
			$top_bar_hide_array = array_merge($top_bar_hide_array, $top_bar_default_dependency);
			$temp_top_no = array(
				'hidden_values'   => array('','no')
			);
		}


		rebellion_edge_add_meta_box_field(array(
			'name'          => 'edgtf_top_bar_meta',
			'type'          => 'select',
			'label'         => esc_html__('Enable Top Bar on This Page', 'rebellion'),
			'description'   => esc_html__('Enabling this option will enable top bar on this page', 'rebellion'),
			'parent'        => $header_meta_box,
			'default_value' => '',
			'options'       => array(
				''    => esc_html__('Default', 'rebellion'),
				'yes' => esc_html__('Yes', 'rebellion'),
				'no'  => esc_html__('No', 'rebellion')
			),
			'args' => array(
				"dependence" => true,
				'show'       => $top_bar_show_array,
				'hide'       => $top_bar_hide_array
			)
		));

		$top_bar_container = rebellion_edge_add_admin_container_no_style(array_merge(array(
			'name'            => 'top_bar_container_no_style',
			'parent'          => $header_meta_box,
			'hidden_property' => 'edgtf_top_bar_meta'
		),
			$temp_top_no));

		rebellion_edge_add_meta_box_field(array(
			'name'    => 'edgtf_top_bar_skin_meta',
			'type'    => 'select',
			'label'   => esc_html__('Top Bar Skin', 'rebellion'),
			'options' => array(
				''      => esc_html__('Default', 'rebellion'),
				'light' => esc_html__('Light', 'rebellion'),
				'dark'  => esc_html__('Dark', 'rebellion')
			),
			'parent'  => $top_bar_container
		));

		rebellion_edge_add_meta_box_field(array(
			'name'   => 'edgtf_top_bar_background_color_meta',
			'type'   => 'color',
			'label'  => esc_html__('Top Bar Background Color', 'rebellion'),
			'parent' => $top_bar_container
		));

		rebellion_edge_add_meta_box_field(array(
			'name'        => 'edgtf_top_bar_background_transparency_meta',
			'type'        => 'text',
			'label'       => esc_html__('Top Bar Background Color Transparency', 'rebellion'),
			'description' => esc_html__('Set top bar background color transparenct. Value should be between 0 and 1', 'rebellion'),
			'parent'      => $top_bar_container,
			'args'        => array(
				'col_width' => 3
			)
		));
	}

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_header_meta_fields');
}
