<?php

if(!function_exists('rebellion_edge_map_footer_meta_fields')) {

    function rebellion_edge_map_footer_meta_fields() {

        $footer_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('page', 'post', 'event', 'album'),
                'title' => esc_html__('Footer', 'rebellion'),
                'name' => 'footer_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name' => 'edgtf_disable_footer_meta',
                    'type' => 'yesno',
                    'default_value' => 'no',
                    'label' => esc_html__('Disable Footer for this Page', 'rebellion'),
                    'description' => esc_html__('Enabling this option will hide footer on this page', 'rebellion'),
                    'parent' => $footer_meta_box,
                )
            );
        }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_footer_meta_fields');
}