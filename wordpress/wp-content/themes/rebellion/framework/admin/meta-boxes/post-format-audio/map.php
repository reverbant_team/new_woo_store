<?php

/*** Audio Post Format ***/

if(!function_exists('rebellion_edge_map_post_format_audio_meta_fields')) {

    function rebellion_edge_map_post_format_audio_meta_fields() {

		$audio_post_format_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Audio Post Format', 'rebellion'),
				'name' 	=> 'post_format_audio_meta'
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_audio_post_type_meta',
				'type'        => 'select',
				'label'       => esc_html__('Audio Type', 'rebellion'),
				'description' => esc_html__('Choose audio type', 'rebellion'),
				'parent'      => $audio_post_format_meta_box,
				'default_value' => 'self',
				'options'     => array(
					'self'			=> esc_html__('Self Hosted', 'rebellion'),
					'soundcloud'	=> esc_html__('Soundcloud', 'rebellion')
				),
				'args' => array(
					'dependence' => true,
					'hide' => array(
						'self'		=> '#edgtf_edgtf_audio_soundcloud_container',
						'soundcloud' => '#edgtf_edgtf_audio_self_hosted_container'
					),
					'show' => array(
						'self'		=> '#edgtf_edgtf_audio_self_hosted_container',
						'soundcloud' => '#edgtf_edgtf_audio_soundcloud_container'
					)
				)
			)
		);

		$edgtf_audio_self_hosted_container = rebellion_edge_add_admin_container(
			array(
				'parent' => $audio_post_format_meta_box,
				'name' => 'edgtf_audio_self_hosted_container',
				'hidden_property' => 'edgtf_audio_post_type_meta',
				'hidden_value' => 'soundcloud'
			)
		);

		$edgtf_audio_soundcloud_container = rebellion_edge_add_admin_container(
			array(
				'parent' => $audio_post_format_meta_box,
				'name' => 'edgtf_audio_soundcloud_container',
				'hidden_property' => 'edgtf_audio_post_type_meta',
				'hidden_value' => 'self'
			)
		);


		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_audio_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Self Hosted Link', 'rebellion'),
				'description' => esc_html__('Enter audio link', 'rebellion'),
				'parent'      => $edgtf_audio_self_hosted_container,

			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_audio_soundcloud_link_meta',
				'type'        => 'text',
				'label'       => esc_html__('Soundcloud Link', 'rebellion'),
				'description' => esc_html__('Enter soundcloud link', 'rebellion'),
				'parent'      => $edgtf_audio_soundcloud_container,

			)
		);
    }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_post_format_audio_meta_fields');
}
