<?php

if(!function_exists('rebellion_edge_map_title_meta_fields')) {

    function rebellion_edge_map_title_meta_fields() {

        $title_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('page', 'post', 'event', 'album'),
                'title' => esc_html__('Title', 'rebellion'),
                'name' => 'title_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name' => 'edgtf_show_title_area_meta',
                    'type' => 'select',
                    'default_value' => '',
                    'label' => esc_html__('Show Title Area', 'rebellion'),
                    'description' => esc_html__('Disabling this option will turn off page title area', 'rebellion'),
                    'parent' => $title_meta_box,
                    'options' => array(
                        '' => '',
                        'no' => esc_html__('No', 'rebellion'),
                        'yes' => esc_html__('Yes', 'rebellion')
                    ),
                    'args' => array(
                        "dependence" => true,
                        "hide" => array(
                            "" => "",
                            "no" => "#edgtf_edgtf_show_title_area_meta_container",
                            "yes" => ""
                        ),
                        "show" => array(
                            "" => "#edgtf_edgtf_show_title_area_meta_container",
                            "no" => "",
                            "yes" => "#edgtf_edgtf_show_title_area_meta_container"
                        )
                    )
                )
            );

            $show_title_area_meta_container = rebellion_edge_add_admin_container(
                array(
                    'parent' => $title_meta_box,
                    'name' => 'edgtf_show_title_area_meta_container',
                    'hidden_property' => 'edgtf_show_title_area_meta',
                    'hidden_value' => 'no'
                )
            );


                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_in_grid_meta',
                        'type' => 'select',
                        'default_value' => '',
                        'label' => esc_html__('Title Area in Grid', 'rebellion'),
                        'description' => esc_html__('Choose wheter for title content to be in grid', 'rebellion'),
                        'parent' => $show_title_area_meta_container,
                        'options' => array(
                            '' => '',
                            'yes' => esc_html__('Yes', 'rebellion'),
                            'no' => esc_html__('No', 'rebellion')
                        )
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_type_meta',
                        'type' => 'select',
                        'default_value' => '',
                        'label' => esc_html__('Title Area Type', 'rebellion'),
                        'description' => esc_html__('Choose title type', 'rebellion'),
                        'parent' => $show_title_area_meta_container,
                        'options' => array(
                            '' => '',
                            'standard' => esc_html__('Standard', 'rebellion'),
                            'breadcrumb' => esc_html__('Breadcrumb', 'rebellion')
                        ),
                        'args' => array(
                            "dependence" => true,
                            "hide" => array(
                                "standard" => "",
                                "standard" => "",
                                "breadcrumb" => "#edgtf_edgtf_title_area_type_meta_container"
                            ),
                            "show" => array(
                                "" => "#edgtf_edgtf_title_area_type_meta_container",
                                "standard" => "#edgtf_edgtf_title_area_type_meta_container",
                                "breadcrumb" => ""
                            )
                        )
                    )
                );

                $title_area_type_meta_container = rebellion_edge_add_admin_container(
                    array(
                        'parent' => $show_title_area_meta_container,
                        'name' => 'edgtf_title_area_type_meta_container',
                        'hidden_property' => 'edgtf_title_area_type_meta',
                        'hidden_value' => '',
                        'hidden_values' => array('breadcrumb'),
                    )
                );

                    rebellion_edge_add_meta_box_field(
                        array(
                            'name' => 'edgtf_title_area_enable_breadcrumbs_meta',
                            'type' => 'select',
                            'default_value' => '',
                            'label' => esc_html__('Enable Breadcrumbs', 'rebellion'),
                            'description' => esc_html__('This option will display Breadcrumbs in Title Area', 'rebellion'),
                            'parent' => $title_area_type_meta_container,
                            'options' => array(
                                '' => '',
                                'no' => esc_html__('No', 'rebellion'),
                                'yes' => esc_html__('Yes', 'rebellion')
                            ),
                        )
                    );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_animation_meta',
                        'type' => 'select',
                        'default_value' => '',
                        'label' => esc_html__('Animations', 'rebellion'),
                        'description' => esc_html__('Choose an animation for Title Area', 'rebellion'),
                        'parent' => $show_title_area_meta_container,
                        'options' => array(
                            '' => '',
                            'no' => esc_html__('No Animation', 'rebellion'),
                            'right-left' => esc_html__('Text right to left', 'rebellion'),
                            'left-right' => esc_html__('Text left to right', 'rebellion')
                        )
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_vertial_alignment_meta',
                        'type' => 'select',
                        'default_value' => '',
                        'label' => esc_html__('Vertical Alignment', 'rebellion'),
                        'description' => esc_html__('Specify title vertical alignment', 'rebellion'),
                        'parent' => $show_title_area_meta_container,
                        'options' => array(
                            '' => '',
                            'header_bottom' => esc_html__('From Bottom of Header', 'rebellion'),
                            'window_top' => esc_html__('From Window Top', 'rebellion')
                        )
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_content_alignment_meta',
                        'type' => 'select',
                        'default_value' => '',
                        'label' => esc_html__('Horizontal Alignment', 'rebellion'),
                        'description' => esc_html__('Specify title horizontal alignment', 'rebellion'),
                        'parent' => $show_title_area_meta_container,
                        'options' => array(
                            '' => '',
                            'left' => esc_html__('Left', 'rebellion'),
                            'center' => esc_html__('Center', 'rebellion'),
                            'right' => esc_html__('Right', 'rebellion')
                        )
                    )
                );

        		rebellion_edge_add_meta_box_field(
        			array(
        				'name'			=> 'edgtf_title_area_text_size_meta',
        				'type'			=> 'select',
        				'default_value'	=> '',
        				'label'			=> esc_html__('Text Size', 'rebellion'),
        				'description'	=> esc_html__('Choose a default Title size', 'rebellion'),
        				'parent'		=> $show_title_area_meta_container,
        				'options'		=> array(
        					'' => '',
        					'small'     => esc_html__('Small', 'rebellion'),
        					'medium'    => esc_html__('Medium', 'rebellion'),
        					'large'     => esc_html__('Large', 'rebellion')


        				)
        			)
        		);

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_text_color_meta',
                        'type' => 'color',
                        'label' => esc_html__('Title Color', 'rebellion'),
                        'description' => esc_html__('Choose a color for title text', 'rebellion'),
                        'parent' => $show_title_area_meta_container
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_breadcrumb_color_meta',
                        'type' => 'color',
                        'label' => esc_html__('Breadcrumb Color', 'rebellion'),
                        'description' => esc_html__('Choose a color for breadcrumb text', 'rebellion'),
                        'parent' => $show_title_area_meta_container
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_background_color_meta',
                        'type' => 'color',
                        'label' => esc_html__('Background Color', 'rebellion'),
                        'description' => esc_html__('Choose a background color for Title Area', 'rebellion'),
                        'parent' => $show_title_area_meta_container
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_hide_background_image_meta',
                        'type' => 'yesno',
                        'default_value' => 'no',
                        'label' => esc_html__('Hide Background Image', 'rebellion'),
                        'description' => esc_html__('Enable this option to hide background image in Title Area', 'rebellion'),
                        'parent' => $show_title_area_meta_container,
                        'args' => array(
                            "dependence" => true,
                            "dependence_hide_on_yes" => "#edgtf_edgtf_hide_background_image_meta_container",
                            "dependence_show_on_yes" => ""
                        )
                    )
                );

                $hide_background_image_meta_container = rebellion_edge_add_admin_container(
                    array(
                        'parent' => $show_title_area_meta_container,
                        'name' => 'edgtf_hide_background_image_meta_container',
                        'hidden_property' => 'edgtf_hide_background_image_meta',
                        'hidden_value' => 'yes'
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_background_image_meta',
                        'type' => 'image',
                        'label' => esc_html__('Background Image', 'rebellion'),
                        'description' => esc_html__('Choose an Image for Title Area', 'rebellion'),
                        'parent' => $hide_background_image_meta_container
                    )
                );

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_title_area_background_image_responsive_meta',
                        'type' => 'select',
                        'default_value' => '',
                        'label' => esc_html__('Background Responsive Image', 'rebellion'),
                        'description' => esc_html__('Enabling this option will make Title background image responsive', 'rebellion'),
                        'parent' => $hide_background_image_meta_container,
                        'options' => array(
                            '' => '',
                            'no' => esc_html__('No', 'rebellion'),
                            'yes' => esc_html__('Yes', 'rebellion')
                        ),
                        'args' => array(
                            "dependence" => true,
                            "hide" => array(
                                "" => "",
                                "no" => "",
                                "yes" => "#edgtf_edgtf_title_area_background_image_responsive_meta_container, #edgtf_edgtf_title_area_height_meta"
                            ),
                            "show" => array(
                                "" => "#edgtf_edgtf_title_area_background_image_responsive_meta_container, #edgtf_edgtf_title_area_height_meta",
                                "no" => "#edgtf_edgtf_title_area_background_image_responsive_meta_container, #edgtf_edgtf_title_area_height_meta",
                                "yes" => ""
                            )
                        )
                    )
                );

                $title_area_background_image_responsive_meta_container = rebellion_edge_add_admin_container(
                    array(
                        'parent' => $hide_background_image_meta_container,
                        'name' => 'edgtf_title_area_background_image_responsive_meta_container',
                        'hidden_property' => 'edgtf_title_area_background_image_responsive_meta',
                        'hidden_value' => 'yes'
                    )
                );

                    rebellion_edge_add_meta_box_field(
                        array(
                            'name' => 'edgtf_title_area_background_image_parallax_meta',
                            'type' => 'select',
                            'default_value' => '',
                            'label' => esc_html__('Background Image in Parallax', 'rebellion'),
                            'description' => esc_html__('Enabling this option will make Title background image parallax', 'rebellion'),
                            'parent' => $title_area_background_image_responsive_meta_container,
                            'options' => array(
                                '' => '',
                                'no' => esc_html__('No', 'rebellion'),
                                'yes' => esc_html__('Yes', 'rebellion'),
                                'yes_zoom' => esc_html__('Yes, with zoom out', 'rebellion')
                            )
                        )
                    );

                rebellion_edge_add_meta_box_field(array(
                    'name' => 'edgtf_title_area_height_meta',
                    'type' => 'text',
                    'label' => esc_html__('Height', 'rebellion'),
                    'description' => esc_html__('Set a height for Title Area', 'rebellion'),
                    'parent' => $show_title_area_meta_container,
                    'args' => array(
                        'col_width' => 2,
                        'suffix' => 'px'
                    )
                ));

        		rebellion_edge_add_meta_box_field(
        			array(
        				'name' => 'edgtf_title_area_border_bottom_meta',
        				'type' => 'select',
        				'default_value' => '',
        				'label' => esc_html__('Enable Border Bottom', 'rebellion'),
        				'description' => esc_html__('This option will display Border Bottom in Title Area', 'rebellion'),
        				'parent' => $show_title_area_meta_container,
        				'options' => array(
        					'' => '',
        					'no' => esc_html__('No', 'rebellion'),
        					'yes' => esc_html__('Yes', 'rebellion')
        				)
        			)
        		);

                rebellion_edge_add_meta_box_field(array(
                    'name' => 'edgtf_title_area_subtitle_meta',
                    'type' => 'text',
                    'default_value' => '',
                    'label' => esc_html__('Subtitle Text', 'rebellion'),
                    'description' => esc_html__('Enter your subtitle text', 'rebellion'),
                    'parent' => $show_title_area_meta_container,
                    'args' => array(
                        'col_width' => 6
                    )
                ));

                rebellion_edge_add_meta_box_field(
                    array(
                        'name' => 'edgtf_subtitle_color_meta',
                        'type' => 'color',
                        'label' => esc_html__('Subtitle Color', 'rebellion'),
                        'description' => esc_html__('Choose a color for subtitle text', 'rebellion'),
                        'parent' => $show_title_area_meta_container
                    )
                );
            }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_title_meta_fields');
}
