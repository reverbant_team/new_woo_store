<?php

if(!function_exists('rebellion_edge_map_blog_meta_fields')) {

    function rebellion_edge_map_blog_meta_fields() {

        $edgt_blog_categories = array();
        $categories = get_categories();
        foreach($categories as $category) {
            $edgt_blog_categories[$category->term_id] = $category->name;
        }

        $blog_meta_box = rebellion_edge_add_meta_box(
            array(
                'scope' => array('page'),
                'title' => esc_html__('Blog', 'rebellion'),
                'name' => 'blog_meta'
            )
        );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_blog_category_meta',
                    'type'        => 'selectblank',
                    'label'       => esc_html__('Blog Category', 'rebellion'),
                    'description' => esc_html__('Choose category of posts to display (leave empty to display all categories)', 'rebellion'),
                    'parent'      => $blog_meta_box,
                    'options'     => $edgt_blog_categories
                )
            );

            rebellion_edge_add_meta_box_field(
                array(
                    'name'        => 'edgtf_show_posts_per_page_meta',
                    'type'        => 'text',
                    'label'       => esc_html__('Number of Posts', 'rebellion'),
                    'description' => esc_html__('Enter the number of posts to display', 'rebellion'),
                    'parent'      => $blog_meta_box,
                    'options'     => $edgt_blog_categories,
                    'args'        => array("col_width" => 3)
                )
            );
	
    }

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_blog_meta_fields');
}

