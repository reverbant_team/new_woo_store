<?php

/*** Quote Post Format ***/

if(!function_exists('rebellion_edge_map_post_format_quote_meta_fields')) {

    function rebellion_edge_map_post_format_quote_meta_fields() {

		$quote_post_format_meta_box = rebellion_edge_add_meta_box(
			array(
				'scope' =>	array('post'),
				'title' => esc_html__('Quote Post Format', 'rebellion'),
				'name' 	=> 'post_format_quote_meta'
			)
		);

		rebellion_edge_add_meta_box_field(
			array(
				'name'        => 'edgtf_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__('Quote Text', 'rebellion'),
				'description' => esc_html__('Enter Quote text', 'rebellion'),
				'parent'      => $quote_post_format_meta_box,

			)
		);
	}

    add_action('rebellion_edge_meta_boxes_map', 'rebellion_edge_map_post_format_quote_meta_fields');
}
