<?php

if ( ! function_exists('rebellion_edge_general_options_map') ) {
    /**
     * General options page
     */
    function rebellion_edge_general_options_map() {

        rebellion_edge_add_admin_page(
            array(
                'slug'  => '',
                'title' => esc_html__('General', 'rebellion'),
                'icon'  => 'fa fa-institution'
            )
        );

        $panel_design_style = rebellion_edge_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_design_style',
                'title' => esc_html__('Design Style', 'rebellion')
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'google_fonts',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'rebellion'),
                'description'   => esc_html__('Choose a default Google font for your site', 'rebellion'),
                'parent' => $panel_design_style
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'additional_google_fonts',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Additional Google Fonts', 'rebellion'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#edgtf_additional_google_fonts_container"
                )
            )
        );

        $additional_google_fonts_container = rebellion_edge_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'additional_google_fonts_container',
                'hidden_property'   => 'additional_google_fonts',
                'hidden_value'      => 'no'
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font1',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'rebellion'),
                'description'   => esc_html__('Choose additional Google font for your site', 'rebellion'),
                'parent'        => $additional_google_fonts_container
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font2',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'rebellion'),
                'description'   => esc_html__('Choose additional Google font for your site', 'rebellion'),
                'parent'        => $additional_google_fonts_container
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font3',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'rebellion'),
                'description'   => esc_html__('Choose additional Google font for your site', 'rebellion'),
                'parent'        => $additional_google_fonts_container
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font4',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'rebellion'),
                'description'   => esc_html__('Choose additional Google font for your site', 'rebellion'),
                'parent'        => $additional_google_fonts_container
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'additional_google_font5',
                'type'          => 'font',
                'default_value' => '-1',
                'label'         => esc_html__('Font Family', 'rebellion'),
                'description'   => esc_html__('Choose additional Google font for your site', 'rebellion'),
                'parent'        => $additional_google_fonts_container
            )
        );

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'predefined_color_skin',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__('Predefined Color Skin', 'rebellion'),
				'description'   => esc_html__('Choose predefined color skin', 'rebellion'),
				'parent'        => $panel_design_style,
				'options'       => array(
					''			=> esc_html__('Default/Dark', 'rebellion'),
					'dark'		=> esc_html__('Dark', 'rebellion'),
					'light'   	=> esc_html__('Light', 'rebellion')
				)
			)
		);

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'first_color',
                'type'          => 'color',
                'label'         => esc_html__('First Main Color', 'rebellion'),
                'description'   => esc_html__('Choose the most dominant theme color. Default color is #ab2eba', 'rebellion'),
                'parent'        => $panel_design_style
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'page_background_color',
                'type'          => 'color',
                'label'         => esc_html__('Page Background Color', 'rebellion'),
                'description'   => esc_html__('Choose the background color for page content. Default color is #ffffff', 'rebellion'),
                'parent'        => $panel_design_style
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'selection_color',
                'type'          => 'color',
                'label'         => esc_html__('Text Selection Color', 'rebellion'),
                'description'   => esc_html__('Choose the color users see when selecting text', 'rebellion'),
                'parent'        => $panel_design_style
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'boxed',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Boxed Layout', 'rebellion'),
                'description'   => '',
                'parent'        => $panel_design_style,
                'args'          => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#edgtf_boxed_container"
                )
            )
        );

        $boxed_container = rebellion_edge_add_admin_container(
            array(
                'parent'            => $panel_design_style,
                'name'              => 'boxed_container',
                'hidden_property'   => 'boxed',
                'hidden_value'      => 'no'
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'page_background_color_in_box',
                'type'          => 'color',
                'label'         => esc_html__('Page Background Color', 'rebellion'),
                'description'   => esc_html__('Choose the page background color outside box.', 'rebellion'),
                'parent'        => $boxed_container
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'boxed_background_image',
                'type'          => 'image',
                'label'         => esc_html__('Background Image', 'rebellion'),
                'description'   => esc_html__('Choose an image to be displayed in background', 'rebellion'),
                'parent'        => $boxed_container
            )
        );

		rebellion_edge_add_admin_field(
			array(
				'name'          => 'boxed_background_image_repeating',
				'type'          => 'select',
				'default_value' => 'no',
				'label'         => esc_html__('Use Background Image as Pattern', 'rebellion'),
				'description'   => esc_html__('Set this option to "yes" to use the background image as repeating pattern', 'rebellion'),
				'parent'        => $boxed_container,
				'options'       => array(
					'no'	=>	'No',
					'yes'	=>	'Yes'
				)
			)
		);

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'boxed_background_image_attachment',
                'type'          => 'select',
                'default_value' => 'fixed',
                'label'         => esc_html__('Background Image Behaviour', 'rebellion'),
                'description'   => esc_html__('Choose background image behaviour', 'rebellion'),
                'parent'        => $boxed_container,
                'options'       => array(
                    'fixed'     => esc_html__('Fixed', 'rebellion'),
                    'scroll'    => esc_html__('Scroll', 'rebellion')
                )
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'initial_content_width',
                'type'          => 'select',
                'default_value' => '',
                'label'         => esc_html__('Initial Width of Content', 'rebellion'),
                'description'   => esc_html__('Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'rebellion'),
                'parent'        => $panel_design_style,
                'options'       => array(
                    ""          => "1300px - ". esc_html__("default", 'rebellion'),
                    "grid-1300" => "1300px",
                    "grid-1200" => "1200px",
                    "grid-1000" => "1000px",
                    "grid-800"  => "800px"
                )
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'preload_pattern_image',
                'type'          => 'image',
                'label'         => esc_html__('Preload Pattern Image', 'rebellion'),
                'description'   => esc_html__('Choose preload pattern image to be displayed until images are loaded ', 'rebellion'),
                'parent'        => $panel_design_style
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name' => 'element_appear_amount',
                'type' => 'text',
                'label' => esc_html__('Element Appearance', 'rebellion'),
                'description' => esc_html__('For animated elements, set distance (related to browser bottom) to start the animation', 'rebellion'),
                'parent' => $panel_design_style,
                'args' => array(
                    'col_width' => 2,
                    'suffix' => 'px'
                )
            )
        );

        $panel_settings = rebellion_edge_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_settings',
                'title' => esc_html__('Settings', 'rebellion')
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'smooth_scroll',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Smooth Scroll', 'rebellion'),
                'description'   => esc_html__('Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'rebellion'),
                'parent'        => $panel_settings
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'smooth_page_transitions',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__('Smooth Page Transitions', 'rebellion'),
                'description'   => esc_html__('Enabling this option will perform a smooth transition between pages when clicking on links.', 'rebellion'),
                'parent'        => $panel_settings,
                'args'          => array(
                    "dependence" => true,
                    "dependence_hide_on_yes" => "",
                    "dependence_show_on_yes" => "#edgtf_page_transitions_container"
                )
            )
        );

        $page_transitions_container = rebellion_edge_add_admin_container(
            array(
                'parent'            => $panel_settings,
                'name'              => 'page_transitions_container',
                'hidden_property'   => 'smooth_page_transitions',
                'hidden_value'      => 'no'
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'smooth_pt_bgnd_color',
                'type'          => 'color',
                'label'         => esc_html__('Page Loader Background Color', 'rebellion'),
                //'description'   => 'Enabling this option will perform a smooth transition between pages when clicking on links.',
                'parent'        => $page_transitions_container
            )
        );

        $group_pt_spinner_animation = rebellion_edge_add_admin_group(array(
            'name'          => 'group_pt_spinner_animation',
            'title'         => esc_html__('Loader Style', 'rebellion'),
            'description'   => esc_html__('Define styles for loader spinner animation', 'rebellion'),
            'parent'        => $page_transitions_container
        ));

        $row_pt_spinner_animation = rebellion_edge_add_admin_row(array(
            'name'      => 'row_pt_spinner_animation',
            'parent'    => $group_pt_spinner_animation
        ));

        rebellion_edge_add_admin_field(array(
            'type'          => 'selectsimple',
            'name'          => 'smooth_pt_spinner_type',
            'default_value' => '',
            'label'         => esc_html__('Spinner Type', 'rebellion'),
            'parent'        => $row_pt_spinner_animation,
            'options'       => array(
                "pulse" => esc_html__("Pulse", 'rebellion'),
                "double_pulse" => esc_html__("Double Pulse", 'rebellion'),
                "cube" => esc_html__("Cube", 'rebellion'),
                "rotating_cubes" => esc_html__("Rotating Cubes", 'rebellion'),
                "stripes" =>esc_html__( "Stripes", 'rebellion'),
                "wave" => esc_html__("Wave", 'rebellion'),
                "two_rotating_circles" => esc_html__("2 Rotating Circles", 'rebellion'),
                "five_rotating_circles" => esc_html__("5 Rotating Circles", 'rebellion'),
                "atom" => esc_html__("Atom", 'rebellion'),
                "clock" => esc_html__("Clock", 'rebellion'),
                "mitosis" => esc_html__("Mitosis", 'rebellion'),
                "lines" => esc_html__("Lines", 'rebellion'),
                "fussion" => esc_html__("Fussion", 'rebellion'),
                "wave_circles" => esc_html__("Wave Circles", 'rebellion'),
                "pulse_circles" => esc_html__("Pulse Circles", 'rebellion')
            )
        ));

        rebellion_edge_add_admin_field(array(
            'type'          => 'colorsimple',
            'name'          => 'smooth_pt_spinner_color',
            'default_value' => '',
            'label'         => esc_html__('Spinner Color', 'rebellion'),
            'parent'        => $row_pt_spinner_animation
        ));

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'show_back_button',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Show "Back To Top Button"', 'rebellion'),
                'description'   => esc_html__('Enabling this option will display a Back to Top button on every page', 'rebellion'),
                'parent'        => $panel_settings
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'responsiveness',
                'type'          => 'yesno',
                'default_value' => 'yes',
                'label'         => esc_html__('Responsiveness', 'rebellion'),
                'description'   => esc_html__('Enabling this option will make all pages responsive', 'rebellion'),
                'parent'        => $panel_settings
            )
        );

        $panel_custom_code = rebellion_edge_add_admin_panel(
            array(
                'page'  => '',
                'name'  => 'panel_custom_code',
                'title' => esc_html__('Custom Code', 'rebellion')
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'custom_css',
                'type'          => 'textarea',
                'label'         => esc_html__('Custom CSS', 'rebellion'),
                'description'   => esc_html__('Enter your custom CSS here', 'rebellion'),
                'parent'        => $panel_custom_code
            )
        );

        rebellion_edge_add_admin_field(
            array(
                'name'          => 'custom_js',
                'type'          => 'textarea',
                'label'         => esc_html__('Custom JS', 'rebellion'),
                'description'   => esc_html__('Enter your custom Javascript here', 'rebellion'),
                'parent'        => $panel_custom_code
            )
        );

		$panel_google_api = rebellion_edge_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_google_api',
				'title' => esc_html__('Google API', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(
			array(
				'name'        => 'google_maps_api_key',
				'type'        => 'text',
				'label'       => esc_html__('Google Maps Api Key', 'rebellion'),
				'description' => esc_html__('Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our documentation.', 'rebellion'),
				'parent'      => $panel_google_api
			)
		);

    }

    add_action( 'rebellion_edge_options_map', 'rebellion_edge_general_options_map', 1);

}