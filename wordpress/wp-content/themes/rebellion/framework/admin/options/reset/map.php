<?php

if ( ! function_exists('rebellion_edge_reset_options_map') ) {
	/**
	 * Reset options panel
	 */
	function rebellion_edge_reset_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__('Reset', 'rebellion'),
				'icon'  => 'fa fa-retweet'
			)
		);

		$panel_reset = rebellion_edge_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__('Reset', 'rebellion')
			)
		);

		rebellion_edge_add_admin_field(array(
			'type'	=> 'yesno',
			'name'	=> 'reset_to_defaults',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Reset to Defaults', 'rebellion'),
			'description'	=> esc_html__('This option will reset all Edge Options values to defaults', 'rebellion'),
			'parent'		=> $panel_reset
		));

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_reset_options_map', 23 );

}