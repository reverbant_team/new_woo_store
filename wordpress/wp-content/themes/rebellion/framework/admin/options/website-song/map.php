<?php

if ( ! function_exists('rebellion_edge_website_song_options_map') ) {

	function rebellion_edge_website_song_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug'  => '_website_song_page',
				'title' => esc_html__('Website Song', 'rebellion'),
				'icon'  => 'fa fa-play-circle'
			)
		);

		/**
		 * Enable Website Song
		 */
		$edgtf_panel_website_song = rebellion_edge_add_admin_panel(array(
			'page'  => '_website_song_page',
			'name'  => 'edgtf_panel_website_song',
			'title' => esc_html__('Enable Song on Load', 'rebellion')
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'yesno',
			'name'			=> 'edgtf_enable_song_on_load',
			'default_value'	=> 'no',
			'label'			=> esc_html__('Enable Song on Load', 'rebellion'),
			'description'	=> esc_html__('Enabling this option will play your selected song when the website loads', 'rebellion'),
			'args'			=> array(
				'dependence' => true,
				'dependence_hide_on_yes' => '',
				'dependence_show_on_yes' => '#edgtf_edgtf_panel_song_on_load'
			),
			'parent'		=> $edgtf_panel_website_song
		));

		$edgtf_panel_song_on_load = rebellion_edge_add_admin_panel(array(
			'page'  			=> '_website_song_page',
			'name'  			=> 'edgtf_panel_song_on_load',
			'title' 			=> esc_html__('Website Song', 'rebellion'),
			'hidden_property'	=> 'edgtf_enable_song_on_load',
			'hidden_value'		=> 'no'
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'file',
			'name'			=> 'edgtf_song_on_load',
			'default_value'	=> '',
			'label'			=> esc_html__('Audio File', 'rebellion'),
			'parent'		=> $edgtf_panel_song_on_load
		));
	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_website_song_options_map', 13);
}