<?php

if ( ! function_exists('rebellion_edge_load_elements_map') ) {
	/**
	 * Add Elements option page for shortcodes
	 */
	function rebellion_edge_load_elements_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug' => '_elements_page',
				'title' => esc_html__('Elements', 'rebellion'),
				'icon' => 'fa fa-flag-o'
			)
		);

		do_action( 'rebellion_edge_options_elements_map' );

	}

	add_action('rebellion_edge_options_map', 'rebellion_edge_load_elements_map', 11);

}