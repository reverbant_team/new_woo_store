<?php

if ( ! function_exists('rebellion_edge_sidebar_options_map') ) {

	function rebellion_edge_sidebar_options_map() {

		rebellion_edge_add_admin_page(
			array(
				'slug'  => '_sidebar_page',
				'title' => esc_html__('Sidebar', 'rebellion'),
				'icon'  => 'fa fa-indent'
			)
		);

		$panel_widgets = rebellion_edge_add_admin_panel(
			array(
				'page'  => '_sidebar_page',
				'name'  => 'panel_widgets',
				'title' => esc_html__('Widgets', 'rebellion')
			)
		);

		/**
		 * Navigation style
		 */
		rebellion_edge_add_admin_field(array(
			'type'			=> 'color',
			'name'			=> 'sidebar_background_color',
			'default_value'	=> '',
			'label'			=> esc_html__('Sidebar Background Color', 'rebellion'),
			'description'	=> esc_html__('Choose background color for sidebar', 'rebellion'),
			'parent'		=> $panel_widgets
		));

		$group_sidebar_padding = rebellion_edge_add_admin_group(array(
			'name'		=> 'group_sidebar_padding',
			'title'		=> esc_html__('Padding', 'rebellion'),
			'parent'	=> $panel_widgets
		));

		$row_sidebar_padding = rebellion_edge_add_admin_row(array(
			'name'		=> 'row_sidebar_padding',
			'parent'	=> $group_sidebar_padding
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_top',
			'default_value'	=> '',
			'label'			=> esc_html__('Top Padding', 'rebellion'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_right',
			'default_value'	=> '',
			'label'			=> esc_html__('Right Padding', 'rebellion'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_bottom',
			'default_value'	=> '',
			'label'			=> esc_html__('Bottom Padding', 'rebellion'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'textsimple',
			'name'			=> 'sidebar_padding_left',
			'default_value'	=> '',
			'label'			=> esc_html__('Left Padding', 'rebellion'),
			'args'			=> array(
				'suffix'	=> 'px'
			),
			'parent'		=> $row_sidebar_padding
		));

		rebellion_edge_add_admin_field(array(
			'type'			=> 'select',
			'name'			=> 'sidebar_alignment',
			'default_value'	=> '',
			'label'			=> esc_html__('Text Alignment', 'rebellion'),
			'description'	=> esc_html__('Choose text aligment', 'rebellion'),
			'options'		=> array(
				'left' => esc_html__('Left', 'rebellion'),
				'center' => esc_html__('Center', 'rebellion'),
				'right' => esc_html__('Right', 'rebellion')
			),
			'parent'		=> $panel_widgets
		));

	}

	add_action( 'rebellion_edge_options_map', 'rebellion_edge_sidebar_options_map', 9);

}