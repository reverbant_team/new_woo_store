<?php

if ( ! function_exists('rebellion_edge_page_options_map') ) {

    function rebellion_edge_page_options_map() {

        rebellion_edge_add_admin_page(
            array(
                'slug'  => '_page_page',
                'title' => esc_html__('Page', 'rebellion'),
                'icon'  => 'fa fa-file-o'
            )
        );

        $custom_sidebars = rebellion_edge_get_custom_sidebars();

        $panel_sidebar = rebellion_edge_add_admin_panel(
            array(
                'page'  => '_page_page',
                'name'  => 'panel_sidebar',
                'title' => esc_html__('Design Style', 'rebellion')
            )
        );

        rebellion_edge_add_admin_field(array(
            'name'        => 'page_sidebar_layout',
            'type'        => 'select',
            'label'       => esc_html__('Sidebar Layout', 'rebellion'),
            'description' => esc_html__('Choose a sidebar layout for pages', 'rebellion'),
            'default_value' => 'default',
            'parent'      => $panel_sidebar,
            'options'     => array(
                'default'			=> esc_html__('No Sidebar', 'rebellion'),
                'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'rebellion'),
                'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'rebellion'),
                'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'rebellion'),
                'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'rebellion')
            )
        ));


        if(count($custom_sidebars) > 0) {
            rebellion_edge_add_admin_field(array(
                'name' => 'page_custom_sidebar',
                'type' => 'selectblank',
                'label' => esc_html__('Sidebar to Display', 'rebellion'),
                'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'rebellion'),
                'parent' => $panel_sidebar,
                'options' => $custom_sidebars
            ));
        }
    }

    add_action( 'rebellion_edge_options_map', 'rebellion_edge_page_options_map', 8);

}