<?php if($query_results->max_num_pages>1){ ?>
	<div class="edgtf-albums-list-paging">
		<span class="edgtf-albums-list-load-more">
			<?php 
				echo rebellion_edge_get_button_html(array(
					'link' => 'javascript: void(0)',
					'text' => esc_html__('Show more', 'edge-cpt')
				));
			?>
		</span>
	</div>
<?php }