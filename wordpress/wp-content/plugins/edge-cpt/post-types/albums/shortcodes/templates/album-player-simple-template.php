<div class="edgtf-audio-player-wrapper <?php echo esc_attr($player_classes) ?>" <?php edgt_core_inline_style($player_styles); ?> >
	<div id= "edgtf-player-<?php echo esc_attr($player_id); ?>" class="jp-jplayer"></div>
	<div id= "edgtf-player-container-<?php echo esc_attr($player_id); ?>" class="edgtf-audio-player-holder edgtf-audio-player-simple " data-album-id="<?php echo esc_attr($album); ?>">
		<div class="edgtf-audio-player-progress-holder">
			<div class="jp-audio player-box edgtf-player-box ">
				<div class="jp-gui jp-interface">
					<div class="jp-progress">
						<div class="jp-seek-bar">
							<div class="jp-play-bar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class = "edgtf-audio-player-main-holder">
			<div class="edgtf-audio-player-controls-holder">
				<div class="jp-audio player-box">
					<div class="jp-gui jp-interface">
						<ul class="jp-controls">
							<li <?php edgt_core_inline_style($nav_buttons_styles); ?>><a class="jp-previous" ><i class="fa fa-fast-backward"></i></a></li><!-- 
							--><li <?php edgt_core_inline_style($play_button_styles); ?>><a class="jp-play" ><i class="fa fa-play edgtf-play-button"></i><i class="fa fa-pause edgtf-pause-button"></i></a></li><!-- 
							--><li <?php edgt_core_inline_style($nav_buttons_styles); ?>><a class="jp-next" ><i class="fa fa-fast-forward"></i></a></li>
						</ul>
					</div>
					<div class="jp-type-playlist">
						<div class="jp-playlist">
							<ul class="tracks-list">
								<li></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="edgtf-audio-player-details-holder">
				<div class= "edgtf-audio-player-details">
					<span class="edgtf-audio-player-title"></span>
					<div class="edgtf-audio-player-time">
						<div class="jp-audio player-box">
							<div class="jp-gui jp-interface">
								<div class="time-box">
									<span class="jp-current-time"></span>/<span class="jp-duration"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>