<div class="edgtf-album" >
	<div class="edgtf-album-inner" >
		<a class ="edgtf-album-link" href="<?php echo esc_url($album_link); ?>"></a>
		<div class = "edgtf-album-image-holder">
		<?php
			echo get_the_post_thumbnail(get_the_ID(),'full');
		?>				
		</div>
		<div class="edgtf-album-text-overlay">
			<div class="edgtf-album-text-overlay-inner">
				<div class="edgtf-album-text-holder">
					<?php 
						echo $artist_html;
					?>
					<h4 class="edgtf-album-title">
						<?php echo esc_attr(get_the_title()); ?>
					</h4>	
				</div>
			</div>	
		</div>
	</div>
	<?php
	if ($show_stores == 'yes'):
		$stores = get_post_meta(get_the_ID(), 'edgtf_store_name', true);
		$stores_links = get_post_meta(get_the_ID(), 'edgtf_store_link', true);
		$i = 0;
	?>
	<?php if(is_array($stores) && count($stores) > 0 && implode($stores) != ''): ?>
			<div class="edgtf-album-stores clearfix">
				<?php
				foreach($stores as $store) :
				    if ( strpos($stores_list, $store) !== false ): ?>

					<span class="edgtf-album-store">
						<?php if(isset($stores_links[$i]) && $stores_links[$i]) : ?>
							<a class="edgtf-album-store-link" href="<?php echo esc_url($stores_links[$i]); ?>" target = "_blank">
								<?php echo edgtf_fn_single_stores_names_and_images($store, 'image'); ?>
							</a>
						<?php else: ?>
							<?php echo edgtf_fn_single_stores_names_and_images($store, 'image'); ?>
						<?php endif; ?>
					</span>
					<?php
					endif;
					$i++;
				endforeach;
				?>
			</div>
	<?php endif; 
	endif; ?>
</div>