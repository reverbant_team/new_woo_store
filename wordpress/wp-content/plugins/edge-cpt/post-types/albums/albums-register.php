<?php
namespace EdgeCore\PostTypes\Albums;

use EdgeCore\Lib\PostTypeInterface;

/**
 * Class AlbumsRegister
 * @package EdgeCore\PostTypes\Albums
 */
class AlbumsRegister implements PostTypeInterface {
    /**
     * @var string
     */
    private $base;

    public function __construct() {
        $this->base				= 'album';
        $this->taxGenreBase		= 'album-genre';
        $this->taxLabelBase		= 'album-label';
        $this->taxArtistBase	= 'album-artist';

        add_filter('single_template', array($this, 'registerSingleTemplate'));
    }

    /**
     * @return string
     */
    public function getBase() {
        return $this->base;
    }

    /**
     * Registers custom post type with WordPress
     */
    public function register() {
        $this->registerPostType();
        $this->registerTax();
    }

    /**
     * Registers album single template if one does'nt exists in theme.
     * Hooked to single_template filter
     * @param $single string current template
     * @return string string changed template
     */
    public function registerSingleTemplate($single) {
        global $post;

        if($post->post_type == $this->base) {
            if(!file_exists(get_template_directory().'/single-album.php')) {
                return EDGE_CORE_CPT_PATH.'/albums/templates/single-'.$this->base.'.php';
            }
        }

        return $single;
    }

    /**
     * Registers custom post type with WordPress
     */
    private function registerPostType() {
        global $rebellion_edge_Framework, $rebellion_edge_options;

        $menuPosition = 5;
        $menuIcon = 'dashicons-admin-post';
        $slug = $this->base;

        if(edgt_core_theme_installed()) {
            $menuPosition = $rebellion_edge_Framework->getSkin()->getMenuItemPosition('albums');
            $menuIcon = $rebellion_edge_Framework->getSkin()->getMenuIcon('albums');

       }

        register_post_type( $this->base,
            array(
                'labels'		=> array(
                    'name'			=> esc_html__( 'Albums','edge-cpt' ),
                    'singular_name'	=> esc_html__( 'Album','edge-cpt' ),
                    'add_item'		=> esc_html__( 'New Album','edge-cpt' ),
                    'add_new_item'	=> esc_html__( 'Add New Album','edge-cpt' ),
                    'edit_item'		=> esc_html__( 'Edit Album','edge-cpt' )
                ),
                'public'		=> true,
                'has_archive'	=> true,
                'rewrite'		=> array('slug' => 'album'),
                'menu_position'	=> $menuPosition,
                'show_ui'		=> true,
                'supports'		=> array('author', 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'comments'),
                'menu_icon'		=>  $menuIcon
            )
        );
    }

    /**
     * Registers custom taxonomy with WordPress
     */
    private function registerTax() {
        $label_labels = array(
            'name'				=> esc_html__( 'Labels', 'edge-cpt' ),
            'singular_name'		=> esc_html__( 'Label', 'edge-cpt' ),
            'search_items'		=> esc_html__( 'Search Labels', 'edge-cpt' ),
            'all_items'			=> esc_html__( 'All Labels', 'edge-cpt' ),
            'parent_item'		=> esc_html__( 'Parent Label', 'edge-cpt' ),
            'parent_item_colon'	=> esc_html__( 'Parent Label:', 'edge-cpt' ),
            'edit_item'			=> esc_html__( 'Edit Label', 'edge-cpt' ),
            'update_item'		=> esc_html__( 'Update Label', 'edge-cpt' ),
            'add_new_item'		=> esc_html__( 'Add New Label', 'edge-cpt' ),
            'new_item_name'		=> esc_html__( 'New Label Name', 'edge-cpt' ),
            'menu_name'			=> esc_html__( 'Labels', 'edge-cpt' ),
        );

        register_taxonomy($this->taxLabelBase, array($this->base), array(
            'hierarchical'		=> true,
            'labels'			=> $label_labels,
            'show_ui'			=> true,
            'query_var'			=> true,
	        'show_admin_column'	=> true
        ));

		$genre_labels = array(
			'name'				=> esc_html__( 'Genres', 'edge-cpt' ),
			'singular_name'		=> esc_html__( 'Genre', 'edge-cpt' ),
			'search_items'		=> esc_html__( 'Genres', 'edge-cpt' ),
			'all_items'			=> esc_html__( 'Genres', 'edge-cpt' ),
			'parent_item'		=> esc_html__( 'Parent Genre', 'edge-cpt' ),
			'parent_item_colon'	=> esc_html__( 'Parent Genres:', 'edge-cpt' ),
			'edit_item'			=> esc_html__( 'Edit Genre', 'edge-cpt' ),
			'update_item'		=> esc_html__( 'Update Genre', 'edge-cpt' ),
			'add_new_item'		=> esc_html__( 'Add New Genre', 'edge-cpt' ),
			'new_item_name'		=> esc_html__( 'New Genre', 'edge-cpt' ),
			'menu_name'			=> esc_html__( 'Genres', 'edge-cpt' ),
		);

		register_taxonomy($this->taxGenreBase,array($this->base), array(
			'hierarchical'		=> true,
			'labels'			=> $genre_labels,
			'show_ui'			=> true,
			'query_var'			=> true,
			'show_admin_column'	=> true
		));

		$artist_labels = array(
			'name'				=> esc_html__( 'Artists', 'edge-cpt' ),
			'singular_name'		=> esc_html__( 'Artist', 'edge-cpt' ),
			'search_items'		=> esc_html__( 'Artists', 'edge-cpt' ),
			'all_items'			=> esc_html__( 'Artists', 'edge-cpt' ),
			'parent_item'		=> esc_html__( 'Parent Artist', 'edge-cpt' ),
			'parent_item_colon'	=> esc_html__( 'Parent Artists:', 'edge-cpt' ),
			'edit_item'			=> esc_html__( 'Edit Artist', 'edge-cpt' ),
			'update_item'		=> esc_html__( 'Update Artist', 'edge-cpt' ),
			'add_new_item'		=> esc_html__( 'Add New Artist', 'edge-cpt' ),
			'new_item_name'		=> esc_html__( 'New Artist Name', 'edge-cpt' ),
			'menu_name'			=> esc_html__( 'Artists', 'edge-cpt' ),
		);

		register_taxonomy($this->taxArtistBase,array($this->base), array(
			'hierarchical'		=> true,
			'labels'			=> $artist_labels,
			'show_ui'			=> true,
			'query_var'			=> true,
			'show_admin_column'	=> true
		));
    }

}