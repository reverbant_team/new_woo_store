<div class="edgtf-event-content edgtf-events<?php echo esc_attr(get_the_ID()) ?>" <?php edgt_core_inline_style($border_color_style); ?> >
	<div class="edgtf-event-date-holder" <?php edgt_core_inline_style($text_color_style); ?>>
		<?php  if (!empty($date)) { ?>
			<div class="edgtf-event-date-holder-left">
                            <span class="edgt-event-day-number-holder">
                                <?php echo date( 'd' , strtotime( $date ) ); ?>
                            </span>
			</div>
			<div class="edgtf-event-date-holder-right">
                            <span class="edgt-event-day-holder">
                                <?php echo date( 'M' , strtotime( $date ) ); ?>
                            </span>
                            <span class="edgt-event-month-holder">
                                <?php echo date( 'D' , strtotime( $date ) ); ?>
                            </span>
			</div>
		<?php }  ?>


	</div>
	<div class="edgtf-event-title-holder">
		<<?php echo $title_tag ?> class="edgtf-event-title" <?php edgt_core_inline_style($text_color_style); ?>>
		<a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" target="_blank">
			<?php echo esc_attr($title); ?>
		</a>
	</<?php echo $title_tag ?>>
</div>
<div class="edgtf-event-buy-tickets-holder">
	<?php

	if($tickets_status == 'available'){

		$button_params = array(
			'text'          => esc_html__( 'Buy Tickets','edge-cpt' ),
			'custom_class'  => 'event-buy-tickets-button',
			'link'          => $link,
			'target'        => $target,
			'type'          => 'outline',
			'button_skin'   => $button_skin,
		);

		echo rebellion_edge_execute_shortcode('edgtf_button', $button_params);
	}
	else if($tickets_status == 'sold')
	{ ?>

		<span class="edgt-event-sold-out-holder" <?php edgt_core_inline_style($text_color_style); ?>>
                              <?php  echo esc_html__( 'Sold out!','edge-cpt' ); ?>
                            </span>
		<?php
	}
	else {
		?>
		<span class="edgt-event-sold-out-holder" <?php edgt_core_inline_style($text_color_style); ?>>
                              <?php  echo esc_html__( 'Free!','edge-cpt' ); ?>
                            </span>
		<?php
	}
	?>
</div>
</div>