<?php if($query_results->max_num_pages>1){ ?>
	<div class="edgtf-events-list-paging">
		<span class="edgtf-events-list-load-more">
			<?php 
				echo rebellion_edge_get_button_html(array(
					'link' => 'javascript: void(0)',
					'text' => esc_html__('Show more', 'edge-cpt'),
					'button_skin'   => $button_skin,
				));
			?>
		</span>
		<div class="edgtf-stripes">
			<div class="edgtf-rect1"></div>
			<div class="edgtf-rect2"></div>
			<div class="edgtf-rect3"></div>
			<div class="edgtf-rect4"></div>
			<div class="edgtf-rect5"></div>
		</div>
	</div>
<?php }