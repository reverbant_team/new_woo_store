<?php
namespace RebellionEdge\Modules\Shortcodes\Lib;

use RebellionEdge\Modules\Shortcodes\Accordion\Accordion;
use RebellionEdge\Modules\Shortcodes\AccordionTab\AccordionTab;
use RebellionEdge\Modules\Shortcodes\AlbumDisc\AlbumDisc;
use RebellionEdge\Modules\Shortcodes\Blockquote\Blockquote;
use RebellionEdge\Modules\Shortcodes\BlogList\BlogList;
use RebellionEdge\Modules\Shortcodes\BlogSlider\BlogSlider;
use RebellionEdge\Modules\Shortcodes\BoxedIcon\BoxedIcon;
use RebellionEdge\Modules\Shortcodes\BoxedIcons\BoxedIcons;
use RebellionEdge\Modules\Shortcodes\Button\Button;
use RebellionEdge\Modules\Shortcodes\CallToAction\CallToAction;
use RebellionEdge\Modules\Shortcodes\Client\Client;
use RebellionEdge\Modules\Shortcodes\Clients\Clients;
use RebellionEdge\Modules\Shortcodes\Counter\Countdown;
use RebellionEdge\Modules\Shortcodes\Counter\Counter;
use RebellionEdge\Modules\Shortcodes\CustomFont\CustomFont;
use RebellionEdge\Modules\Shortcodes\DeviceShowcase\DeviceShowcase;
use RebellionEdge\Modules\Shortcodes\Dropcaps\Dropcaps;
use RebellionEdge\Modules\Shortcodes\ElementsHolder\ElementsHolder;
use RebellionEdge\Modules\Shortcodes\ElementsHolderItem\ElementsHolderItem;
use RebellionEdge\Modules\Shortcodes\GoogleMap\GoogleMap;
use RebellionEdge\Modules\Shortcodes\Highlight\Highlight;
use RebellionEdge\Modules\Shortcodes\Icon\Icon;
use RebellionEdge\Modules\Shortcodes\IconListItem\IconListItem;
use RebellionEdge\Modules\Shortcodes\IconWithText\IconWithText;
use RebellionEdge\Modules\Shortcodes\ImageGallery\ImageGallery;
use RebellionEdge\Modules\Shortcodes\ImageWithText\ImageWithText;
use RebellionEdge\Modules\Shortcodes\Message\Message;
use RebellionEdge\Modules\Shortcodes\OrderedList\OrderedList;
use RebellionEdge\Modules\Shortcodes\PieCharts\PieChartBasic\PieChartBasic;
use RebellionEdge\Modules\Shortcodes\PieCharts\PieChartDoughnut\PieChartDoughnut;
use RebellionEdge\Modules\Shortcodes\PieCharts\PieChartDoughnut\PieChartPie;
use RebellionEdge\Modules\Shortcodes\PieCharts\PieChartWithIcon\PieChartWithIcon;
use RebellionEdge\Modules\Shortcodes\PricingTables\PricingTables;
use RebellionEdge\Modules\Shortcodes\PricingTable\PricingTable;
use RebellionEdge\Modules\Shortcodes\Process\ProcessHolder;
use RebellionEdge\Modules\Shortcodes\Process\ProcessItem;
use RebellionEdge\Modules\Shortcodes\ProgressBar\ProgressBar;
use RebellionEdge\Modules\Shortcodes\Separator\Separator;
use RebellionEdge\Modules\Shortcodes\SocialShare\SocialShare;
use RebellionEdge\Modules\Shortcodes\Tabs\Tabs;
use RebellionEdge\Modules\Shortcodes\Tab\Tab;
use RebellionEdge\Modules\Shortcodes\Team\Team;
use RebellionEdge\Modules\Shortcodes\UnorderedList\UnorderedList;
use RebellionEdge\Modules\Shortcodes\VerticalSplitSlider\VerticalSplitSlider;
use RebellionEdge\Modules\Shortcodes\VerticalSplitSliderContentItem\VerticalSplitSliderContentItem;
use RebellionEdge\Modules\Shortcodes\VerticalSplitSliderLeftPanel\VerticalSplitSliderLeftPanel;
use RebellionEdge\Modules\Shortcodes\VerticalSplitSliderRightPanel\VerticalSplitSliderRightPanel;
use RebellionEdge\Modules\Shortcodes\VideoButton\VideoButton;
use RebellionEdge\Modules\Shortcodes\ItemShowcase\ItemShowcase;
use RebellionEdge\Modules\Shortcodes\ItemShowcaseListItem\ItemShowcaseListItem;
use RebellionEdge\Modules\Shortcodes\AudioPlaylist\AudioPlaylist;
use RebellionEdge\Modules\Shortcodes\SectionSubtitle\SectionSubtitle;
use RebellionEdge\Modules\Shortcodes\Banner\Banner;
use RebellionEdge\Modules\Shortcodes\FrameCarousel\FrameCarousel;

/**
 * Class ShortcodeLoader
 */
class ShortcodeLoader {
	/**
	 * @var private instance of current class
	 */
	private static $instance;
	/**
	 * @var array
	 */
	private $loadedShortcodes = array();

	/**
	 * Private constuct because of Singletone
	 */
	private function __construct() {}

	/**
	 * Private sleep because of Singletone
	 */
	private function __wakeup() {}

	/**
	 * Private clone because of Singletone
	 */
	private function __clone() {}

	/**
	 * Returns current instance of class
	 * @return ShortcodeLoader
	 */
	public static function getInstance() {
		if(self::$instance == null) {
			return new self;
		}

		return self::$instance;
	}

	/**
	 * Adds new shortcode. Object that it takes must implement ShortcodeInterface
	 * @param ShortcodeInterface $shortcode
	 */
	private function addShortcode(ShortcodeInterface $shortcode) {
		if(!array_key_exists($shortcode->getBase(), $this->loadedShortcodes)) {
			$this->loadedShortcodes[$shortcode->getBase()] = $shortcode;
		}
	}

	/**
	 * Adds all shortcodes.
	 *
	 * @see ShortcodeLoader::addShortcode()
	 */
	private function addShortcodes() {
		$this->addShortcode(new Accordion());
		$this->addShortcode(new AccordionTab());
		$this->addShortcode(new AlbumDisc());
		$this->addShortcode(new AudioPlaylist());
		$this->addShortcode(new Banner());
		$this->addShortcode(new Blockquote());
		$this->addShortcode(new BlogList());
		$this->addShortcode(new BlogSlider());
		$this->addShortcode(new BoxedIcons());
		$this->addShortcode(new BoxedIcon());
		$this->addShortcode(new Button());
		$this->addShortcode(new CallToAction());
		$this->addShortcode(new Clients());
		$this->addShortcode(new Client());
		$this->addShortcode(new Counter());
		$this->addShortcode(new Countdown());
		$this->addShortcode(new CustomFont());
		$this->addShortcode(new DeviceShowcase());
		$this->addShortcode(new Dropcaps());
		$this->addShortcode(new ElementsHolder());
		$this->addShortcode(new ElementsHolderItem());
		$this->addShortcode(new FrameCarousel());
		$this->addShortcode(new GoogleMap());
		$this->addShortcode(new Highlight());
		$this->addShortcode(new Icon());
		$this->addShortcode(new IconListItem());
		$this->addShortcode(new IconWithText());
		$this->addShortcode(new ImageGallery());
		$this->addShortcode(new ImageWithText());
		$this->addShortcode(new Message());
		$this->addShortcode(new OrderedList());
		$this->addShortcode(new PieChartBasic());
		$this->addShortcode(new PieChartPie());
		$this->addShortcode(new PieChartDoughnut());
		$this->addShortcode(new PieChartWithIcon());
		$this->addShortcode(new PricingTables());
		$this->addShortcode(new PricingTable());
		$this->addShortcode(new ProgressBar());
		$this->addShortcode(new ProcessHolder());
		$this->addShortcode(new ProcessItem());
		$this->addShortcode(new SectionSubtitle());
		$this->addShortcode(new Separator());
		$this->addShortcode(new SocialShare());
		$this->addShortcode(new Tabs());
		$this->addShortcode(new Tab());
		$this->addShortcode(new Team());
		$this->addShortcode(new UnorderedList());
		$this->addShortcode(new VideoButton());
		$this->addShortcode(new VerticalSplitSlider());
		$this->addShortcode(new VerticalSplitSliderLeftPanel());
		$this->addShortcode(new VerticalSplitSliderRightPanel());
		$this->addShortcode(new VerticalSplitSliderContentItem());
		$this->addShortcode(new ItemShowcase());
		$this->addShortcode(new ItemShowcaseListItem());
	}
	/**
	 * Calls ShortcodeLoader::addShortcodes and than loops through added shortcodes and calls render method
	 * of each shortcode object
	 */
	public function load() {
		$this->addShortcodes();

		foreach ($this->loadedShortcodes as $shortcode) {
			add_shortcode($shortcode->getBase(), array($shortcode, 'render'));
		}
	}
}

$shortcodeLoader = ShortcodeLoader::getInstance();
$shortcodeLoader->load();