<?php
/*
Plugin Name: Edge Twitter Feed
Description: Plugin that adds Twitter feed functionality to our theme
Author: Edge Themes
Version: 1.0
*/
define('EDGEF_TWITTER_FEED_VERSION', '1.0');
define('EDGEF_TWITTER_ABS_PATH', dirname(__FILE__));
define('EDGEF_TWITTER_REL_PATH', dirname(plugin_basename(__FILE__ )));

include_once 'load.php';

if(!function_exists('edgt_twitter_feed_text_domain')) {
    /**
     * Loads plugin text domain so it can be used in translation
     */
    function edgt_twitter_feed_text_domain() {
        load_plugin_textdomain('edge-twitter-feed', false, EDGEF_TWITTER_REL_PATH.'/languages');
    }

    add_action('plugins_loaded', 'edgt_twitter_feed_text_domain');
}