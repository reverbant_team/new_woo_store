<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */

// Mike Blackman - Last-band site-cookie
function wpb_cookies_lastband() {
	$visit_time = date('F j, Y  g:i a');
	$year_secs = 3 * 365 * 24 * 60 * 60; // 3 years, 365 days/yr, 24 hrs/day, 60 mins/hr, 60 secs/min
	$uri = $_SERVER['REQUEST_URI'];

	// find /bands/, /product/, or /product-category/
	$ipos = stripos($uri, "bands/");
	if (!$ipos)
		$ipos = stripos($uri, "product/");
	if (!$ipos)
		$ipos = stripos($uri, "product-category/");
	if ($ipos>0) {
		// Found something, continue
		$pos = stripos($uri, "/", $ipos); // Get slash
		$endpos = stripos($uri, "-", $pos); // get hyphen. Everything between=band
		$band_name = substr($uri, $pos+1, $endpos-$pos-1);

		if ($band_name=="scr" || $band_name=="sheryl" || $band_name=="" /* temporary /bands/ page default */)
			$band_name = "sheryl-crow";

		setcookie('last-band', $band_name, time()+$year_secs, "/");
	} else if (stripos(" $uri", "/tmm-")>0 || stripos(" $uri", "/wp-admin")>0 || stripos(" $uri", "ajax=")>0 || stripos(" $uri", "wp-login.php" )>0) {
		// Do nothing for woo-commerce or admin pages
	} else {
		// Not a band page, do we need to redirect?

		$band = $_COOKIE['last-band'];
		if (is_null($band)) $band = "sheryl-crow"; // DEFAULT FOR NOW
		wp_redirect("/bands/$band/", 302);
		exit; // don't do anything else
	}
}
wpb_cookies_lastband();

/* Remove product meta - Mike B*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

/* Attempt to remove Add to Cart button for Categories */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action( 'woocommerce_after_subcategory', 'woocommerce_template_loop_add_to_cart');

